#ifndef _IMU_MODULE_H
#define _IMU_MODULE_H

void calc_gyro();
void imuReadCallback();
void imu_module_init(void);
void imu_module_add_tasks(void);
void imu_module_enable_tasks(void);

class imu {
  public:
    imu();
    float get_pitch();
    float get_roll();
    float get_yaw();
    unsigned long get_imuperiod();
    void get_quaternion(float *x, float *y, float *z, float *w);
    void get_euler (float *euler_x, float *euler_y, float *euler_z);
    void get_rpy (float *roll, float *pitch, float *yaw);
    void get_gyro (float *gyro_roll, float *gyro_pitch, float *gyro_yaw);
    void get_accel (float *accel_roll, float *accel_pitch, float *accel_yaw);
    void get_accelWorld (float *accel_roll, float *accel_pitch, float *accel_yaw);
    void get_accelReal (float *accel_roll, float *accel_pitch, float *accel_yaw);
    void get_accelRealWorld (float *accel_roll, float *accel_pitch, float *accel_yaw);
};

#endif
