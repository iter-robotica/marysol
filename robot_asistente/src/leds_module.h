#ifndef _leds_MODULE_H
#define _leds_MODULE_H
#include "Arduino.h"

void simpleColorCallback();
void colorWipeCallback();
void RainbowCallback();
void rainbowCycleCallback();
void theaterChaseCallback();
void rainbow2Callback();
void theaterChaseRainbowCallback();
void whiteOverRainbowCallback();
void pulseWhiteUpCallback();
void pulseWhiteDownCallback();
void rainbowFade2WhiteCallback();
void PulsesCallback();
void ColorLineCallback();
void switchCallback();

void leds_module_init(void);
void leds_module_add_tasks(void);
void leds_module_enable_tasks(uint16_t task);
void leds_module_disable_tasks(void);
void leds_module_enable_task();

class LedStrip {
  public:
    LedStrip();
    void set_leds (uint8_t green, uint8_t red, uint8_t blue, uint8_t task, uint8_t period);
  private:
    uint32_t Wheel (uint16_t WheelPos);
};

#endif
