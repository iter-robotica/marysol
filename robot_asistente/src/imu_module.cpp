#include "prj_variants.h"
#include "imu_module.h"
#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskSchedulerDeclarations.h>

#ifdef CFG_USE_ROS
#include "ros_communications.h"
#endif

#ifdef CFG_USE_STABILITY_CTRL
#include "stability_control.h"
extern stability_control controller;
#endif

#ifdef CFG_USE_LCD
#include "lcd.h"
extern lcd_16X2 LCD_16X2;
#endif

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h

#include "I2Cdev.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

imu mpu6050 = imu();

// uncomment "OUTPUT_READABLE_QUATERNION" if you want to see the actual
// quaternion components in a [w, x, y, z] format (not best for parsing
// on a remote host such as Processing or something though)
//#define OUTPUT_READABLE_QUATERNION

// uncomment "OUTPUT_READABLE_EULER" if you want to see Euler angles
// (in degrees) calculated from the quaternions coming from the FIFO.
// Note that Euler angles suffer from gimbal lock (for more info, see
// http://en.wikipedia.org/wiki/Gimbal_lock)
//#define OUTPUT_READABLE_EULER

// uncomment "OUTPUT_READABLE_YAWPITCHROLL" if you want to see the yaw/
// pitch/roll angles (in degrees) calculated from the quaternions coming
// from the FIFO. Note this also requires gravity vector calculations.
// Also note that yaw/pitch/roll angles suffer from gimbal lock (for
// more info, see: http://en.wikipedia.org/wiki/Gimbal_lock)
// #define OUTPUT_READABLE_YAWPITCHROLL

// uncomment "OUTPUT_READABLE_GYRO" if you want to see gyro. This gyro
// reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
//#define OUTPUT_READABLE_GYRO

// uncomment "OUTPUT_READABLE_REALACCEL_GRAVITY" if you want to see acceleration
// components with gravity removed. This acceleration reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
//#define OUTPUT_READABLE_REALACCEL_GRAVITY

// uncomment "OUTPUT_READABLE_WORLDACCEL_GRAVITY" if you want to see acceleration
// components with gravity and adjusted for the world frame of
// reference (yaw is relative to initial orientation, since no magnetometer
// is present in this case). Could be quite handy in some cases.
//#define OUTPUT_READABLE_WORLDACCEL_GRAVITY

// uncomment "OUTPUT_READABLE_REALACCEL" if you want to see acceleration
// components with gravity removed. This acceleration reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
//#define OUTPUT_READABLE_REALACCEL

// uncomment "OUTPUT_READABLE_WORLDACCEL" if you want to see acceleration
// components with gravity removed and adjusted for the world frame of
// reference (yaw is relative to initial orientation, since no magnetometer
// is present in this case). Could be quite handy in some cases.
//#define OUTPUT_READABLE_WORLDACCEL

// uncomment "OUTPUT_TEAPOT" if you want output that matches the
// format used for the InvenSense teapot demo
//#define OUTPUT_TEAPOT


// ================================================================
// ===                      INTERNAL DATA                       ===
// ================================================================

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;               // [w, x, y, z]         quaternion container
VectorInt16 aa;             // [x, y, z]            accel sensor measurements
float d[3];                 // [x, y, z]            distance in xyz axes
float d_old[3];             // [x, y, z]            distance in xyz axes one period earlier to calculate speed
float speed_xyz[3];         // [x, y, z]            accel sensor measurements one period earlier to calculate speed
VectorInt16 aaWorld;        // [x, y, z]            world-frame accel sensor measurements
VectorInt16 aaReal;         // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaRealWorld;    // [x, y, z]            gravity-free world-frame accel sensor measurements
VectorFloat gravity;        // [x, y, z]            gravity vector
float euler[3];             // [psi, theta, phi]    Euler angle container
float ypr[3];               // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
float gyro_ypr[3];          // [x, y, z]            gyro in rpy angles
float ypr_old[3];           // [yaw, pitch, roll]   yaw/pitch/roll one period earlier to calculate gyro
float periodo_imu;

#ifdef OUTPUT_TEAPOT
// packet structure for InvenSense teapot demo
uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };
#endif

uint32_t imucount, imuoldcount = 0, imuperiod, counter;


// ================================================================
// ===                  GET ANGLES ROUTINE                      ===
// ================================================================

imu::imu() {

}

float imu::get_yaw (){
  return ypr[0] * 180/M_PI;
}

float imu::get_pitch (){
  return ypr[1] * 180/M_PI;
}

float imu::get_roll (){
  return ypr[2] * 180/M_PI;
}

unsigned long imu::get_imuperiod (){
  return imuperiod;
}

void imu::get_quaternion(float *x, float *y, float *z, float *w) {    //NO TENGO NI IDEA SI ESTA DECLARACIÓN ESTÁ BIEN
    *x = q.x;
    *y = q.y;
    *z = q.z;
    *w = q.w;
}

void imu::get_euler(float *euler_x, float *euler_y, float *euler_z) {
    *euler_x = euler[0] * 180/M_PI;
    *euler_y = euler[1] * 180/M_PI;
    *euler_z = euler[2] * 180/M_PI;
}

void imu::get_rpy(float *roll, float *pitch, float *yaw) {
    *yaw = ypr[0] * 180/M_PI;
    *pitch = ypr[1] * 180/M_PI;
    *roll = ypr[2] * 180/M_PI;
}

void imu::get_gyro(float *gyro_roll, float *gyro_pitch, float *gyro_yaw) {
    *gyro_yaw = gyro_ypr[0] * 180/M_PI;
    *gyro_pitch = gyro_ypr[1] * 180/M_PI;
    *gyro_roll = gyro_ypr[2] * 180/M_PI;
}

void imu::get_accel(float *accel_roll, float *accel_pitch, float *accel_yaw) {
    *accel_roll = aa.x * (9.81/8192.0);
    *accel_pitch = aa.y * (9.81/8192.0);
    *accel_yaw = aa.z * (9.81/8192.0);
}

void imu::get_accelWorld(float *accel_roll, float *accel_pitch, float *accel_yaw) {
    *accel_roll = aaWorld.x * (9.81/8192.0);
    *accel_pitch = aaWorld.y * (9.81/8192.0);
    *accel_yaw = aaWorld.z * (9.81/8192.0);
}

void imu::get_accelReal(float *accel_roll, float *accel_pitch, float *accel_yaw) {
    *accel_roll = aaReal.x * (9.81/8192.0);
    *accel_pitch = aaReal.y * (9.81/8192.0);
    *accel_yaw = aaReal.z * (9.81/8192.0);
}

void imu::get_accelRealWorld(float *accel_roll, float *accel_pitch, float *accel_yaw) {
    *accel_roll = aaRealWorld.x * (9.81/8192.0);
    *accel_pitch = aaRealWorld.y * (9.81/8192.0);
    *accel_yaw = aaRealWorld.z * (9.81/8192.0);
}


// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

#define INTERRUPT_PIN 16  // use pin 16 on Arduino Uno & most boards
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}


// ================================================================
// ===                ANGULAR VELOCITY ROUTINE                  ===
// ================================================================

void calc_gyro()
{
  for (int i = 0; i < 3; i++)
  {
    // Fixing the problem when angle crosses from 180 to -180 and vice versa in every angle
    if (abs(ypr_old[i] - ypr[i]) > 6){  // if the difference is bigger than 344º
      // When angle crosses from 180 to -180
      if (ypr_old[i] > 0 && ypr[i] < 0)
        gyro_ypr[i] = 1000000 * (ypr[i] + 2 * M_PI - ypr_old[i]) / imuperiod;  // we add 360º
      // When angle crosses from -180 to 180
      if (ypr_old[i] < 0 && ypr[i] > 0)
        gyro_ypr[i] = 1000000 * (ypr[i] - 2 * M_PI - ypr_old[i]) / imuperiod;  // we substract 360º
    }
    else
      gyro_ypr[i] = 1000000 * (ypr[i] - ypr_old[i]) / imuperiod;
    ypr_old[i] = ypr[i];
  }
}


// ================================================================
// ===                    CALLBACK ROUTINE                      ===
// ================================================================

extern Scheduler runner;
Task imuReadTask(5000, TASK_FOREVER, &imuReadCallback);
void imuReadCallback() {
  // if programming failed, don't try to do anything
  if (!dmpReady) return;

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();
  // wait for MPU interrupt or extra packet(s) available
  if (!mpuInterrupt || fifoCount < packetSize) return;
  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

if(fifoCount < packetSize){
        //Lets go back and wait for another interrupt. We shouldn't be here, we got an interrupt from another event
    // This is blocking so don't do it   while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
}
  // check for overflow (this should never happen unless our code is too inefficient)
  else if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
      // reset so we can continue cleanly
      mpu.resetFIFO();
    //  fifoCount = mpu.getFIFOCount();  // will be zero after reset no need to ask
      Serial.println(F("FIFO overflow!"));

  // otherwise, check for DMP data ready interrupt (this should happen frequently)
} else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {

      // read a packet from FIFO
while(fifoCount >= packetSize){ // Lets catch up to NOW, someone is using the dreaded delay()!
  mpu.getFIFOBytes(fifoBuffer, packetSize);
  // track FIFO count here in case there is > 1 packet available
  // (this lets us immediately read more without waiting for an interrupt)
  fifoCount -= packetSize;
}

imucount = micros();
imuperiod = imucount - imuoldcount;
periodo_imu = imuperiod / 1000000.0;  // m/s
imuoldcount = imucount;
counter++;

mpu.dmpGetQuaternion(&q, fifoBuffer);
mpu.dmpGetEuler(euler, &q);
mpu.dmpGetGravity(&gravity, &q);
mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
calc_gyro();
mpu.dmpGetAccel(&aa, fifoBuffer);
mpu.dmpGetLinearAccelInWorld(&aaWorld, &aa, &q);
mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
mpu.dmpGetLinearAccelInWorld(&aaRealWorld, &aaReal, &q);


#ifdef OUTPUT_TEAPOT
    // display quaternion values in InvenSense Teapot demo format:
    teapotPacket[2] = fifoBuffer[0];
    teapotPacket[3] = fifoBuffer[1];
    teapotPacket[4] = fifoBuffer[4];
    teapotPacket[5] = fifoBuffer[5];
    teapotPacket[6] = fifoBuffer[8];
    teapotPacket[7] = fifoBuffer[9];
    teapotPacket[8] = fifoBuffer[12];
    teapotPacket[9] = fifoBuffer[13];
    teapotPacket[11]++; // packetCount, loops at 0xFF on purpose
#endif

/*for (int i = 0; i < 3; i++)
{
  //we store the actual value for the next cycle
  ypr_old[i] = ypr[i];
}
  aa_old.x = aa.x;
  aa_old.y = aa.y;
  aa_old.z = aa.z;*/

#ifdef CFG_USE_ROS
  stateArray[2] = int16_t(100 * mpu6050.get_pitch());
  stateArray[3] = int16_t(100 * mpu6050.get_yaw());
  stateArray[4] = int16_t(100 * mpu6050.get_roll());
  mpu6050.get_quaternion(&(imuArray[0]), &(imuArray[1]), &(imuArray[2]), &(imuArray[3]));
  mpu6050.get_gyro(&(imuArray[4]), &(imuArray[5]), &(imuArray[6]));
  mpu6050.get_accel(&(imuArray[7]), &(imuArray[8]), &(imuArray[9]));
  //mpu6050.get_accelWorld(&(imuArray[7]), &(imuArray[8]), &(imuArray[9]));
#endif
/*#ifdef CFG_USE_STABILITY_CTRL
  controller.setAngles(int16_t(mpu6050.get_pitch()), int16_t(mpu6050.get_yaw()), int16_t(mpu6050.get_roll()));
#endif*/

/*{
if (counter % 10 == 0) {
  bool print_any = false;

  #ifdef OUTPUT_READABLE_QUATERNION
      // display quaternion values in easy matrix form: w x y z
      Serial.print("quat[xyzw]\t");
      Serial.print(q.x);
      Serial.print("\t");
      Serial.print(q.y);
      Serial.print("\t");
      Serial.print(q.z);
      Serial.print("\t");
      Serial.print(q.w);
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_EULER
      // display Euler angles in degrees
      Serial.print("euler\t");
      Serial.print(euler[0] * 180/M_PI);
      Serial.print("\t");
      Serial.print(euler[1] * 180/M_PI);
      Serial.print("\t");
      Serial.print(euler[2] * 180/M_PI);
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_YAWPITCHROLL
      // display Euler angles in degrees
      Serial.print("ypr\t");
      Serial.print(ypr[0] * 180/M_PI);
      Serial.print("\t");
      Serial.print(ypr[1] * 180/M_PI);
      Serial.print("\t");
      Serial.print(ypr[2] * 180/M_PI);
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_GYRO
      // display Gyro in m/s
      Serial.print("gyro\t");
      Serial.print(gyro_ypr[0] * 180/M_PI);
      Serial.print("\t");
      Serial.print(gyro_ypr[1] * 180/M_PI);
      Serial.print("\t");
      Serial.print(gyro_ypr[2] * 180/M_PI);
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_REALACCEL_GRAVITY
      // display real acceleration, adjusted with gravity
      Serial.print("arealG\t");
      Serial.print(aa.x * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aa.y * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aa.z * (9.81/8192.0));
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_WORLDACCEL_GRAVITY
      // display initial world-frame acceleration, adjusted with gravity
      // and rotated based on known orientation from quaternion
      Serial.print("aworldG\t");
      Serial.print(aaWorld.x * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aaWorld.y * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aaWorld.z * (9.81/8192.0));
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_REALACCEL
      // display real acceleration, adjusted to remove gravity
      Serial.print("areal\t");
      Serial.print(aaReal.x * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aaReal.y * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aaReal.z * (9.81/8192.0));
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_READABLE_WORLDACCEL
      // display initial world-frame acceleration, adjusted to remove gravity
      // and rotated based on known orientation from quaternion
      Serial.print("aaRealWorld\t");
      Serial.print(aaRealWorld.x * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aaRealWorld.y * (9.81/8192.0));
      Serial.print("\t");
      Serial.print(aaRealWorld.z * (9.81/8192.0));
      Serial.print("\t");
      print_any = true;
  #endif

  #ifdef OUTPUT_TEAPOT
      // display quaternion values in InvenSense Teapot demo format:
      Serial.write(teapotPacket, 14);
      print_any = true;
  #endif

    if (print_any)  {
        Serial.print("Periodo imu: ");
        Serial.print(imuperiod);
        Serial.print("\tcounter: ");
        Serial.println(counter);
      }
    }}*/
  }
}


// ================================================================
// ===                      INIT ROUTINE                        ===
// ================================================================

void imu_module_init(void) {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    Serial.println(F("Initializing I2C devices..."));
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        Wire.setClock(800000); // 400kHz I2C clock. Comment this line if having compilation difficulties
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(800, true);
    #endif

    // initialize device
    mpu.initialize();
        Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
    // verify connection
    while(!mpu.testConnection()){
      delay(500);
      mpu.initialize();
      Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
    }
    pinMode(INTERRUPT_PIN, INPUT);
    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();
    // verify dmp
    while(devStatus != 0){
      delay(1000);
      devStatus = mpu.dmpInitialize();
      Serial.println(devStatus ? F("MPU6050 dmp status failed") : F("MPU6050 dmp status successful"));
    }

    // supply your own gyro offsets here, scaled for min sensitivity
    mpu.setXGyroOffset(51);
    mpu.setYGyroOffset(8);
    mpu.setZGyroOffset(21);
    mpu.setXAccelOffset(1150);
    mpu.setYAccelOffset(-50);
    mpu.setZAccelOffset(1060);

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // Calibration Time: generate offsets and calibrate our MPU6050
        mpu.CalibrateAccel(6);
        mpu.CalibrateGyro(6);
        mpu.PrintActiveOffsets();
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.print(F("Enabling interrupt detection (Teensy external interrupt "));
        Serial.print(digitalPinToInterrupt(INTERRUPT_PIN));
        Serial.println(F(")..."));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }
    #ifdef CFG_USE_LCD
      LCD_16X2.print_line(1, 0, "4 IMU init           ");
      delay(1000);
    #endif
}


// ================================================================
// ===                   ADD TAKSK ROUTINE                      ===
// ================================================================

void imu_module_add_tasks(void) {
  runner.addTask(imuReadTask);
}


// ================================================================
// ===                   ENABLE TASK ROUTINE                    ===
// ================================================================

void imu_module_enable_tasks(void) {
  imuReadTask.enable();
}
