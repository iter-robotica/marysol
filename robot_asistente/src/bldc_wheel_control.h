#ifndef _BLDC_WHEEL_CONTROL_H
#define _BLDC_WHEEL_CONTROL_H

#include <Arduino.h>
#include "prj_variants.h"

void motorControlCurrent(float leftMotor, float rightMotor);
void motorControlRPM(float leftMotor, float rightMotor);
void motorControlDuty(float leftMotor, float rightMotor);
void motorControlCurrentBrake(float leftMotor, float rightMotor);
void bldc_wheel_control_init(void);
void bldc_wheel_control_add_tasks(void);
void bldc_wheel_control_enable_tasks(void);
extern float watts1, watts2;

#endif
