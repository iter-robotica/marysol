#ifndef _OCTOSONAR_H
#define _OCTOSONAR_H
#include "Arduino.h"

void octosonar_init(void);

void octosonarCallback();
void octosonar_add_tasks(void);
void octosonar_enable_tasks(void);


#endif
