#include "prj_variants.h"
#include <TaskSchedulerDeclarations.h>
#include "stability_control.h"
#ifdef CFG_USE_BLDC
#include "bldc_wheel_control.h"
#include "bldc_uart_comm.h"
#endif
#ifdef CFG_USE_ROS
#include "ros_communications.h"
#endif

stability_control controller = stability_control();

double factor;
double Setpoint_speed_left = 0.0, Input_speed_left = 0.0, Output_speed_left = 0.0;
double Setpoint_speed_right = 0.0, Input_speed_right = 0.0, Output_speed_right = 0.0;
PID leftPID(&Input_speed_left, &Output_speed_left, &Setpoint_speed_left, KP_left, KI_left, KD_left, DIRECT);
PID rightPID(&Input_speed_right, &Output_speed_right, &Setpoint_speed_right, KP_right, KI_right, KD_right, DIRECT);

stability_control::stability_control() {
  //Setting default values
    Kp_left = KP_left;    Kd_left = KD_left;    Ki_left = KI_left;
    Kp_right = KP_right;  Kd_right = KD_right;  Ki_right = KI_right;
}
void stability_control::setGainsCb(float kp_ang, float kd_ang, float ki_ang, float kp_sp, float kd_sp, float ki_sp, float kp_st, float kd_st, float ki_st) {
    Kp_left = kp_ang;   Kd_left = kd_ang;   Ki_left = ki_ang;
    Kp_right = kp_sp;   Kd_right = kd_sp;   Ki_right = ki_sp;
    factor = kp_st;
}
void stability_control::setLimitsCb(float min_speed, float max_speed) {
    Min_Speed = min_speed;
    Max_Speed = max_speed;
}

float speed11, speed22, speed11_old, speed22_old, cambio1, cambio2, speed_a, speed_b;
void stability_control::setOdometry(int16_t Odo1, int16_t Odo2) {
  Odometry1 = + Odo1;
  Odometry2 = - Odo2;
}
void stability_control::getGains(float *kp_a, float *kd_a, float *ki_a, float *kp_s, float *kd_s, float *ki_s, float *kp_t, float *kd_t, float *ki_t) {
    *kp_a = Kp_left;     *kd_a = Kd_left;     *ki_a = Ki_left;
    *kp_s = Kp_right;    *kd_s = Kd_right;    *ki_s = Ki_right;
    *kp_t = 0.0;      *kd_t = 0.0;      *ki_t = 0.0;
}

int16_t stability_control::getSpeed() {
    return (Input_speed_left + Input_speed_right) / 2;
}
void stability_control::getControlLoop(int16_t *a,int16_t *b, int16_t *c, int16_t *d, int16_t *e) {
    *a = Setpoint_speed_left;
    *b = Setpoint_speed_right;
    *c = Input_speed_left;
    *d = Input_speed_right;
    *e = (Input_speed_left - Input_speed_right) / 2;
}
void stability_control::resetPID() {
      leftPID.SetMode(MANUAL);
      rightPID.SetMode(MANUAL);
}

uint32_t tiempo_anterior_controller, periodo_controller;
long counter_controller;
extern Scheduler runner;
void stability_control::setControlCurrent(float i_left, float i_right) {
    //periodo_controller = micros() - tiempo_anterior_controller;
    //tiempo_anterior_controller = micros();
    //counter_controller++;

    #ifdef CFG_USE_ROS
      //controller.getGains(&(gainsArray[0]), &(gainsArray[1]), &(gainsArray[2]), &(gainsArray[3]), &(gainsArray[4]), &(gainsArray[5]), &(gainsArray[6]), &(gainsArray[7]), &(gainsArray[8]));
      controller.getControlLoop(&(control_loop_stateArray[0]), &(control_loop_stateArray[1]), &(control_loop_stateArray[2]), &(control_loop_stateArray[3]), &(control_loop_stateArray[4]));
    #endif
    #ifdef CFG_USE_BLDC
      motorControlCurrent(i_left, i_right);
    #endif
}

void stability_control::setControlCurrentBreak(float i_left, float i_right) {
    //periodo_controller = micros() - tiempo_anterior_controller;
    //tiempo_anterior_controller = micros();
    //counter_controller++;

    #ifdef CFG_USE_ROS
      //controller.getGains(&(gainsArray[0]), &(gainsArray[1]), &(gainsArray[2]), &(gainsArray[3]), &(gainsArray[4]), &(gainsArray[5]), &(gainsArray[6]), &(gainsArray[7]), &(gainsArray[8]));
      controller.getControlLoop(&(control_loop_stateArray[0]), &(control_loop_stateArray[1]), &(control_loop_stateArray[2]), &(control_loop_stateArray[3]), &(control_loop_stateArray[4]));
    #endif
    #ifdef CFG_USE_BLDC
      motorControlCurrentBrake(i_left, i_right);
    #endif
}

void stability_control::setControlRPM(float v_left, float v_right) {
  //periodo_controller = micros() - tiempo_anterior_controller;
  //tiempo_anterior_controller = micros();
  //counter_controller++;
  /*if (counter_controller % 20 == 0){
    Serial.print("periodo_controller: ");
    Serial.println(periodo_controller);
  */

  #ifdef CFG_USE_ROS
    //controller.getGains(&(gainsArray[0]), &(gainsArray[1]), &(gainsArray[2]), &(gainsArray[3]), &(gainsArray[4]), &(gainsArray[5]), &(gainsArray[6]), &(gainsArray[7]), &(gainsArray[8]));
    controller.getControlLoop(&(control_loop_stateArray[0]), &(control_loop_stateArray[1]), &(control_loop_stateArray[2]), &(control_loop_stateArray[3]), &(control_loop_stateArray[4]));
  #endif
  #ifdef CFG_USE_BLDC
    //if (int16_t(controller.motor1) != 0.0 && int16_t(controller.motor2) != 0.0)
    motorControlRPM(v_left, v_right);
    //else
      //motorControlCurrent(0, 0);
  #endif
}

void stability_control::setControlDuty(float d_left, float d_right) {
    //periodo_controller = micros() - tiempo_anterior_controller;
    //tiempo_anterior_controller = micros();
    //counter_controller++;

    #ifdef CFG_USE_ROS
      //controller.getGains(&(gainsArray[0]), &(gainsArray[1]), &(gainsArray[2]), &(gainsArray[3]), &(gainsArray[4]), &(gainsArray[5]), &(gainsArray[6]), &(gainsArray[7]), &(gainsArray[8]));
      controller.getControlLoop(&(control_loop_stateArray[0]), &(control_loop_stateArray[1]), &(control_loop_stateArray[2]), &(control_loop_stateArray[3]), &(control_loop_stateArray[4]));
    #endif
    #ifdef CFG_USE_BLDC
      motorControlDuty(d_left, d_right);
    #endif
}

void stability_control::setSpeeds(int16_t speed1, int16_t speed2) {
    // Wheel Speed
    Input_speed_left = Input_speed_left * 0.0 + speed1 * 1.00;
    Input_speed_right = Input_speed_right * 0.0 + speed2 * 1.00;
}

void stability_control::setAngles(int16_t angle_pitch, int16_t angle_yaw, int16_t angle_roll) {

  angle_pitch_adjusted = angle_pitch;
  angle_yaw_adjusted = angle_yaw;
  angle_roll_adjusted = angle_roll;
}

void stability_control::startPID() {
    leftPID.SetMode(AUTOMATIC);    leftPID.SetOutputLimits(-MAX_CONTROL_OUTPUT, MAX_CONTROL_OUTPUT);      leftPID.SetSampleTime(5);
    rightPID.SetMode(AUTOMATIC);   rightPID.SetOutputLimits(-MAX_CONTROL_OUTPUT, MAX_CONTROL_OUTPUT);     rightPID.SetSampleTime(5);
}
