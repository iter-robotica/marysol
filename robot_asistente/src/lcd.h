#ifndef _LCD_H
#define _LCD_H
#include "Arduino.h"

void lcd_init(void);

void lcdWriteCallback();
void lcd_add_tasks(void);
void lcd_enable_tasks(void);

class lcd_16X2 {
  public:
    lcd_16X2();
    void print_line (int16_t line, int16_t column, String text);
    void set_line_up (String line_up);
    void set_line_down (String line_down);
    void write_line_up (String line_up);
    void write_line_down (String line_down);
    void clear ();
};

#endif
