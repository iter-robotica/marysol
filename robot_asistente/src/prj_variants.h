#ifndef _PROJECT_VARIANTS_H
#define _PROJECT_VARIANTS_H

/****               Definimos las variantes de robot                   ****/
/**** Éstas condicionarán las configuraciones posteriores del software ****/
#define ROBOT_VAR_MARYSOL 1
#define ROBOT_VAR_COSME 2
#define ROBOT_VAR_GARA 3
#define ROBOT_VAR_COSMITO 4

///// Escogemos la variante activa
#define ROBOT_VAR ROBOT_VAR_MARYSOL

/**** Elección de configuraciones parciales según la variante del robot ****/
#if (ROBOT_VAR == ROBOT_VAR_MARYSOL)
#define CFG_ROBOT_NAME "Marysol"
#define CFG_USE_BLDC
#define CFG_USE_STABILITY_CTRL
#define CFG_USE_BLDC_VALUES
#define CFG_USE_ROS
#define CFG_USE_ROS_CONTROL
#define CFG_USE_IMU
#define CFG_USE_OCTOSONAR
#define CFG_USE_LCD
#define CFG_USE_LEDS

#else  // (ROBOT_VAR == ROBOT_VAR_MARYSOL)
#error "VARIANTE DE ROBOT NO DEFINIDA"
#endif // (ROBOT_VAR == ROBOT_VAR_MARYSOL)

#endif // _PROJECT_VARIANTS_H
