#include "lcd.h"
#include "prj_variants.h"
#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskSchedulerDeclarations.h>

#ifdef CFG_USE_BLDC
#include "bldc_wheel_control.h"
extern float battery_level;
#endif

#ifdef CFG_USE_IMU
#include "imu_module.h"
extern float ypr[3];
#endif

#ifdef CFG_USE_ROS
#include "ros_communications.h"
extern ros::NodeHandle nh;
#endif

#include  <Wire.h>
#include  <LiquidCrystal_I2C.h>

#define I2C_ADDR 0x20

// Constructor de la librería de LCD 16x2
// Aqui se configuran los pines asignados a la pantalla del PCF8574
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

lcd_16X2 LCD_16X2 = lcd_16X2();

String stringOne = "RosSerial -OFF- ";
String stringTwo = " All systems ready. Waiting for Rosserial ";

String lcd_16_1 = stringOne.substring(0, 16);
String lcd_16_2 = stringTwo.substring(0, 16);

u_int8_t contador_1, contador_2, cont_1, cont_2 = 0;

lcd_16X2::lcd_16X2() {

}

void lcd_16X2::print_line (int16_t line, int16_t column, String text){
  lcd.setCursor (column, line);
  lcd.print(text);
}

void lcd_16X2::write_line_up(String line_up){
  lcd.setCursor (0, 0);
  lcd.print(line_up);
}

void lcd_16X2::write_line_down(String line_down){
  lcd.setCursor (0, 1);
  lcd.print(line_down);
}

void lcd_16X2::set_line_up(String line_up){
  if (line_up != stringOne)
  {
    while (line_up.length() < 16)
      line_up = line_up + " ";
    stringOne = line_up;
    contador_1 = 0;
    cont_1 = 0;
  }
}

void lcd_16X2::set_line_down(String line_down){
  if (line_down != stringTwo)
  {
    while (line_down.length() < 16)
      line_down = line_down + " ";
    stringTwo = line_down;
    contador_2 = 0;
    cont_2 = 0;
  }
}

void lcd_16X2::clear (){
  lcd.clear();
}

extern Scheduler runner;
Task lcdWriteTask(500000, TASK_FOREVER, &lcdWriteCallback);
void lcdWriteCallback() {
  #ifdef CFG_USE_ROS
  if (!nh.connected())
  {
    stringOne = "RosSerial -OFF- ";
    stringTwo = " All systems ready. Waiting for Rosserial ";
  }
  else
  {
    stringOne = "RosSerial -ON- ";
    stringTwo = " All systems ready ";
  }
  #endif

  lcd_16_1 = stringOne.substring(contador_1, contador_1 + 16);
  LCD_16X2.write_line_up(lcd_16_1);
  lcd_16_2 = stringTwo.substring(contador_2, contador_2 + 16);
  LCD_16X2.write_line_down(lcd_16_2);
  if (stringOne.length() - 16 > contador_1 && stringOne.length() > 16)
    contador_1++;
  else if (cont_1 < 4)
    cont_1++;
  else
  {
    contador_1 = 0;
    cont_1 = 0;
  }
  if (stringTwo.length() - 16 > contador_2 && stringTwo.length() > 16)
    contador_2++;
  else if (cont_2 < 4)
    cont_2++;
  else
  {
    contador_2 = 0;
    cont_2 = 0;
  }
}

void lcd_init(){
  //Serial.println("Iniciando LCD           ");
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Serial.println(F("Initializing I2C devices..."));
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
      Wire.setClock(800000); // 400kHz I2C clock. Comment this line if having compilation difficulties
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(800, true);
  #endif
  // Indicar a la libreria que tenemos conectada una pantalla de 16x2
  lcd.begin(16, 4);
  // Mover el cursor a la primera posición de la pantalla (0, 0)
  lcd.home();
  //Enciende la Luz del Fondo del LCD
  //lcd.setBacklightPin(3, POSITIVE);
  //lcd.setBacklight(HIGH);
  // Imprimir "Marysol Teensy" en la primera linea
  lcd.print(" Marysol Teensy ");
  // Mover el cursor a la segunda linea (1) primera columna
  lcd.setCursor (0, 1);
  // Imprimir otra cadena en esta posicion
  lcd.print("1 LCD Init                 ");
  // Esperar un segundo
  delay(1000);
}

void lcd_add_tasks(void) {

    runner.addTask(lcdWriteTask);
}

void lcd_enable_tasks(void) {

    lcdWriteTask.enable();
}
