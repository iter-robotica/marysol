#include <Arduino.h>
#include "prj_variants.h"
#include "bldc_wheel_control.h"
#include "main.h"

/*#include "I2Cdev.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif*/

#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskScheduler.h>

#ifdef CFG_USE_ROS
#include "ros_communications.h"
#endif

#ifdef CFG_USE_RC_PPM
#include "rc_ppm.h"
#endif

#ifdef CFG_USE_IMU
#include "imu_module.h"
#endif

#ifdef CFG_USE_LEDS
#include "leds_module.h"
#endif

#ifdef CFG_USE_LCD
#include "lcd.h"
extern lcd_16X2 LCD_16X2;
#endif

#ifdef CFG_USE_OCTOSONAR
#include "octosonar.h"
#endif

#ifdef CFG_USE_STABILITY_CTRL
#include "stability_control.h"
extern uint32_t tiempo_anterior_controller;
#endif

#define PORT_EMGCY 2

bool EMGCY;
Scheduler runner;

void init_modules(void) {
  #ifdef CFG_USE_LCD
    Serial.println("LCD init");
    lcd_init();
  #endif
  #ifdef CFG_USE_ROS
    Serial.println("ROS init");
    ros_communications_init();
  #endif
  #ifdef CFG_USE_BLDC
    Serial.println("BLDC init");
    bldc_wheel_control_init();
  #endif
  #ifdef CFG_USE_IMU
    Serial.println("IMU init");
    imu_module_init();
  #endif
  #ifdef CFG_USE_OCTOSONAR
    Serial.println("Octosonar init");
    octosonar_init();
  #endif
  #ifdef CFG_USE_LEDS
    Serial.println("Leds init");
    octosonar_init();
  #endif
}

void add_module_tasks(void) {
  #ifdef CFG_USE_LCD
    lcd_add_tasks();
  #endif
  #ifdef CFG_USE_ROS
    ros_communications_add_tasks();
  #endif
  #ifdef CFG_USE_BLDC
    bldc_wheel_control_add_tasks();
  #endif
  #ifdef CFG_USE_OCTOSONAR
    octosonar_add_tasks();
  #endif
  #ifdef CFG_USE_IMU
    imu_module_add_tasks();
  #endif
  #ifdef CFG_USE_LEDS
    leds_module_add_tasks();
  #endif
}

void enable_module_tasks(void) {
  #ifdef CFG_USE_ROS
    ros_communications_enable_tasks();
  #endif
  #ifdef CFG_USE_BLDC
    bldc_wheel_control_enable_tasks();
  #endif
  #ifdef CFG_USE_IMU
    imu_module_enable_tasks();
  #endif
  #ifdef CFG_USE_OCTOSONAR
    octosonar_enable_tasks();
  #endif
  #ifdef CFG_USE_LCD
    lcd_enable_tasks();
  #endif
  #ifdef CFG_USE_LEDS
    //leds_module_enable_tasks(1);
  #endif
}

void micro_init(void) {
  delay(5000);
  pinMode(PORT_EMGCY, INPUT);
  Serial.begin(115200);
  Serial.println("Micro init");
  init_modules();
  Serial.println("Tasks init");
  add_module_tasks();
  enable_module_tasks();
}

void setup(void){
  micro_init();
}

uint32_t tiempo_anterior_main, periodo_main, periodo_controller_main, tiempo_anterior_old;
long counter_main, counter_controller_old;

void loop(void){
  // Intentamos que los botones de acción o emergencia se refresquen a
  // cada vuelta del bucle, para que sirvan a todas las tareas del
  // scheduler.
  periodo_main = micros() - tiempo_anterior_main;
  tiempo_anterior_main = micros();
  counter_main++;
  EMGCY = digitalRead(PORT_EMGCY);
  if (EMGCY == HIGH){
    stateArray[5] = 1;
  }else{
    stateArray[5] = 0;
  }
  if (tiempo_anterior_controller != tiempo_anterior_old){
    //periodo_controller_main = micros() - tiempo_anterior_old;
    //Serial.print("per_main: ");
    //Serial.print(periodo_controller_main);
    //Serial.print("\tmicros: ");
    //Serial.print(micros());
    //Serial.print("\tt_ant: ");
    //Serial.print(tiempo_anterior_controller);
    if (periodo_controller_main > 1000000){
      //Serial.println("\tparo: ");
      #ifdef CFG_USE_BLDC
        EMGCY = HIGH;
      #endif
    }
    //else {
    //  Serial.println("\tok: ");
    //}
    tiempo_anterior_old = tiempo_anterior_controller;
  }
  runner.execute();
}
