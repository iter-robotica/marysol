#include <Arduino.h>
#include "prj_variants.h"

#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskSchedulerDeclarations.h>

#ifdef CFG_USE_CAN
#include <FlexCAN.h>
static CAN_message_t msg;
#endif

#include "bldc_uart_comm.h"

#ifdef CFG_USE_ROS
#include "ros_communications.h"
#endif

#ifdef CFG_USE_STABILITY_CTRL
#include "stability_control.h"
extern stability_control controller;
#endif

#ifdef CFG_USE_LCD
#include "lcd.h"
extern lcd_16X2 LCD_16X2;
#endif

extern Scheduler runner;
extern bool EMGCY;

unsigned long count;
unsigned long oldcount;
unsigned long period;
bool old_state_buzzer = false;
float battery_level;

bool estado_anterior_buzzer = false;
int16_t leftMotorCommand, rightMotorCommand;
int16_t raw_rpm1, raw_rpm2;

#define BUZZER_PIN 15
#define BATTERY_LOW_LEVEL 1100

#define MOTOR_POLES 15
#define WHEEL_PERIMETER 80  // cm
#define LEFT_MOTOR_CAN_ID 2
#define RIGHT_MOTOR_CAN_ID 1
#define REMOTE_VESC_CAN_ID LEFT_MOTOR_CAN_ID
#define BLDC_SET_RPM CAN_PACKET_SET_RPM
#define BLDC_SET_CURRENT CAN_PACKET_SET_CURRENT
#define BLDC_SET_DUTY CAN_PACKET_SET_DUTY
#define BLDC_SET_CURRENT_BRAKE CAN_PACKET_SET_CURRENT_BRAKE

void setMotors(float, float, int);
#ifdef CFG_USE_CAN
void setMotorsCAN(float, float, int);
#endif
void motorsRPM(float, float);
void motorsCurrent(float, float);
void motorsDuty(float, float);
void motorsBrake(float, float);

int vesc_id = -1;
#ifdef CFG_USE_BLDC_VALUES
void getValuesCallback();
#endif
Task timerTask(1000, TASK_FOREVER, &bldc_interface_uart_run_timer);
Task readSerialTask(0, TASK_FOREVER, &bldc_uart_comm_read);
#ifdef CFG_USE_BLDC_VALUES
//Task getValuesTask(TASK_SECOND, TASK_FOREVER, &getValuesCallback);
Task getValuesTask(10000, TASK_FOREVER, &getValuesCallback);
#endif
#ifdef CFG_USE_ROS
uint32_t tiempo_anterior_bldc, periodo_bldc;
long counter_variadores;
float v1_filtered, v2_filtered;
int32_t odometryArray_old[ODOMETRY_SIZE];
void calc_speed(float periodo_bldc_speed)
{
  //from ticks per nanosec to rpm
  float v1_real = (60 * 1000000 * ((odometryArray[0] - odometryArray_old[0]) / (periodo_bldc_speed * 86)));
  float v2_real = (60 * 1000000 * ((odometryArray[1] - odometryArray_old[1]) / (periodo_bldc_speed * 86)));
  v1_filtered = v1_real * 0.3 + v1_filtered * 0.7;
  v2_filtered = v2_real * 0.3 + v2_filtered * 0.7;
  stateArray[6] = int16_t(v1_filtered);
  stateArray[7] = int16_t(v2_filtered);

  /*Serial.print("contador: ");
  Serial.print(counter_variadores);
  Serial.print("  periodo: ");
  Serial.print(periodo_bldc_speed);
  Serial.print("  OdoA: ");
  Serial.print(odometryArray[0]);
  Serial.print("  OldA: ");
  Serial.print(odometryArray_old[0]);
  Serial.print("  speed_a1: ");
  Serial.print(stateArray[0]);
  Serial.print("  speed_a2: ");
  Serial.println(stateArray[6]);*/

  /*Serial.print("  OdoB: ");
  Serial.print(odometryArray[1]);
  Serial.print("  OldB: ");
  Serial.print(odometryArray_old[1]);
  Serial.print("  speed_b1: ");
  Serial.print(stateArray[1]);
  Serial.print("  speed_b2: ");
  Serial.println(stateArray[7]);*/

  odometryArray_old[0] = odometryArray[0];
  odometryArray_old[1] = odometryArray[1];
}
#endif
float watts1, watts2;

#ifdef CFG_USE_BLDC_VALUES
static  void bldc_val_received_1(mc_values *val) {
#ifdef CFG_USE_ROS
    stateArray[0] = val->rpm/MOTOR_POLES;
    odometryArray[0] = val->tachometer;
    batteryArray[0] = int16_t(val->v_in * 100.0);
    esc_faultArray[0] = val->fault_code;
    watts1 = val->watt_hours;
#endif
    raw_rpm1 = val->rpm;
    getValuesTask.restart();
}

static  void bldc_val_received_2(mc_values *val) {
#ifdef CFG_USE_ROS
    stateArray[1] = val->rpm/MOTOR_POLES;
    odometryArray[1] = val->tachometer;
    batteryArray[1] = int16_t(val->v_in * 100.0);
    esc_faultArray[1] = val->fault_code;
    watts2 = val->watt_hours;
#endif
    raw_rpm2 = val->rpm;
    count = micros();
    period = count - oldcount;
    oldcount = count;
    getValuesTask.restart();

    /*Serial.print("  stateArray[0]: ");
    Serial.print(stateArray[0]);
    Serial.print("  stateArray[1]: ");
    Serial.println(stateArray[1]);
    setMotors(15*20, 15*20, BLDC_SET_RPM);*/
}

void getValuesCallback() {

    bldc_interface_set_forward_can(vesc_id);
    // toggle vesc to be asked for values on each task execution
    if (vesc_id == REMOTE_VESC_CAN_ID) {
        bldc_interface_set_rx_value_func(bldc_val_received_1);
        vesc_id = -1;
    } else if (vesc_id == -1) {
        bldc_interface_set_rx_value_func(bldc_val_received_2);
        vesc_id = REMOTE_VESC_CAN_ID;
    }
    bldc_interface_get_values();

    battery_level = (batteryArray[0] + batteryArray[1]) / 2;

    if (battery_level < BATTERY_LOW_LEVEL){
      if (estado_anterior_buzzer == false){
        digitalWrite(BUZZER_PIN, HIGH);
        estado_anterior_buzzer = true;
      }
      else{
        digitalWrite(BUZZER_PIN, LOW);
        estado_anterior_buzzer = false;
      }
    }
    else
      digitalWrite(BUZZER_PIN, LOW);

    #ifdef CFG_USE_STABILITY_CTRL
      controller.setOdometry(odometryArray[0], odometryArray[1]);
      controller.setSpeeds(stateArray[0], stateArray[1]);
    #endif

    periodo_bldc = micros() - tiempo_anterior_bldc;

    if (periodo_bldc > 100000)
    {
      counter_variadores++;
      calc_speed(periodo_bldc);
      tiempo_anterior_bldc = micros();
      //Serial.println(periodo_bldc);
    }
}
#endif



void motorControlCurrent(float leftMotor, float rightMotor) {
    #ifdef CFG_USE_BLDC
        setMotors(leftMotor, rightMotor, BLDC_SET_CURRENT);
    #else
        // TODO: BLDC ALTERNATIVE
    #endif
}

void motorControlRPM(float leftMotor, float rightMotor) {
    #ifdef CFG_USE_BLDC
        setMotors(MOTOR_POLES * leftMotor, MOTOR_POLES * rightMotor, BLDC_SET_RPM);
    #else
        /* TODO: BLDC ALTERNATIVE */
    #endif
}

void motorControlDuty(float leftMotor, float rightMotor) {
    #ifdef CFG_USE_BLDC
        setMotors(leftMotor, rightMotor, BLDC_SET_DUTY);
    #else
        /* TODO: BLDC ALTERNATIVE */
    #endif
}

void motorControlCurrentBrake(float leftMotor, float rightMotor) {
    #ifdef CFG_USE_BLDC
        setMotors(leftMotor, rightMotor, BLDC_SET_CURRENT_BRAKE);
    #else
        /* TODO: BLDC ALTERNATIVE */
    #endif
}


void setMotors(float leftMotor, float rightMotor, int func) {
  // Serial.print("EMGCY: ");
  // Serial.println(EMGCY);
    if (EMGCY == HIGH) {
        #ifdef CFG_USE_STABILITY_CTRL
            controller.resetPID();
        #endif //CFG_USE_STABILITY_CTRL
        leftMotor = 0.0;
        rightMotor = 0.0;
        func = BLDC_SET_CURRENT;
    }
    #ifdef CFG_DBG_BLDC_MOTORS
        Serial.print("motorCommand: ");Serial.print(leftMotor);Serial.print(" ");Serial.println(rightMotor);
    #endif
    #ifdef CFG_USE_CAN
        setMotorsCAN(leftMotor, rightMotor, func);
    #else
        switch (func) {
            case BLDC_SET_RPM :
                motorsRPM(leftMotor, rightMotor);
                break;
            case BLDC_SET_CURRENT :
                motorsCurrent(leftMotor, rightMotor);
                break;
            case BLDC_SET_DUTY :
                motorsDuty(leftMotor, rightMotor);
                break;
            case BLDC_SET_CURRENT_BRAKE :
                motorsBrake(leftMotor, rightMotor);
                break;
        }
    #endif
}

void motorsRPM(float leftMotor, float rightMotor) {
    bldc_interface_set_forward_can(REMOTE_VESC_CAN_ID);
    bldc_interface_set_rpm(leftMotor);
    bldc_interface_set_forward_can(-1);
    bldc_interface_set_rpm(rightMotor);
}

void motorsCurrent(float leftMotor, float rightMotor) {

    bldc_interface_set_forward_can(REMOTE_VESC_CAN_ID);
    bldc_interface_set_current(leftMotor);
    bldc_interface_set_forward_can(-1);
    bldc_interface_set_current(rightMotor);
}

void motorsDuty(float leftMotor, float rightMotor) {

    bldc_interface_set_forward_can(REMOTE_VESC_CAN_ID);
    bldc_interface_set_duty_cycle(leftMotor);
    bldc_interface_set_forward_can(-1);
    bldc_interface_set_duty_cycle(rightMotor);
}

void motorsBrake(float leftMotor, float rightMotor) {

    bldc_interface_set_forward_can(REMOTE_VESC_CAN_ID);
    bldc_interface_set_current_brake(leftMotor);
    bldc_interface_set_forward_can(-1);
    bldc_interface_set_current_brake(rightMotor);
}

#ifdef CFG_USE_CAN
void setMotorsCAN(float leftMotor, float rightMotor, int func) {

    float scale;
    switch (func) {
        case CAN_PACKET_SET_RPM :
            scale = 1.0;
            break;
        case CAN_PACKET_SET_CURRENT :
            scale = 1000.0;
            break;
        case CAN_PACKET_SET_DUTY :
            scale = 100000.0;
            break;
        case CAN_PACKET_SET_CURRENT_BRAKE :
            scale = 1000.0;
            break;
    }
    msg.ext = 1;
    int32_t send_index = 0;
    buffer_append_int32(msg.buf, (int32_t)(leftMotor * scale), &send_index);
    uint8_t controller_id = 2;
    msg.id = controller_id | ((uint32_t)func << 8);
    msg.len = send_index;
    Can0.write(msg);
    msg.ext = 1;
    send_index = 0;
    buffer_append_int32(msg.buf, (int32_t)(rightMotor * scale), &send_index);
    controller_id = 1;
    msg.id = controller_id | ((uint32_t)func << 8);
    msg.len = send_index;
    Can0.write(msg);
}
#endif

void bldc_wheel_control_init(void) {
  #ifdef CFG_USE_LCD
    LCD_16X2.print_line(1, 0, "3 BLDC init           ");
    delay(1000);
  #endif
  bldc_uart_comm_init();
  pinMode(BUZZER_PIN, OUTPUT);
  #ifdef CFG_USE_CAN
      Can0.begin();
  #endif
}

void bldc_wheel_control_add_tasks(void) {
  #ifdef CFG_USE_BLDC_VALUES
      runner.addTask(getValuesTask);
  #endif
  runner.addTask(timerTask);
  runner.addTask(readSerialTask);
}

void bldc_wheel_control_enable_tasks(void) {
  #ifdef CFG_USE_BLDC_VALUES
      getValuesTask.enableDelayed(1250);
  #endif
  #ifdef CFG_USE_BLDC
      timerTask.enable();
      readSerialTask.enableDelayed(500);
  #endif
}
