#ifndef _STABILITY_CTRL_H
#define _STABILITY_CTRL_H

#include "Arduino.h"
#include "PID_v1.h"

// Default control terms
#define KP_left 0.5 //0.0
#define KD_left 0.0 //0.0
#define KI_left 0.1 //0.0
#define KP_right 0.5 //0.0
#define KD_right 0.0 //0.0
#define KI_right 0.1 //0.0
#define MAX_Speed 1.0 //0.0
#define MIN_Speed 0.05 //0.0

#define MAX_CONTROL_OUTPUT 10

class stability_control {
public:
    stability_control();
    void init();
    void setAngles(int16_t angle_pitch, int16_t angle_yaw, int16_t angle_roll);
    void setSpeeds(int16_t speed1, int16_t speed2);
    void setControlCurrent(float i_left, float i_right);
    void setControlCurrentBreak(float i_left, float i_right);
    void setControlDuty(float d_left, float d_right);
    void setControlRPM(float v_left, float v_right);
    void setOdometry(int16_t Odo1, int16_t Odo2);
    void setLimitsCb(float min_speed, float max_speed);
    void setGainsCb(float kp_a, float kd_a, float ki_a, float kp_s, float kd_s, float ki_s, float kp_st, float kd_st, float ki_st);
    void getGains(float *kp_a, float *kd_a, float *ki_a, float *kp_s, float *kd_s, float *ki_s, float *kp_t, float *kd_t, float *ki_t);
    int16_t getSpeed();
    void getControlLoop(int16_t *a,int16_t *b, int16_t *c, int16_t *d, int16_t *e);
    void resetPID();
    void startPID();
    float motor1;
    float motor2;

private:
    float angle_pitch_adjusted;
    float angle_yaw_adjusted;
    float angle_roll_adjusted;
    float wheel_velocity;

    // Default control values from constant definitions
    float Kp_left = KP_left;
    float Kd_left = KD_left;
    float Ki_left = KI_left;
    float Kp_right = KP_right;
    float Kd_right = KD_right;
    float Ki_right = KI_right;
    float Max_Speed = MAX_Speed;
    float Min_Speed = MIN_Speed;
    float Odometry1;
    float Odometry2;
};

#endif
