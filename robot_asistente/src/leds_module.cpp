#include "prj_variants.h"
#include "leds_module.h"
#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskSchedulerDeclarations.h>

#ifdef CFG_USE_ROS
#include "ros_communications.h"
#endif

#ifdef CFG_USE_STABILITY_CTRL
#include "stability_control.h"
extern stability_control controller;
#endif

#ifdef CFG_USE_LCD
#include "lcd.h"
extern lcd_16X2 LCD_16X2;
#endif

// NeoPixel test program showing use of the WHITE channel for RGBW
// pixels only (won't look correct on regular RGB NeoPixel strips).

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    15

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(175, LED_PIN, NEO_RGB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, avoid connecting
// on a live circuit... if you must, connect GND first. Minimize
// distance between Arduino and first pixel.

uint16_t Red = 0;
uint16_t Green = 255;
uint16_t Blue = 0;
uint16_t task_ = 2;
uint16_t Period = 50;
uint16_t global_i = 0;
uint16_t global_j = 0;
uint16_t global_k = 0;
bool direction = true;
uint16_t Brightness = 255;

extern Scheduler runner;
Task simpleColorTask(Period * 1000, TASK_FOREVER, &simpleColorCallback);
Task colorWipeTask(Period * 1000, TASK_FOREVER, &colorWipeCallback);
Task RainbowTask(Period * 1000, TASK_FOREVER, &RainbowCallback);
Task rainbowCycleTask(Period * 1000, TASK_FOREVER, &rainbowCycleCallback);
Task theaterChaseTask(Period * 1000, TASK_FOREVER, &theaterChaseCallback);
Task rainbow2Task(Period * 1000, TASK_FOREVER, &rainbow2Callback);
Task theaterChaseRainbowTask(Period * 1000, TASK_FOREVER, &theaterChaseRainbowCallback);
Task whiteOverRainbowTask(Period * 1000, TASK_FOREVER, &whiteOverRainbowCallback);
Task pulseWhiteUpTask(Period * 1000, TASK_FOREVER, &pulseWhiteUpCallback);
Task pulseWhiteDownTask(Period * 1000, TASK_FOREVER, &pulseWhiteDownCallback);
Task rainbowFade2WhiteTask(Period * 1000, TASK_FOREVER, &rainbowFade2WhiteCallback);
Task PulsesTask(Period * 10000, TASK_FOREVER, &PulsesCallback);
Task ColorLineTask(Period * 1000, TASK_FOREVER, &ColorLineCallback);
Task switchTask(20000000, TASK_FOREVER, &switchCallback);

// ================================================================
// ===                   FUNCTIONS ROUTINE                      ===
// ================================================================

LedStrip Led_Strip = LedStrip();

LedStrip::LedStrip() {

}

void LedStrip::set_leds(uint8_t green, uint8_t red, uint8_t blue, uint8_t task, uint8_t period){
  switchTask.disable();
  Red = red;
  Green = green;
  Blue = blue;
  Period = period;
  leds_module_enable_tasks(task);
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel2(uint16_t WheelPos) {
  if(WheelPos < 85) {
   return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

uint32_t Wheel(uint16_t WheelPos) {
  if(WheelPos < 85) {
   return strip.Color((WheelPos * 3)/2, (255 - WheelPos * 3)/2, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return strip.Color((255 - WheelPos * 3)/2, 0, (WheelPos * 3)/2);
  } else {
   WheelPos -= 170;
   return strip.Color(0, (WheelPos * 3)/2, (255 - WheelPos * 3)/2);
  }
}

// ================================================================
// ===                    CALLBACK ROUTINE                      ===
// ================================================================

//1
void simpleColorCallback() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(Green, Red, Blue));
  }
  strip.show();
  simpleColorTask.disable();
}

//2
void colorWipeCallback() {
  strip.setPixelColor(global_i, strip.Color(Green, Red, Blue));
  strip.show();
  global_i += 1;
  if (global_i >= strip.numPixels()){
    colorWipeTask.disable();
  }
}

//3
void RainbowCallback() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels())) & 255));
  }
  strip.show();
  RainbowTask.disable();
}

//4
void rainbowCycleCallback() {
  for(uint8_t i=0; i< strip.numPixels(); i++) {
    strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + global_j) & 255));
  }
  strip.show();
  global_j += 10;
}

//5
void theaterChaseCallback() {
  strip.clear();
  for(uint16_t i=0; i<strip.numPixels(); i += 3) {
    strip.setPixelColor(i, strip.Color(Green, Red, Blue));
  }
  strip.show();
  theaterChaseTask.disable();
}

//6
void rainbow2Callback() {
  for(uint16_t j=0; j< global_j; j++){
    for(uint16_t i=j; i< strip.numPixels(); i += global_j) {
      uint32_t color = Wheel((j * 255 / global_j) & 255);
      strip.setPixelColor(i, color);
    }
  }
  strip.show();

  if (direction == true){
    global_j += 1;
  }else{
    global_j -= 1;
  }

  if(global_j >= 64 && direction == true) {
    direction = false;
  }
  else if(global_j <= 16 && direction == false) {
    direction = true;
  }
}

//7
void theaterChaseRainbowCallback() {
  strip.clear();
  for(uint16_t c=global_i; c<strip.numPixels(); c += 4) {
    strip.setPixelColor(c, Wheel((c - global_j) & 255));
  }
  strip.show();
  global_i += 1;
  if (global_i > 4){
    global_i = 0;
  }
  global_j += 1;
}

//8
void whiteOverRainbowCallback() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {  // For each pixel in strip...
    if(((i >= global_i) && (i <= global_j)) || ((global_i > global_j) && ((i >= global_i) || (i <= global_j)))) {
      strip.setPixelColor(i, strip.Color(0, 0, 0)); // Set off
    } else {                                             // else set rainbow
      strip.setPixelColor(i, Wheel((i + global_i) & 255));
    }
  }
  strip.show();
  global_i++;
  if(++global_i >= strip.numPixels()) {
    global_i = 0;
  }
  if(++global_j >= strip.numPixels()) {
    global_j = 0;
  }
  //whiteOverRainbowTask.setInterval(10000 + 10 * global_i);
}

//9
void pulseWhiteUpCallback(){
  if (direction == true){
    global_i++; global_j++; global_k++;
  }else{
    global_i--; global_j--; global_k--;
  }
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(uint32_t(global_i * Green / 255), uint32_t(global_j * Red / 255), uint32_t(global_k * Blue / 255)));
  }
  strip.show();
  if(global_i >= 64 && direction == true) {
    direction = false;
  }
  if(global_i <= 8 && direction == false) {
    direction = true;
  }
}

//10
void pulseWhiteDownCallback(){
  uint32_t color = Wheel((global_j) & 255);
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
  }
  strip.show();
  global_j += 1;
  if (global_j >= 256){
    global_j = 0;
  }
}

//11
void rainbowFade2WhiteCallback(){
  uint32_t color = Wheel((global_j) & 255);
  for(uint8_t i=0; i<strip.numPixels(); i += 2) {
    strip.setPixelColor(i, color);
  }
  color = Wheel((255 - global_j) & 255);
  for(uint16_t i=1; i<strip.numPixels(); i += 2) {
    strip.setPixelColor(i, color);
  }
  strip.show();
  global_j += 1;
  if (global_j >= 256){
    global_j = 0;
  }
}

//12
void PulsesCallback(){
  if (global_i % 2 == 0){
    for(uint16_t i=0; i<strip.numPixels(); i++) {
        strip.setPixelColor(i, strip.Color(Green, Red, Blue));
    }
  }
  else{
    for(uint16_t i=0; i<strip.numPixels(); i++) {
        strip.setPixelColor(i, strip.Color(0, 0, 0));
    }
  }
  strip.show();
  global_i += 1;
}

//13
void ColorLineCallback() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {  // For each pixel in strip...
    if(((i >= global_i) && (i <= global_j)) || ((global_i > global_j) && ((i >= global_i) || (i <= global_j)))) {
      strip.setPixelColor(i, strip.Color(0, 0, 0)); // Set off
    } else {                                             // else set rainbow
      strip.setPixelColor(i, strip.Color(Green, Red, Blue));
    }
  }
  strip.show();
  global_i++;
  global_j = global_i - 10;
  if(++global_i >= strip.numPixels()) {
    global_i = 0;
  }
}

// ================================================================
// ===                      SWITCH TASK                         ===
// ================================================================

void switchCallback(){
  leds_module_enable_tasks(task_);
  task_++;
  if (task_ >= 14){
    task_ = 2;
    Red = 0;
    Green = 255;
    Blue = 0;
  }
}


// ================================================================
// ===                      INIT ROUTINE                        ===
// ================================================================

void leds_module_init(void) {
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
    clock_prescale_set(clock_div_1);
  #endif
  pinMode(LED_PIN, OUTPUT);
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.setBrightness(Brightness);
  strip.show();            // Turn OFF all pixels ASAP
  #ifdef CFG_USE_LCD
    LCD_16X2.print_line(1, 0, "6 Leds init           ");
    delay(1000);
  #endif
}

// ================================================================
// ===                   ADD TAKSK ROUTINE                      ===
// ================================================================

void leds_module_add_tasks(void) {
  runner.addTask(simpleColorTask);
  runner.addTask(colorWipeTask);
  runner.addTask(RainbowTask);
  runner.addTask(rainbowCycleTask);
  runner.addTask(theaterChaseTask);
  runner.addTask(rainbow2Task);
  runner.addTask(theaterChaseRainbowTask);
  runner.addTask(whiteOverRainbowTask);
  runner.addTask(pulseWhiteUpTask);
  runner.addTask(pulseWhiteDownTask);
  runner.addTask(rainbowFade2WhiteTask);
  runner.addTask(PulsesTask);
  runner.addTask(ColorLineTask);
  runner.addTask(switchTask);
}

// ================================================================
// ===                   ENABLE TASK ROUTINE                    ===
// ================================================================

void leds_module_enable_task() {
  switchTask.enable();
}

void leds_module_enable_tasks(uint16_t task) {
  leds_module_disable_tasks();
  global_i = 0;
  global_j = 0;
  global_k = 0;
  direction = true;
  Serial.print(task_);
  Serial.print(" - ");
  Serial.println(Period);
  switch (task)
  {
    case 0:
        Red = 0; Green = 0; Blue = 0;
        simpleColorCallback();
        break;
    case 1:
        simpleColorTask.setInterval(Period * 1000);
        simpleColorTask.enable();
        break;
    case 2:
        colorWipeTask.setInterval(Period * 1000);
        colorWipeTask.enable();
        break;
    case 3:
        RainbowTask.setInterval(Period * 1000);
        RainbowTask.enable();
        break;
    case 4:
        rainbowCycleTask.setInterval(Period * 1000);
        rainbowCycleTask.enable();
        break;
    case 5:
        theaterChaseTask.setInterval(Period * 1000);
        theaterChaseTask.enable();
        break;
    case 6:
        rainbow2Task.setInterval(Period * 1000);
        rainbow2Task.enable();
        break;
    case 7:
        theaterChaseRainbowTask.setInterval(Period * 1000);
        theaterChaseRainbowTask.enable();
        break;
    case 8:
        whiteOverRainbowTask.setInterval(Period * 1000);
        whiteOverRainbowTask.enable();
        break;
    case 9:
        pulseWhiteUpTask.setInterval(Period * 1000);
        pulseWhiteUpTask.enable();
        break;
    case 10:
        pulseWhiteDownTask.setInterval(Period * 1000);
        pulseWhiteDownTask.enable();
        break;
    case 11:
        rainbowFade2WhiteTask.setInterval(Period * 1000);
        rainbowFade2WhiteTask.enable();
        break;
    case 12:
        PulsesTask.setInterval(Period * 10000);
        PulsesTask.enable();
        break;
    case 13:
        ColorLineTask.setInterval(Period * 1000);
        ColorLineTask.enable();
        break;
    }
}

// ================================================================
// ===                  DISABLE TASK ROUTINE                    ===
// ================================================================

void leds_module_disable_tasks(void) {
  simpleColorTask.disable();
  colorWipeTask.disable();
  RainbowTask.disable();
  rainbowCycleTask.disable();
  theaterChaseTask.disable();
  rainbow2Task.disable();
  theaterChaseRainbowTask.disable();
  whiteOverRainbowTask.disable();
  pulseWhiteUpTask.disable();
  pulseWhiteDownTask.disable();
  rainbowFade2WhiteTask.disable();
  PulsesTask.disable();
  ColorLineTask.disable();
}
