#define USE_TEENSY_HW_SERIAL
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/UInt8MultiArray.h>
#include <std_msgs/UInt32MultiArray.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <geometry_msgs/Twist.h>
#include "prj_variants.h"

// ROS constants and variables
extern ros::NodeHandle nh;
/****************************************************
state array
state int16 array
0   left motor rpm                    int16
1   right motor rpm                   int16
2   IMU angle                         int16 (Degrees)
3   IMU yaw                           int16 (Degrees)
4   IMU roll                          int16 (Degrees)
5   robot_state                       int16 (0 = ok, 1 = stop)
6   left motor rpm from vesc          int16
7   right motor rpm from vesc         int16
*/
extern std_msgs::Int16MultiArray stateData;
extern ros::Publisher statePub;
#define STATE_SIZE 8
extern int16_t stateArray[STATE_SIZE];
extern std_msgs::MultiArrayDimension stateDim;
/*
imu array
state float32 array
0   orientation x             float32
1   orientation y             float32
2   orientation z             float32
3   orientation w             float32
4   angular_velocity x        float32
5   angular_velocity y        float32
6   angular_velocity z        float32
7   linear_acceleration x     float32
8   linear_acceleration y     float32
9   linear_acceleration z     float32
10   linear_speed x           float32
11   linear_speed y           float32
12   linear_speed z           float32
*/
//#ifdef CFG_USE_IMU
extern std_msgs::Float32MultiArray imuData;
extern ros::Publisher imuPub;
#define IMU_SIZE 13
extern float imuArray[IMU_SIZE];
extern std_msgs::MultiArrayDimension imuDim;
//#endif
/*
octosonar array
state int32 array
0   sensor A value            int32
1   sensor B value            int32
2   sensor C value            int32
3   sensor D value            int32
4   sensor E value            int32
5   sensor F value            int32
6   sensor G value            int32
7   sensor H value            int32
8   sensor I value            int32
9   sensor J value            int32
10   sensor K value           int32
11   sensor L value           int32
*/
//#ifdef CFG_USE_OCTOSONAR
extern std_msgs::Int32MultiArray octosonarData;
extern ros::Publisher octosonarPub;
#define OCTOSONAR_SIZE 12
extern int32_t octosonarArray[OCTOSONAR_SIZE];
extern std_msgs::MultiArrayDimension octosonarDim;
//#endif
/*
odometry int32 array
0   left motor tachometer   int32 -  90 steps / rev
1   right motor tachometer
*/
extern std_msgs::Int32MultiArray odometryData;
extern ros::Publisher odometryPub;
#define ODOMETRY_SIZE 2
extern int32_t odometryArray[ODOMETRY_SIZE];
extern std_msgs::MultiArrayDimension odometryDim;

/*
Only for tunning values 20Hz
control_loop_state int16 array
0   left motor command (current)    int16 (centiAmperes)
1   right motor command (current)
2   target angle                    int16(centiDegrees)
3   target speed                    int16 (mm/s)
4   target steering                 int16
*/
extern std_msgs::Int16MultiArray control_loop_stateData;
extern ros::Publisher control_loop_statePub;
#define CONTROL_LOOP_STATE_SIZE 5
extern int16_t control_loop_stateArray[CONTROL_LOOP_STATE_SIZE];
extern std_msgs::MultiArrayDimension control_loop_stateDim;
/*
Slow values  1Hz
battery int16 array
0  left motor V in         int16 (centiVolts)
1  right motor V in
*/
extern std_msgs::Int16MultiArray batteryData;
extern ros::Publisher batteryPub;
#define BATTERY_SIZE 2
extern int16_t batteryArray[BATTERY_SIZE];
extern std_msgs::MultiArrayDimension batteryDim;

/*
Only update when changed

esc_fault uint8 array
0  left motor fault code   uint8
1  right motor fault code
*/
extern std_msgs::UInt8MultiArray esc_faultData;
extern ros::Publisher esc_faultPub;
#define ESC_FAULT_SIZE 2
extern uint8_t esc_faultArray[ESC_FAULT_SIZE];
extern std_msgs::MultiArrayDimension esc_faultDim;
/*

Gains
gains float32 array
0  kp angle                (float)
1  kd angle
2  ki angle
3  kp speed
4  kd speed
5  ki speed
6  kp steer
7  kd steer
8  ki steer
*/
/*extern std_msgs::Float32MultiArray gainsData;
extern ros::Publisher gainsPub;
#define GAINS_SIZE 9
extern float gainsArray[GAINS_SIZE];
extern std_msgs::MultiArrayDimension gainsDim;*/
/*
Sonars
sonars float32 array
0-7 sensores inferiores anillo
8-11 sensores superiores traseros
*/
extern std_msgs::Float32MultiArray sensorsData;
extern ros::Publisher sonarsPub;
#define SENSOR_SIZE 12
/*
-------------------------------------------------------
Commands

Gains
gains float32 array
0   kp angle                (float)
1   kd angle
2   ki angle
3   kp speed
4   kd speed
5   ki speed
6   kp steer
7   kd steer
8   ki steer
*/
#ifdef CFG_USE_STABILITY_CTRL
/*void gainsCb(const std_msgs::Float32MultiArray& msg);
extern ros::Subscriber<std_msgs::Float32MultiArray> gainsSub;*/

/*Limits
limits float32 array
0   max_speed                (float)
1   min_speed
*/
void limitsCb(const std_msgs::Float32MultiArray& msg);
extern ros::Subscriber<std_msgs::Float32MultiArray> robotlimitsSub;
#endif

#ifdef CFG_USE_ROS_CONTROL
/*

control mediante intensidad
control float32 array
0   left_wheel                float32
1   right_wheel               float32
*/
void controlCurrentCallback(const std_msgs::Float32MultiArray& msg);
extern ros::Subscriber<std_msgs::Float32MultiArray> controlCurrentSub;
/*

control mediante velocidad
control float32 array
0   left_wheel                float32
1   right_wheel               float32
*/
void controlRPMCallback(const std_msgs::Float32MultiArray& msg);
extern ros::Subscriber<std_msgs::Float32MultiArray> controlRPMSub;
/*

control mediante ciclo de trabajo
control float32 array
0   left_wheel                float32
1   right_wheel               float32
*/
void controlDutyCallback(const std_msgs::Float32MultiArray& msg);
extern ros::Subscriber<std_msgs::Float32MultiArray> controlDutySub;
/*

control mediante intensidad y freno
control float32 array
0   left_wheel                float32
1   right_wheel               float32
*/
void controlCurrentBreakCallback(const std_msgs::Float32MultiArray& msg);
extern ros::Subscriber<std_msgs::Float32MultiArray> controlCurrentBreakSub;
/*

control de la tira de leds
control UInt8 array
0   green                UInt8_t  (0-255) green RGB-component
1   red                  UInt8_t  (0-255) red RGB-component
2   blue                 UInt8_t  (0-255) blue RGB-component
3   task                 UInt8_t  (0-255) task number
4   period               UInt8_t  (0-255) millisec
*/
void controlLedsCallback(const std_msgs::UInt8MultiArray& msg);
extern ros::Subscriber<std_msgs::UInt8MultiArray> controlLedsrSub;

#endif

#ifdef CFG_USE_LCD
/*

0   steering                string
******************************************************/
void lineUp(const std_msgs::String& msg);
extern ros::Subscriber<std_msgs::String> lineUpSub;
/*

0   steering                string
******************************************************/
void lineDown(const std_msgs::String& msg);
extern ros::Subscriber<std_msgs::String> lineDownSub;

#endif

void ros_communications_init();
void ros_communications_add_tasks();
void ros_communications_enable_tasks();
