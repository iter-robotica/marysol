#include "octosonar.h"
#include "prj_variants.h"
#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskSchedulerDeclarations.h>

#ifdef CFG_USE_LCD
#include "lcd.h"
extern lcd_16X2 LCD_16X2;
#endif

#define SONAR_ADDR 0x20
#define SONAR_INT 17
#define ACTIVE_SONARS 0xFFFF
#define MSG_SIZE 12

#ifdef CFG_USE_ROS
#include "ros_communications.h"
#endif

#include <Wire.h>
#include <OctoSonar.h>

OctoSonarX2 myOcto(SONAR_ADDR, SONAR_INT);

int pin_read[MSG_SIZE] = {0,2,1,4,6,5,8,10,9,12,14,13};


extern Scheduler runner;
Task octosonarTask(5000, TASK_FOREVER, &octosonarCallback);
void octosonarCallback() {
  OctoSonar::doSonar();  // call every cycle, OctoSonar handles the spacing
  for (int i = 0; i < MSG_SIZE; i++) {
    #ifdef CFG_USE_ROS
      if (myOcto.read(pin_read[i]) == 0.0 || myOcto.read(pin_read[i]) > 2500)
        octosonarArray[i] = 2500;
      else
        octosonarArray[i] = int32_t(myOcto.read(pin_read[i]));
    #endif
    /*if (myOcto.read(pin_read[i]) == 0.0)
      Serial.print("max");
    else
      Serial.print(myOcto.read(pin_read[i]));
    if (pin_read[i] == 0 || pin_read[i] == 4 || pin_read[i] == 8 )
      Serial.print("  /");
    Serial.print("\t");*/
  }
  //Serial.println();
}

void octosonar_init(){
  myOcto.begin(ACTIVE_SONARS);   // initialize bus, pins etc
  #ifdef CFG_USE_LCD
    LCD_16X2.print_line(1, 0, "5 Octosonar init    ");
    delay(1000);
  #endif
}

void octosonar_add_tasks(void) {

    runner.addTask(octosonarTask);
}

void octosonar_enable_tasks(void) {

    octosonarTask.enable();
}
