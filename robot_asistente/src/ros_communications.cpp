#include "ros_communications.h"
#include "prj_variants.h"

#define _TASK_MICRO_RES
#define _TASK_TIMECRITICAL
#include <TaskSchedulerDeclarations.h>
// Tasks
void rosCallback();
Task rosTask(10000, TASK_FOREVER, &rosCallback);

void rosCallback() {
    //if (!nh.connected())
        nh.spinOnce();
}

extern Scheduler runner;

#ifdef CFG_USE_STABILITY_CTRL
#include "stability_control.h"
extern stability_control controller;
#endif

#ifdef CFG_USE_LCD
#include "lcd.h"
extern lcd_16X2 LCD_16X2;
#endif

#ifdef CFG_USE_OCTOSONAR
#include "octosonar.h"
#endif

#ifdef CFG_USE_LEDS
#include "leds_module.h"
extern LedStrip Led_Strip;
#endif

ros::NodeHandle nh;
// telemetry
std_msgs::Int16MultiArray stateData;
ros::Publisher statePub("state", &stateData);
int16_t stateArray[STATE_SIZE];
std_msgs::MultiArrayDimension stateDim;

//#ifdef CFG_USE_IMU
std_msgs::Float32MultiArray imuData;
ros::Publisher imuPub("imu_data", &imuData);
float imuArray[IMU_SIZE];
std_msgs::MultiArrayDimension imuDim;
//#endif

//#ifdef CFG_USE_OCTOSONAR
std_msgs::Int32MultiArray octosonarData;
ros::Publisher octosonarPub("octosonar_data", &octosonarData);
int32_t octosonarArray[OCTOSONAR_SIZE];
std_msgs::MultiArrayDimension octosonarDim;
//#endif

std_msgs::Int32MultiArray odometryData;
ros::Publisher odometryPub("odometry", &odometryData);
int32_t odometryArray[ODOMETRY_SIZE];
std_msgs::MultiArrayDimension odometryDim;

std_msgs::Int16MultiArray control_loop_stateData;
ros::Publisher control_loop_statePub("control_loop_state", &control_loop_stateData);
int16_t control_loop_stateArray[CONTROL_LOOP_STATE_SIZE];
std_msgs::MultiArrayDimension control_loop_stateDim;

std_msgs::Int16MultiArray batteryData;
ros::Publisher batteryPub("battery", &batteryData);
int16_t batteryArray[BATTERY_SIZE];
std_msgs::MultiArrayDimension batteryDim;

std_msgs::UInt8MultiArray esc_faultData;
ros::Publisher esc_faultPub("esc_fault", &esc_faultData);
uint8_t esc_faultArray[ESC_FAULT_SIZE];
std_msgs::MultiArrayDimension esc_faultDim;

/*std_msgs::Float32MultiArray gainsData;
ros::Publisher gainsPub("gains", &gainsData);
float gainsArray[GAINS_SIZE];
std_msgs::MultiArrayDimension gainsDim;*/


// settings
#ifdef CFG_USE_STABILITY_CTRL
//ros::Subscriber<std_msgs::Float32MultiArray> gainsSub("commands/gains", gainsCb);
ros::Subscriber<std_msgs::Float32MultiArray> robotlimitsSub("commands/robot_limits", limitsCb);
#endif //CFG_USE_STABILITY_CTRL
#ifdef CFG_USE_ROS_CONTROL
ros::Subscriber<std_msgs::Float32MultiArray> controlCurrentSub("commands/controlCurrent", controlCurrentCallback);
ros::Subscriber<std_msgs::Float32MultiArray> controlRPMSub("commands/controlRPM", controlRPMCallback);
ros::Subscriber<std_msgs::Float32MultiArray> controlDutySub("commands/controlDuty", controlDutyCallback);
ros::Subscriber<std_msgs::Float32MultiArray> controlCurrentBreakSub("commands/controlCurrentBreak", controlCurrentBreakCallback);
ros::Subscriber<std_msgs::UInt8MultiArray> controlLedsSub("commands/Leds", controlLedsCallback);

#endif // CFG_USE_ROS_CONTROL
#ifdef CFG_USE_LCD
ros::Subscriber<std_msgs::String> lineUpSub("lcd_line_up", lineUp);
ros::Subscriber<std_msgs::String> lineDownSub("lcd_line_down", lineDown);
#endif // CFG_USE_LCD

void telemetryStateCallback();
#ifdef CFG_USE_IMU
void telemetryImuCallback();
#endif
#ifdef CFG_USE_OCTOSONAR
void telemetryOctosonarCallback();
#endif
void telemetryOdometryCallback();
void telemetryControlLoopStateCallback();
void telemetryBatteryCallback();
//void telemetryGainsCallback();

Task telemetryStateTask(100000, TASK_FOREVER, &telemetryStateCallback);
#ifdef CFG_USE_IMU
Task telemetryImuTask(100000, TASK_FOREVER, &telemetryImuCallback);
#endif
#ifdef CFG_USE_OCTOSONAR
Task telemetryOctosonarTask(200000, TASK_FOREVER, &telemetryOctosonarCallback);
#endif
Task telemetryOdometryTask(100000, TASK_FOREVER, &telemetryOdometryCallback);
Task telemetryControlLoopStateTask(TASK_SECOND, TASK_FOREVER, &telemetryControlLoopStateCallback);
Task telemetryBatteryTask(TASK_SECOND, TASK_FOREVER, &telemetryBatteryCallback);
//Task telemetryGainsTask(TASK_SECOND, TASK_FOREVER, &telemetryGainsCallback);

//uint32_t tiempo_anterior_telemetry, periodo_telemetry;
long counter_telemetry;
/*periodo_telemetry = micros() - tiempo_anterior_telemetry;
tiempo_anterior_telemetry = micros();
if (periodo_telemetry > 6000)
{
  Serial.print("  Periodo telemetria: ");
  Serial.println(periodo_telemetry);
}*/

//int32_t odometryArray_ant[ODOMETRY_SIZE];

void telemetryStateCallback() {
  //Serial.println("  Enviamos STATE ");
  statePub.publish( &stateData);
}

#ifdef CFG_USE_IMU
void telemetryImuCallback() {
  //Serial.println("  Enviamos IMU ");
  imuPub.publish( &imuData);
}
#endif

#ifdef CFG_USE_OCTOSONAR
void telemetryOctosonarCallback() {
  //Serial.println("  Enviamos OCTOSONAR ");
  octosonarPub.publish( &octosonarData);
}
#endif

void telemetryOdometryCallback() {
  //Serial.println("  Enviamos ODOMETRY ");
  odometryPub.publish( &odometryData);
}

void telemetryControlLoopStateCallback() {
  //Serial.println("  Enviamos CONTROL LOOP ");
  control_loop_statePub.publish( &control_loop_stateData);
}

void telemetryBatteryCallback() {
  //Serial.println("  Enviamos BATTERY ");
  batteryPub.publish( &batteryData);
}

/*void telemetryGainsCallback() {
  //Serial.println("  Enviamos GAINGS ");
  gainsPub.publish( &gainsData);
}*/


void ros_communications_init() {
    #ifdef CFG_USE_LCD
      LCD_16X2.print_line(1, 0, "2 Rosserial init           ");
      delay(1000);
    #endif

    nh.initNode();
    stateData.layout.dim = &stateDim;
    stateData.layout.dim[0].label = "values";
    stateData.layout.dim[0].size = STATE_SIZE;
    stateData.layout.dim[0].stride = STATE_SIZE;
    stateData.layout.data_offset = 0;
    stateData.layout.dim_length = 1;
    stateData.data = stateArray;
    stateData.data_length = STATE_SIZE;
    nh.advertise(statePub);

    #ifdef CFG_USE_IMU
    imuData.layout.dim = &imuDim;
    imuData.layout.dim[0].label = "values";
    imuData.layout.dim[0].size = IMU_SIZE;
    imuData.layout.dim[0].stride = IMU_SIZE;
    imuData.layout.data_offset = 0;
    imuData.layout.dim_length = 1;
    imuData.data = imuArray;
    imuData.data_length = IMU_SIZE;
    nh.advertise(imuPub);
    #endif

    #ifdef CFG_USE_OCTOSONAR
    octosonarData.layout.dim = &octosonarDim;
    octosonarData.layout.dim[0].label = "values";
    octosonarData.layout.dim[0].size = OCTOSONAR_SIZE;
    octosonarData.layout.dim[0].stride = OCTOSONAR_SIZE;
    octosonarData.layout.data_offset = 0;
    octosonarData.layout.dim_length = 1;
    octosonarData.data = octosonarArray;
    octosonarData.data_length = OCTOSONAR_SIZE;
    nh.advertise(octosonarPub);
    #endif

    odometryData.layout.dim = &odometryDim;
    odometryData.layout.dim[0].label = "values";
    odometryData.layout.dim[0].size = ODOMETRY_SIZE;
    odometryData.layout.dim[0].stride = ODOMETRY_SIZE;
    odometryData.layout.data_offset = 0;
    odometryData.layout.dim_length = 1;
    odometryData.data = odometryArray;
    odometryData.data_length = ODOMETRY_SIZE;
    nh.advertise(odometryPub);

    control_loop_stateData.layout.dim = &control_loop_stateDim;
    control_loop_stateData.layout.dim[0].label = "values";
    control_loop_stateData.layout.dim[0].size = CONTROL_LOOP_STATE_SIZE;
    control_loop_stateData.layout.dim[0].stride = CONTROL_LOOP_STATE_SIZE;
    control_loop_stateData.layout.data_offset = 0;
    control_loop_stateData.layout.dim_length = 1;
    control_loop_stateData.data = control_loop_stateArray;
    control_loop_stateData.data_length = CONTROL_LOOP_STATE_SIZE;
    nh.advertise(control_loop_statePub);

    batteryData.layout.dim = &batteryDim;
    batteryData.layout.dim[0].label = "values";
    batteryData.layout.dim[0].size = BATTERY_SIZE;
    batteryData.layout.dim[0].stride = BATTERY_SIZE;
    batteryData.layout.data_offset = 0;
    batteryData.layout.dim_length = 1;
    batteryData.data = batteryArray;
    batteryData.data_length = BATTERY_SIZE;
    nh.advertise(batteryPub);

    esc_faultData.layout.dim = &esc_faultDim;
    esc_faultData.layout.dim[0].label = "values";
    esc_faultData.layout.dim[0].size = ESC_FAULT_SIZE;
    esc_faultData.layout.dim[0].stride = ESC_FAULT_SIZE;
    esc_faultData.layout.data_offset = 0;
    esc_faultData.layout.dim_length = 1;
    esc_faultData.data = esc_faultArray;
    esc_faultData.data_length = ESC_FAULT_SIZE;
    nh.advertise(esc_faultPub);

    /*gainsData.layout.dim = &gainsDim;
    gainsData.layout.dim[0].label = "values";
    gainsData.layout.dim[0].size = GAINS_SIZE;
    gainsData.layout.dim[0].stride = GAINS_SIZE;
    gainsData.layout.data_offset = 0;
    gainsData.layout.dim_length = 1;
    gainsData.data = gainsArray;
    gainsData.data_length = GAINS_SIZE;
    nh.advertise(gainsPub);*/


#ifdef CFG_USE_STABILITY_CTRL
    //nh.subscribe(gainsSub);
    nh.subscribe(robotlimitsSub);
#endif // CFG_USE_STABILITY_CTRL
#ifdef CFG_USE_ROS_CONTROL
    nh.subscribe(controlCurrentSub);
    nh.subscribe(controlRPMSub);
    nh.subscribe(controlDutySub);
    nh.subscribe(controlCurrentBreakSub);
    nh.subscribe(controlLedsSub);
#endif // CFG_USE_ROS_CONTROL
#ifdef CFG_USE_LCD
    nh.subscribe(lineUpSub);
    nh.subscribe(lineDownSub);
#endif // CFG_USE_LCD

}

void ros_communications_add_tasks() {
  runner.addTask(rosTask);
  runner.addTask(telemetryStateTask);
  #ifdef CFG_USE_IMU
  runner.addTask(telemetryImuTask);
  #endif
  #ifdef CFG_USE_OCTOSONAR
  runner.addTask(telemetryOctosonarTask);
  #endif
  runner.addTask(telemetryOdometryTask);
  runner.addTask(telemetryControlLoopStateTask);
  runner.addTask(telemetryBatteryTask);
  //runner.addTask(telemetryGainsTask);
}

void ros_communications_enable_tasks() {
  rosTask.enableDelayed(1000);
  telemetryStateTask.enableDelayed(100000);
  #ifdef CFG_USE_IMU
  telemetryImuTask.enableDelayed(200000);
  #endif
  #ifdef CFG_USE_OCTOSONAR
  telemetryOctosonarTask.enableDelayed(200000);
  #endif
  telemetryOdometryTask.enableDelayed(300000);
  telemetryControlLoopStateTask.enableDelayed(400000);
  telemetryBatteryTask.enableDelayed(500000);
  //telemetryGainsTask.enableDelayed(600000);
}

#ifdef CFG_USE_STABILITY_CTRL
void gainsCb(const std_msgs::Float32MultiArray& msg) {
  controller.setGainsCb(msg.data[0], msg.data[1], msg.data[2], msg.data[3], msg.data[4], msg.data[5], msg.data[6], msg.data[7], msg.data[8]);
  //Serial.println("gains--------");
}
void limitsCb(const std_msgs::Float32MultiArray& msg) {
  controller.setLimitsCb(msg.data[0], msg.data[1]);
  /*#ifdef CFG_USE_LCD
    LCD_16X2.clear();
    LCD_16X2.print_line(0, 0, "Min V/I: " + String(msg.data[0]) );
    LCD_16X2.print_line(1, 0, "Max V/I: " + String(msg.data[1]) );
  #endif*/
  //Serial.println("Limits--------");
}
#endif //CFG_USE_STABILITY_CTRL

#ifdef CFG_USE_ROS_CONTROL
float data0_ant, data1_ant = 0;
void controlCurrentCallback(const std_msgs::Float32MultiArray& msg) {
  controller.setControlCurrent(msg.data[0], msg.data[1]);       // Intensity control
  /*#ifdef CFG_USE_LCD
    if (data0_ant != msg.data[0] || data1_ant != msg.data[1])
    {
      LCD_16X2.clear();
      LCD_16X2.print_line(0, 0, "v_right: " + String(msg.data[0]) );
      LCD_16X2.print_line(1, 0, "v_left:  " + String(msg.data[1]) );
    }
  #endif*/
}
void controlRPMCallback(const std_msgs::Float32MultiArray& msg) {
  controller.setControlRPM(msg.data[0], msg.data[1]);        // Speed control
  /*#ifdef CFG_USE_LCD
    if (data0_ant != msg.data[0] || data1_ant != msg.data[1])
    {
      LCD_16X2.clear();
      LCD_16X2.print_line(0, 0, "i_left:  " + String(msg.data[0]) );
      LCD_16X2.print_line(1, 0, "i_right: " + String(msg.data[1]) );
    }
  #endif*/
}
void controlDutyCallback(const std_msgs::Float32MultiArray& msg) {
  controller.setControlDuty(msg.data[0], msg.data[1]);        // Duty control
  /*#ifdef CFG_USE_LCD
    if (data0_ant != msg.data[0] || data1_ant != msg.data[1])
    {
      LCD_16X2.clear();
      LCD_16X2.print_line(0, 0, "i_left:  " + String(msg.data[0]) );
      LCD_16X2.print_line(1, 0, "i_right: " + String(msg.data[1]) );
    }
  #endif*/
}
void controlCurrentBreakCallback(const std_msgs::Float32MultiArray& msg) {
  controller.setControlCurrentBreak(msg.data[0], msg.data[1]);       // Intensity control
  /*#ifdef CFG_USE_LCD
    if (data0_ant != msg.data[0] || data1_ant != msg.data[1])
    {
      LCD_16X2.clear();
      LCD_16X2.print_line(0, 0, "v_right: " + String(msg.data[0]) );
      LCD_16X2.print_line(1, 0, "v_left:  " + String(msg.data[1]) );
    }
  #endif*/
}
void controlLedsCallback(const std_msgs::UInt8MultiArray& msg) {
  Led_Strip.set_leds(msg.data[0], msg.data[1], msg.data[2], msg.data[3], msg.data[4]);
}

#endif

#ifdef CFG_USE_LCD
void lineUp(const std_msgs::String& msg) {
  LCD_16X2.set_line_up(msg.data);
}
void lineDown(const std_msgs::String& msg) {
  LCD_16X2.set_line_down(msg.data);
}
#endif
