#ifndef _ROS_openai_ros_RLExperimentInfo_h
#define _ROS_openai_ros_RLExperimentInfo_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace openai_ros
{

  class RLExperimentInfo : public ros::Msg
  {
    public:
      typedef int32_t _episode_number_type;
      _episode_number_type episode_number;
      typedef float _episode_reward_type;
      _episode_reward_type episode_reward;

    RLExperimentInfo():
      episode_number(0),
      episode_reward(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_episode_number;
      u_episode_number.real = this->episode_number;
      *(outbuffer + offset + 0) = (u_episode_number.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_episode_number.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_episode_number.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_episode_number.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->episode_number);
      union {
        float real;
        uint32_t base;
      } u_episode_reward;
      u_episode_reward.real = this->episode_reward;
      *(outbuffer + offset + 0) = (u_episode_reward.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_episode_reward.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_episode_reward.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_episode_reward.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->episode_reward);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_episode_number;
      u_episode_number.base = 0;
      u_episode_number.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_episode_number.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_episode_number.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_episode_number.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->episode_number = u_episode_number.real;
      offset += sizeof(this->episode_number);
      union {
        float real;
        uint32_t base;
      } u_episode_reward;
      u_episode_reward.base = 0;
      u_episode_reward.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_episode_reward.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_episode_reward.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_episode_reward.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->episode_reward = u_episode_reward.real;
      offset += sizeof(this->episode_reward);
     return offset;
    }

    const char * getType(){ return "openai_ros/RLExperimentInfo"; };
    const char * getMD5(){ return "117729220546455cc216a7df0b6e91d0"; };

  };

}
#endif
