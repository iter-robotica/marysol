#ifndef BLDC_UART_COMM_H_
#define BLDC_UART_COMM_H_
#include <Arduino.h>
extern "C" {
#include "bldc_interface_uart.h"
#include "bldc_interface.h"
#include "buffer.h"

}

// Functions
void bldc_uart_comm_init(void);
void bldc_uart_comm_read(void);
#endif /* BLDC_UART_COMM_H_ */
