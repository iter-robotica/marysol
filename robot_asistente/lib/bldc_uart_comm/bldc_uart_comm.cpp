
#include "bldc_uart_comm.h"
#include <string.h>

// Private functions
static void send_packet(unsigned char *data, unsigned int len);

/**
 * Callback that the packet handler uses to send an assembled packet.
 *
 * @param data
 * Data array pointer
 * @param len
 * Data array length
 */
static void send_packet(unsigned char *data, unsigned int len) {
	if (len > (PACKET_MAX_PL_LEN + 5)) {
		return;
	}

	// Wait for the previous transmission to finish.

	// Copy this data to a new buffer in case the provided one is re-used
	// after this function returns.
#if defined(__MK20DX128__) || defined(__MK20DX256__)
	static uint8_t buffer[PACKET_MAX_PL_LEN + 5];
	memcpy(buffer, data, len);
#endif
	// Send the data over UART
#if defined(__MK20DX128__) || defined(__MK20DX256__)
	Serial1.write(buffer, len);
#else
	Serial1.write(data,len);
#endif
}

static void bldc_val_received(mc_values *val) {
	Serial.print("RPM: ");
	Serial.println(val->rpm);
	//Serial.write(' ');
	//Serial.print("Tacho: ");
	//Serial.println(val->tachometer);
	//Serial.write(' ');
}

void bldc_uart_comm_init(void) {
	// Initialize UART
	Serial1.begin(1000000);
	// Initialize the bldc interface and provide a send function
	bldc_interface_uart_init(send_packet);
	//bldc_interface_set_rx_value_func(bldc_val_received);
}

void bldc_uart_comm_read(void) {
	// Start processing thread
	while (Serial1.available()) {
		bldc_interface_uart_process_byte(Serial1.read());
	}
}

void bldc_uart_comm_1khz_timer(void) {
	// Start timer thread
	bldc_interface_uart_run_timer();
}
