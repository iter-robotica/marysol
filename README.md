# Marysol

Este proyecto tiene como objetivo principal el desarrollo de un robot de protocolo que
sirva como guía a las visitas que recibe el Instituto Tecnológico y de Energías Renovables ITER.
![Marysol_2](media/Marysol_2.png)


## Modo Real
En este modo se recogen los datos de los sensores directamente así como los motores de Actuadores

### Robots
* URDF Marysol 0 - Funcionando (Versión muy simple "kinect2, imu, odom" para pruebas)
* URDF Marysol 1 - Funcionando.
* URDF Marysol 2 - Funcionando.

### Actuadores
* ControlDiferencial - Se utiliza un paquete ya definido, no se puede comprobar su funcionalidad hasta que no se resuelva el problema de movilidad con las ruedas.

### Sensores
* PointCloud - Funcionando para ambas cámaras.
* LaserScan - Funcionando, se obtienen a partir de los PointCloud
* Imu - Se ha conseguido crear un tópico tipo sensor_msgs/Imu Message completo.
* Octosonar - Un arduino recogerá los valores y los enviará a la Teensy para que los reenvíe por el serialpython (Ricardo).
* Gamepad - Mando por USB con el que podemos controlar el robot


## Modo Simulación
En este modo se recogen los datos de los a sensores a través de la virtualización de un mundo Gazebo, así como una emulación de los motores para este mismo mundo.

### Robots
* URDF Marysol 0 - Funcionando (Versión muy simple "kinect2, imu, odom" para pruebas)
* URDF Marysol 1 - Funcionando.
* URDF Marysol 2 - Funcionando.

### Actuadores
* ControlDiferencial - Funcionando.

### Sensores
* PointCloud - Funcionando.
* LaserScan - Funcionando.
* Imu - Funcionando.
* Octosonar - Funcionando.
* Gamepad - Mando por USB con el que podemos controlar el robot

## Modo rosbag
En este modo los datos de los sensores son reproducidos por una bolsa de trabajo previamente grabada, de resto los demás requerimientos son iguales a los anteriores según se haya guardado la bolsa para simulación o real. Aquí simplemente cabe destacar la posibilidad de reproducir o grabar los datos de los sensores.
* Grabar - Funcionando.
* Reproducir - Falla en en momento de poner el global_frame a world, desde el odo funciona.

## Misiones
* Mapear - Funcionando.
* Navegar - Falta desarrollar el nodo de función.
* Aprendizaje por refuerzo - Funcionando.

## Desarrollado bajo el grupo

* [ITER](https://www.iter.es/) - Instituto Tecnológico y de Energías Renovables - Área de Robótica

## Autores

* **Richard David** - *Desarrollo principal en ROS y código arduino* - rimartin@iter.es
* **Ricardo Pérez** - *Desarrollo principal en ROS y código arduino* - jperez@iter.es

## Licencia

Este proyecto es un desarrollo privado y no se permite el acceso a su contenido sin autorización.
