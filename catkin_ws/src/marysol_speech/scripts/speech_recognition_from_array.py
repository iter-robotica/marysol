#!/usr/bin/env python3
import rospy
import roslib
from std_msgs.msg import String, Bool
import time, logging
from datetime import datetime
import threading, collections, queue, os, os.path
from audio_common_msgs.msg import AudioData
import deepspeech
import numpy as np
import pyaudio
import wave
import webrtcvad
from halo import Halo
from scipy import signal
import sys

logging.basicConfig(level=20)

#############################################################
#############################################################
class SpeechRecognition():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("speech_recognition")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        p = pyaudio.PyAudio()
        info = p.get_host_api_info_by_index(0)
        numdevices = info.get('deviceCount')

        from os.path import expanduser

        self.vad_aggressiveness = rospy.get_param("/speech_recognition/vad_aggressiveness", 3)
        self.nospinner = rospy.get_param("/speech_recognition/nospinner", True)
        self.model = rospy.get_param("/speech_recognition/model", expanduser("~") + '/marysol/catkin_ws/marysol_heavy_data/deepspeech_models/output_graph.pbmm')
        self.scorer = rospy.get_param("/speech_recognition/scorer", expanduser("~") + '/marysol/catkin_ws/marysol_heavy_data/deepspeech_models/ITER_Marysol.scorer')
        self.device = rospy.get_param("/speech_recognition/device", 0)
        self.rate = rospy.get_param("/speech_recognition/rate", 16000)

        self.string_pub = rospy.Publisher('/speech/mic_speech', String, queue_size=10)
        self.string = String()

        self.listening_pub = rospy.Publisher('/speech/listening', Bool, queue_size=10)
        self.listening = False

        rospy.Subscriber("/speech_audio", AudioData, self.AudioChannelCallback)
        rospy.Subscriber("/is_speeching", Bool, self.ListeningCallback)

        if os.path.isdir(self.model):
            model_dir = self.model
            self.model = os.path.join(model_dir, 'output_graph.pb')
            self.scorer = os.path.join(model_dir, self.scorer)

        #print('Initializing model...')
        logging.info("self.model: %s", self.model)
        self.this_model = deepspeech.Model(self.model)
        if self.scorer:
            logging.info("self.scorer: %s", self.scorer)
            self.this_model.enableExternalScorer(self.scorer)

        # Stream from microphone to DeepSpeech using VAD
        spinner = None
        if not self.nospinner:
            spinner = Halo(spinner='line')
        self.stream_context = self.this_model.createStream()

    #############################################################################
    def AudioChannelCallback(self, msg):
    #############################################################################
        stream_context = self.this_model.createStream()
        stream_context.feedAudioContent(np.frombuffer(msg.data, np.int16))
        text = stream_context.finishStream()
        self.string.data = text
        if len(text) >= 1:
            self.string_pub.publish(self.string)

    #############################################################################
    def ListeningCallback(self, msg):
    #############################################################################
        self.listening_pub.publish(msg.data)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    speech_recognition = SpeechRecognition()
    rospy.spin()
