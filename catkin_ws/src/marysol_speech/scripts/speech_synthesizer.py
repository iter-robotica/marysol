#!/usr/bin/env python3
import rospy
import vlc
from gtts import gTTS
from std_msgs.msg import Bool, String
import os
import os.path
import time
import rospkg

#############################################################
#############################################################
class SpeechSynthesizer():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("speech_synthesizer")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        rospack = rospkg.RosPack()
        self.pkg_path = rospack.get_path("marysol_speech")

        # subscriptions
        rospy.Subscriber("/speech/string_to_speech", String, self.stringCallback)
        rospy.Subscriber("/speech/ShutUp", Bool, self.shutupCallback)
        self.playing_pub = rospy.Publisher('/speech/playing', Bool, queue_size=10)
        self.playing = Bool()
        self.ShutUp = False

    #############################################################
    def stringCallback(self, msg):
    #############################################################
        rospy.loginfo("MSG: %s " % msg.data)

        speech = self.pkg_path + "/media/voice/" + msg.data + ".mp3"
        speech = speech.replace(" ","_").lower()
        message = ""
        if "speech_" in msg.data:
            from os.path import expanduser
            ruta_speechs = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/speechs/"
            if os.path.exists(ruta_speechs + msg.data.replace("speech_","").lower().replace(" ","_") + ".txt"):
                with open(ruta_speechs + msg.data.replace("speech_","").lower().replace(" ","_") + ".txt") as f:
                    lines = f.readlines()
                    message = ""
                    for line in lines:
                        message += line.replace("\n"," ")
            else:
                message = "archivo de narración, " + msg.data.replace("_"," ").replace("speech_","") + ", no encontrado"
        else:
            message = msg.data

        def play():
            p = vlc.MediaPlayer(speech)
            self.playing.data = True
            self.playing_pub.publish(self.playing)
            p.play()
            time.sleep(0.5)
            while p.is_playing():
                if self.ShutUp:
                    p.stop()
            self.ShutUp = False
            time.sleep(0.5)
            self.playing.data = False
            self.playing_pub.publish(self.playing)

        def save():
            # try:
            language = 'es'
            myobj = gTTS(text=message, lang=language, slow=False)
            print(speech)
            myobj.save(speech)
            # except:
            #     print("Error en la creación del archivo")
            #     time.sleep(2)

        if os.path.isfile(speech) and os.path.getsize(speech) > 10:
            print("cargamos " + speech)
            play()
        else:
            print("creamos " + speech)
            save()
            while os.path.getsize(speech) < 10:
                save()
            play()

    #############################################################
    def shutupCallback(self, msg):
    #############################################################
        #rospy.loginfo("MSG: %s " % msg.data)
        self.ShutUp = msg.data


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        speech_synthesizer = SpeechSynthesizer()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
