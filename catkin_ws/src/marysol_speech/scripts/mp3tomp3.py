#!/usr/bin/env python3

from std_msgs.msg import UInt32MultiArray, String, Bool, Int16MultiArray, String
import numpy as np

from os.path import expanduser

from gtts import gTTS


#############################################################
#############################################################
class VisualWindow():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################

        # from os import walk
        # ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/renders/"
        # dir, subdirs, archivos = next(walk(ruta))
        #
        # for file in archivos:
        #     file = file.replace("_"," ").replace(".mp4","")
        #     print(file)
        #     speech = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/voice/" + str(file) + ".mp3"
        #     speech = speech.replace(" ","_").lower()
        #     language = 'es'
        #     myobj = gTTS(text=str(file), lang=language, tld=language, slow=False)
        #     myobj.save(speech)

        self.common_questions = {}
        import yaml
        import os
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/data/common_questions.yaml") as file:
            self.common_questions = yaml.load(file, Loader=yaml.FullLoader)

        for file in self.common_questions:
            file = file.replace("_"," ")
            speech = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/voice/" + str(file) + ".mp3"
            speech = speech.replace(" ","_").lower()
            if not os.path.exists(speech):
                print(file)
                language = 'es'
                myobj = gTTS(text=str(file), lang=language, tld=language, slow=False)
                myobj.save(speech)


        # import os
        # archivos = [
        #         "mi nombre es marysol",
        #         "soy un robot",
        #         "No, soy un robot",
        #         "Fui desarrollada en el instituto tecnológico y de energías renovables, dentro del área de robótica y automatización",
        #         "Me dedico a servir de guía para los usuarios que visitan el centro de visitantes del instituto tecnológico y de energías renovables",
        #         "En la actualidad, el iter es un centro de investigación de referencia internacional en energías renovables, ingeniería, telecomunicaciones y medio ambiente",
        #         "Mi nombre es la fusión de dos palabras que tienen que ver con la energía y el medio ambiente, mar, y, sol",
        #         "Si, utilizo diversos sistemas de inteligencia artificial como para la localización, el habla, el reconocimiento de voz, la navegación, la renderización de mi avatar, etcétera",
        #         "Funciono gracias al uso de ROS, que es un marco de trabajo para proyectos de robótica, y una serie de módulos programados mayormente en python y c++",
        #         "Mi desarrollo empezó en el año 2017, actualmente sigo en desarrollo y actualización",
        #         "Dispongo de varios sensores para la percepción, unas ruedas motorizadas para el movimiento, una batería, un ordenador para el procesamiento de datos, así como una pantalla, micrófono y altavoz para interactuar con los usuarios, entre otras cosas",
        #         "Como sensores dispongo de dos cámaras de profundidad, un octosonar, un altavoz omnidireccional, una unidad de medida inercial y unos encoders para las ruedas"
        #        ]
        #
        # for file in archivos:
        #     speech = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/voice/" + str(file) + ".mp3"
        #     speech = speech.replace(" ","_").lower()
        #     if not os.path.exists(speech):
        #         print(file)
        #         language = 'es'
        #         myobj = gTTS(text=str(file), lang=language, tld=language, slow=False)
        #         myobj.save(speech)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    visual_window = VisualWindow()
