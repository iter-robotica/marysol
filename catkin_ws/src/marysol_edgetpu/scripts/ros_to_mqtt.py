#!/usr/bin/env /home/tunel-backup-pc/marysol/catkin_ws/marysol_heavy_data/virtualenv/mqttpython/bin/python2.7

# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import rospkg
import os

#mqtt
import paho.mqtt.client as mqtt
import json

client = mqtt.Client()
client.connect("127.0.0.1", 1883, 60)
client.loop_start()


#############################################################
#############################################################
class Ros2Mqtt():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("ros_to_mqtt")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        rospack = rospkg.RosPack()
        pkg_path = rospack.get_path("marysol_edgetpu")

        #### parameters #######
        self.rate = rospy.get_param("~rate", 2)
        self.input = rospy.get_param('~color_topic', "/d435/color/image_raw")
        self.depth_input  = rospy.get_param('~depth_topic', "/d435/depth/image_rect_raw")
        self.output = pkg_path + rospy.get_param('~output', "/media/in_out.jpg")

        # internal data
        self.bridge = CvBridge()
        rospy.logdebug("Waiting for " + self.input + " to be READY...")
        img = None
        while img is None and not rospy.is_shutdown():
            try:
                img = rospy.wait_for_message(self.input, Image, timeout=5.0)
                rospy.loginfo("Current " + self.input + " READY=>")
            except:
                rospy.loginfo("Current " + self.input + " not ready yet, retrying for getting it")

        rospy.Subscriber(self.input, Image, self.Callback)

    #############################################################
    def Callback(self,msg):
    #############################################################

        try:
            # Convert your ROS Image message to OpenCV2
            cv2_img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError, e:
            rospy.loginfo(e)
        else:
            #Publicar MQTT 1
            temp_msg = cv2_img.tolist()
            final_msg = json.dumps(temp_msg)
            final_msg = final_msg.encode()
            client.publish("imagen/mqttToRos", final_msg)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        ros_to_mqtt = Ros2Mqtt()
        #ros_to_mqtt.spin()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
