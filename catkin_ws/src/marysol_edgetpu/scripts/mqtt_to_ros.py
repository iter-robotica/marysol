#!/usr/bin/env /home/marysol/marysol/catkin_ws/marysol_heavy_data/virtualenv/mqttpython/bin/python2.7

import os

import rospy
import rospkg
from sensor_msgs.msg import Image

import cv2
from marysol_msg.msg import Object, Box
from cv_bridge import CvBridge, CvBridgeError
from PIL import Image as ImagePIL

#mqtt
import paho.mqtt.client as mqtt
import json
import numpy as np

#############################################################
#############################################################
class Mqtt2Ros():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("mqtt_to_ros")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        rospack = rospkg.RosPack()
        pkg_path = rospack.get_path("marysol_edgetpu")

        # parameters
        self.output = rospy.get_param('~output', "/d435/color/image_raw_objects")

        # subscriptions
        self.image_pub = rospy.Publisher(self.output, Image, queue_size=10)

        # internal data
        self.bridge = CvBridge()

        self.client = mqtt.Client()
        self.client.connect("127.0.0.1", 1883, 60)
        self.client.subscribe('imagen/#', 0)
        self.client.loop_start()
        self.client.message_callback_add("imagen/objectToMqtt", self.on_message)

    #############################################################
    def on_message(self, client, userdata, msg):
    #############################################################

        decoded = msg.payload.decode("utf-8", "ignore")
        msg_list = json.loads(decoded)

        temp_array = np.array(msg_list)
        temp_bgr = ImagePIL.fromarray(np.uint8(temp_array), 'RGB')
        b, g, r = temp_bgr.split()
        img = ImagePIL.merge("RGB", (r,g,b))

        try:
            ros_img = self.bridge.cv2_to_imgmsg(img, encoding="bgr8")
        except CvBridgeError, e:
            rospy.loginfo(e)
        else:
            # Publish ros Image
            self.image_pub.publish(ros_img)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        mqtt_to_ros = Mqtt2Ros()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
