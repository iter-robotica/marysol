#!/usr/bin/env python3

import os

import rospy
import rospkg

from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
from PIL import ImageFont, ImageDraw, Image
from marysol_msg.msg import Object, Box

#mqtt
import paho.mqtt.client as mqtt
import json
import numpy as np


#############################################################
#############################################################
class FindObject():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("FindObject")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        rospack = rospkg.RosPack()
        pkg_path = rospack.get_path("marysol_edgetpu")

        #### parameters #######
        self.model = pkg_path + rospy.get_param('~model', "/models/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite")
        self.label = pkg_path + rospy.get_param('~labels', "/labels/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu_es.txt")
        self.threshold = rospy.get_param('~threshold', 0.25)
        self.keep_aspect_ratio = rospy.get_param('~keep_aspect_ratio', "store_true")

        # Initialize engine.
        self.engine = DetectionEngine(self.model)
        self.labels = dataset_utils.read_label_file(self.label) if self.label else None

        # subscriptions
        self.object_pub = rospy.Publisher('/objects', Object, queue_size=10)

        # internal_data
        self.client = mqtt.Client()
        self.client.connect("127.0.0.1", 1883, 60)
        self.client.subscribe('imagen/#', 0)
        self.client.loop_start()
        self.client.message_callback_add("imagen/mqttToRos", self.on_message)

    #############################################################
    def on_message(self, client, userdata, msg):
    #############################################################

        decoded = msg.payload.decode("utf-8", "ignore")
        msg_list = json.loads(decoded)

        temp_array = np.array(msg_list)
        temp_bgr = Image.fromarray(np.uint8(temp_array), 'RGB')
        b, g, r = temp_bgr.split()
        img = Image.merge("RGB", (r,g,b))

        try:
            draw = ImageDraw.Draw(img)

            # Run inference.
            objs = self.engine.detect_with_image(img,
                                            threshold=self.threshold,
                                            keep_aspect_ratio=self.keep_aspect_ratio,
                                            relative_coord=False,
                                            top_k=10)

            # Print and draw detected objects.
            Object_ = Object()
            for obj in objs:
                box = obj.bounding_box.flatten().tolist()
                Object_.label.append(self.labels[obj.label_id])
                Object_.id.append(obj.label_id)
                Object_.score.append(obj.score)
                Box_ = Box()
                Box_.data = box
                #Object_.range = CalcRange(box, self.depth_input)
                Object_.box.append(Box_)
                draw.rectangle(box, outline='red')
                font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeMono.ttf", 28, encoding="unic")
                draw.text((box[0],box[1]), self.labels[obj.label_id] + " " + str(obj.score), fill=(255,0,0,255), font=font, anchor=None, spacing=0, align="left")

            self.object_pub.publish(Object_)

            #PUBLICAR OTRO MQTT 2
            """temp_msg = np.array(img)
            temp_msg = temp_msg.tolist()
            final_msg = json.dumps(temp_msg)
            final_msg = final_msg.encode()
            self.client.publish("imagen/objectToMqtt", final_msg)"""

        except:
            pass

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        find_object = FindObject()
        #find_object.spin()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
