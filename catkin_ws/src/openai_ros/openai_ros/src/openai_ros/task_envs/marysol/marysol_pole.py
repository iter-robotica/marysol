import rospy
import numpy
import time
import math
from gym import spaces
from openai_ros.robot_envs import marysol_env
from gym.envs.registration import register
import PSpincalc as sp

"""
https://answers.ros.org/question/326226/importerror-dynamic-module-does-not-define-module-export-function-pyinit__tf2/

Issue is because tf2_ros was compiled for python2. Detailed discussion here.

To recompile for python3 (melodic):

Install some prerequisites to use Python3 with ROS.

sudo apt update
sudo apt install python3-catkin-pkg-modules python3-rospkg-modules python3-empy
Prepare catkin workspace

mkdir -p ~/catkin_ws/src; cd ~/catkin_ws
catkin_make
source devel/setup.bash
wstool init
wstool set -y src/geometry2 --git https://github.com/ros/geometry2 -v 0.6.5
wstool up
rosdep install --from-paths src --ignore-src -y -r
Finally compile for Python 3

catkin_make --cmake-args \
            -DCMAKE_BUILD_TYPE=Release \
            -DPYTHON_EXECUTABLE=/usr/bin/python3 \
            -DPYTHON_INCLUDE_DIR=/usr/include/python3.6m \
            -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.6m.so
Do not forget to always source your workspace!
"""

# The path is __init__.py of openai_ros, where we import the marysolPoleEnv directly
max_episode_steps_per_episode = 1000 # Can be any Value

register(
        id='marysolPole-v0',
        entry_point='openai_ros.task_envs.marysol.marysol_pole:marysolPoleEnv',
        max_episode_steps=max_episode_steps_per_episode,
    )

class marysolPoleEnv(marysol_env.marysolEnv):
    def __init__(self):
        """
        This Task Env is designed for having the marysol in some kind of pole.
        It will learn how to stay up.
        """

        # Only variable needed to be set here
        number_actions = rospy.get_param('/marysolPole/n_actions')
        self.action_space = spaces.Discrete(number_actions)

        # We set the reward range, which is not compulsory but here we do it.
        self.reward_range = (-numpy.inf, numpy.inf)

        """
        We set the Observation space for the 6 observations
        cube_observations = [
            round(current_disk_roll_vel, 0),
            round(y_distance, 1),
            round(roll, 1),
            round(pitch, 1),
            round(y_linear_speed,1),
            round(yaw, 1),
        ]
        """

        self.roll = self.pitch = self.yaw = 0.0

        # Actions and Observations
        self.dec_obs = rospy.get_param('/marysolPole/number_decimals_precision_obs', 1)
        self.linear_speed = rospy.get_param('/marysolPole/linear_speed')
        self.linear_turn_speed = rospy.get_param('/marysolPole/linear_turn_speed')
        self.angular_speed = rospy.get_param('/marysolPole/angular_speed')
        self.init_linear_speed = rospy.get_param('/marysolPole/init_linear_speed')
        self.init_linear_turn_speed = rospy.get_param('/marysolPole/init_linear_turn_speed')

        self.n_observations = rospy.get_param('/marysolPole/n_observations')
        self.max_pitch_value = rospy.get_param('/marysolPole/max_pitch_value')
        self.max_quaternion_value = rospy.get_param('/marysolPole/max_quaternion_value')

        # Here we will add any init functions prior to starting the MyRobotEnv
        super(marysolPoleEnv, self).__init__()

        # We create two arrays based on the binary values that will be assigned
        # In the discretization method.

        high = numpy.full((self.n_observations), self.max_quaternion_value)
        low = numpy.full((self.n_observations), self.max_quaternion_value)

        # We only use two integers
        self.observation_space = spaces.Box(low, high)


        rospy.logdebug("ACTION SPACES TYPE===>"+str(self.action_space))
        print("ACTION SPACES TYPE===>"+str(self.action_space))
        rospy.logdebug("OBSERVATION SPACES TYPE===>"+str(self.observation_space))
        print("OBSERVATION SPACES TYPE===>"+str(self.observation_space))

        # Rewards
        self.forwards_reward = rospy.get_param('/marysolPole/forwards_reward')
        self.no_action_reward = rospy.get_param('/marysolPole/no_action_reward')
        self.backwards_reward = rospy.get_param('/marysolPole/backwards_reward')
        self.end_episode_points = rospy.get_param('/marysolPole/end_episode_points')

        self.cumulated_steps = 0.0

    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """
        self.move_base( self.init_linear_speed,
                        self.init_linear_turn_speed,
                        epsilon=0.05,
                        update_rate=10,
                        min_laser_distance=-1)

        return True


    def _init_env_variables(self):
        """
        Inits variables needed to be initialised each time we reset at the start
        of an episode.
        :return:
        """
        # For Info Purposes
        self.cumulated_reward = 0.0
        # Set to false Done, because its calculated asyncronously
        self._episode_done = False

        # We wait a small ammount of time to start everything because in very fast resets, imu scan values are sluggish
        # and sometimes still have values from the prior position that triguered the done.
        time.sleep(1.0)

        # TODO: Add reset of published filtered imu readings
        imu = self.get_imu()


    def _set_action(self, action):
        """
        This set action will Set the linear and angular speed of the marysol
        based on the action number given.
        :param action: The action integer that set s what movement to do next.
        """

        rospy.logdebug("Start Set Action ==>"+str(action))
        # We convert the actions to speed movements to send to the parent class CubeSingleDiskEnv
        if action == 0: #FORWARD
            linear_speed = self.linear_speed
            angular_speed = 0.0
            self.last_action = "FORWARDS"
        elif action == 1: #NONE
            linear_speed = 0.0
            angular_speed = 0.0
            self.last_action = "NONE"
        elif action == 2: #BACKWARDS
            linear_speed = - self.linear_speed
            angular_speed = 0.0
            self.last_action = "BACKWARDS"

        elif action == 3: #FORWARD
            linear_speed = self.linear_speed / 2
            angular_speed = 0.0
            self.last_action = "FORWARDS"
        elif action == 4: #BACKWARDS
            linear_speed = - self.linear_speed / 2
            angular_speed = 0.0
            self.last_action = "BACKWARDS"

        elif action == 5: #FORWARD
            linear_speed = self.linear_speed / 4
            angular_speed = 0.0
            self.last_action = "FORWARDS"
        elif action == 6: #BACKWARDS
            linear_speed = - self.linear_speed / 4
            angular_speed = 0.0
            self.last_action = "BACKWARDS"


        # We tell marysol the linear and angular speed to set to execute
        self.move_base( linear_speed,
                        angular_speed,
                        epsilon=0.05,
                        update_rate=10,
                        min_laser_distance=-1)

        rospy.logdebug("END Set Action ==>"+str(action)+", NAME="+str(self.last_action))

    def _get_obs(self):
        """
        Here we define what sensor data defines our robots observations
        To know which Variables we have acces to, we need to read the
        marysolEnv API DOCS
        :return:
        """
        rospy.logdebug("Start Get Observation ==>")
        # We get the imu and odom
        imu = self.get_imu()
        odom = self.get_odom()

        rospy.logdebug("BEFORE DISCRET _episode_done==>"+str(self._episode_done))

        discretized_observations = self.discretize_observation( imu,
                                                                odom
                                                                )

        rospy.logdebug("Observations ==> "+str(discretized_observations))
        rospy.logdebug("AFTER DISCRET_episode_done ==> "+str(self._episode_done))
        rospy.logdebug("END Get Observation ==> ")

        return discretized_observations


    def _is_done(self, observations):

        if self._episode_done:
            rospy.logdebug("marysol is Too Close to wall==>"+str(self._episode_done))
        else:
            rospy.logerr("marysol is Ok ==>")

        return self._episode_done

    def _compute_reward(self, observations, done):

        if not done:
            if self.last_action == "FORWARDS":
                reward = self.forwards_reward
            elif self.last_action == "NONE":
                reward = self.no_action_reward
            elif self.last_action == "BACKWARDS":
                reward = self.backwards_reward
            reward = reward / (1 + abs(self.pitch))
            #reward = 1
        else:
            reward = - self.end_episode_points


        rospy.logdebug("reward = " + str(reward))
        self.cumulated_reward += reward
        rospy.logdebug("Cumulated_reward = " + str(self.cumulated_reward))
        self.cumulated_steps += 1
        rospy.logdebug("Cumulated_steps = " + str(self.cumulated_steps))

        return reward


    # Internal TaskEnv Methods

    def discretize_observation(self,imu,odom):
        """
        Getting the pitch value from the quaternion.
        """
        self._episode_done = False

        orientation_list = [imu.orientation.x, imu.orientation.y, imu.orientation.z, imu.orientation.w]
        (self.roll, self.pitch, self.yaw) = sp.Q2EA(orientation_list, EulerOrder="zyx", ignoreAllChk=True)[0]

        if (self.max_pitch_value <= abs(self.pitch)):
            rospy.logerr("done Validation >>> item = abs(" + str(self.pitch)+") > "+str(self.max_pitch_value))
            self._episode_done = True
        else:
            rospy.logwarn("NOT done Validation >>> item = abs(" + str(self.pitch)+") < "+str(self.max_pitch_value))

        observation_space = [odom.twist.twist.linear.x,
                            #odom.twist.twist.linear.y,
                            #odom.twist.twist.linear.z,
                            #odom.twist.twist.angular.x,
                            #odom.twist.twist.angular.y,
                            odom.twist.twist.angular.z,
                            imu.orientation.x,
                            imu.orientation.y,
                            imu.orientation.z,
                            imu.orientation.w
                            ]
        #observation_space.append(odom.twist.twist.linear.x)
        #observation_space.append(odom.twist.twist.linear.y)
        #observation_space.append(odom.twist.twist.linear.z)
        #observation_space.append(odom.twist.twist.angular.x)
        #observation_space.append(odom.twist.twist.angular.y)
        #observation_space.append(odom.twist.twist.angular.z)

        #print(orientation_list)
        #print(observation_space)

        return observation_space
