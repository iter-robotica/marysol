import rospy
import numpy
import time
import math
from gym import spaces
from openai_ros.robot_envs import marysol_env_octo
from gym.envs.registration import register
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Header
import PSpincalc as sp
import random
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

# The path is __init__.py of openai_ros, where we import the DiffDriveControllerEnv directly
max_episode_steps_per_episode = 250 # Can be any Value

register(
        id='DiffDriveController-v0',
        entry_point='openai_ros.task_envs.marysol.diff_drive_controller:DiffDriveControllerEnv',
        max_episode_steps=max_episode_steps_per_episode,
    )

class DiffDriveControllerEnv(marysol_env_octo.marysolEnv):
    def __init__(self):
        """
        This Task Env is designed for having the marysol in some kind of DiffDriveController.
        It will learn how to stay up.
        """

        rospy.init_node('DiffDriveController', anonymous=True, log_level=rospy.FATAL)
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        # Only variable needed to be set here
        number_actions = rospy.get_param('/DiffDriveController/n_actions')
        self.action_space = spaces.Discrete(number_actions)

        # We set the reward range, which is not compulsory but here we do it.
        self.reward_range = (-numpy.inf, numpy.inf)

        self.roll = self.pitch = self.yaw = 0.0

        # Actions and Observations
        self.dec_obs = rospy.get_param('/DiffDriveController/number_decimals_precision_obs', 1)
        self.wheel_current_forward = rospy.get_param('/DiffDriveController/wheel_current_forward')
        self.wheel_current_turning = rospy.get_param('/DiffDriveController/wheel_current_turning')
        self.range_linear_x = rospy.get_param('/DiffDriveController/range_linear_x')
        self.range_angular_z = rospy.get_param('/DiffDriveController/range_angular_z')
        self.end_episode_points = rospy.get_param('/DiffDriveController/end_episode_points')


        self.n_observations = rospy.get_param('/DiffDriveController/n_observations')
        self.observation_limits = rospy.get_param('/DiffDriveController/observation_limits')

        # Here we will add any init functions prior to starting the MyRobotEnv
        super(DiffDriveControllerEnv, self).__init__()

        # We create two arrays based on the binary values that will be assigned
        # In the discretization method.

        high = numpy.full((self.n_observations), self.observation_limits)
        low = numpy.full((self.n_observations), self.observation_limits)

        # We only use two integers
        self.observation_space = spaces.Box(low, high)


        rospy.logdebug("ACTION SPACES TYPE===>"+str(self.action_space))
        rospy.logdebug("OBSERVATION SPACES TYPE===>"+str(self.observation_space))

        # Rewards
        self.step_reward = rospy.get_param('/DiffDriveController/step_reward')

        self.cumulated_steps = 0.0
        self.episode_steps = 0.0

        self.left_current = 0.0
        self.right_current = 0.0
        self.cmd_vel = Twist()

    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """

        return True


    def _init_env_variables(self):
        """
        Inits variables needed to be initialised each time we reset at the start
        of an episode.
        :return:
        """
        # For Info Purposes
        self.cumulated_reward = 0.0
        # Set to false Done, because its calculated asyncronously
        self._episode_done = False
        self.left_current = 0.0
        self.right_current = 0.0
        self.episode_steps = 0.0

        # We wait a small ammount of time to start everything because in very fast resets, imu scan values are sluggish
        # and sometimes still have values from the prior position that triguered the done.

        # TODO: Add cmd_vel value
        self.cmd_vel.linear.x = random.uniform(-self.range_linear_x, self.range_linear_x)
        self.cmd_vel.angular.z = random.uniform(-self.range_angular_z, self.range_angular_z)
        odom = Odometry()
        odom.twist.twist.angular.z = 0
        odom.twist.twist.linear.x = 0
        self._odom_pub.publish(odom)

        rospy.logdebug("marysol Base Twist Cmd>>" + str(self.cmd_vel))
        self._check_publishers_connection()
        self._cmd_vel_pub.publish(self.cmd_vel)
        time.sleep(1.0)

    def _set_action(self, action):
        """
        This set action will Set the linear and angular speed of the marysol
        based on the action number given.
        :param action: The action integer that set s what movement to do next.
        """

        rospy.logdebug("Start Set Action ==>"+str(action))
        # We convert the actions to speed movements to send to the parent class CubeSingleDiskEnv
        if action == 0: #FORWARD
            self.left_current = self.left_current + self.wheel_current_forward
            self.right_current = self.right_current + self.wheel_current_forward
            self.last_action = "FORWARDS"
        elif action == 1: #NONE
            self.left_current = self.left_current
            self.right_current = self.right_current
            self.last_action = "NONE"
        elif action == 2: #BACKWARDS
            self.left_current = self.left_current - self.wheel_current_forward
            self.right_current = self.right_current - self.wheel_current_forward
            self.last_action = "BACKWARDS"
        elif action == 3: #TURN_LEFT
            self.left_current = self.left_current - self.wheel_current_turning
            self.right_current = self.right_current + self.wheel_current_turning
            self.last_action = "TURN_LEFT"
        elif action == 4: #TURN_RIGHT
            self.left_current = self.left_current + self.wheel_current_turning
            self.right_current = self.right_current - self.wheel_current_turning
            self.last_action = "TURN_RIGHT"


        # We tell marysol the linear and angular speed to set to execute
        self.diffdrivecontroller( self.left_current,
                                  self.right_current)

        rospy.logdebug("END Set Action ==>"+str(action)+", NAME="+str(self.last_action))

    def _get_obs(self):
        """
        Here we define what sensor data defines our robots observations
        To know which Variables we have acces to, we need to read the
        marysolEnv API DOCS
        :return:
        """
        rospy.logdebug("Start Get Observation ==>")

        self._cmd_vel_pub.publish(self.cmd_vel)

        # We get the imu and odom
        imu = self.get_imu()
        if (self.episode_steps > 10):
            odom = self.get_odom()
        else:
            odom = Odometry()
            odom.twist.twist.angular.z = 0
            odom.twist.twist.linear.x = 0
            self._odom_pub.publish(odom)

        cmd_vel = self.get_cmd_vel()

        stop = self.get_stop()

        rospy.logdebug("BEFORE DISCRET _episode_done==>"+str(self._episode_done))

        discretized_observations = self.discretize_observation( imu,
                                                                odom,
                                                                cmd_vel,
                                                                stop
                                                                )

        rospy.logdebug("Observations ==> "+str(discretized_observations))
        rospy.logdebug("AFTER DISCRET_episode_done ==> "+str(self._episode_done))
        rospy.logdebug("END Get Observation ==> ")

        return discretized_observations

    def _is_done(self, observations):

        if self._episode_done:
            rospy.logdebug("marysol is Too Close to wall==>"+str(self._episode_done))
        else:
            rospy.logerr("marysol is Ok ==>")

        return self._episode_done

    def _compute_reward(self, observations, done):

        reward = 0.0
        relacion = abs(self.range_linear_x / self.range_angular_z)
        if not done:
            reward = self.step_reward / (1 + abs(observations[0] - observations[2]) + relacion * abs(observations[1] - observations[3]))

        else:
            reward = -1 * self.end_episode_points


        rospy.logdebug("reward = " + str(reward))
        self.cumulated_reward += reward
        rospy.logdebug("Cumulated_reward = " + str(self.cumulated_reward))
        self.cumulated_steps += 1
        rospy.logdebug("Cumulated_steps = " + str(self.cumulated_steps))
        self.episode_steps += 1

        return reward


    # Internal TaskEnv Methods

    def discretize_observation(self,imu,odom,cmd_vel,stop):
        """
        Getting the pitch value from the quaternion.
        """
        self._episode_done = False

        """orientation_list = [imu.orientation.x, imu.orientation.y, imu.orientation.z, imu.orientation.w]
        (self.roll, self.pitch, self.yaw) = sp.Q2EA(orientation_list, EulerOrder="zyx", ignoreAllChk=True)[0]"""

        if stop == True:
            rospy.logerr("done Validation >>> item = stop(" + str(stop) + ") > ")
            self._episode_done = True
        else:
            rospy.logwarn("NOT done Validation")

        observation_space = [odom.twist.twist.linear.x,
                            odom.twist.twist.angular.z,
                            cmd_vel.linear.x,
                            cmd_vel.angular.z,
                            imu.orientation.x,
                            imu.orientation.y,
                            imu.orientation.z,
                            imu.orientation.w,
                            imu.linear_acceleration.x
                            ]

        return observation_space
