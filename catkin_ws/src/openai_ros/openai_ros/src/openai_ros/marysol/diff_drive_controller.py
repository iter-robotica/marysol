import numpy
import rospy
import time
from openai_ros import robot_gazebo_env
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Image
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import Imu
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import Bool
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist


class marysolEnv(robot_gazebo_env.RobotGazeboEnv):
    """Superclass for all CubeSingleDisk environments.
    """

    def __init__(self):
        """
        Initializes a new marysolEnv environment.
        marysol doesnt use controller_manager, therefore we wont reset the
        controllers in the standard fashion. For the moment we wont reset them.

        To check any topic we need to have the simulations running, we need to do two things:
        1) Unpause the simulation: without that th stream of data doesnt flow. This is for simulations
        that are pause for whatever the reason
        2) If the simulation was running already for some reason, we need to reset the controlers.
        This has to do with the fact that some plugins with tf, dont understand the reset of the simulation
        and need to be reseted to work properly.

        The Sensors: The sensors accesible are the ones considered usefull for AI learning.

        Sensor Topic List:
        * /odom : Odometry readings of the Base of the Robot
        * /kobuki/laser/scan: Laser Readings

        Actuators Topic List: /cmd_vel,

        Args:
        """
        rospy.logdebug("Start marysolEnv INIT...")
        # Variables that we give through the constructor.
        # None in this case

        rospy.init_node('DiffDriveController', anonymous=True, log_level=rospy.FATAL)
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        # Internal Vars
        # Doesnt have any accesibles
        self.controllers_list = []

        # It doesnt use namespace
        self.robot_name_space = ""

        # We launch the init function of the Parent Class robot_gazebo_env.RobotGazeboEnv
        super(marysolEnv, self).__init__(controllers_list=self.controllers_list,
                                            robot_name_space=self.robot_name_space,
                                            reset_controls=False,
                                            start_init_physics_parameters=False,
                                            reset_world_or_sim="WORLD")

        # Only variable needed to be set here
        number_actions = rospy.get_param('/DiffDriveController/n_actions')
        self.action_space = spaces.Discrete(number_actions)

        # We set the reward range, which is not compulsory but here we do it.
        self.reward_range = (-numpy.inf, numpy.inf)

        self.roll = self.pitch = self.yaw = 0.0

        # Actions and Observations
        self.dec_obs = rospy.get_param('/DiffDriveController/number_decimals_precision_obs', 1)
        self.wheel_current_forward = rospy.get_param('/DiffDriveController/wheel_current_forward')
        self.wheel_current_turning = rospy.get_param('/DiffDriveController/wheel_current_turning')
        self.range_linear_x = rospy.get_param('/DiffDriveController/range_linear_x')
        self.range_angular_z = rospy.get_param('/DiffDriveController/range_angular_z')
        self.end_episode_points = rospy.get_param('/DiffDriveController/end_episode_points')


        self.n_observations = rospy.get_param('/DiffDriveController/n_observations')
        self.observation_limits = rospy.get_param('/DiffDriveController/observation_limits')

        # Here we will add any init functions prior to starting the MyRobotEnv
        super(DiffDriveControllerEnv, self).__init__()

        # We create two arrays based on the binary values that will be assigned
        # In the discretization method.

        high = numpy.full((self.n_observations), self.observation_limits)
        low = numpy.full((self.n_observations), self.observation_limits)

        # We only use two integers
        self.observation_space = spaces.Box(low, high)


        rospy.logdebug("ACTION SPACES TYPE===>"+str(self.action_space))
        rospy.logdebug("OBSERVATION SPACES TYPE===>"+str(self.observation_space))

        # Rewards
        self.step_reward = rospy.get_param('/DiffDriveController/step_reward')

        self.cumulated_steps = 0.0
        self.episode_steps = 0.0

        self.left_current = 0.0
        self.right_current = 0.0
        self.cmd_vel = Twist()




        self.gazebo.unpauseSim()
        #self.controllers_object.reset_controllers()
        self._check_all_sensors_ready()

        # We Start all the ROS related Subscribers and publishers
        rospy.Subscriber("/odom", Odometry, self._odom_callback)

        rospy.Subscriber("/imu", Imu, self._imu_callback)

        rospy.Subscriber("/kinect2/scan", LaserScan, self._laser_scan_callback)

        rospy.Subscriber("/cmd_vel", Twist, self._cmd_vel_callback)

        rospy.Subscriber("/stop", Bool, self._stop_callback)

        self._cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

        self._controlCb_pub = rospy.Publisher('/commands/control', Float32MultiArray, queue_size=1)

        self._odom_pub = rospy.Publisher('/odom', Odometry, queue_size=1)

        self._check_publishers_connection()

        self.gazebo.pauseSim()

        rospy.logdebug("Finished marysolEnv INIT...")

    # Methods needed by the RobotGazeboEnv
    # ----------------------------


    def _check_all_systems_ready(self):
        """
        Checks that all the sensors, publishers and other simulation systems are
        operational.
        """
        self._check_all_sensors_ready()
        return True


    # CubeSingleDiskEnv virtual methods
    # ----------------------------

    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_odom_ready()
        self._check_imu_ready()
        #self._check_laser_scan_ready()
        self._check_stop_ready()
        rospy.logdebug("ALL SENSORS READY")

    def _check_odom_ready(self):
        self.odom = None
        rospy.logdebug("Waiting for /odom to be READY...")
        while self.odom is None and not rospy.is_shutdown():
            try:
                self.odom = rospy.wait_for_message("/odom", Odometry, timeout=5.0)
                rospy.logdebug("Current /odom READY=>")

            except:
                rospy.logerr("Current /odom not ready yet, retrying for getting odom")
        return self.odom

    def _check_imu_ready(self):
        self.imu = None
        rospy.logdebug("Waiting for /imu to be READY...")
        while self.imu is None and not rospy.is_shutdown():
            try:
                self.imu = rospy.wait_for_message("/imu", Imu, timeout=5.0)
                rospy.logdebug("Current /imu READY=>")

            except:
                rospy.logerr("Current /imu not ready yet, retrying for getting imu")
        return self.imu


    def _check_laser_scan_ready(self):
        self.laser_scan = None
        rospy.logdebug("Waiting for /kinect2/scan to be READY...")
        while self.laser_scan is None and not rospy.is_shutdown():
            try:
                self.laser_scan = rospy.wait_for_message("/kinect2/scan", LaserScan, timeout=5.0)
                rospy.logdebug("Current /kinect2/scan READY=>")

            except:
                rospy.logerr("Current /kinect2/scan not ready yet, retrying for getting laser_scan")
        return self.laser_scan

    def _check_stop_ready(self):
        self.stop = None
        rospy.logdebug("Waiting for /stop to be READY...")
        while self.stop is None and not rospy.is_shutdown():
            try:
                self.stop = rospy.wait_for_message("/stop", Bool, timeout=5.0)
                rospy.logdebug("Current /stop READY=>")

            except:
                rospy.logerr("Current /stop not ready yet, retrying for getting stop topic")
        return self.stop


    def _odom_callback(self, data):
        self.odom = data

    def _imu_callback(self, data):
        self.imu = data

    def _laser_scan_callback(self, data):
        self.laser_scan = data

    def _cmd_vel_callback(self, data):
        self.cmd_vel = data

    def _stop_callback(self, data):
        self.stop = data


    def _check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz

        while self._cmd_vel_pub.get_num_connections() == 0 and not rospy.is_shutdown():
            rospy.logdebug("No susbribers to _cmd_vel_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_cmd_vel_pub Publisher Connected")

        """while self._controlCb.get_num_connections() == 0 and not rospy.is_shutdown():
            rospy.logdebug("No susbribers to _controlCb_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_controlCb_pub Publisher Connected")"""

        rospy.logdebug("All Publishers READY")

    # Methods that the TrainingEnvironment will need to define here as virtual
    # because they will be used in RobotGazeboEnv GrandParentClass and defined in the
    # TrainingEnvironment.
    # ----------------------------
    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """
        raise NotImplementedError()

    def _init_env_variables(self):
        """Inits variables needed to be initialised each time we reset at the start
        of an episode.
        """
        raise NotImplementedError()

    def _compute_reward(self, observations, done):
        """Calculates the reward to give based on the observations given.
        """
        raise NotImplementedError()

    def _set_action(self, action):
        """Applies the given action to the simulation.
        """
        raise NotImplementedError()

    def _get_obs(self):
        raise NotImplementedError()

    def _is_done(self, observations):
        """Checks if episode done based on observations given.
        """
        raise NotImplementedError()

    # Methods that the TrainingEnvironment will need.
    # ----------------------------
    def move_base(self, linear_speed, angular_speed, epsilon=0.05, update_rate=10, min_laser_distance=-1):
        """
        It will move the base based on the linear and angular speeds given.
        It will wait untill those twists are achived reading from the odometry topic.
        :param linear_speed: Speed in the X axis of the robot base frame
        :param angular_speed: Speed of the angular turning of the robot base frame
        :param epsilon: Acceptable difference between the speed asked and the odometry readings
        :param update_rate: Rate at which we check the odometry.
        :return:
        """
        cmd_vel_value = Twist()
        cmd_vel_value.linear.x = linear_speed
        cmd_vel_value.angular.z = angular_speed
        rospy.logdebug("marysol Base Twist Cmd>>" + str(cmd_vel_value))
        self._check_publishers_connection()
        self._cmd_vel_pub.publish(cmd_vel_value)
        time.sleep(0.2)
        #time.sleep(0.02)

    def diffdrivecontroller(self, left_wheel, right_wheel):
        """
        It will move the base based on the linear and angular speeds given.
        It will wait untill those twists are achived reading from the odometry topic.
        :param left_wheel: Speed in the left_wheel
        :param right_wheel: Speed of the right_wheel
        :return:
        """
        WheelCurrent = Float32MultiArray()
        WheelCurrent.data = []
        WheelCurrent.data.insert(0, left_wheel)
        WheelCurrent.data.insert(1, right_wheel)
        rospy.logdebug("marysol Command >>" + str(WheelCurrent))
        self._check_publishers_connection()
        self._controlCb_pub.publish(WheelCurrent)
        time.sleep(0.1)
        #time.sleep(0.02)

    def get_odom(self):
        return self.odom

    def get_imu(self):
        return self.imu

    def get_laser_scan(self):
        return self.laser_scan

    def get_cmd_vel(self):
        return self.cmd_vel

    def get_stop(self):
        return self.stop

    def reinit_sensors(self):
        """
        This method is for the tasks so that when reseting the episode
        the sensors values are forced to be updated with the real data and

        """
