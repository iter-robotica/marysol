#!/usr/bin/env python

import math
import rospy
import time
import numpy as np
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from std_msgs.msg import Float32MultiArray

#############################################################
#############################################################
class Angle2Path():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("follow_path")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 5)

        # internal data
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0)) #tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.transform = None
        while self.transform == None and not rospy.is_shutdown():
            try:
                self.transform = self.tf_buffer.lookup_transform("footprint", "map", rospy.Time(0), rospy.Duration(1.0))
            except:
                rospy.loginfo("Esperando por transformada")
                time.sleep(5)
        self.NextPath = Float32MultiArray()
        self.NextPath.data = np.zeros(4)
        self.sen_1 = 0
        self.cos_1 = 0
        self.sen_2 = 0
        self.cos_2 = 0
        self.count = 0

        # subscriptions
        self.next_path = rospy.Publisher('/next_path', Float32MultiArray, queue_size=10)
        rospy.Subscriber("/move_base/TrajectoryPlannerROS/global_plan", Path, self.PlanCallback)

    #############################################################
    def PlanCallback(self,msg):
    #############################################################

        # self.count += 1
        # if self.count > :
        #     self.count = 0
        if True:
            # try:
            camino = msg
            h = 0
            pose_stamped = PoseStamped()
            pose_stamped.header.frame_id = "map"
            pose_stamped.header.stamp = rospy.Time.now()
            d_1 = 0.5
            h_1 = 10000
            d_2 = 1.5
            h_2 = 10000
            pos = 0
            for pose in camino.poses:
                if len(camino.poses) > 0:
                    # 0.1 - Este punto lo obtenemos a partir del paht que va desde el robot hasta el objetivo
                    pose_stamped.pose.position.x = pose.pose.position.x
                    pose_stamped.pose.position.y = pose.pose.position.y
                    # 0.2 - Obtenemos la pose de este punto con respecto al footprint
                    transform = self.tf_buffer.lookup_transform("footprint", "map", rospy.Time(0), rospy.Duration(1.0))
                    pose_transformed = tf2_geometry_msgs.do_transform_pose(pose_stamped, transform)
                    print(pose_transformed.pose.position.x)
                    if pose_transformed.pose.position.x > 0:
                        h = (pose_transformed.pose.position.y**(2) + pose_transformed.pose.position.x**(2))**(0.5)
                        if abs(h - d_1) < abs(h_1 - d_1):
                            h_1 = h
                            self.sen_1 = pose_transformed.pose.position.x / h_1
                            self.cos_1 = pose_transformed.pose.position.y / h_1
                        if abs(h - d_2) < abs(h_2 - d_2):
                            h_2 = h
                            self.sen_2 = pose_transformed.pose.position.x / h_2
                            self.cos_2 = pose_transformed.pose.position.y / h_2
                        #     poss = pos
                            if abs(h) < abs(d_2 * 1.1) and abs(h) > abs(d_2 * 0.9):
                                break
                    #
                    # pos += 1
            if h_1 != 10000 and h_2 != 10000:
                self.NextPath.data[0] = self.sen_1
                self.NextPath.data[1] = self.cos_1
                self.NextPath.data[2] = self.sen_2
                self.NextPath.data[3] = self.cos_2
                self.next_path.publish(self.NextPath)
                # print(poss)

                # print("-----------------------------")data: [-0.8197803497314453, -0.5726780891418457, -0.8197803497314453, -0.5726780891418457]
                # if 0 < len(camino.poses):
                #     print(len(camino.poses))
                #     print("------")
                #     print(h_1)
                #     print(self.sen_1)
                #     print(self.cos_1)
                #     print("------")
                #     print(h_2)
                #     print(self.sen_2)
                #     print(self.cos_2)
            # except:
            #     print("Esperando por transformada")

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    while True:
        angle2path = Angle2Path()
        rospy.spin()
