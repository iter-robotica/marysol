#!/usr/bin/env python

import math
import rospy
import tf
import time
import actionlib
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from nav_msgs.msg import Path
from std_msgs.msg import Bool, Int32MultiArray, Int32, Float32
from std_srvs.srv import *
import roslib

#############################################################
#############################################################
class FollowPath():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("follow_path")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 1)
        self.speed_ = rospy.get_param("~speed", 0.35)
        self.speed_max = rospy.get_param("~speed_max", 0.5)
        self.speed_min = rospy.get_param("~speed_min", 0.2)

        # subscriptions
        self.cmd_pub = rospy.Publisher('/follow_path/cmd_vel_filtered', Twist, queue_size=10)
        self.distance_pub = rospy.Publisher('/follow_path/distance', Int32, queue_size=10)
        self.point_reached_pub = rospy.Publisher('/follow_path/point_reached', Bool, queue_size=10)
        self.pose_reached_pub = rospy.Publisher('/follow_path/pose_reached', Bool, queue_size=10)
        rospy.Subscriber("/move_base/TrajectoryPlannerROS/global_plan", Path, self.PlanCallback)
        rospy.Subscriber('/move_base/current_goal', PoseStamped, self.GoalCallback)
        rospy.Subscriber('/octosonar/lower_front', Float32, self.LowerFrontCallback)

        # internal data
        self.speed = self.speed_
        self.distancia_minima = Float32()
        self.goal = MoveBaseGoal()
        self.client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0)) #tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.camino = Path()
        self.goal.target_pose = PoseStamped()
        self.pose_stamped = PoseStamped()
        self.pose_transformed = PoseStamped()
        self.twist = Twist()
        self.point_reached = Bool()
        self.point_reached = True
        self.pose_reached = Bool()
        self.distance = Int32()
        self.yaw = 0.0
        self.lx = 0.0
        self.az = 0.0
        self.len = 0
        self.accuracy = 10
        self.Ticks = 0
        self.IA_giro = 1
        self.IA_frente = 1
        self.accion = 3
        self.accion_previa = -1
        self.punto_proximo = 0.0
        self.h = 0.0
        self.a = 0.0
        self.b = 0.0

        self.twist.linear.x = self.lx
        self.twist.angular.z = self.az
        self.cmd_pub.publish(self.twist)

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################
        if self.Ticks <= 2:
            # Publicamos la velocidad calculada en el PlanCallBack
            follow_path.calculo_speed()
            follow_path.Flujo()
            self.twist.linear.x = self.lx
            self.twist.angular.z = self.az
            self.cmd_pub.publish(self.twist)
            self.point_reached_pub.publish(self.point_reached)
            self.pose_reached_pub.publish(self.pose_reached)
            self.distance_pub.publish(self.distance)

        else:
            #print(" Ticks excedidos ")
            self.lx = 0.0
            self.az = 0.0
            self.twist.linear.x = self.lx
            self.twist.angular.z = self.az
            self.cmd_pub.publish(self.twist)
            self.point_reached_pub.publish(self.point_reached)
            self.pose_reached_pub.publish(self.pose_reached)
            self.distance_pub.publish(self.distance)
            follow_path.Parar()
            if self.point_reached == False:
                print("  *   Camino Inaccesible   * ")
                follow_path.ClearCostMaps()
                follow_path.Girar360()

        print(self.Ticks)
        self.Ticks += 1


    #############################################################
    def calculo_speed(self):
    #############################################################

        # La distancia de los sensores range va desde 0.15 a 2.00 parametros
        #  Establecemos la velocidad de crucero para una distancia no inferior a 0.4 m
        #  1 = x - 0.4; x = 0.6 metros
        self.speed = self.speed_ * (self.distancia_minima.data + 0.6)
        # Establecemos los rangos maximos y minimos
        if self.speed > self.speed_max:
            self.speed = self.speed_max
        elif self.speed < self.speed_min:
            self.speed = self.speed_min
        #print(self.speed)

    #############################################################
    def Flujo(self):
    #############################################################

        # 0 - Calculamos el punto del camino mas cercano al frame del robot
        follow_path.CalculoPuntoMasCercano()

        # 1 - Calcular la transformada de ese punto con respecto a mapa
        if self.point_reached == False:
            follow_path.CalculoTransformada()

            if self.len > self.punto_proximo + 50 and self.a < self.speed * 0.75:
                if self.b < 0.0:
                    # 4 - Si el valor de "a" es menor a un minimo cuando no estamos cerca del objetivo 2
                    follow_path.GirarDerecha()
                else:
                    # 4 - Si el valor de "a" es menor a un minimo menor cuando estamos cerca del objetivo 3
                    follow_path.GirarIzquierda()

            # 4 - Si el valor de "a" es mayor a los limites anteriores 4
            elif self.len >= self.punto_proximo + 20:
                follow_path.AvanceConGiro()

            else:
                self.point_reached = True
                follow_path.ClearCostMaps() # limpiamos el mapa una vez llegado a un punto del camino
                # 5 - Si hemos llegado al sitio buscamos obtener el angulo deseado 5
                if self.len < self.punto_proximo + 20 and 180 * self.yaw / math.pi > + 15:
                    follow_path.GirarIzquierda()

                # 5 - Si hemos llegado al sitio buscamos obtener el angulo deseado 5
                elif self.len < self.punto_proximo + 20 and 180 * self.yaw / math.pi < - 15:
                    follow_path.GirarDerecha()

                # 6 - Una vez alcanzada la posicion deseada paramos el robot 7
                else:
                    follow_path.Parar()
                    self.pose_reached = True


        """print("       p: ", self.punto_proximo)
        print("  h_prox: ", h_proximo)
        print("    hipo: ", self.speed)
        print("       h: ", self.h)
        print(" self.lx: ", self.lx)
        print(" self.az: ", self.az)
        print("       a: ", self.a)
        print("       b: ", self.b)
        print("      ac: ", self.accuracy)
        print("       l: ", self.len)
        print("       y: ", 180 * self.yaw / math.pi)
        print("----------------------------------------------------------------")"""

    #############################################################
    def CalculoPuntoMasCercano(self):

        self.pose_stamped.header.frame_id = "map"
        self.pose_stamped.header.stamp = rospy.Time.now()
        self.punto_proximo = 100000000.0
        contador = 0
        h_proximo = 100000000.0
        for pose in self.camino.poses:
            if contador < len(self.camino.poses):
                # 0.1 - Este punto lo obtenemos a partir del paht que va desde el robot hasta el objetivo
                self.pose_stamped.pose.position.x = self.camino.poses[contador].pose.position.x
                self.pose_stamped.pose.position.y = self.camino.poses[contador].pose.position.y
                # 0.2 - Obtenemos la pose de este punto con respecto al footprint
                transform = self.tf_buffer.lookup_transform("footprint", "map", rospy.Time(0), rospy.Duration(1.0))
                self.pose_transformed = tf2_geometry_msgs.do_transform_pose(self.pose_stamped, transform)
                self.h = (self.pose_transformed.pose.position.y**(2) + self.pose_transformed.pose.position.x**(2))**(0.5)
                if self.h < h_proximo:
                    self.punto_proximo = contador
                    h_proximo = self.h
                contador += 1

    #############################################################
    def CalculoTransformada(self):

        self.len = len(self.camino.poses)

        # Que punto del paht que va desde el robot hasta el target vamos a coger
        if self.len > self.punto_proximo + 20:
            self.accuracy = self.punto_proximo + 20
        else:
            self.accuracy = self.len - 1

        # 1 - Definimos la Pose del punto al que el robot tiene que seguir con respecto a mapa
        self.pose_stamped.header.frame_id = "map"
        self.pose_stamped.header.stamp = rospy.Time.now()
        # 1.1 - Este punto lo obtenemos a partir del paht que va desde el robot hasta el objetivo
        self.pose_stamped.pose.position.x = self.camino.poses[int(self.accuracy)].pose.position.x
        self.pose_stamped.pose.position.y = self.camino.poses[int(self.accuracy)].pose.position.y
        self.pose_stamped.pose.position.z = 0.0
        self.pose_stamped.pose.orientation.x = self.goal.target_pose.pose.orientation.x
        self.pose_stamped.pose.orientation.y = self.goal.target_pose.pose.orientation.y
        self.pose_stamped.pose.orientation.z = self.goal.target_pose.pose.orientation.z
        self.pose_stamped.pose.orientation.w = self.goal.target_pose.pose.orientation.w

        # 2 - Obtenemos la pose de este punto con respecto a footprint
        transform = self.tf_buffer.lookup_transform("footprint", "map", rospy.Time(0), rospy.Duration(1.0))
        self.pose_transformed = tf2_geometry_msgs.do_transform_pose(self.pose_stamped, transform)
        # 2.1 - Icluido el angulo con respecto a footprint
        roll = pitch = 0.0
        orientation_list = [self.pose_transformed.pose.orientation.x, self.pose_transformed.pose.orientation.y, self.pose_transformed.pose.orientation.z, self.pose_transformed.pose.orientation.w]
        (roll, pitch, self.yaw) = euler_from_quaternion (orientation_list)

        # 3 - Asignamos los valores de velocidad angular y velocidad lineal a los catetos del angulo yaw formado por este punto y el eje x
        self.h = (self.pose_transformed.pose.position.y**(2) + self.pose_transformed.pose.position.x**(2))**(0.5)
        relacion = self.speed / self.h
        self.a = self.pose_transformed.pose.position.x * relacion
        self.b = self.pose_transformed.pose.position.y * relacion

        self.distance = self.len

    #############################################################
    def GirarIzquierda(self):

        # 4.1 - Le damos a la velocidad linear el valor de cero para evitar que vaya hacia atras
        self.lx = 0.0
        if self.accion != 0:
            self.accion_previa = self.accion
        self.accion = 0
        self.az = + self.speed
        print("  *   GirarIzquierda   * " + str(self.lx) + " - " + str(self.az))

    #############################################################
    def GirarDerecha(self):

        # 4.1 - Le damos a la velocidad linear el valor de cero para evitar que vaya hacia atras
        self.lx = 0.0
        if self.accion != 1:
            self.accion_previa = self.accion

        self.accion = 1
        self.az = - self.speed
        print("  *   GirarDerecha   * " + str(self.lx) + " - " + str(self.az))

    #############################################################
    def AvanceConGiro(self):
        if self.accion != 2:
            self.accion_previa = self.accion

        self.accion = 2
        # 4.2 - Obtienen el valor de los catetos correspondientes
        self.lx = self.speed * self.a
        self.az = self.speed * self.b
        print("  *   AvanceConGiro   * " + str(self.lx) + " - " + str(self.az))

    #############################################################
    def Parar(self):
        if self.accion != 3:
            self.accion_previa = self.accion

        self.accion = 3

        self.lx = 0.0
        self.az = 0.0
        self.twist.linear.x = self.lx
        self.twist.angular.z = self.az
        self.cmd_pub.publish(self.twist)
        print("  *   Parar   * " + str(self.lx) + " - " + str(self.az))

    #############################################################
    def Girar360(self):
        if self.accion != 4:
            self.accion_previa = self.accion
        self.accion = 4
        print("  *   Girar360   * " + str(self.lx) + " - " + str(self.az))
        yaw_inicial = self.yaw
        paso_positivo = False
        paso_negativo = False
        while paso_positivo == False or paso_negativo == False or 180 * abs(yaw_inicial - self.yaw) / math.pi > 15 :
            #time.sleep(1)
            follow_path.CalculoTransformada()
            follow_path.GirarDerecha()
            #print("Yaw: " + str(self.yaw) + ", Inicial: " + str(yaw_inicial))
            self.twist.linear.x = self.lx
            self.twist.angular.z = self.az
            self.cmd_pub.publish(self.twist)
            self.point_reached_pub.publish(self.point_reached)
            self.pose_reached_pub.publish(self.pose_reached)
            self.distance_pub.publish(self.distance)
            if self.yaw < 0:
                paso_negativo = True
            if self.yaw > 0:
                paso_positivo = True

    #############################################################
    def PlanCallback(self,msg):
    #############################################################

        self.camino = msg
        self.Ticks = 0

    #############################################################
    def LowerFrontCallback(self,msg):
    #############################################################

        self.distancia_minima = msg


    #############################################################
    def GoalCallback(self,msg):
    #############################################################
        # Al publicarse el target lo guardamos para luego publicarlo periodicamente
        self.goal.target_pose.pose.position.x = msg.pose.position.x
        self.goal.target_pose.pose.position.y = msg.pose.position.y
        self.goal.target_pose.pose.position.z = msg.pose.position.z
        self.goal.target_pose.pose.orientation.x = msg.pose.orientation.x
        self.goal.target_pose.pose.orientation.y = msg.pose.orientation.y
        self.goal.target_pose.pose.orientation.z = msg.pose.orientation.z
        self.goal.target_pose.pose.orientation.w = msg.pose.orientation.w
        self.point_reached = False
        self.point_reached_pub.publish(self.point_reached)
        self.pose_reached = False
        self.pose_reached_pub.publish(self.pose_reached)


    #############################################################
    def ClearCostMaps(self):
    #############################################################
        rospy.wait_for_service('/move_base/clear_costmaps')
        try:
            clear_costmap = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
            clear_costmap()
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    while True:
        try:
            follow_path = FollowPath()
            follow_path.ClearCostMaps()
            follow_path.spin()
        except rospy.ROSInterruptException:
            print("  *   Parar   * ")
            self.lx = 0.0
            self.az = 0.0
            self.twist.linear.x = self.lx
            self.twist.angular.z = self.az
            self.cmd_pub.publish(self.twist)
