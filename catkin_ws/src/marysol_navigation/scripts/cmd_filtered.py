#!/usr/bin/env python

import math
import rospy
import tf
import actionlib
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from nav_msgs.msg import Path
from std_msgs.msg import Bool
from std_srvs.srv import *
import roslib

#############################################################
#############################################################
class CmdFiltered():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("cmd_filtered")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 2)
        self.hipotenusa = rospy.get_param("~speed", 0.4)

        # subscriptions
        self.cmd_pub = rospy.Publisher('/cmd_vel_filtered', Twist, queue_size=10)
        rospy.Subscriber("/move_base/TrajectoryPlannerROS/global_plan", Path, self.PlanCallback)
        rospy.Subscriber('/move_base/current_goal', PoseStamped, self.GoalCallback)

        # internal data
        self.goal = MoveBaseGoal()
        self.client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0)) #tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.goal.target_pose = PoseStamped()
        self.pose_stamped = PoseStamped()
        self.pose_transformed = PoseStamped()
        self.twist = Twist()
        self.yaw = 0.0
        self.lx = 0.0
        self.az = 0.0
        self.len = 0
        self.accuracy = 10
        self.PointReached = True
        self.Ticks = 2
        self.divisor = 1.0
        self.estado = 0

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################
        if self.PointReached == False and self.Ticks <= 2:
            # Replicamos el target obtendio en el GoalCallback para obtener el nuevo path
            if self.estado == 4:
                self.goal.target_pose.header.stamp = rospy.Time.now()
                self.goal.target_pose.header.frame_id = "map"
                self.client.send_goal(self.goal)
            # Tambien publicamos la velocidad calculada en el PlanCallBack
            self.twist.linear.x = self.lx
            self.twist.angular.z = self.az
            self.cmd_pub.publish(self.twist)
            self.Ticks += 1
        else:
            print("  *   Point Reached   * ")
            self.lx = 0.0
            self.az = 0.0
            self.twist.linear.x = self.lx
            self.twist.angular.z = self.az
            self.cmd_pub.publish(self.twist)


    #############################################################
    def PlanCallback(self,msg):
    #############################################################
        self.Ticks = 0
        if self.PointReached == False:
            self.len = len(msg.poses)

            # 0 - Que punto del paht que va desde el robot hasta el target vamos a coger
            if self.len > 20:
                self.accuracy = 20
            else:
                self.accuracy = self.len - 1

            # 1 - Definimos la Pose del punto al que el robot tiene que seguir con respecto a mapa
            self.pose_stamped.header.frame_id = "map"
            self.pose_stamped.header.stamp = rospy.Time.now()
            # 1.1 - Este punto lo obtenemos a partir del paht que va desde el robot hasta el objetivo
            self.pose_stamped.pose.position.x = msg.poses[int(self.accuracy)].pose.position.x
            self.pose_stamped.pose.position.y = msg.poses[int(self.accuracy)].pose.position.y
            self.pose_stamped.pose.position.z = 0.0
            self.pose_stamped.pose.orientation.x = self.goal.target_pose.pose.orientation.x
            self.pose_stamped.pose.orientation.y = self.goal.target_pose.pose.orientation.y
            self.pose_stamped.pose.orientation.z = self.goal.target_pose.pose.orientation.z
            self.pose_stamped.pose.orientation.w = self.goal.target_pose.pose.orientation.w

            transform = self.tf_buffer.lookup_transform("footprint", "map", rospy.Time(0), rospy.Duration(1.0))
            # 2 - Obtenemos la pose de este punto con respecto a footprint
            self.pose_transformed = tf2_geometry_msgs.do_transform_pose(self.pose_stamped, transform)
            # 2.1 - Icluido el angulo con respecto a footprint
            roll = pitch = 0.0
            orientation_list = [self.pose_transformed.pose.orientation.x, self.pose_transformed.pose.orientation.y, self.pose_transformed.pose.orientation.z, self.pose_transformed.pose.orientation.w]
            (roll, pitch, self.yaw) = euler_from_quaternion (orientation_list)

            # 3 - Asignamos los valores de velocidad angular y velocidad lineal a los catetos del angulo yaw formado por este punto y el eje x
            h = (self.pose_transformed.pose.position.y**(2) + self.pose_transformed.pose.position.x**(2))**(0.5)
            relacion = self.hipotenusa / h
            a = self.pose_transformed.pose.position.x * relacion
            b = self.pose_transformed.pose.position.y * relacion

            # 4 - Comprobamos si el robot ya a llegado al destino
            if self.len >= 20:
                # 4 - Si el valor de "a" es menor a un minimo cuando no estamos cerca del objetivo
                if a < self.hipotenusa * 0.75 and self.len > 50:
                    self.estado = 2
                    print("  *   segunda   * ", str(self.hipotenusa * 0.75))
                    # 4.1 - Le damos a la velocidad linear el valor de cero para evitar que vaya hacia atras
                    self.lx = 0.0
                    # 4.2 - Si ademas, la componente angular es mayor que cero
                    if b > 0.0:
                        # 4.2.1 Girara hacia la derecha a velocidad constante
                        self.az = + self.hipotenusa * self.divisor
                    else:
                        # 4.2.2 Si no, girara hacia la izquierda a velociad constante
                        self.az = - self.hipotenusa * self.divisor
                # 4 - Si el valor de "a" es menor a un minimo menor cuando estamos cerca del objetivo
                elif a < self.hipotenusa * 0.95 and self.len < 50:
                    self.estado = 3
                    print("  *   tercera   * ", str(self.hipotenusa * 0.95))
                    # 4.1 - Le damos a la velocidad linear el valor de cero para evitar que vaya hacia atras
                    self.lx = 0.0
                    if b > 0.0:
                        # 4.2.1 Girara hacia la derecha a velocidad constante
                        self.az = + self.hipotenusa * self.divisor
                    else:
                        # 4.2.2 Si no, girara hacia la izquierda a velociad constante
                        self.az = - self.hipotenusa * self.divisor
                    #self.az = 3 * b
                # 4 - Si el valor de "a" es mayor a los limites anteriores
                else:
                    self.estado = 4
                    print("  *   cuarta   * ")
                    # 4.2 - Obtienen el valor de los catetos correspondientes
                    self.lx = a
                    self.az = b * self.divisor

            # 5 - Si hemos llegado al sitio buscamos obtener el angulo deseado
            elif 180 * self.yaw / math.pi > + 15:
                # 5.1 - Girando a la derecha
                self.estado = 5
                print("  *   quinta   * ")
                self.az = + self.hipotenusa * self.divisor
                self.lx = 0.0
            elif 180 * self.yaw / math.pi < - 15:
                # 5.2 - Girando a la izquierda
                self.estado = 6
                print("  *   sexta   * ")
                self.az = - self.hipotenusa * self.divisor
                self.lx = 0.0

            # 6 - Una vez alcanzada la posicion deseada paramos el robot
            else:
                estado = 7
                print("  *   septima   * ")
                self.PointReached = True
                self.lx = 0.0
                self.az = 0.0
                self.twist.linear.x = self.lx
                self.twist.angular.z = self.az
                self.cmd_pub.publish(self.twist)

            """print("    hipo: ", self.hipotenusa)
            print("       h: ", h)
            print(" self.lx: ", self.lx)
            print(" self.az: ", self.az)
            print("       a: ", a)
            print("       b: ", b)
            print("      ac: ", self.accuracy)
            print("       l: ", self.len)
            print("       y: ", 180 * self.yaw / math.pi)
            print("----------------------------------------------------------------")"""

    #############################################################
    def GoalCallback(self,msg):
    #############################################################
        # Al publicarse el target lo guardamos para luego publicarlo periodicamente
        self.goal.target_pose.pose.position.x = msg.pose.position.x
        self.goal.target_pose.pose.position.y = msg.pose.position.y
        self.goal.target_pose.pose.position.z = msg.pose.position.z
        self.goal.target_pose.pose.orientation.x = msg.pose.orientation.x
        self.goal.target_pose.pose.orientation.y = msg.pose.orientation.y
        self.goal.target_pose.pose.orientation.z = msg.pose.orientation.z
        self.goal.target_pose.pose.orientation.w = msg.pose.orientation.w
        self.PointReached = False


    #############################################################
    def ClearCostMaps(self):
    #############################################################
        rospy.wait_for_service('/move_base/clear_costmaps')
        try:
            clear_costmap = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
            clear_costmap()
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        cmd_filtered = CmdFiltered()
        cmd_filtered.spin()
    except rospy.ROSInterruptException:
        pass
