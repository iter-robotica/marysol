#!/usr/bin/env python

import math
import rospy
import tf
import actionlib
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from nav_msgs.msg import Path
from nav_msgs.msg import Odometry
from std_msgs.msg import Bool, Int32
from std_srvs.srv import *
import roslib

#############################################################
#############################################################
class PointToPoint():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("point_to_point")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.puntos = rospy.get_param("~poses", 4)
        self.poses_x = rospy.get_param("~x")
        self.poses_y = rospy.get_param("~y")

        # subscriptions
        self.plan_pub = rospy.Publisher('/follow_path/marysol_plan', Path, queue_size=10)
        rospy.Subscriber("/follow_path/point_reached", Bool, self.PointReachedCallback)
        rospy.Subscriber("/follow_path/pose_reached", Bool, self.PoseReachedCallback)
        rospy.Subscriber("/follow_path/distance", Int32, self.DistanceCallback)

        # internal data
        self.point_reached = Bool()
        self.pose_reached = Bool()
        self.distance = Int32()
        self.Path = Path()
        self.Path.header.frame_id = "map"

        for q in range(self.puntos):
            Pose = PoseStamped()
            Pose.pose.position.x = self.poses_x[q]
            Pose.pose.position.y = self.poses_y[q]
            Pose.pose.position.z = 0.0
            Pose.pose.orientation.x = 0.0
            Pose.pose.orientation.y = 0.0
            Pose.pose.orientation.z = 0.0
            Pose.pose.orientation.w = 0.0
            self.Path.poses.append(Pose)
            q += 1

        self.target = 0

        self.goal = MoveBaseGoal()
        self.goal.target_pose = PoseStamped()
        self.goal.target_pose.pose.position.z = 0.0
        self.goal.target_pose.pose.orientation.x = 0.0
        self.goal.target_pose.pose.orientation.y = 0.0
        self.goal.target_pose.pose.orientation.z = 0.0
        self.goal.target_pose.pose.orientation.w = 1.0
        self.client = actionlib.SimpleActionClient('move_base',MoveBaseAction)

        print("Nuevo Objetivo " + str(self.target))
        self.goal.target_pose.header.stamp = rospy.Time.now()
        self.goal.target_pose.header.frame_id = "map"
        self.goal.target_pose.pose.position.x = self.Path.poses[0].pose.position.x
        self.goal.target_pose.pose.position.y = self.Path.poses[0].pose.position.y
        self.client.send_goal(self.goal)

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        #if self.point_reached == False:
        self.client.send_goal(self.goal)
        self.Path.header.stamp = rospy.Time.now()
        self.plan_pub.publish(self.Path)

    #############################################################
    def PointReachedCallback(self,msg):
    #############################################################

        self.point_reached = msg.data
        if self.point_reached == True:
            self.target += 1
            if self.target >= len(self.Path.poses):
                self.target = 0
            print("Nuevo Objetivo " + str(self.target))
            self.goal.target_pose.header.stamp = rospy.Time.now()
            self.goal.target_pose.pose.position.x = self.Path.poses[self.target].pose.position.x
            self.goal.target_pose.pose.position.y = self.Path.poses[self.target].pose.position.y
            self.client.send_goal(self.goal)

    #############################################################
    def PoseReachedCallback(self,msg):
    #############################################################

        self.pose_reached = msg.data

    #############################################################
    def DistanceCallback(self,msg):
    #############################################################

        self.distance = msg.data

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        point_to_point = PointToPoint()
        point_to_point.spin()
    except rospy.ROSInterruptException:
        pass
