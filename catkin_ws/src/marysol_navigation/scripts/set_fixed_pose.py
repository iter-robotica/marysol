#!/usr/bin/env python

import math
import rospy
import tf
import actionlib
import tf2_ros
import tf2_geometry_msgs
import geometry_msgs.msg
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
from std_msgs.msg import Bool
import roslib

#############################################################
#############################################################
class SetFixedPose():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("set_fixed_pose")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.delay = rospy.get_param("~delay", 5)
        self.x = rospy.get_param("~x", 0.0)
        self.y = rospy.get_param("~y", 0.0)
        self.a = rospy.get_param("~a", 0.0)

        # subscriptions
        rospy.Subscriber("/initialpose", PoseWithCovarianceStamped, self.InitialPoseCallback)

        # internal data
        self.fixed_pose = PoseWithCovarianceStamped()
        self.broadcaster = tf2_ros.StaticTransformBroadcaster()
        self.static_transformStamped = geometry_msgs.msg.TransformStamped()
        self.static_transformStamped.header.frame_id = "map"
        self.static_transformStamped.child_frame_id = "odom"
        self.static_transformStamped.transform.translation.x = self.x
        self.static_transformStamped.transform.translation.y = self.y
        self.static_transformStamped.transform.translation.z = 0.0
        q = quaternion_from_euler (0.0,
                                   0.0,
                                   self.a)
        self.static_transformStamped.transform.rotation.x = q[0]
        self.static_transformStamped.transform.rotation.y = q[1]
        self.static_transformStamped.transform.rotation.z = q[2]
        self.static_transformStamped.transform.rotation.w = q[3]

        """self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.static_transformStamped.header.stamp = rospy.Time.now()
        self.broadcaster.sendTransform(self.static_transformStamped)"""

    #############################################################
    def InitialPoseCallback(self,msg):
    #############################################################
        # Al publicarse el target lo guardamos para luego publicarlo periodicamente
        self.static_transformStamped.transform.translation.x = msg.pose.pose.position.x
        self.static_transformStamped.transform.translation.y = msg.pose.pose.position.y
        self.static_transformStamped.transform.translation.z = msg.pose.pose.position.z
        self.static_transformStamped.transform.rotation.x = msg.pose.pose.orientation.x
        self.static_transformStamped.transform.rotation.y = msg.pose.pose.orientation.y
        self.static_transformStamped.transform.rotation.z = msg.pose.pose.orientation.z
        self.static_transformStamped.transform.rotation.w  = msg.pose.pose.orientation.w
        self.static_transformStamped.header.stamp = rospy.Time.now()
        self.broadcaster.sendTransform(self.static_transformStamped)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        set_fixed_pose = SetFixedPose()
        #set_fixed_pose.spin()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
