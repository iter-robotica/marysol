import roslib
import rospy
from std_srvs.srv import *

#############################################################
#############################################################
class ClearCostmaps(object):
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################

        for cls in reversed(self.__class__.mro()):
            if hasattr(cls, 'init'):
                cls.init(self)

    #############################################################
    def init(self):
    #############################################################

        rospy.wait_for_service('/move_base/clear_costmaps')
        try:
          clear_costmap = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
          rospy.loginfo("Created costmap service proxy")
          while not rospy.is_shutdown():
            try:
              rospy.loginfo("clearing costmap")
              clear_costmap()
            except rospy.ServiceException, e:
              rospy.logerr("could not call service: %s", str(e))
        except rospy.ServiceException, e:
          rospy.logerr("could not create service proxy: %s", str(e))
