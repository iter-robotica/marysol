#!/usr/bin/env python3

import rospy
from std_msgs.msg import UInt8MultiArray
import numpy as np
import roslib
import random

#############################################################
#############################################################
class StripLeds():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("test_leds")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 0.1)

        # subscriptions
        self.color_pub = rospy.Publisher('/commands/Leds', UInt8MultiArray, queue_size=10)
        self.color = UInt8MultiArray();
        self.TaskNumber = 0

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)
        random.seed();
        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.color.data = []
        self.color.data.append(random.randint(0, 127))
        self.color.data.append(random.randint(0, 127))
        self.color.data.append(random.randint(0, 127))
        self.color.data.append(self.TaskNumber)
        self.color.data.append(50)
        print(self.color)
        self.color_pub.publish(self.color)
        self.TaskNumber += 1
        if self.TaskNumber >= 14:
            self.TaskNumber = 0

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        strip_leds = StripLeds()
        strip_leds.spin()
    except rospy.ROSInterruptException:
        pass
