#!/usr/bin/env python3

import rospy
from std_msgs.msg import UInt8MultiArray, String, Int16MultiArray, Bool
from actionlib_msgs.msg import GoalStatusArray
import numpy as np
import roslib
import random

#############################################################
#############################################################
class StripLeds():
#############################################################
#############################################################

    """
    This class outputs a leds state according to:
        1º  The robot has crashed - Blinking red
        2º  Charging - wipe green
        3º  Low battery - orange
        4º  Awaiting user voice command - Fireworks
        5º  Command voice accepted - Blinking green
        6º  Command voice without answer - orange
        7º  On the way - RainbowCycle
        8º  Recovery behaviour - TheaterChase
        9º  No task - Elastic Rainbow
    """

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("strip_leds")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 5)
        self.battery_low = rospy.get_param("~battery_low", 12.8)

        # subscriptions
        self.task_pub = rospy.Publisher('/commands/Leds', UInt8MultiArray, queue_size=10)
        rospy.Subscriber("/speech/string_to_speech", String, self.SpeechCallback)
        rospy.Subscriber("/move_base/status", GoalStatusArray, self.GoalStatusCallback)
        rospy.Subscriber("/battery", Int16MultiArray, self.BatteryCallback)
        rospy.Subscriber("/speech/playing", Bool, self.playingCallback)

        # internal_data
        self.goal_status = 0
        self.battery_level = 13.0
        self.speech_out = "None"
        self.on_charge = False
        self.task = UInt8MultiArray()
        self.Task = 0
        self.lastTask = -1
        self.playing = False
        self.ticks_since_speech = 0

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)
        random.seed();
        ###### main loop  ######
        while not rospy.is_shutdown():
            self.ticks_since_speech += 1
            r.sleep()

    #############################################################
    def select_task(self):
    #############################################################

        if self.ticks_since_speech < 10:
            if self.speech_out == "Te escucho":
                self.Task = 4
                self.pub_led_task(0,0,0,8,50)   # Fireworks
            elif self.speech_out == "De acuerdo":
                self.Task = 5
                self.pub_led_task(255,0,0,12,50)   # Blinking green
            elif self.speech_out == "Lo siento, no he podido enternderte":
                self.Task = 6
                self.pub_led_task(117,200,20,1,50)   # Simple orange
            else:
                self.Task = 10
                self.pub_led_task(0,0,0,7,50)   # TheaterChase

        elif self.speech_out != "Te escucho" and self.playing == False:
            if self.goal_status == 4:
                self.Task = 1
                self.pub_led_task(0,255,0,12,50)   # Blinking red
            elif self.on_charge == True:
                self.Task = 2
                self.pub_led_task(255,0,0,2,50)   # Wipe green
            elif self.battery_level <= 12.8:
                self.Task = 3
                self.pub_led_task(117,255,20,1,50)   # Simple orange
            elif self.goal_status == 1:
                self.Task = 7
                self.pub_led_task(0,0,0,7,50)   # TheaterChase
            elif self.goal_status == 3:
                self.Task = 8
                self.pub_led_task(0,0,0,4,50)   # RainbowCycle
            else:
                self.Task = 9
                self.pub_led_task(0,0,0,6,50)   # RainbowCycle

    #############################################################
    def pub_led_task(self, green, red, blue, task, period):
    #############################################################

        if self.Task != self.lastTask:
            self.task.data = []
            self.task.data.append(green)
            self.task.data.append(red)
            self.task.data.append(blue)
            self.task.data.append(task)
            self.task.data.append(period)
            self.task_pub.publish(self.task)
            self.lastTask = self.Task
            print(self.task)

    #############################################################
    def SpeechCallback(self, msg):
    #############################################################
        #rospy.loginfo("MSG: %s " % msg)
        self.speech_out = msg.data
        self.ticks_since_speech = 0
        self.select_task()

    #############################################################
    def GoalStatusCallback(self, msg):
    #############################################################
        #rospy.loginfo("MSG: %s " % msg)
        if len(msg.status_list) > 0:
            self.goal_status = msg.status_list[-1].status
            self.select_task()
        """
        PENDING         = 0   # The goal has yet to be processed by the action server
        ACTIVE          = 1   # The goal is currently being processed by the action server
        PREEMPTED       = 2   # The goal received a cancel request after it started executing
                              #   and has since completed its execution (Terminal State)
        SUCCEEDED       = 3   # The goal was achieved successfully by the action server (Terminal State)
        ABORTED         = 4   # The goal was aborted during execution by the action server due
                              #    to some failure (Terminal State)
        REJECTED        = 5   # The goal was rejected by the action server without being processed,
                              #    because the goal was unattainable or invalid (Terminal State)
        PREEMPTING      = 6   # The goal received a cancel request after it started executing
                              #    and has not yet completed execution
        RECALLING       = 7   # The goal received a cancel request before it started executing,
                              #    but the action server has not yet confirmed that the goal is canceled
        RECALLED        = 8   # The goal received a cancel request before it started executing
                              #    and was successfully cancelled (Terminal State)
        LOST            = 9   # An action client can determine that a goal is LOST. This should not be
                              #    sent over the wire by an action server
        """

    #############################################################
    def BatteryCallback(self, msg):
    #############################################################
        #rospy.loginfo("MSG: %s " % msg)
        #self.battery_level = (msg.data[0] + msg.data[1]) / 2
        self.select_task()
        """
        LiFePo Battery 4x nominal cell (3.2) = 12.8 v
        Set Limit at 12.8 v.
        """

    #############################################################
    def playingCallback(self,msg):
    #############################################################
        self.playing = msg.data

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        strip_leds = StripLeds()
        strip_leds.spin()
    except rospy.ROSInterruptException:
        pass
