#!/usr/bin/env python

import rospy
import roslib
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
import numpy as np
import math

#############################################################
#############################################################
class Cmd2Duty():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("cmd2duty")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.w = rospy.get_param("~base_width", 0.505)
        self.timeout_ticks = rospy.get_param("~timeout_ticks", 10)
        self.rate = rospy.get_param("~rate", 25)

        # subscriptions
        self.pub_cmd_duty = rospy.Publisher('commands/controlDuty', Float32MultiArray, queue_size=10)
        rospy.Subscriber('cmd_vel', Twist, self.twistCallback)

        # internal data
        self.DRL_left = 0.0
        self.DRL_right = 0.0
        self.ticks_since_target = 0
        self.Duty = Float32MultiArray()
        self.left = 0
        self.right = 0
        self.action_space_ratio = 0.1
        self.sumatoria_izq = 0
        self.sumatoria_der = 0

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)
        self.ticks_since_target = self.timeout_ticks

        ###### main loop  ######
        while not rospy.is_shutdown():
            if self.ticks_since_target < self.timeout_ticks:
                self.updateDuty(self.DRL_left, self.DRL_right)
                self.ticks_since_target += 1
            else:
                self.updateDuty(0.0, 0.0)
            r.sleep()

    #############################################################
    def updateDuty(self, left, right):
    #############################################################

        self.Duty.data = []
        self.Duty.data.append(self.left)
        self.Duty.data.append(self.right)
        self.pub_cmd_duty.publish(self.Duty)

        #self.ticks_since_target += 1

    #############################################################
    def twistCallback(self,msg):
    #############################################################
        # rospy.loginfo("-D- twistCallback: %s" % str(msg))
        self.ticks_since_target = 0

        dx = msg.linear.x #np.clip(msg.linear.x, -0.25, 0.25)
        dr = msg.angular.z #np.clip(msg.angular.z, -0.25, 0.25)

        # dx = (l + r) / 2
        # dr = (r - l) / w
        right = dx + dr * self.w / 2
        left = dx - dr * self.w / 2

        """#regresion lineal
        #y=0.5259x+0.0353
        right = 0.5259 * right
        left = 0.5259 * left

        #Regresion cuadratica
        if right > 0:
            right = 0.8659 * math.pow(abs(right), 2) + 0.0941 * right
        else:
            right = - (0.8659 * math.pow(abs(right), 2) + 0.0941 * right)

        if left > 0:
            left = 0.8659 * math.pow(abs(left), 2) + 0.0941 * left
        else:
            left = - (0.8659 * math.pow(abs(left), 2) + 0.0941 * left)"""

        #Regresion cubica
        #y=6.2737x3-3.8767x2+1.1797x+0.0074
        if right > 0:
            right = 6.2737 * math.pow(abs(right), 3) - 3.8767 * math.pow(abs(right), 2) + 1.1797 * abs(right)
        else:
            right = - (6.2737 * math.pow(abs(right), 3) - 3.8767 * math.pow(abs(right), 2) + 1.1797 * abs(right))

        if left > 0:
            left = 6.2737 * math.pow(abs(left), 3) - 3.8767 * math.pow(abs(left), 2) + 1.1797 * abs(left)
        else:
            left = - (6.2737 * math.pow(abs(left), 3) - 3.8767 * math.pow(abs(left), 2) + 1.1797 * abs(left))

        self.right = + right
        self.left = - left

        # rospy.loginfo("publishing: (%d, %d)", left, right)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        cmd2duty = Cmd2Duty()
        cmd2duty.spin()
    except rospy.ROSInterruptException:
        pass
