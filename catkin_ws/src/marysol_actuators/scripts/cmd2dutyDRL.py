#!/usr/bin/env python3

import rospy
import roslib
from std_msgs.msg import Float32MultiArray, Int16MultiArray
from geometry_msgs.msg import Twist
import numpy as np
import math
import os
from os.path import expanduser
import sys

#############################################################
#############################################################
class Cmd2Duty():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################

        rospy.init_node("cmd2dutyDRL")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.w = rospy.get_param("~base_width", 0.505)
        self.timeout_ticks = rospy.get_param("~timeout_ticks", 10)
        self.rate = rospy.get_param("~rate", 25)
        self.world = rospy.get_param("~world", "real")
        self.agent = rospy.get_param("~agent", "trpo")
        self.env = rospy.get_param('~env', "speed_control-v4")
        self.train = rospy.get_param('~train', True)

        # subscriptions
        msg = None
        while msg is None and not rospy.is_shutdown():
            from nav_msgs.msg import Odometry
            try:
                msg = rospy.wait_for_message("odom", Odometry, timeout=5.0)
                rospy.loginfo("Current /odom READY=>")
            except:
                rospy.loginfo("Current /odom not ready yet, retrying for getting it")
        msg = None
        while msg is None and not rospy.is_shutdown():
            from std_msgs.msg import Int16MultiArray
            try:
                msg = rospy.wait_for_message("/state", Int16MultiArray, timeout=5.0)
                rospy.loginfo("Current /state READY=>")
                rospy.Subscriber("/state", Int16MultiArray, self.state_Callback)
            except:
                rospy.loginfo("Current /state not ready yet, retrying for getting it")
        self.pub_cmd_duty = rospy.Publisher('commands/controlDuty', Float32MultiArray, queue_size=10)
        self.pub_cmd_RPM = rospy.Publisher('commands/controlRPM_info', Float32MultiArray, queue_size=10)
        rospy.Subscriber('/speed_control/action_space', Float32MultiArray, self.DRL_Callback)
        rospy.Subscriber('/speed_control_twist/action_space', Float32MultiArray, self.DRL_Callback)
        rospy.Subscriber('/cmd_vel', Twist, self.twistCallback)
        rospy.Subscriber('/state', Int16MultiArray, self.state_Callback)

        # internal data
        self.ticks_since_target = 0
        self.Duty = Float32MultiArray()
        self.DRL_left = 0.0
        self.DRL_right = 0.0
        self.RPM = Float32MultiArray()
        self.right_RPM = 0.0
        self.left_RPM = 0.0
        self.stop = True

        # Train
        if self.train:
            pass

            # ## Hay que cambiar la linea 134 del train.py del stable_baselines por estas dos, o donde tengamos instalado el rl-stable_baselines
            # """from os.path import expanduser
            # with open(expanduser("~") + '/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/hyperparams/{}.yml'.format(args.algo), 'r') as f:"""
            # # asi como crear los hyperparametros para este modelo y agente en la carpeta rl-baselines-zoo/hyperparams/{agente}.yml (modelo dentro del archivo)
            # ## Hay que cambiar la linea 218 del train.py del stable_baselines por esta, o donde tengamos instalado el rl-stable_baselines
            # """log_path = expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/{}/{}/".format(args.log_folder, args.algo)"""
            #
            # # Seguir entrenando un modelo, copiamos el modelo ya entrenado y lo guardamos en la carpeta models del pkg marysol_openai como se indica, al finalizar debemos de llevarlo a la carpeta models del marysol_openai
            # #   python3 expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/train.py --algo trpo --env speed_control-v4 -i expanduser("~") + "/marysol/catkin_ws/src/marysol_openai/models/real/speed_control-v4/trpo/speed_control-v4.zip --save-freq 2000
            # if os.path.exists(expanduser("~") + "/marysol/catkin_ws/src/marysol_openai/models/" + self.world + "/" + self.env + "/" + self.agent + "/" + self.env + ".zip"):
            #     os.system("python3 " +
            #         expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/train.py --algo " + self.agent + " --env " + self.env +
            #         " -i " + expanduser("~") + "/marysol/catkin_ws/src/marysol_openai/models/" + self.world + "/" + self.env + "/" + self.agent + "/" + self.env + ".zip"
            #         " --save-freq 2000 &"
            #         )
            #
            # # Entrtenar por primer vez un modelo, se guardara automaticamente en la carpeta logs de la libreria rl-baselines-zoo, al finalizar debemos de llevarlo a la carpeta models del marysol_openai
            # #   python3 expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/train.py --algo trpo --env speed_control-v4 --save-freq 2000
            # else:
            #     print("Nuevo modelo")
            #     os.system("python3 " +
            #         expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/train.py --algo " + self.agent + " --env " + self.env +
            #         " --save-freq 2000 &"
            #         )

        # Enjoy
        else:
            # Lanzar un modelo ya entrenado y debidamente guardado
            #   python3 enjoy.py --algo trpo --env speed_control-v4 -f expanduser("~") + "/marysol/catkin_ws/src/marysol_openai/models/real/speed_control-v4/ -n 1000000000000 &

            os.system("python3 " +
                expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/rl-baselines-zoo/enjoy.py --algo " +
                self.agent + " --env " + self.env + " -f " + expanduser("~") + "/marysol/catkin_ws/src/marysol_openai/models/" +
                self.world + "/" + self.env + "/ -n 1000000000000 &")


    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)
        self.ticks_since_target = self.timeout_ticks

        ###### main loop  ######
        while not rospy.is_shutdown():
            if self.ticks_since_target < self.timeout_ticks:
                self.updateDuty(self.DRL_left, self.DRL_right)
                self.updateRPM(self.left_RPM, self.right_RPM)
                #self.ticks_since_target += 1
            else:
                self.updateDuty(0.0, 0.0)
                self.updateRPM(0.0, 0.0)
            r.sleep()

    #############################################################
    def updateDuty(self, left, right):
    #############################################################

        self.Duty.data = []
        if not self.stop:
            self.Duty.data.append(left)
            self.Duty.data.append(right)
            self.pub_cmd_duty.publish(self.Duty)

    #############################################################
    def updateRPM(self, left, right):
    #############################################################

        self.RPM.data = []
        self.RPM.data.append(left)
        self.RPM.data.append(right)
        self.pub_cmd_RPM.publish(self.RPM)

    #############################################################
    def twistCallback(self,msg):
    #############################################################
        # rospy.loginfo("-D- twistCallback: %s" % str(msg))
        self.ticks_since_target = 0

        dx = msg.linear.x #np.clip(msg.linear.x, -0.25, 0.25)
        dr = msg.angular.z #np.clip(msg.angular.z, -0.25, 0.25)

        # dx = (l + r) / 2
        # dr = (r - l) / w
        right = dx + dr * self.w / 2
        left = dx - dr * self.w / 2

        #rpm = 60 / (2 * pi * r) m/s
        #rpm = 60 / (2 * 3.1416 * 0.125) m/s
        self.right_RPM = + right * 76.3942
        self.left_RPM = - left * 76.3942

        # rospy.loginfo("publishing: (%d, %d)", left, right)

    #############################################################
    def DRL_Callback(self,msg):
    #############################################################
        # rospy.loginfo("-D- twistCallback: %s" % str(msg))
        self.DRL_left = msg.data[0]
        self.DRL_right = msg.data[1]

        #############################################################
    def state_Callback(self,msg):
    #############################################################
        # rospy.loginfo("-D- twistCallback: %s" % str(msg))
        if len(msg.data) >= 5:
            if msg.data[5] == 0:
                self.stop = False
            elif msg.data[5] == 1:
                self.stop = True
        else:
            self.stop = False

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        cmd2duty = Cmd2Duty()
        cmd2duty.spin()
    except rospy.ROSInterruptException:
        pass
