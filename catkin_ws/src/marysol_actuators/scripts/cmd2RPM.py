#!/usr/bin/env python

import rospy
import roslib
from std_msgs.msg import Int32, Float32MultiArray
from geometry_msgs.msg import Twist
import numpy as np

#############################################################
#############################################################
class Cmd2RPM():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("cmd2RPM")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.w = rospy.get_param("~base_width", 0.505)
        self.timeout_ticks = rospy.get_param("~timeout_ticks", 10)
        self.rate = rospy.get_param("~rate", 25)

        # subscriptions
        self.pub_cmd = rospy.Publisher('commands/controlRPM', Float32MultiArray, queue_size=10)
        #rospy.Subscriber('follow_path/cmd_vel_filtered', Twist, self.twistCallback)
        rospy.Subscriber('cmd_vel', Twist, self.twistCallback)

        # internal data
        self.ticks_since_target = 0
        self.ControlCb = Float32MultiArray()
        self.ControlCb.data = []
        self.left = 0
        self.right = 0
        self.action_space_ratio = 0.1
        self.sumatoria_izq = 0
        self.sumatoria_der = 0

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)
        self.ticks_since_target = self.timeout_ticks

        ###### main loop  ######
        while not rospy.is_shutdown():
            if self.ticks_since_target < self.timeout_ticks:
                cmd2RPM.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.ControlCb.data = []
        self.ControlCb.data.append(self.left)
        self.ControlCb.data.append(self.right)
        self.pub_cmd.publish(self.ControlCb)

        self.ticks_since_target += 1

    #############################################################
    def twistCallback(self,msg):
    #############################################################
        # rospy.loginfo("-D- twistCallback: %s" % str(msg))
        self.ticks_since_target = 0

        dx = msg.linear.x #np.clip(msg.linear.x, -0.25, 0.25)
        dr = msg.angular.z #np.clip(msg.angular.z, -0.25, 0.25)

        # dx = (l + r) / 2
        # dr = (r - l) / w
        right = dx + dr * self.w / 2
        left = dx - dr * self.w / 2

        #rpm = 60 / (2 * pi * r) m/s
        #rpm = 60 / (2 * 3.1416 * 0.125) m/s
        self.right = + right * 76.3942
        self.left = - left * 76.3942

        # rospy.loginfo("publishing: (%d, %d)", left, right)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        cmd2RPM = Cmd2RPM()
        cmd2RPM.spin()
    except rospy.ROSInterruptException:
        pass
