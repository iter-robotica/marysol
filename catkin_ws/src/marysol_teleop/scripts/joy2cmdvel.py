#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from std_msgs.msg import Float32MultiArray, MultiArrayDimension, String
import roslib

#############################################################
#############################################################
class Joy2Cmdvel():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("joy2cmdvel")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.escala_inicial = rospy.get_param("~initial_speed", 0.25)
        self.velocidad_minima = rospy.get_param("~min_joy_speed", 0.05)
        self.velocidad_maxima = rospy.get_param("~max_joy_speed", 1.00)
        self.velocidad_minima_ruedas_ = rospy.get_param("~min_robot_speed", 0.05)
        self.velocidad_maxima_ruedas_ = rospy.get_param("~max_robot_speed", 1.00)

        # subscriptions
        rospy.Subscriber('/joy', Joy, self.Callback)
        self.twist_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        self.debug_line_up_pub = rospy.Publisher('/debug_line_up', String, queue_size=10)
        self.debug_line_down_pub = rospy.Publisher('/debug_line_down', String, queue_size=10)
        self.limits_pub = rospy.Publisher('/commands/robot_limits', Float32MultiArray, queue_size=10)

        # internal data
        self.debug_line_up = String()
        self.debug_line_down = String()
        self.twist = Twist()
        self.limits = Float32MultiArray()
        self.limits.layout.dim.append(MultiArrayDimension())
        self.limits.layout.dim[0].label = "values"
        self.limits.layout.dim[0].size = 2
        self.limits.layout.dim[0].stride = 2
        self.limits.layout.data_offset = 0
        self.limits.data = [0] * 2

        self.escala_lineal = self.escala_inicial
        self.escala_angular = self.escala_inicial
        self.velocidad_minima_ruedas = self.velocidad_minima_ruedas_
        self.velocidad_maxima_ruedas = self.velocidad_maxima_ruedas_
        self.limits.data[0] = self.velocidad_minima_ruedas
        self.limits.data[1] = self.velocidad_maxima_ruedas
        self.limits_pub.publish(self.limits)
        self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.twist_pub.publish(self.twist)

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        if msg.buttons[4] == 1:
            self.velocidad_maxima_ruedas = self.velocidad_maxima_ruedas + 0.01
            self.limits.data[0] = self.velocidad_minima_ruedas
            self.limits.data[1] = self.velocidad_maxima_ruedas
            self.limits_pub.publish(self.limits)

        if msg.buttons[0] == 1:
            self.velocidad_maxima_ruedas = self.velocidad_maxima_ruedas - 0.01
            self.limits.data[0] = self.velocidad_minima_ruedas
            self.limits.data[1] = self.velocidad_maxima_ruedas
            self.limits_pub.publish(self.limits)

        if msg.buttons[3] == 1:
            self.velocidad_minima_ruedas = self.velocidad_minima_ruedas + 0.01
            self.limits.data[0] = self.velocidad_minima_ruedas
            self.limits.data[1] = self.velocidad_maxima_ruedas
            self.limits_pub.publish(self.limits)

        if msg.buttons[1] == 1:
            self.velocidad_minima_ruedas = self.velocidad_minima_ruedas - 0.01
            self.limits.data[0] = self.velocidad_minima_ruedas
            self.limits.data[1] = self.velocidad_maxima_ruedas
            self.limits_pub.publish(self.limits)

        if msg.buttons[10] == 1:
            self.escala_lineal = self.escala_inicial
            self.escala_angular = self.escala_inicial
            self.velocidad_minima_ruedas = self.velocidad_minima_ruedas_
            self.velocidad_maxima_ruedas = self.velocidad_maxima_ruedas_
            self.limits.data[0] = self.velocidad_minima_ruedas
            self.limits.data[1] = self.velocidad_maxima_ruedas
            self.limits_pub.publish(self.limits)

        """if msg.buttons[13] == 1:
            self.escala_lineal = self.escala_inicial
        if msg.buttons[14] == 1:
            self.escala_lineal = self.escala_inicial"""

        if msg.buttons[6] == 1 and self.escala_lineal < 3:
            self.escala_lineal = self.escala_lineal + 0.01
        if msg.buttons[8] == 1 and self.escala_lineal > 0.1:
            self.escala_lineal = self.escala_lineal - 0.01

        if msg.buttons[7] == 1 and self.escala_angular < 3:
            self.escala_angular = self.escala_angular + 0.01
        if msg.buttons[9] == 1 and self.escala_angular > 0.1:
            self.escala_angular = self.escala_angular - 0.01

        if msg.axes[7] == 0:
            self.twist.linear.x = msg.axes[1] * self.escala_lineal
        else:
            self.twist.linear.x = msg.axes[7] * self.escala_lineal

        if msg.axes[6] == 0:
            self.twist.angular.z = msg.axes[2] * self.escala_angular
        else:
            self.twist.angular.z = msg.axes[6] * self.escala_angular

        self.debug_line_up.data = str("{:.2f}".format(self.twist.linear.x)) + "/" + str("{:.2f}".format(self.escala_lineal)) + "/"  + str("{:.2f}".format(self.velocidad_maxima_ruedas))
        self.debug_line_up_pub.publish(self.debug_line_up)
        self.debug_line_down.data = str("{:.2f}".format(self.twist.angular.z)) + "/" + str("{:.2f}".format(self.escala_angular)) + "/" + str("{:.2f}".format(self.velocidad_minima_ruedas))
        self.debug_line_down_pub.publish(self.debug_line_down)



#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        joy2cmdvel = Joy2Cmdvel()
        joy2cmdvel.spin()
    except rospy.ROSInterruptException:
        pass
