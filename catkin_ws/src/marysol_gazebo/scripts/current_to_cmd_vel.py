#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
import roslib

#############################################################
#############################################################
class CurrentToCmdVel():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("current_to_cmd_vel")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.w = rospy.get_param("~base_width", 0.505)

        # subscriptions
        rospy.Subscriber('/commands/control', Float32MultiArray, self.Callback)
        self.twist_pub = rospy.Publisher('/cmd_vel_', Twist, queue_size=10)

        # internal data
        self.twist = Twist()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.twist_pub.publish(self.twist)

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        # dx = (l + r) / 2
        # dr = (r - l) / w

        self.twist.linear.x = (msg.data[0] + msg.data[1]) / 2
        self.twist.linear.y = 0
        self.twist.linear.z = 0
        self.twist.angular.x = 0
        self.twist.angular.y = 0
        self.twist.angular.z = (msg.data[1] - msg.data[0]) / self.w

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        current_to_cmd_vel = CurrentToCmdVel()
        current_to_cmd_vel.spin()
    except rospy.ROSInterruptException:
        pass
