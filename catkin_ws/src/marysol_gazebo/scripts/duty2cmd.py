#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Twist
import roslib

#############################################################
#############################################################
class Duty2Cmd():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("duty2cmd")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 20)
        self.w = rospy.get_param("~base_width", 0.505)

        # subscriptions
        rospy.Subscriber('commands/controlDuty', Float32MultiArray, self.Callback)
        self.twist_pub = rospy.Publisher('/cmd_vel_', Twist, queue_size=10)

        # internal data
        self.twist = Twist()
        self.twist.linear.x = 0.0
        self.twist.linear.y = 0.0
        self.twist.linear.z = 0.0
        self.twist.angular.x = 0.0
        self.twist.angular.y = 0.0
        self.twist.angular.z = 0.0

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        l = -msg.data[0]
        r = msg.data[1]

        # dx = (l + r) / 2
        # dr = (r - l) / w

        self.twist.linear.x = (l + r) / 2
        self.twist.angular.z = (r - l) / self.w

        self.twist_pub.publish(self.twist)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        duty2cmd = Duty2Cmd()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
