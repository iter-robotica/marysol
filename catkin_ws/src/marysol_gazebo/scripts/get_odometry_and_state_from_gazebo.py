#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist, Quaternion
from tf.broadcaster import TransformBroadcaster
from std_msgs.msg import Int32MultiArray, Int16MultiArray
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped
import threading

#############################################################################
class GetOdometryAndStateFromGazebo:
#############################################################################

    #############################################################################
    def __init__(self):
    #############################################################################
        rospy.init_node("get_odometry_and_state_from_gazebo")
        self.nodename = rospy.get_name()
        rospy.loginfo("-I- %s started" % self.nodename)

        self.rate = rospy.get_param('~rate', 10)
        self.PPV = rospy.get_param('~PPV', 102.05)
        self.left_wheel_frame = rospy.get_param('~left_wheel_frame', "wheel_left_link")
        self.right_wheel_frame = rospy.get_param('~right_wheel_frame', "wheel_right_link")
        self.base_link_frame = rospy.get_param('~base_link_frame', "base_link")

        self.state_pub = rospy.Publisher('/state', Int16MultiArray, queue_size=10)
        self.state_array = Int16MultiArray()

        self.odometry_pub = rospy.Publisher('/odometry', Int32MultiArray, queue_size=10)
        self.odometry_array = Int32MultiArray()
        self.odometry_array.data = []
        self.odometry_array.data.append(0)
        self.odometry_array.data.append(0)

        #### parameters #######
        self.angle_left_wheel_old = 0.0
        self.angle_right_wheel_old = 0.0


    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        self.pose_stamped = PoseStamped()
        self.pose_stamped.header.frame_id = self.base_link_frame
        self.pose_stamped.pose.position.x = 0.0
        self.pose_stamped.pose.position.y = 0.0
        self.pose_stamped.pose.position.z = 0.0
        self.pose_stamped.pose.orientation.x = 0
        self.pose_stamped.pose.orientation.y = 0
        self.pose_stamped.pose.orientation.z = 0
        self.pose_stamped.pose.orientation.w = 1

        self.pose_transformed = PoseStamped()

        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0))
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        self.time_old = rospy.Time()

        ###### main loop  ######
        while not rospy.is_shutdown():

            get_odometry_and_state_from_gazebo.get_angle()
            r.sleep()


    #############################################################
    def get_angle(self):
    #############################################################

        # 1 - Define a reference pose in the frame map

        t = rospy.Time.from_sec(self.time_old.to_sec())
        elapsed = t.to_sec()

        elapsed = rospy.Time.now() - self.time_old
        #print(elapsed.to_sec())
        self.time_old = rospy.Time.now()

        if elapsed.to_sec() > 0:

            self.state_array.data = []
            # 1.1 - Give this pose the odom yaw orientation from the map point of view
            self.pose_stamped.header.stamp = rospy.Time.now()


            # Left wheel
            # 2 - Get the pose from the self.frame_id_in point view
            transform = self.tf_buffer.lookup_transform(self.left_wheel_frame, self.pose_stamped.header.frame_id, rospy.Time(0), rospy.Duration(1))
            self.pose_transformed = tf2_geometry_msgs.do_transform_pose(self.pose_stamped, transform)
            # 3 - Get the pitch angle from this self.pose_transformed
            roll = pitch = yaw = 0.0
            orientation_list = [self.pose_transformed.pose.orientation.x, self.pose_transformed.pose.orientation.y, self.pose_transformed.pose.orientation.z, self.pose_transformed.pose.orientation.w]
            (roll, pitch, yaw) = euler_from_quaternion (orientation_list)

            if abs(yaw) > 3.1416 / 2:
                if yaw > 0 and self.angle_left_wheel_old < 0:
                    self.angle_left_wheel_old = 2 * 3.1416 + self.angle_left_wheel_old
                elif yaw < 0 and self.angle_left_wheel_old > 0:
                    self.angle_left_wheel_old = self.angle_left_wheel_old - 2 * 3.1416

            angle_difference_left = yaw - self.angle_left_wheel_old
            self.angle_left_wheel_old = yaw

            angular_velocity_left = angle_difference_left / elapsed.to_sec()

            tachometer_increased_left = angle_difference_left * (self.PPV / (2 * 3.1416))
            self.odometry_array.data[0] += tachometer_increased_left

            rpm_left = angular_velocity_left * 9.5493
            #rpm = 60 / (2 * pi * r) m/s
            #rpm = 60 / (2 * 3.1416 * 0.125) m/s
            mps_left = rpm_left / 76.3942
            self.state_array.data.append(rpm_left)


            # Right Wheel
            transform = self.tf_buffer.lookup_transform(self.right_wheel_frame, self.pose_stamped.header.frame_id, rospy.Time(0), rospy.Duration(1))
            self.pose_transformed = tf2_geometry_msgs.do_transform_pose(self.pose_stamped, transform)
            # 3 - Get the pitch angle from this self.pose_transformed
            roll = pitch = yaw = 0.0
            orientation_list = [self.pose_transformed.pose.orientation.x, self.pose_transformed.pose.orientation.y, self.pose_transformed.pose.orientation.z, self.pose_transformed.pose.orientation.w]
            (roll, pitch, yaw) = euler_from_quaternion (orientation_list)

            yaw = -yaw
            if abs(yaw) > 3.1416 / 2:
                if yaw > 0 and self.angle_right_wheel_old < 0:
                    self.angle_right_wheel_old = 2 * 3.1416 + self.angle_right_wheel_old
                elif yaw < 0 and self.angle_right_wheel_old > 0:
                    self.angle_right_wheel_old = self.angle_right_wheel_old - 2 * 3.1416

            angle_difference_right = yaw - self.angle_right_wheel_old
            self.angle_right_wheel_old = yaw

            angular_velocity_right = angle_difference_right / elapsed.to_sec()

            tachometer_increased_right = angle_difference_right * (self.PPV / (2 * 3.1416))
            self.odometry_array.data[1] += tachometer_increased_right

            rpm_right = angular_velocity_right * 9.5493
            #rpm = 60 / (2 * pi * r) m/s
            #rpm = 60 / (2 * 3.1416 * 0.125) m/s
            #mps_right = rpm_left / 76.3942
            self.state_array.data.append(rpm_right)
            self.state_array.data.append(0)
            self.state_array.data.append(0)
            self.state_array.data.append(0)
            self.state_array.data.append(0) # Parada de emergencia
            self.state_array.data.append(0)
            self.state_array.data.append(0)

            #self.state_array.header.stamp = rospy.Time.now()
            self.state_pub.publish(self.state_array)
            self.odometry_pub.publish(self.odometry_array)

            #except:
                #pass



#############################################################################
#############################################################################
if __name__ == '__main__':
    """ main """
    try:
        get_odometry_and_state_from_gazebo = GetOdometryAndStateFromGazebo()
        get_odometry_and_state_from_gazebo.spin()
    except rospy.ROSInterruptException:
        pass
