#!/usr/bin/env python

import rospy
#import roslib
from math import sin, cos, pi

from geometry_msgs.msg import Twist, Quaternion
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from tf.broadcaster import TransformBroadcaster
from std_msgs.msg import Int32MultiArray, Float32MultiArray, Int16MultiArray
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import threading

#############################################################################
class GetOdomFromGazebo:
#############################################################################

    #############################################################################
    def __init__(self):
    #############################################################################
        rospy.init_node("get_odom_from_gazebo")
        self.nodename = rospy.get_name()
        rospy.loginfo("-I- %s started" % self.nodename)

        #### parameters #######
        self.publishing_rate = rospy.get_param('~publishing_rate', 25)  # the rate at which to publish the transform
        self.updating_rate = rospy.get_param('~updating_rate', 5)  # the rate at which to get the transform
        self.ticks_meter = float(rospy.get_param('~ticks_meter', 130))  # The number of wheel encoder ticks per meter of travel
        self.base_width = float(rospy.get_param('base_width', 0.505)) # The wheel base width in meters

        self.base_frame_id = rospy.get_param('~base_frame_id','footprint') # the name of the base frame of the robot
        self.odom_frame_id = rospy.get_param('~odom_frame_id', 'odom') # the name of the odometry reference frame

        self.encoder_min = rospy.get_param('~encoder_min', -32768)
        self.encoder_max = rospy.get_param('~encoder_max', 32768)
        self.encoder_low_wrap = rospy.get_param('~wheel_low_wrap', (self.encoder_max - self.encoder_min) * 0.3 + self.encoder_min )
        self.encoder_high_wrap = rospy.get_param('~wheel_high_wrap', (self.encoder_max - self.encoder_min) * 0.7 + self.encoder_min )

        # subscriptions
        rospy.Subscriber("/odom_", Odometry, self.odometryCallback)
        rospy.Subscriber('/imu', Imu, self.imuCallback)
        self.odomPub = rospy.Publisher("/odom", Odometry, queue_size=10)
        self.odomBroadcaster = TransformBroadcaster()

        # internal data
        self.odom = Odometry()
        self.odom.header.frame_id = self.odom_frame_id
        self.odom.child_frame_id = self.base_frame_id
        self.imu = Imu()
        self.left = 0               # actual values coming back from robot
        self.right = 0
        self.yaw = 0
        self.quaternion = Quaternion()
        self.quaternion.x = 0
        self.quaternion.y = 0
        self.quaternion.z = 0
        self.quaternion.w = 1


    #############################################################################
    def spin(self):
    #############################################################################

        x = threading.Thread(target=get_odom_from_gazebo.publishing_data, args=(self.publishing_rate,))
        y = threading.Thread(target=get_odom_from_gazebo.updating_data, args=(self.updating_rate,))

        x.start()
        y.start()

    #############################################################################
    def publishing_data(self, rate=25):
    #############################################################################
        r = rospy.Rate(rate)

        ###### main loop  ######
        while not rospy.is_shutdown():

            # publish the odom information
            self.odom.header.stamp = rospy.Time.now()
            self.odomPub.publish(self.odom)

            # publish the transform
            self.odomBroadcaster.sendTransform(
                (self.odom.pose.pose.position.x, self.odom.pose.pose.position.y, self.odom.pose.pose.position.z),
                (self.quaternion.x, self.quaternion.y, self.quaternion.z, self.quaternion.w),
                rospy.Time.now(),
                self.base_frame_id,
                self.odom_frame_id
                )

            r.sleep()

    #############################################################################
    def updating_data(self, rate=5):
    #############################################################################
        r = rospy.Rate(rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            get_odom_from_gazebo.update()

            r.sleep()

    #############################################################################
    def update(self):
    #############################################################################
        pass


    #############################################################################
    def odometryCallback(self, msg):
    #############################################################################
        self.odom.pose.pose.position.x = msg.pose.pose.position.x
        self.odom.pose.pose.position.y = msg.pose.pose.position.y
        self.odom.pose.pose.position.z = 0.0              # 0
        self.odom.pose.covariance = msg.pose.covariance
        self.odom.twist.twist.linear.x = msg.twist.twist.linear.x
        self.odom.twist.twist.linear.y = 0.0             # 0
        self.odom.twist.twist.linear.z = 0.0             # 0

    #############################################################################
    def imuCallback(self, msg):
    #############################################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        roll = pitch = yaw = 0.0
        orientation_list = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]
        (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
        #q = quaternion_from_euler(roll, pitch, yaw)
        q = quaternion_from_euler(0, 0, yaw) # Ya que el roll y el pitch se cogen del gel_base_link_joint
        self.quaternion.x = q[0]
        self.quaternion.y = q[1]
        self.quaternion.z = q[2]
        self.quaternion.w = q[3]

        self.odom.pose.pose.orientation = self.quaternion

        self.odom.twist.twist.angular.x = 0.0           # 0
        self.odom.twist.twist.angular.y = 0.0           # 0
        self.odom.twist.twist.angular.z = msg.angular_velocity.z


#############################################################################
#############################################################################
if __name__ == '__main__':
    """ main """
    try:
        get_odom_from_gazebo = GetOdomFromGazebo()
        get_odom_from_gazebo.spin()
    except rospy.ROSInterruptException:
        pass
