#!/usr/bin/env python

import rospy
from std_msgs.msg import Int16MultiArray


#############################################################
#############################################################
class FakeBattery():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("fake_battery")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 1)
        initial_level = rospy.get_param("~level", 14.337) * 100

        # subscriptions
        self.bat_pub = rospy.Publisher('/battery', Int16MultiArray, queue_size=10)

        # internal data
        self.battey = Int16MultiArray()
        self.battey.data.append(initial_level)
        self.battey.data.append(initial_level)
        self.level = self.battey.data[0]

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################
        self.level = self.level * 0.9999
        self.battey.data = []
        self.battey.data.append(self.level)
        self.battey.data.append(self.level)
        self.bat_pub.publish(self.battey)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        fake_battery = FakeBattery()
        fake_battery.spin()
    except rospy.ROSInterruptException:
        pass
