#include "ros/ros.h"
#include "sensor_msgs/Range.h"
#include <string>

#define FIELD_OF_VIEW 0.261799
#define MIN_RANGE 0.02
#define MAX_RANGE 4.00

class MsgsPublisher{
private:
  ros::NodeHandle nh_;
  ros::Publisher sonarPub_;
  sensor_msgs::Range sonarMsg_;

public:
  MsgsPublisher(){

    sonarMsg_.radiation_type = sensor_msgs::Range::ULTRASOUND;
    sonarMsg_.header.frame_id = "/sonar_range";
    sonarMsg_.field_of_view = FIELD_OF_VIEW;
    sonarMsg_.min_range = MIN_RANGE;
    sonarMsg_.max_range = MAX_RANGE;

  }

  ~MsgsPublisher(){
  }

  void init(std::string topic){
    sonarPub_ = nh_.advertise<sensor_msgs::Range>(topic, 50);
  }

  void PublishMsgs(float data){
    sonarMsg_.range = data;
    sonarMsg_.header.stamp = ros::Time::now();
    sonarPub_.publish(sonarMsg_);
  }
};
