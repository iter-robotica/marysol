#include "ros/ros.h"
#include "sensor_msgs/Range.h"
#include "std_msgs/Float32MultiArray.h"
#include "msgs_publisher.cpp"

class ArrayToRange{
private:
  ros::NodeHandle nh_;

//Variable para suscribirnos al array que genera el arduino
  ros::Subscriber sensorArray_;

//Variable para publicar los valores que general el arduino y los correspondientes mensajes
  MsgsPublisher sonarAMsg_;
  MsgsPublisher sonarBMsg_;
  MsgsPublisher sonarCMsg_;
  MsgsPublisher sonarDMsg_;
  MsgsPublisher sonarEMsg_;
  MsgsPublisher sonarFMsg_;
  MsgsPublisher sonarGMsg_;
  MsgsPublisher sonarHMsg_;
  MsgsPublisher sonarIMsg_;
  MsgsPublisher sonarJMsg_;
  MsgsPublisher sonarKMsg_;
  MsgsPublisher sonarLMsg_;

public:
  ArrayToRange(){

    //Inicializamos los Publisher
    sonarAMsg_.init("/sensor_ring/sonarA_range");
    sonarBMsg_.init("/sensor_ring/sonarB_range");
    sonarCMsg_.init("/sensor_ring/sonarC_range");
    sonarDMsg_.init("/sensor_ring/sonarD_range");
    sonarEMsg_.init("/sensor_ring/sonarE_range");
    sonarFMsg_.init("/sensor_ring/sonarF_range");
    sonarGMsg_.init("/sensor_ring/sonarG_range");
    sonarHMsg_.init("/sensor_ring/sonarH_range");
    sonarIMsg_.init("/sensor_ring/sonarI_range");
    sonarJMsg_.init("/sensor_ring/sonarJ_range");
    sonarKMsg_.init("/sensor_ring/sonarK_range");
    sonarLMsg_.init("/sensor_ring/sonarL_range");

    sensorArray_ = nh_.subscribe("/sensor_ring/sonars_array_raw", 1, &ArrayToRange::ArrayToRangeCallback, this);

  }

  ~ArrayToRange(){

  }

  void ArrayToRangeCallback(const std_msgs::Float32MultiArray::ConstPtr& arraySub){
    sonarAMsg_.PublishMsgs(arraySub->data[0]/10);
    sonarBMsg_.PublishMsgs(arraySub->data[1]/10);
    sonarCMsg_.PublishMsgs(arraySub->data[2]/10);
    sonarDMsg_.PublishMsgs(arraySub->data[3]/10);
    sonarEMsg_.PublishMsgs(arraySub->data[4]/10);
    sonarFMsg_.PublishMsgs(arraySub->data[5]/10);
    sonarGMsg_.PublishMsgs(arraySub->data[6]/10);
    sonarHMsg_.PublishMsgs(arraySub->data[7]/10);
    sonarIMsg_.PublishMsgs(arraySub->data[8]/10);
    sonarJMsg_.PublishMsgs(arraySub->data[9]/10);
    sonarKMsg_.PublishMsgs(arraySub->data[10]/10);
    sonarLMsg_.PublishMsgs(arraySub->data[11]/10);
  }
};

int main(int argc, char **argv){
  ros::init(argc, argv, "Array_To_Range");
  ArrayToRange atr;
  ros::spin();
  return 0;
}
