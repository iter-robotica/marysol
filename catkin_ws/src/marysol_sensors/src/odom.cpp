#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
#include "message_filters/synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"
#include "message_filters/subscriber.h"

//typedef message_filters::sync_policies::ApproximateTime<std_msgs::Float32MultiArray> MySyncPolicy;

#define PI 3.14159265359
#define RADIO 0.125
#define ENCODER 86
#define WHEELDIST 0.505

class Odometry
{
private:
    ros::NodeHandle nh_;
    //message_filters::Subscriber<std_msgs::Float32MultiArray> odom_ard_;
    
    //PROBANDO
    ros::Subscriber odom_ard_;

    ros::Publisher odom_pub_;
    //message_filters::Synchronizer<MySyncPolicy> sync_;

    ros::Time current_time_;
    ros::Time last_time_;
    tf::TransformBroadcaster odom_broadcaster;
    double x_;
    double y_;
    double th_;
    float last_rwc_;
    float last_lwc_;
    float current_lwc_;
    float current_rwc_;
    double distancePerCount_;

public:
    Odometry()
    //: 
      //odom_ard_(nh_, "/odometry", 1)
      //sync_(MySyncPolicy(10), odom_ard_)
    {
        x_ = 0.0;
        y_ = 0.0;
        th_ = 0.0;
        last_lwc_ = 0;
        last_rwc_ = 0;
        current_rwc_ = 0;
        current_lwc_ = 0;
        distancePerCount_ = (2 * PI * RADIO) / ENCODER; //0,00913253678
        current_time_ = ros::Time::now();
        last_time_ = ros::Time::now();
        odom_pub_ = nh_.advertise<nav_msgs::Odometry>("marysol/odom",50);
        //sync_.registerCallback(boost::bind(&Odometry_prueba::odometry_prueba_callback, this, _1));

        //PROBANDO
        odom_ard_ = nh_.subscribe("/odometry", 1, &Odometry::Odometry_callback, this);

        ROS_INFO("Iniciando");

    }

    ~Odometry()
    {

    }

    void Odometry_callback(const std_msgs::Int32MultiArray::ConstPtr& odom_ard_sub)
    {
    	//ROS_INFO("Entra en el callback");
        
        current_time_ = ros::Time::now();
        current_lwc_ = odom_ard_sub->data[0];
        current_rwc_ = odom_ard_sub->data[1];
        current_rwc_ *= -1;

        //calculamos la velocidad lineal de las ruedas
        double dt = (current_time_ - last_time_).toSec();
        double ld = (current_lwc_ - last_lwc_) * distancePerCount_; //distancia lineal rueda izquierda
        double rd = (current_rwc_ - last_rwc_) * distancePerCount_; //distancia lineal rueda derecha
        double vlw = ld / dt;   //velocidad lineal rueda izquierda
        double vrw = rd / dt;   //velocidad lineal rueda derecha
        

        //double vx = (ld + rd) / 2.;  //desplazamiento total del robot por el eje X
        double vx = (vlw + vrw) / 2.; //velocidad media del robot

        double vy = 0;  //desplazamiento total del robot por el eje y

        //double th = (current_rwc_ - current_lwc_) / 45. * PI;
        //double th = (ld - rd) / WHEELDIST;
        //double th = ((8 * PI * RADIO) / (distancePerCount_ * WHEELDIST)) * ( current_lwc_- current_rwc_);

        //double vth = th / dt;
        double vth = (vrw - vlw) / WHEELDIST;
        //double vth = (rd - ld) / WHEELDIST;

        //Calculamos la odometria a partir de la velocidad
        double delta_x = (vx * cos(th_) - vy * sin(th_)) * dt;
        double delta_y = (vx * sin(th_) + vy * cos(th_)) * dt;
        double delta_th = vth * dt;

        x_ -= delta_x;
        y_ -= delta_y;
        th_ -= delta_th;

        last_lwc_ = current_lwc_;
        last_rwc_ = current_rwc_;
        last_time_ = current_time_;     

        //odometry
        geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th_);

        geometry_msgs::TransformStamped odom_trans;
        odom_trans.header.stamp = current_time_;
        odom_trans.header.frame_id = "odom";
        odom_trans.child_frame_id = "footprint";

        odom_trans.transform.translation.x = x_;
        odom_trans.transform.translation.y = y_;
        odom_trans.transform.translation.z = 0.0;
        odom_trans.transform.rotation = odom_quat;

        odom_broadcaster.sendTransform(odom_trans);

        //publish
        nav_msgs::Odometry odom;
        odom.header.stamp = current_time_;
        odom.header.frame_id = "odom";

        //set the position
        odom.pose.pose.position.x = x_;
        odom.pose.pose.position.y = y_;
        odom.pose.pose.position.z = 0.0;
        odom.pose.pose.orientation = odom_quat;


        //set the velocity
        odom.child_frame_id = "footprint";
        odom.twist.twist.linear.x = vx;
        odom.twist.twist.linear.y = vy;
        odom.twist.twist.angular.z = vth;


        //publish the message
        odom_pub_.publish(odom);

    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Odometry");
    Odometry op;
    ros::spin();
    return 0;
}

