#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/Int16.h>

double pi = 3.1415926535897931;

void stateCallback(const std_msgs::Int16MultiArray::ConstPtr& stateArray)
{
  static tf::TransformBroadcaster br;
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(0.0 ,0.0 ,0.125));
  tf::Quaternion q;
  double angle_pitch = double(stateArray->data[2]) / 100;
  angle_pitch *= M_PI / 180;
  double angle_roll = double(stateArray->data[4]) / 100;
  angle_roll *= M_PI / 180;
  q.setRPY(angle_roll, angle_pitch, 0.0);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(
    transform, ros::Time::now(), "footprint", "base_link"));
}

void odometryCallback(const std_msgs::Int32MultiArray::ConstPtr& odometryArray)
{
  static tf::TransformBroadcaster br1;
  tf::Transform transform1;
  transform1.setOrigin(tf::Vector3(0.0, 0.27, 0.0));
  tf::Quaternion q1;
  double right_wheel_count = - double(odometryArray->data[0]);
  right_wheel_count *= 2 * M_PI / 86;
  q1.setRPY(pi/2, right_wheel_count, 0.0);
  transform1.setRotation(q1);
  br1.sendTransform(tf::StampedTransform(
    transform1, ros::Time::now(), "base_link", "wheel_left_link"));

  static tf::TransformBroadcaster br2;
  tf::Transform transform2;
  transform2.setOrigin(tf::Vector3(0.0, -0.27, 0.0));
  tf::Quaternion q2;
  double left_wheel_count = + double(odometryArray->data[1]);
  left_wheel_count *= 2 * M_PI / 86;
  q2.setRPY(pi/2, left_wheel_count, 0.0);
  transform2.setRotation(q2);
  br2.sendTransform(tf::StampedTransform(
    transform2, ros::Time::now(), "base_link", "wheel_right_link"));
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "get_joint_states");
  ros::NodeHandle nh;
  ros::Subscriber sub_st = nh.subscribe("state", 1, stateCallback);
  ros::Subscriber sub_od = nh.subscribe("odometry", 1, odometryCallback);
  ros::spin();
  return 0;
}
