#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32MultiArray, Bool
from sensor_msgs.msg import Range
from std_msgs.msg import Float32
import numpy as np
import roslib

#############################################################
#############################################################
class GetOctosonar():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("get_octosonar")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######

        # subscriptions
        rospy.Subscriber('/octosonar_data', Int32MultiArray, self.Callback)
        self.pub_A = rospy.Publisher('/octosonar/A', Range, queue_size=10)
        self.pub_B = rospy.Publisher('/octosonar/B', Range, queue_size=10)
        self.pub_C = rospy.Publisher('/octosonar/C', Range, queue_size=10)
        self.pub_D = rospy.Publisher('/octosonar/D', Range, queue_size=10)
        self.pub_E = rospy.Publisher('/octosonar/E', Range, queue_size=10)
        self.pub_F = rospy.Publisher('/octosonar/F', Range, queue_size=10)
        self.pub_G = rospy.Publisher('/octosonar/G', Range, queue_size=10)
        self.pub_H = rospy.Publisher('/octosonar/H', Range, queue_size=10)
        self.pub_I = rospy.Publisher('/octosonar/I', Range, queue_size=10)
        self.pub_J = rospy.Publisher('/octosonar/J', Range, queue_size=10)
        self.pub_K = rospy.Publisher('/octosonar/K', Range, queue_size=10)
        self.pub_L = rospy.Publisher('/octosonar/L', Range, queue_size=10)
        self.pub_lower = rospy.Publisher('/octosonar/lower', Float32, queue_size=10)
        self.pub_lower_front = rospy.Publisher('/octosonar/lower_front', Float32, queue_size=10)

        # internal data
        FIELD_OF_VIEW = 0.261799
        MIN_RANGE = 0.02
        MAX_RANGE = 2.50

        self.rate = 10

        self.menor = Float32()

        self.A = Range()
        self.A.header.frame_id = "Sensor_A_link"
        self.A.radiation_type = 0       # ultrasound 0, infrared 1
        self.A.field_of_view = FIELD_OF_VIEW
        self.A.min_range = MIN_RANGE
        self.A.max_range = MAX_RANGE

        self.B = Range()
        self.B.header.frame_id = "Sensor_B_link"
        self.B.radiation_type = 0       # ultrasound 0, infrared 1
        self.B.field_of_view = FIELD_OF_VIEW
        self.B.min_range = MIN_RANGE
        self.B.max_range = MAX_RANGE

        self.C = Range()
        self.C.header.frame_id = "Sensor_C_link"
        self.C.radiation_type = 0       # ultrasound 0, infrared 1
        self.C.field_of_view = FIELD_OF_VIEW
        self.C.min_range = MIN_RANGE
        self.C.max_range = MAX_RANGE

        self.D = Range()
        self.D.header.frame_id = "Sensor_D_link"
        self.D.radiation_type = 0       # ultrasound 0, infrared 1
        self.D.field_of_view = FIELD_OF_VIEW
        self.D.min_range = MIN_RANGE
        self.D.max_range = MAX_RANGE

        self.E = Range()
        self.E.header.frame_id = "Sensor_E_link"
        self.E.radiation_type = 0       # ultrasound 0, infrared 1
        self.E.field_of_view = FIELD_OF_VIEW
        self.E.min_range = MIN_RANGE
        self.E.max_range = MAX_RANGE

        self.F = Range()
        self.F.header.frame_id = "Sensor_F_link"
        self.F.radiation_type = 0       # ultrasound 0, infrared 1
        self.F.field_of_view = FIELD_OF_VIEW
        self.F.min_range = MIN_RANGE
        self.F.max_range = MAX_RANGE

        self.G = Range()
        self.G.header.frame_id = "Sensor_G_link"
        self.G.radiation_type = 0       # ultrasound 0, infrared 1
        self.G.field_of_view = FIELD_OF_VIEW
        self.G.min_range = MIN_RANGE
        self.G.max_range = MAX_RANGE

        self.H = Range()
        self.H.header.frame_id = "Sensor_H_link"
        self.H.radiation_type = 0       # ultrasound 0, infrared 1
        self.H.field_of_view = FIELD_OF_VIEW
        self.H.min_range = MIN_RANGE
        self.H.max_range = MAX_RANGE

        self.I = Range()
        self.I.header.frame_id = "Sensor_I_link"
        self.I.radiation_type = 0       # ultrasound 0, infrared 1
        self.I.field_of_view = FIELD_OF_VIEW
        self.I.min_range = MIN_RANGE
        self.I.max_range = MAX_RANGE

        self.J = Range()
        self.J.header.frame_id = "Sensor_J_link"
        self.J.radiation_type = 0       # ultrasound 0, infrared 1
        self.J.field_of_view = FIELD_OF_VIEW
        self.J.min_range = MIN_RANGE
        self.J.max_range = MAX_RANGE

        self.K = Range()
        self.K.header.frame_id = "Sensor_K_link"
        self.K.radiation_type = 0       # ultrasound 0, infrared 1
        self.K.field_of_view = FIELD_OF_VIEW
        self.K.min_range = MIN_RANGE
        self.K.max_range = MAX_RANGE

        self.L = Range()
        self.L.header.frame_id = "Sensor_L_link"
        self.L.radiation_type = 0       # ultrasound 0, infrared 1
        self.L.field_of_view = FIELD_OF_VIEW
        self.L.min_range = MIN_RANGE
        self.L.max_range = MAX_RANGE

        self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################
        pass

    #############################################################
    def Callback(self,msg):
    #############################################################

        menor = 1000000000000
        for x in range(len(msg.data)):
            if float(msg.data[x]) / 1000 < menor:
                menor = float(msg.data[x]) / 1000
        self.pub_lower.publish(menor)

        self.A.range = float(msg.data[9]) / 1000
        self.B.range = float(msg.data[10]) / 1000
        self.C.range = float(msg.data[11]) / 1000
        self.D.range = float(msg.data[0]) / 1000
        self.E.range = float(msg.data[1]) / 1000
        self.F.range = float(msg.data[2]) / 1000
        self.G.range = float(msg.data[3]) / 1000
        self.H.range = float(msg.data[4]) / 1000
        self.I.range = float(msg.data[5]) / 1000
        self.J.range = float(msg.data[6]) / 1000
        self.K.range = float(msg.data[7]) / 1000
        self.L.range = float(msg.data[8]) / 1000

        """self.A.range = float(msg.data[0]) / 1000
        self.B.range = float(msg.data[1]) / 1000
        self.C.range = float(msg.data[2]) / 1000
        self.D.range = float(msg.data[3]) / 1000
        self.E.range = float(msg.data[4]) / 1000
        self.F.range = float(msg.data[5]) / 1000
        self.G.range = float(msg.data[6]) / 1000
        self.H.range = float(msg.data[7]) / 1000
        self.I.range = float(msg.data[8]) / 1000
        self.J.range = float(msg.data[9]) / 1000
        self.K.range = float(msg.data[10]) / 1000
        self.L.range = float(msg.data[11]) / 1000"""

        self.A.header.stamp = rospy.Time.now()
        self.B.header.stamp = rospy.Time.now()
        self.C.header.stamp = rospy.Time.now()
        self.D.header.stamp = rospy.Time.now()
        self.E.header.stamp = rospy.Time.now()
        self.F.header.stamp = rospy.Time.now()
        self.G.header.stamp = rospy.Time.now()
        self.H.header.stamp = rospy.Time.now()
        self.I.header.stamp = rospy.Time.now()
        self.J.header.stamp = rospy.Time.now()
        self.K.header.stamp = rospy.Time.now()
        self.L.header.stamp = rospy.Time.now()

        self.pub_A.publish(self.A)
        self.pub_B.publish(self.B)
        self.pub_C.publish(self.C)
        self.pub_D.publish(self.D)
        self.pub_E.publish(self.E)
        self.pub_F.publish(self.F)
        self.pub_G.publish(self.G)
        self.pub_H.publish(self.H)
        self.pub_I.publish(self.I)
        self.pub_J.publish(self.J)
        self.pub_K.publish(self.K)
        self.pub_L.publish(self.L)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_octosonar = GetOctosonar()
        get_octosonar.spin()
    except rospy.ROSInterruptException:
        pass
