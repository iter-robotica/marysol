#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped
from tf import TransformListener

#############################################################
#############################################################
class PoseFromMap():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("pose_from_frame")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        # subscriptions
        rospy.Subscriber('/PoseStamped_msg', PoseStamped, self.Callback)
        self.pose_publish = rospy.Publisher('/PoseStampedTransformed', PoseStamped, queue_size=10)

        # internal data
        self.tf_listener = TransformListener()

    #############################################################
    def Callback(self, msg):
    #############################################################

        pose_stamped_transformed = self.tf_listener.transformPose("/map", msg)
        self.pose_publish.publish(pose_stamped_transformed)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_pose_from_frame = PoseFromMap()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
