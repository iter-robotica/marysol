#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32MultiArray
import roslib

#############################################################
#############################################################
class z():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("get_laserscan_from_sonar")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        self.scan1_pub = rospy.Publisher('~scan1', LaserScan, queue_size=10)
        self.scan2_pub = rospy.Publisher('~scan2', LaserScan, queue_size=10)
        self.scan3_pub = rospy.Publisher('~scan3', LaserScan, queue_size=10)
        self.scan4_pub = rospy.Publisher('~scan4', LaserScan, queue_size=10)

        rospy.Subscriber('/sensor_ring/sonars_array_raw', Float32MultiArray, self.Callback)

        self.rate = rospy.get_param("/get_laserscan_from_sonar/rate", 10)

        laser_frequency = 10

        self.scan1 = LaserScan()
        self.scan1.header.frame_id = 'SensorRing_1_link'
        self.scan1.angle_min = 0
        self.scan1.angle_max = 3.14 / 2
        self.scan1.angle_increment = 3.14 / 4
        self.scan1.time_increment = (1.0 / laser_frequency) / 3
        self.scan1.range_min = 0.0
        self.scan1.range_max = 100.0

        self.scan2 = LaserScan()
        self.scan2.header.frame_id = 'SensorRing_2_link'
        self.scan2.angle_min = 0
        self.scan2.angle_max = 3.14 / 2
        self.scan2.angle_increment = 3.14 / 4
        self.scan2.time_increment = (1.0 / laser_frequency) / 3
        self.scan2.range_min = 0.0
        self.scan2.range_max = 100.0

        self.scan3 = LaserScan()
        self.scan3.header.frame_id = 'SensorRing_3_link'
        self.scan3.angle_min = 0
        self.scan3.angle_max = 3.14 / 2
        self.scan3.angle_increment = 3.14 / 4
        self.scan3.time_increment = (1.0 / laser_frequency) / 3
        self.scan3.range_min = 0.0
        self.scan3.range_max = 100.0

        self.scan4 = LaserScan()
        self.scan4.header.frame_id = 'SensorRing_4_link'
        self.scan4.angle_min = 0
        self.scan4.angle_max = 3.14 / 2
        self.scan4.angle_increment = 3.14 / 4
        self.scan4.time_increment = (1.0 / laser_frequency) / 3
        self.scan4.range_min = 0.0
        self.scan4.range_max = 100.0

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.scan1.header.stamp = rospy.Time.now()
        self.scan1_pub.publish(self.scan1)

        self.scan2.header.stamp = rospy.Time.now()
        self.scan2_pub.publish(self.scan2)

        self.scan3.header.stamp = rospy.Time.now()
        self.scan3_pub.publish(self.scan3)

        self.scan4.header.stamp = rospy.Time.now()
        self.scan4_pub.publish(self.scan4)

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        self.scan1.ranges = []
        self.scan1.intensities = []
        self.scan1.ranges.append(msg.data[0] / 1000)
        self.scan1.intensities.append(1)
        self.scan1.ranges.append(msg.data[1] / 1000)
        self.scan1.intensities.append(1)
        self.scan1.ranges.append(msg.data[2] / 1000)
        self.scan1.intensities.append(1)

        self.scan2.ranges = []
        self.scan2.intensities = []
        self.scan2.ranges.append(msg.data[3] / 1000)
        self.scan2.intensities.append(1)
        self.scan2.ranges.append(msg.data[4] / 1000)
        self.scan2.intensities.append(1)
        self.scan2.ranges.append(msg.data[5] / 1000)
        self.scan2.intensities.append(1)

        self.scan3.ranges = []
        self.scan3.intensities = []
        self.scan3.ranges.append(msg.data[6] / 1000)
        self.scan3.intensities.append(1)
        self.scan3.ranges.append(msg.data[7] / 1000)
        self.scan3.intensities.append(1)
        self.scan3.ranges.append(msg.data[8] / 1000)
        self.scan3.intensities.append(1)

        self.scan4.ranges = []
        self.scan4.intensities = []
        self.scan4.ranges.append(msg.data[9] / 1000)
        self.scan4.intensities.append(1)
        self.scan4.ranges.append(msg.data[10] / 1000)
        self.scan4.intensities.append(1)
        self.scan4.ranges.append(msg.data[11] / 1000)
        self.scan4.intensities.append(1)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_laserscan_from_sonar = GetLaserScanFromSonar()
        get_laserscan_from_sonar.spin()
    except rospy.ROSInterruptException:
        pass
