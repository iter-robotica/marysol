#!/usr/bin/env python

import rospy
from image_geometry import PinholeCameraModel
from sensor_msgs.msg import LaserScan, Image, CameraInfo
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32
import roslib
import struct
import math
import numpy as np
import geometry_msgs.msg
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import tf2_ros
import tf2_geometry_msgs
import cv2
import threading

#############################################################
#############################################################
class Depth2Laser():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("depth2laser")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        self.frame_id_in = rospy.get_param("~frame_id_in", 'kinect2_link')
        self.frame_id_out_1 = rospy.get_param("~frame_id_out_1", 'scan_1_link')
        self.frame_id_out_2 = rospy.get_param("~frame_id_out_2", 'scan_2_link')
        self.input_depth = rospy.get_param("~depth_input", "/kinect2/hd/image_depth_rect")
        self.input_info = rospy.get_param("~image_info", "/kinect2/hd/camera_info")
        self.output_scan_1 = rospy.get_param("~output_scan_1", "/scan_1")
        self.output_scan_2 = rospy.get_param("~output_scan_2", "/scan_2")
        self.angle_offset_1 = rospy.get_param("~angle_offset_1", 0.0)
        self.angle_offset_2 = rospy.get_param("~angle_offset_2", -0.15)
        self.angle_sweep = rospy.get_param("~angle_sweep", 0.15)
        self.angle_speed = rospy.get_param("~angle_speed", 0.15)
        self.scan_time_increment = rospy.get_param("~time_increment", 0.0)
        self.scan_scan_time = rospy.get_param("~scan_time", 0.033)
        self.scan_range_min = rospy.get_param("~min", 0.45)
        self.scan_range_max = rospy.get_param("~max", 10.0)
        self.odom_topic = rospy.get_param("~odom_topic", "/odom")

        self.pitch = 0.0
        self.yaw = 0.0
        self.odom = Odometry()
        self.angle_swep = 0.0

        self.calc_static_variables()

        self.scan_1 = LaserScan()
        self.scan_1.header.frame_id = self.frame_id_out_1
        self.scan_1.time_increment = self.scan_time_increment
        self.scan_1.scan_time = self.scan_scan_time
        self.scan_1.range_min = self.scan_range_min
        self.scan_1.range_max = self.scan_range_max
        self.scan_1.angle_max = self.h_angle_max
        self.scan_1.angle_min = self.h_angle_min
        self.scan_1.angle_increment = self.h_angle_increment
        self.processing_scan_1 = False

        self.scan_2 = LaserScan()
        self.scan_2.header.frame_id = self.frame_id_out_2
        self.scan_2.time_increment = self.scan_time_increment
        self.scan_2.scan_time = self.scan_scan_time
        self.scan_2.range_min = self.scan_range_min
        self.scan_2.range_max = self.scan_range_max
        self.scan_2.angle_max = self.h_angle_max
        self.scan_2.angle_min = self.h_angle_min
        self.scan_2.angle_increment = self.h_angle_increment
        self.processing_scan_2 = False

        rospy.Subscriber(self.input_depth, Image, self.CallbackImage)
        rospy.Subscriber(self.odom_topic, Odometry, self.CallbackOdom)
        self.scan_pub_1 = rospy.Publisher(self.output_scan_1, LaserScan, queue_size=10)
        self.scan_pub_2 = rospy.Publisher(self.output_scan_2, LaserScan, queue_size=10)
        self.pitch_pub = rospy.Publisher("/pitch", Float32, queue_size=10)

    #############################################################
    def spin(self):
    #############################################################

        x = threading.Thread(target=depth2laser.pitch_correction, args=(20,))
        y = threading.Thread(target=depth2laser.calc_swep_angle, args=(10,))
        z = threading.Thread(target=depth2laser.publish_scans, args=(6,))

        x.start()
        y.start()
        z.start()


    #############################################################
    def publish_scans(self, rate=10):
    #############################################################

        r = rospy.Rate(rate)

        ###### main loop  ######
        while not rospy.is_shutdown():

            #print("publish_scans")

            self.scan_1.header.stamp = rospy.Time.now()
            self.scan_pub_1.publish(self.scan_1)

            self.scan_2.header.stamp = rospy.Time.now()
            self.scan_pub_2.publish(self.scan_2)

            r.sleep()

    #############################################################
    def calc_swep_angle(self, rate=10):
    #############################################################

        r = rospy.Rate(rate)

        ###### main loop  ######
        while not rospy.is_shutdown():

            #print("calc_swep_angle")

            self.angle_swep += self.angle_speed / rate
            if self.angle_swep >= self.angle_sweep:
                self.angle_swep = -self.angle_sweep

            r.sleep()


    #############################################################
    def pitch_correction(self, rate=10):
    #############################################################

        r = rospy.Rate(rate)

        # 1 - Define a reference pose in the frame map
        pose_stamped = PoseStamped()
        pose_stamped.header.frame_id = "odom"
        q = None

        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0)) # is there any way to make it faster??
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        while not rospy.is_shutdown():
            try:
                roll = pitch = 0.0
                orientation_list = [self.odom.pose.pose.orientation.x, self.odom.pose.pose.orientation.y, self.odom.pose.pose.orientation.z, self.odom.pose.pose.orientation.w]
                (roll, pitch, self.yaw) = euler_from_quaternion (orientation_list)
                q = quaternion_from_euler (0.0, 0.0, self.yaw)

                # 1.1 - Give this pose the odom yaw orientation from the map point of view
                pose_stamped.header.stamp = rospy.Time.now()
                pose_stamped.pose.position.x = 0.0
                pose_stamped.pose.position.y = 0.0
                pose_stamped.pose.position.z = 0.0
                pose_stamped.pose.orientation.x = q[0]
                pose_stamped.pose.orientation.y = q[1]
                pose_stamped.pose.orientation.z = q[2]
                pose_stamped.pose.orientation.w = q[3]

                # 2 - Get the pose from the self.frame_id_in point view
                pose_transformed = PoseStamped()
                transform = self.tf_buffer.lookup_transform(self.frame_id_in, pose_stamped.header.frame_id, rospy.Time(0), rospy.Duration(1))
                pose_transformed = tf2_geometry_msgs.do_transform_pose(pose_stamped, transform)
                # 3 - Get the pitch angle from this pose_transformed
                roll = yaw = 0.0
                orientation_list = [pose_transformed.pose.orientation.x, pose_transformed.pose.orientation.y, pose_transformed.pose.orientation.z, pose_transformed.pose.orientation.w]
                (roll, self.pitch, yaw) = euler_from_quaternion (orientation_list)

            except:
                pass

            #self.pitch_pub.publish(self.pitch)

            #print(str(self.pitch) + " / " + str(self.yaw))

            r.sleep()


    #############################################################
    def CallbackImage(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        if self.pitch != None:

            #depth2laser.depth2laser(msg,self.angle_offset_1,0.0,self.frame_id_out_1,self.scan_1)
            #depth2laser.depth2laser(msg,self.angle_offset_2,self.angle_swep,self.frame_id_out_2,self.scan_2)

            if self.processing_scan_1 == False:
                #print("**Procesamos Scan1**")
                self.processing_scan_1 = True
                ls1 = threading.Thread(target=depth2laser.depth2laser, args=(msg,self.angle_offset_1,0.0,self.frame_id_out_1,self.scan_1))
                ls1.start()
                #self.scan_1.header.stamp = rospy.Time.now()
                #self.scan_pub_1.publish(self.scan_1)
                self.processing_scan_1 = False
            else:
                print("**Descartamos Scan1**")
                pass

            if self.processing_scan_2 == False:
                #print("**Procesamos Scan2**")
                self.processing_scan_2 = True
                #ls2 = threading.Thread(target=depth2laser.depth2laser, args=(msg,self.angle_offset_2,self.angle_swep,self.frame_id_out_2,self.scan_2))
                #ls2.start()
                #self.scan_2.header.stamp = rospy.Time.now()

                #--------------------Temporal para el entrenamiento de la velocidad
                self.scan_2.ranges = [float('Inf')] * msg.width
                self.scan_2.intensities = []
                #------------------------------------------------------------------

                #self.scan_pub_2.publish(self.scan_2)
                self.processing_scan_2 = False
            else:
                print("**Descartamos Scan2**")
                pass


    #############################################################
    def depth2laser(self,msg,offset,swep,frame_id_out,scan):
    #############################################################

            angle = depth2laser.calc_angle(offset, swep)
            depth2laser.calc_transform(angle, frame_id_out)
            line = depth2laser.calc_line(angle, msg.height)
            depth2laser.fill_ranges(line, msg, scan)
            #depth2laser.filter_scan(scan)

    #############################################################
    def calc_angle(self, offset, swep):
    #############################################################

        angle = self.pitch + offset + swep

        if angle < self.v_angle_min:
            angle = self.v_angle_min
        if angle > self.v_angle_max:
            angle = self.v_angle_max

        return angle

    #############################################################
    def calc_line(self, angle, height):
    #############################################################

        linea = int(angle/self.v_angle_increment) + height/2

        if linea >= height:
            linea = height - 1

        return linea

    #############################################################
    def fill_ranges(self, line, msg, scan):
    #############################################################
        # Recogemos los datos de la linea que queremos transformar
        #scan = LaserScan()
        scan.ranges = [float('Inf')] * msg.width
        ranges_raw = [float('Inf')] * msg.width
        scan.intensities = []

        # Get line from matrix
        start = line * msg.step
        finish = start + msg.step
        linea_scan = msg.data[start:finish]
        # processing line data
        for u in range(msg.width):
            # Getting de depth distance from bytes
            if msg.encoding == "32FC1":
                z = float(struct.unpack('f', linea_scan[4*u:4*u+4])[0])
            elif msg.encoding == "16UC1":
                z = float(struct.unpack('h', linea_scan[2*u:2*u+2])[0]) / 1000
            else:
                return scan
            # Getting de index in the array
            th = -math.atan((u - self.center_x) * self.constant_x)
            index = int((th - self.h_angle_min) / self.h_angle_increment)
            # Getting the x distance from the horizontal line
            x = (u - self.center_x) * z * self.constant_x
            # Getting the radius from camera frame
            rx = (x**(2) + z**(2))**(0.5)
            # Getting the y distance from the vertical line
            xy = (line - self.center_y) * rx * self.constant_y
            # Getting the final radius from camera frame
            rxy = (xy**(2) + rx**(2))**(0.5)

            if index >= 0:
                ranges_raw[index] = rxy

        scan.ranges = ranges_raw


    #############################################################
    def filter_scan(self, scan):
    #############################################################
        #print("----------------------------------------------------")
        rangos = []
        grupo_de_4 = []
        media_ant = 100
        for x in scan.ranges:
            if x < 12.0 and x > 0.1:
                grupo_de_4.append(x)
            else:
                grupo_de_4.append(float('Inf'))
            if len(grupo_de_4) == 4:
                menor = grupo_de_4.index(np.amin(grupo_de_4))
                np.delete(grupo_de_4, menor)
                mayor = grupo_de_4.index(np.amax(grupo_de_4))
                np.delete(grupo_de_4, mayor)
                media = np.mean(grupo_de_4)
                if media_ant < 12 and media_ant > 0.1 and media < 12 and media:
                    p = np.polyfit([0, 4], [media_ant, media], 1)
                    for y in range(4):
                        d = y * p[0] + p[1]
                        rangos.append(d)
                else:
                    for y in range(4):
                        rangos.append(float('Inf'))
                grupo_de_4 = []
                media_ant = media

        scan.ranges = rangos

    #############################################################
    def calc_transform(self, angle, frame_id_out):
    #############################################################

        broadcaster_depth2scan = tf2_ros.StaticTransformBroadcaster()
        static_transformStamped = geometry_msgs.msg.TransformStamped()
        static_transformStamped.header.frame_id = self.frame_id_in
        static_transformStamped.child_frame_id = frame_id_out
        static_transformStamped.transform.translation.x = 0.0
        static_transformStamped.transform.translation.y = 0.0
        static_transformStamped.transform.translation.z = 0.0
        q = quaternion_from_euler (0.0, angle, 0.0)
        static_transformStamped.transform.rotation.x = q[0]
        static_transformStamped.transform.rotation.y = q[1]
        static_transformStamped.transform.rotation.z = q[2]
        static_transformStamped.transform.rotation.w = q[3]
        static_transformStamped.header.stamp = rospy.Time.now()
        broadcaster_depth2scan.sendTransform(static_transformStamped)

    #############################################################
    def CallbackOdom(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))

        self.odom = msg
        #print(self.odom)

    #############################################################
    def angle_between_rays(self,ray1,ray2):
    #############################################################
        dot_product = ray1[0] * ray2[0] + ray1[1] * ray2[1] + ray1[2] * ray2[2]
        magnitude1 = self.magnitude_of_ray(ray1)
        magnitude2 = self.magnitude_of_ray(ray2)
        return math.acos(dot_product / (magnitude1 * magnitude2))

    #############################################################
    def magnitude_of_ray(self,ray):
    #############################################################
        return math.sqrt(pow(ray[0], 2.0) + pow(ray[1], 2.0) + pow(ray[2], 2.0))

    #############################################################
    def calc_static_variables(self):
    #############################################################

        # Waiting till get de depth_info topic
        depth_info = None
        rospy.logdebug("Waiting for " + self.input_info + " to be READY...")
        while depth_info is None and not rospy.is_shutdown():
            try:
                depth_info = rospy.wait_for_message(self.input_info, CameraInfo, timeout=5.0)
                rospy.loginfo("Current " + self.input_info + " READY=>")
            except:
                rospy.loginfo("Current " + self.input_info + " not ready yet, retrying for getting it")

        cam_model_ = PinholeCameraModel()
        cam_model_.fromCameraInfo(depth_info)

        # Getting the constants of the camera
        self.constant_x = 1 / cam_model_.fx()
        self.constant_y = 1 / cam_model_.fy()
        self.center_x = cam_model_.cx()
        self.center_y = cam_model_.cy()

        # Horizontal
        # Calculate angle_min and angle_max by measuring angles between the left ray, right ray, and optical center ray
        raw_pixel_left = (0, self.center_y)
        rect_pixel_left = cam_model_.rectifyPoint(raw_pixel_left)
        left_ray = cam_model_.projectPixelTo3dRay(rect_pixel_left)

        raw_pixel_right = (depth_info.width - 1, self.center_y)
        rect_pixel_right = cam_model_.rectifyPoint(raw_pixel_right)
        right_ray = cam_model_.projectPixelTo3dRay(rect_pixel_right)

        raw_pixel_center = (self.center_x, self.center_y)
        rect_pixel_center = cam_model_.rectifyPoint(raw_pixel_center)
        center_ray = cam_model_.projectPixelTo3dRay(rect_pixel_center)

        self.h_angle_max = + self.angle_between_rays(left_ray, center_ray)
        self.h_angle_min = - self.angle_between_rays(center_ray, right_ray)
        self.h_angle_increment = (self.h_angle_max - self.h_angle_min) / (depth_info.width - 1)

        # Vertical
        # Calculate angle_min and angle_max by measuring angles between the left ray, right ray, and optical center ray
        raw_pixel_top = (self.center_x, 0)
        rect_pixel_top = cam_model_.rectifyPoint(raw_pixel_top)
        top_ray = cam_model_.projectPixelTo3dRay(rect_pixel_top)

        raw_pixel_bottom = (self.center_x, depth_info.height - 1)
        rect_pixel_bottom = cam_model_.rectifyPoint(raw_pixel_bottom)
        bottom_ray = cam_model_.projectPixelTo3dRay(rect_pixel_bottom)

        self.v_angle_max = + self.angle_between_rays(top_ray, center_ray)
        self.v_angle_min = - self.angle_between_rays(center_ray, bottom_ray)
        self.v_angle_increment = (self.v_angle_max - self.v_angle_min) / (depth_info.height - 1)

        # Waiting till get de depth topic
        depth = None
        rospy.logdebug("Waiting for " + self.input_depth + " to be READY...")
        while depth is None and not rospy.is_shutdown():
            try:
                depth = rospy.wait_for_message(self.input_depth, Image, timeout=5.0)
                rospy.loginfo("Current " + self.input_depth + " READY=>")
            except:
                rospy.loginfo("Current " + self.input_depth + " not ready yet, retrying for getting it")


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        depth2laser = Depth2Laser()
        depth2laser.spin()
    except rospy.ROSInterruptException:
        pass
