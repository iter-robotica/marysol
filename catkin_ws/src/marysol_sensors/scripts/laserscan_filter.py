#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float32MultiArray, Bool
import roslib
import numpy as np

#############################################################
#############################################################
class LaserScanFilter():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("laserscan_filter")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.base_frame_id = rospy.get_param('~base_frame_id', "kinect2_link")
        self.laser_input_topic = rospy.get_param('~laser_input_topic', "/scan")
        self.laser_output_topic_1 = rospy.get_param('~laser_output_topic_1', "/scan_1")
        self.laser_output_topic_2 = rospy.get_param('~laser_output_topic_2', "/scan_2")

        # internal data
        self.r = rospy.Rate(self.rate)
        self.laser_up = True
        self.counter = 0
        scan = None
        while scan is None and not rospy.is_shutdown():
            try:
                scan = rospy.wait_for_message(self.laser_input_topic, LaserScan, timeout=5.0)
                rospy.loginfo("Current " + self.laser_input_topic + " READY=>")
                self.LaserScan_1 = scan
                self.LaserScan_2 = scan
                self.spinOnce()
            except:
                rospy.loginfo("Current " + self.laser_input_topic + " not ready yet, retrying for getting it")

        # subscriptions
        rospy.Subscriber("laser_up", Bool, self.Callback_laser_up)
        rospy.Subscriber(self.laser_input_topic, LaserScan, self.Callback_laser)
        self.laser_pub_1 = rospy.Publisher(self.laser_output_topic_1, LaserScan, queue_size=10)
        self.laser_pub_2 = rospy.Publisher(self.laser_output_topic_2, LaserScan, queue_size=10)

    #############################################################
    def spin(self):
    #############################################################

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            self.r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        if not self.laser_up:
            try:
                self.Parser_laser(rospy.wait_for_message(self.laser_input_topic, LaserScan, timeout=5.0))
            except:
                pass

        self.LaserScan_1.header.stamp = rospy.Time.now()
        self.laser_pub_1.publish(self.LaserScan_1)

        self.LaserScan_2.header.stamp = rospy.Time.now()
        self.laser_pub_2.publish(self.LaserScan_2)

    #############################################################
    def Callback_laser(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        if self.laser_up:
            self.Parser_laser(msg)

    #############################################################
    def Parser_laser(self,msg):
    #############################################################
        self.LaserScan_1 = msg
        #print("Range Kinect " + str(range_len))
        #print("----------------------------------------------------")
        rangos_1 = []
        grupo_de_4 = []
        media_ant = None
        last_p = 0
        p = [0]
        for x in msg.ranges:
            if x < 12.0 and x > 0.1:
                grupo_de_4.append(x)
            else:
                grupo_de_4.append(float('nan'))
            if len(grupo_de_4) == 4:
                #menor = grupo_de_4.index(np.amin(grupo_de_4))
                #np.delete(grupo_de_4, menor)
                #menor = grupo_de_4.index(np.amin(grupo_de_4))
                #np.delete(grupo_de_4, menor)
                #mayor = grupo_de_4.index(np.amax(grupo_de_4))
                #np.delete(grupo_de_4, mayor)

                #media = np.mean(grupo_de_4)
                media = np.amax(grupo_de_4)

                if media_ant < 20 and media_ant > 0.0:
                    p = np.polyfit([0, 4], [media_ant, media], 1)
                    if abs(last_p - p[0]) < 0.1:
                        for y in range(4):
                            d = y * p[0] + p[1]
                            rangos_1.append(d)
                    else:
                        for y in range(4):
                            rangos_1.append(float('nan'))
                else:
                    for y in range(4):
                        rangos_1.append(float('nan'))
                grupo_de_4 = []
                media_ant = media
                last_p = p[0]
        self.LaserScan_2.ranges = rangos_1

    #############################################################
    def Callback_laser_up(self,msg):
    #############################################################
        if msg.data == True:
            self.r = rospy.Rate(self.rate)
            self.laser_up = True
        else:
            self.r = rospy.Rate(2)
            self.laser_up = False


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        laserscan_filter = LaserScanFilter()
        laserscan_filter.spin()
    except rospy.ROSInterruptException:
        pass
