#!/usr/bin/env python

import rospy
from std_msgs.msg import String, Int16MultiArray
import roslib

#############################################################
#############################################################
class Lcd():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("lcd")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)

        # subscriptions
        self.lcd_line_up_pub = rospy.Publisher('/lcd_line_up', String, queue_size=10)
        self.lcd_line_down_pub = rospy.Publisher('/lcd_line_down', String, queue_size=10)
        rospy.Subscriber('/debug_line_up', String, self.CallbackDebugLineUp)
        rospy.Subscriber('/debug_line_down', String, self.CallbackDebugLineDown)

        # internal data
        self.lcd_line_up = String()
        self.lcd_line_down = String()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################
        pass

    #############################################################
    def CallbackDebugLineUp(self,msg):
    #############################################################
        self.lcd_line_up.data = msg.data
        self.lcd_line_up_pub.publish(self.lcd_line_up)

    #############################################################
    def CallbackDebugLineDown(self,msg):
    #############################################################
        self.lcd_line_down.data = msg.data
        self.lcd_line_down_pub.publish(self.lcd_line_down)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        lcd = Lcd()
        lcd.spin()
    except rospy.ROSInterruptException:
        pass
