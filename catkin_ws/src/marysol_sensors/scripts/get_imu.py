#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
from std_msgs.msg import Float32MultiArray
import roslib

#############################################################
#############################################################
class GetImu():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("get_imu")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 20)
        self.base_frame_id = rospy.get_param('~base_frame_id', "imu_link")

        # subscriptions
        rospy.Subscriber('/imu_data', Float32MultiArray, self.Callback)
        self.imu_pub = rospy.Publisher('/imu', Imu, queue_size=10)

        # internal data
        self.imu = Imu()
        self.imu.header.frame_id = self.base_frame_id
        self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.imu.header.stamp = rospy.Time.now()
        self.imu_pub.publish(self.imu)

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        self.imu.orientation.x = msg.data[0]
        self.imu.orientation.y = msg.data[1]
        self.imu.orientation.z = msg.data[2]
        self.imu.orientation.w = msg.data[3]
        self.imu.angular_velocity.x = + (msg.data[4] * 3.1416) / 180
        self.imu.angular_velocity.y = - (msg.data[5] * 3.1416) / 180
        self.imu.angular_velocity.z = - (msg.data[6] * 3.1416) / 180
        self.imu.linear_acceleration.x = msg.data[7]
        self.imu.linear_acceleration.y = msg.data[8]
        self.imu.linear_acceleration.z = msg.data[9]

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_imu = GetImu()
        get_imu.spin()
    except rospy.ROSInterruptException:
        pass
