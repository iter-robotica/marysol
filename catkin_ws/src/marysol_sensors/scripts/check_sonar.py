#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32MultiArray
from std_msgs.msg import Bool
import numpy as np
import roslib

#############################################################
#############################################################
class GetOctosonar():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("get_octosonar")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.min_range = rospy.get_param('~min_range', 1000)

        # subscriptions
        rospy.Subscriber('/octosonar_data', Int32MultiArray, self.Callback)
        self.stop_pub = rospy.Publisher('/stop', Bool, queue_size=10)

        # internal data
        self.stop = Bool()
        self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################
        self.stop_pub.publish(self.stop)
        #print(Emergency)

    #############################################################
    def Callback(self,msg):
    #############################################################
        self.stop = False
        for i in range(len(msg.data)):
            if msg.data[i] < self.min_range:
                self.stop = True


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_octosonar = GetOctosonar()
        get_octosonar.spin()
    except rospy.ROSInterruptException:
        pass
