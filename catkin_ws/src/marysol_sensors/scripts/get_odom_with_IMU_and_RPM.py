#!/usr/bin/env python

import rospy
#import roslib
from math import sin, cos, pi

from geometry_msgs.msg import Twist, Quaternion
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from tf.broadcaster import TransformBroadcaster
from std_msgs.msg import Int32MultiArray, Float32MultiArray, Int16MultiArray, Float32MultiArray
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import threading

#############################################################################
class GetOdomWithIMUandRPM:
#############################################################################

    #############################################################################
    def __init__(self):
    #############################################################################
        rospy.init_node("get_odom_with_IMU_and_RPM")
        self.nodename = rospy.get_name()
        rospy.loginfo("-I- %s started" % self.nodename)

        #### parameters #######
        self.publishing_rate = rospy.get_param('~publishing_rate', 25)  # the rate at which to publish the transform
        self.updating_rate = rospy.get_param('~updating_rate', 10)  # the rate at which to get the transform
        self.ticks_meter = float(rospy.get_param('~ticks_meter', 130))  # The number of wheel encoder ticks per meter of travel
        self.base_width = float(rospy.get_param('~base_width', 0.505)) # The wheel base width in meters

        self.base_frame_id = rospy.get_param('~base_frame_id','footprint') # the name of the base frame of the robot
        self.odom_frame_id = rospy.get_param('~odom_frame_id', 'odom') # the name of the odometry reference frame

        self.encoder_min = rospy.get_param('~encoder_min', -32768)
        self.encoder_max = rospy.get_param('~encoder_max', 32768)
        self.encoder_low_wrap = rospy.get_param('~wheel_low_wrap', (self.encoder_max - self.encoder_min) * 0.3 + self.encoder_min )
        self.encoder_high_wrap = rospy.get_param('~wheel_high_wrap', (self.encoder_max - self.encoder_min) * 0.7 + self.encoder_min )

        # subscriptions
        rospy.Subscriber("odometry", Int32MultiArray, self.odometryCallback)
        rospy.Subscriber('imu', Imu, self.imuCallback)
        rospy.Subscriber('state', Int16MultiArray, self.stateCallback)
        #rospy.Subscriber('imu_data', Float32MultiArray, self.imu_dataCallback)
        self.odomPub = rospy.Publisher("odom", Odometry, queue_size=10)
        self.euler_pub = rospy.Publisher("euler", Float32MultiArray, queue_size=10)
        self.odomBroadcaster = TransformBroadcaster()

        # internal data
        self.odom = Odometry()
        self.odom.header.frame_id = self.odom_frame_id
        self.odom.child_frame_id = self.base_frame_id
        self.enc_data_left = None
        self.enc_data_right = None
        self.enc_left = None        # wheel encoder readings
        self.enc_right = None
        self.left = 0               # actual values coming back from robot
        self.right = 0
        self.yaw = 0
        self.lmult = 0
        self.rmult = 0
        self.prev_lencoder = 0
        self.prev_rencoder = 0
        self.x = 0                  # position in xy plane
        self.x_old = None
        self.y = 0
        self.y_old = None
        self.z = 0
        self.th = 0
        self.th_old = None
        self.lx = 0                 # linear speeds
        self.ly = 0
        self.lz = 0
        self.ax = 0                 # angular speeds
        self.ay = 0
        self.az = 0
        self.quaternion = Quaternion()
        self.quaternion.x = 0
        self.quaternion.y = 0
        self.quaternion.z = 0
        self.quaternion.w = 1
        self.euler = Float32MultiArray()


    #############################################################################
    def spin(self):
    #############################################################################

        x = threading.Thread(target=get_odom_with_IMU_and_RPM.publishing_data, args=(self.publishing_rate,))
        y = threading.Thread(target=get_odom_with_IMU_and_RPM.updating_data, args=(self.updating_rate,))

        x.start()
        y.start()

    #############################################################################
    def publishing_data(self, rate=25):
    #############################################################################
        r = rospy.Rate(rate)

        ###### main loop  ######
        while not rospy.is_shutdown():

            # publish the odom information
            self.odom.header.stamp = rospy.Time.now()
            self.odomPub.publish(self.odom)

            # publish the transform
            self.odomBroadcaster.sendTransform(
                (self.x, self.y, self.z),
                (self.quaternion.x, self.quaternion.y, self.quaternion.z, self.quaternion.w),
                rospy.Time.now(),
                self.base_frame_id,
                self.odom_frame_id
                )

            r.sleep()

    #############################################################################
    def updating_data(self, rate=5):
    #############################################################################
        r = rospy.Rate(rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            get_odom_with_IMU_and_RPM.update()

            r.sleep()

    #############################################################################
    def update(self):
    #############################################################################

        # calculate odometry
        if self.enc_left == None:
            d_left = 0.0
            d_right = 0.0
            th = 0.0
        else:
            d_left = (self.left - self.enc_left) / self.ticks_meter
            d_right = (self.right - self.enc_right) / self.ticks_meter
            # Fixing the problem when angle crosses from 180 to -180 and vice versa in yaw angle
            if abs(self.th_old - self.th) > 6:  # if the difference is bigger than 344
                # When angle crosses from 180 to -180
                if self.th_old > 0 and self.th < 0:
                    th = self.th + 2 * pi - self.th_old  # we add 360
                # When angle crosses from -180 to 180
                if self.th_old < 0 and self.th > 0:
                    th = self.th - 2 * pi - self.th_old  # we substract 360
            else:
                th = self.th - self.th_old
        #we store the actual value for the next cycle
        self.th_old = self.th
        self.enc_left = self.left
        self.enc_right = self.right

        # distance traveled is the average of the two wheels
        d = ( d_left + d_right ) / 2
        # this approximation works (in radians) for small angles
        di_th = ( d_right - d_left ) / self.base_width
        # calculate velocities
        #self.lx = d * self.updating_rate

        if (d != 0):
            # calculate distance traveled in x and y
            x = cos( th ) * d
            y = -sin( th ) * d
            # calculate the final position of the robot
            self.x = self.x + ( cos( self.th ) * x - sin( self.th ) * y )
            self.y = self.y + ( sin( self.th ) * x + cos( self.th ) * y )

        self.odom.pose.pose.position.x = self.x
        self.odom.pose.pose.position.y = self.y
        self.odom.pose.pose.position.z = self.z              # 0
        self.odom.pose.pose.orientation = self.quaternion
        self.odom.pose.covariance = [1e-05, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1e-05, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000000000.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000000000.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000000000.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001]
        self.odom.twist.twist.linear.x = self.lx
        self.odom.twist.twist.linear.y = self.ly             # 0
        self.odom.twist.twist.linear.z = self.lz             # 0
        self.odom.twist.twist.angular.x = self.ax            # 0
        self.odom.twist.twist.angular.y = self.ay            # 0
        self.odom.twist.twist.angular.z = self.az


    #############################################################################
    def odometryCallback(self, msg):
    #############################################################################
        if self.enc_data_left == None:
            self.enc_data_left = msg.data[0]
            self.enc_data_right = msg.data[1]

        enc = - (msg.data[0] - self.enc_data_left)
        if (enc < self.encoder_low_wrap and self.prev_lencoder > self.encoder_high_wrap):
            self.lmult = self.lmult + 1

        if (enc > self.encoder_high_wrap and self.prev_lencoder < self.encoder_low_wrap):
            self.lmult = self.lmult - 1

        self.left = 1.0 * (enc + self.lmult * (self.encoder_max - self.encoder_min))
        self.prev_lencoder = enc


        enc = + (msg.data[1] - self.enc_data_right)
        if(enc < self.encoder_low_wrap and self.prev_rencoder > self.encoder_high_wrap):
            self.rmult = self.rmult + 1

        if(enc > self.encoder_high_wrap and self.prev_rencoder < self.encoder_low_wrap):
            self.rmult = self.rmult - 1

        self.right = 1.0 * (enc + self.rmult * (self.encoder_max - self.encoder_min))
        self.prev_rencoder = enc


    #############################################################################
    def imuCallback(self, msg):
    #############################################################################

        #rospy.loginfo("-D- Callback: %s" % str(msg))
        roll = pitch = yaw = 0.0
        orientation_list = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]
        (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
        #q = quaternion_from_euler(roll, pitch, yaw)
        q = quaternion_from_euler(roll, pitch, yaw) # Ya que el roll y el pitch se cogen del get_base_link_joint
        self.quaternion.x = q[0]
        self.quaternion.y = q[1]
        self.quaternion.z = q[2]
        self.quaternion.w = q[3]

        self.th = yaw
        self.az = msg.angular_velocity.z

        self.euler.data = []
        self.euler.data.append(roll)
        self.euler.data.append(pitch)
        self.euler.data.append(yaw)
        self.euler_pub.publish(self.euler)

    #############################################################################
    def stateCallback(self, msg):
    #############################################################################

        #rospy.loginfo("-D- Callback: %s" % str(msg))
        #rpm = 60 / (2 * pi * r) m/s
        #rpm = 60 / (2 * 3.1416 * 0.125) m/s
        right = - msg.data[0] / 76.3942
        left = + msg.data[1] / 76.3942

        # dx = (l + r) / 2
        self.lx = (left + right) / 2

#############################################################################
#############################################################################
if __name__ == '__main__':
    """ main """
    try:
        get_odom_with_IMU_and_RPM = GetOdomWithIMUandRPM()
        get_odom_with_IMU_and_RPM.spin()
    except rospy.ROSInterruptException:
        pass
