#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
import geometry_msgs.msg
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import tf2_ros
import roslib

#############################################################
#############################################################
class GetBaseLinkJoint():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("get_baselink_joint")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = rospy.get_param("~rate", 10)
        self.profundidad_por_defecto = rospy.get_param('~x_offset', 0)
        self.separacion_por_defecto = rospy.get_param('~y_offset', 0)
        self.altura_por_defecto = rospy.get_param('~z_offset', 0)
        self.roll_por_defecto = rospy.get_param('~roll_offset', 0)
        self.pitch_por_defecto = rospy.get_param('~pitch_offset', 0)
        self.yaw_por_defecto = rospy.get_param('~yaw_offset', 0)
        self.footprint_frame = rospy.get_param('~footprint_frame', 'footprint')
        self.base_link_frame = rospy.get_param('~base_link_frame', 'base_link')

        # subscriptions
        rospy.Subscriber('/imu', Imu, self.Callback)

        # internal data
        self.broadcaster = tf2_ros.StaticTransformBroadcaster()
        self.static_transformStamped = geometry_msgs.msg.TransformStamped()
        self.static_transformStamped.header.frame_id = self.footprint_frame
        self.static_transformStamped.child_frame_id = self.base_link_frame
        self.static_transformStamped.transform.translation.x = self.profundidad_por_defecto
        self.static_transformStamped.transform.translation.y = self.separacion_por_defecto
        self.static_transformStamped.transform.translation.z = self.altura_por_defecto
        q = quaternion_from_euler (self.roll_por_defecto,
                                   self.pitch_por_defecto,
                                   self.yaw_por_defecto)
        self.static_transformStamped.transform.rotation.x = q[0]
        self.static_transformStamped.transform.rotation.y = q[1]
        self.static_transformStamped.transform.rotation.z = q[2]
        self.static_transformStamped.transform.rotation.w = q[3]

        self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.static_transformStamped.header.stamp = rospy.Time.now()
        self.broadcaster.sendTransform(self.static_transformStamped)

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        self.static_transformStamped.transform.translation.x = self.profundidad_por_defecto
        self.static_transformStamped.transform.translation.y = self.separacion_por_defecto
        self.static_transformStamped.transform.translation.z = self.altura_por_defecto
        roll = pitch = yaw = 0.0
        orientation_list = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]
        (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
        #print(roll, pitch, yaw)
        q = quaternion_from_euler (roll + self.roll_por_defecto,
                                   pitch + self.pitch_por_defecto,
                                   0) # Ya que el yaw lo calculamos con el get odom with IMU
                                   #yaw + self.yaw_por_defecto)
        self.static_transformStamped.transform.rotation.x = q[0]
        self.static_transformStamped.transform.rotation.y = q[1]
        self.static_transformStamped.transform.rotation.z = q[2]
        self.static_transformStamped.transform.rotation.w = q[3]


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        getbaselinkjoint = GetBaseLinkJoint()
        getbaselinkjoint.spin()
    except rospy.ROSInterruptException:
        pass
