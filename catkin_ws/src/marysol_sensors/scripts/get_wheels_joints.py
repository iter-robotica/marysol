#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32MultiArray
from math import pi
import geometry_msgs.msg
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import tf2_ros
import roslib

#############################################################
#############################################################
class GetWheelsJoints():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("get_wheels_joints")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.rate = int(rospy.get_param("~rate", 10))
        self.PPR = int(rospy.get_param("~PPR", 86))
        self.profundidad_por_defecto = float(rospy.get_param('~x_offset', 0.0))
        self.separacion_por_defecto = float(rospy.get_param('~y_offset', 0.0))
        self.altura_por_defecto = float(rospy.get_param('~z_offset', 0.0))
        self.roll_por_defecto = float(rospy.get_param('~roll_offset', 0.0))
        self.pitch_por_defecto = float(rospy.get_param('~pitch_offset', 0.0))
        self.yaw_por_defecto = float(rospy.get_param('~yaw_offset', 0.0))
        self.base_frame_id = rospy.get_param('~base_frame','base_link')
        self.right_wheel_frame_id = rospy.get_param('~right_wheel_frame', 'wheel_right_link')
        self.left_wheel_frame_id = rospy.get_param('~left_wheel_frame', 'wheel_left_link')

        # subscriptions
        rospy.Subscriber('/odometry', Int32MultiArray, self.Callback)

        # internal data
        #self.timenow = rospy.Time.now()
        self.broadcaster_right = tf2_ros.StaticTransformBroadcaster()
        self.static_transformStamped_right = geometry_msgs.msg.TransformStamped()
        self.static_transformStamped_right.header.frame_id = self.base_frame_id
        self.static_transformStamped_right.child_frame_id = self.right_wheel_frame_id
        self.static_transformStamped_right.transform.translation.x = self.profundidad_por_defecto
        self.static_transformStamped_right.transform.translation.y = - self.separacion_por_defecto
        self.static_transformStamped_right.transform.translation.z = self.altura_por_defecto
        q = quaternion_from_euler (self.roll_por_defecto,
                                   self.pitch_por_defecto,
                                   self.yaw_por_defecto)
        self.static_transformStamped_right.transform.rotation.x = q[0]
        self.static_transformStamped_right.transform.rotation.y = q[1]
        self.static_transformStamped_right.transform.rotation.z = q[2]
        self.static_transformStamped_right.transform.rotation.w = q[3]

        self.broadcaster_left = tf2_ros.StaticTransformBroadcaster()
        self.static_transformStamped_left = geometry_msgs.msg.TransformStamped()
        self.static_transformStamped_left.header.frame_id = self.base_frame_id
        self.static_transformStamped_left.child_frame_id = self.left_wheel_frame_id
        self.static_transformStamped_left.transform.translation.x = self.profundidad_por_defecto
        self.static_transformStamped_left.transform.translation.y = self.separacion_por_defecto
        self.static_transformStamped_left.transform.translation.z = self.altura_por_defecto
        q = quaternion_from_euler (self.roll_por_defecto,
                                   self.pitch_por_defecto,
                                   self.yaw_por_defecto)
        self.static_transformStamped_left.transform.rotation.x = q[0]
        self.static_transformStamped_left.transform.rotation.y = q[1]
        self.static_transformStamped_left.transform.rotation.z = q[2]
        self.static_transformStamped_left.transform.rotation.w = q[3]

        self.spinOnce()

    #############################################################
    def spin(self):
    #############################################################

        r = rospy.Rate(self.rate)

        ###### main loop  ######
        while not rospy.is_shutdown():
            self.spinOnce()
            r.sleep()

    #############################################################
    def spinOnce(self):
    #############################################################

        self.static_transformStamped_right.header.stamp = rospy.Time.now()
        self.broadcaster_right.sendTransform(self.static_transformStamped_right)

        self.static_transformStamped_left.header.stamp = rospy.Time.now()
        self.broadcaster_left.sendTransform(self.static_transformStamped_left)

    #############################################################
    def Callback(self,msg):
    #############################################################
        #rospy.loginfo("-D- Callback: %s" % str(msg))
        self.static_transformStamped_right.transform.translation.x = self.profundidad_por_defecto
        self.static_transformStamped_right.transform.translation.y = - self.separacion_por_defecto
        self.static_transformStamped_right.transform.translation.z = self.altura_por_defecto
        roll = 0.0
        pitch = (2 * pi * msg.data[1]) / self.PPR
        yaw = 0.0
        q = quaternion_from_euler (roll + self.roll_por_defecto,
                                   pitch + self.pitch_por_defecto,
                                   yaw + self.yaw_por_defecto)
        self.static_transformStamped_right.transform.rotation.x = q[0]
        self.static_transformStamped_right.transform.rotation.y = q[1]
        self.static_transformStamped_right.transform.rotation.z = q[2]
        self.static_transformStamped_right.transform.rotation.w = q[3]

        self.static_transformStamped_left.transform.translation.x = self.profundidad_por_defecto
        self.static_transformStamped_left.transform.translation.y = self.separacion_por_defecto
        self.static_transformStamped_left.transform.translation.z = self.altura_por_defecto
        roll = 0.0
        pitch = - (2 * pi * msg.data[0]) / self.PPR
        yaw = 0.0
        q = quaternion_from_euler (roll + self.roll_por_defecto,
                                   pitch + self.pitch_por_defecto,
                                   yaw + self.yaw_por_defecto)
        self.static_transformStamped_left.transform.rotation.x = q[0]
        self.static_transformStamped_left.transform.rotation.y = q[1]
        self.static_transformStamped_left.transform.rotation.z = q[2]
        self.static_transformStamped_left.transform.rotation.w = q[3]

        #rospy.loginfo("Time: %i    Now: %i     Periodo: %i", self.timenow.nsecs, rospy.Time.now().nsecs, rospy.Time.now().nsecs - self.timenow.nsecs)
        #self.timenow = rospy.Time.now()

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        getwheelsjoints = GetWheelsJoints()
        getwheelsjoints.spin()
    except rospy.ROSInterruptException:
        pass
