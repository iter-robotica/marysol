#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseStamped
from apriltag_ros.msg import AprilTagDetectionArray
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import tf2_ros
import tf2_geometry_msgs

#############################################################
#############################################################
class PoseFromMap():
#############################################################
#############################################################

    #############################################################
    def __init__(self):
    #############################################################
        rospy.init_node("pose_from_frame")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters #######
        self.env = rospy.get_param("~env", "real")

        # subscriptions
        rospy.Subscriber("/tag_detections", AprilTagDetectionArray, self.apriltagCallback)
        self.pose_publish = rospy.Publisher('/qr_pose', PoseStamped, queue_size=10)

        # internal data
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0)) # is there any way to make it faster??
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.done = False

    #############################################################
    def apriltagCallback(self, msg):
    #############################################################

        if len(msg.detections) > 0:# and self.done == False:
            self.done = True

            qr_pose = msg.detections[0].pose

            # 1 - Getting the angle between the qr_pose and the kinect2_link as part of the endpoint pose in the frame map

            PoseStamped_orientation = PoseStamped()
            PoseStamped_orientation.header.frame_id = "d435_pose"
            PoseStamped_orientation.header.stamp = rospy.Time.now()

            yaw = math.atan(qr_pose.pose.pose.position.x / qr_pose.pose.pose.position.z)
            angle = (180/3.14) * yaw
            q = quaternion_from_euler (0.0, 0.0, -yaw/2)

            PoseStamped_orientation.pose.orientation.x = q[0]
            PoseStamped_orientation.pose.orientation.y = q[1]
            PoseStamped_orientation.pose.orientation.z = q[2]
            PoseStamped_orientation.pose.orientation.w = q[3]

            pose_transformed_orientation = PoseStamped()
            transform = self.tf_buffer.lookup_transform("map", PoseStamped_orientation.header.frame_id, rospy.Time(0), rospy.Duration(1))
            pose_transformed_orientation = tf2_geometry_msgs.do_transform_pose(PoseStamped_orientation, transform)

            # 1 - Getting the qr_pose in the map frame

            PoseStamped_position = PoseStamped()
            PoseStamped_position.header.frame_id = "d435_pose"
            PoseStamped_position.header.stamp = rospy.Time.now()
            PoseStamped_position.pose.position = qr_pose.pose.pose.position
            PoseStamped_position.pose.position.x, PoseStamped_position.pose.position.y, PoseStamped_position.pose.position.z = PoseStamped_position.pose.position.z, -PoseStamped_position.pose.position.x, 0.0

            h = (PoseStamped_position.pose.position.x**(2) + PoseStamped_position.pose.position.y**(2))**(0.5)
            PoseStamped_position.pose.position.x = (h - 1.5) * math.cos(-yaw)
            if PoseStamped_position.pose.position.x <= 0:
                PoseStamped_position.pose.position.x = 0
            PoseStamped_position.pose.position.y = (h - 1.5) * math.sin(-yaw)
            if PoseStamped_position.pose.position.y <= 0:
                PoseStamped_position.pose.position.y = 0

            pose_transformed_position = PoseStamped()
            transform = self.tf_buffer.lookup_transform("map", PoseStamped_position.header.frame_id, rospy.Time(0), rospy.Duration(1))
            pose_transformed_position = tf2_geometry_msgs.do_transform_pose(PoseStamped_position, transform)
            pose_transformed_position.pose.position.z = 0

            # 3 - Mixing position and orientation to get the final pose

            PoseStamped_final = PoseStamped()
            PoseStamped_final.header.frame_id = "map"
            PoseStamped_final.header.stamp = rospy.Time.now()
            PoseStamped_final.pose.position = pose_transformed_position.pose.position
            PoseStamped_final.pose.orientation = pose_transformed_orientation.pose.orientation
            self.pose_publish.publish(PoseStamped_final)

            print(PoseStamped_position.pose.position)

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_pose_from_frame = PoseFromMap()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
