#!/usr/bin/env python

import rospy
import yaml
from os.path import expanduser

#############################################################
#############################################################
class Check():
#############################################################
#############################################################

    #############################################################
    def __init__(self, env, coordinates, windows, map):
    #############################################################

        self.env = env

        self.coordinates = coordinates

        self.windows_names = windows

        self.common_questions = {}

        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/common_questions.yaml") as file:
            self.common_questions = yaml.load(file, Loader=yaml.FullLoader)

        self.it = None

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called check')

    #############################################################
    def check_resend_goal_command(self, user_input):
    #############################################################
        from resources.utils import normalize
        user_input = normalize(user_input)
        if user_input in "continua adelante sigue":
            return True
        else:
            return False

    #############################################################
    def check_move_command(self, user_input):
    #############################################################
        from resources.utils import normalize
        user_input = user_input.lower()
        command_ = normalize(user_input.split(" ")[0])
        if command_ in "este vete ve camina muevete avanza dirigete anda trasládate marchate esperame":
            if " la " in user_input:
                self.it = user_input.split(" la ")[1].lower()
                return True
            elif " el " in user_input:
                self.it = user_input.split(" el ")[1].lower()
                return True
            elif " al " in user_input:
                self.it = user_input.split(" al ")[1].lower()
                return True
        return False

    #############################################################
    def check_move_around(self, user_input):
    #############################################################
        user_input = user_input.lower()
        if "date una vuelta" in user_input:
            self.it = self.coordinates
            return True
        else:
            return False

    #############################################################
    def check_turn_off(self, user_input):
    #############################################################
        user_input = user_input.lower()
        if "hora de descansar" in user_input:
            self.it = []
            self.it.append("go-punto_de_carga")
            self.it.append("command-shutdown -h now")
            return True
        else:
            return False

    #############################################################
    def check_turn_around(self, user_input):
    #############################################################
        user_input = user_input.lower()
        if "date la vuelta" in user_input:
            return True
        else:
            return False

    #############################################################
    def check_locate(self, user_input):
    #############################################################
        user_input = user_input.lower()
        if "localízate en" in user_input:
            if " la " in user_input:
                self.it = user_input.split(" la ")[1].replace(" ", "_").lower()
            elif " el " in user_input:
                self.it = user_input.split(" el ")[1].replace(" ", "_").lower()
            if self.it not in self.coordinates:
                self.it = "Lo siento, no conozco el lugar " + self.it.replace("_", " ")
            return True
        else:
            return False

    #############################################################
    def check_routine(self, user_input):
    #############################################################
        from resources.utils import normalize
        user_input = user_input.lower()
        command = normalize(user_input)
        if "inicia la rutina" in command:
            user_input = user_input.split("inicia la rutina")[1]
            if " la " in user_input:
                self.it = user_input.split(" la ")[1].replace(" ", "_").lower()
            elif " el " in user_input:
                self.it = user_input.split(" el ")[1].replace(" ", "_").lower()
            elif " los " in user_input:
                self.it = user_input.split(" los ")[1].replace(" ", "_").lower()
            elif " las " in user_input:
                self.it = user_input.split(" las ")[1].replace(" ", "_").lower()
            elif " del " in user_input:
                self.it = user_input.split(" del ")[1].replace(" ", "_").lower()
            return True
        return False

    #############################################################
    def clear_cost_maps(self, user_input):
    #############################################################
        from resources.utils import normalize
        user_input = normalize(user_input).lower()
        if "limpia el mapa" in user_input or "limpia" == user_input:
            return True
        else:
            return False

    #############################################################
    def check_follow(self, user_input):
    #############################################################
        from resources.utils import normalize
        user_input = normalize(user_input).lower()
        if "sigueme" == user_input or "ven conmigo" == user_input or "conmigo" == user_input:
            return True
        else:
            return False

    #############################################################
    def check_show(self, user_input):
    #############################################################
        user_input = user_input.lower()
        self.it = ""
        from resources.utils import normalize
        if "muestrame" in normalize(user_input):
            if "muestrame la " in normalize(user_input):
                self.it = user_input.split(" la ")[1].replace(" ", "_").lower()
            elif "muestrame el " in normalize(user_input):
                self.it = user_input.split(" el ")[1].replace(" ", "_").lower()
            elif "muestrame los " in normalize(user_input):
                self.it = user_input.split(" los ")[1].replace(" ", "_").lower()
            elif "muestrame las " in normalize(user_input):
                self.it = user_input.split(" las ")[1].replace(" ", "_").lower()
            for window in self.windows_names:
                if normalize(window.lower()) == normalize(self.it):
                    self.it = window
                    return True
            self.it = "Lo siento, no conozco la referencia " + self.it.replace("_", " ")
            return False
        else:
            return False

    #############################################################
    def check_common_questions(self, user_input):
    #############################################################
        from resources.utils import normalize
        user_input = normalize(user_input.lower())

        if user_input.split(" ")[0] == "teniente":        user_input = user_input.replace("teniente ","dime quien te ")
        if user_input.split(" ")[0] == "dime" and user_input.split(" ")[1] == "en" and user_input.split(" ")[2] == "este":    user_input = user_input.replace("dime en este ","dime quienes te ")

        if user_input.split(" ")[0] == "vine" and user_input.split(" ")[1] == "si":            user_input = user_input.replace("vine si ","")
        if user_input.split(" ")[0] == "vine":            user_input = user_input.replace("vine ","").replace("porque ","por que ")
        if user_input.split(" ")[0] == "dime" and user_input.split(" ")[1] == "si":            user_input = user_input.replace("dime si ","")
        if user_input.split(" ")[0] == "dime":            user_input = user_input.replace("dime ","").replace("porque ","por que ")
        if user_input.split(" ")[0] == "tiene" and user_input.split(" ")[1] == "si":            user_input = user_input.replace("tiene si ","")
        if user_input.split(" ")[0] == "tiene":           user_input = user_input.replace("tiene ","").replace("porque ","por que ")
        if user_input.split(" ")[0] == "time" and user_input.split(" ")[1] == "si":            user_input = user_input.replace("time si ","")
        if user_input.split(" ")[0] == "time":            user_input = user_input.replace("time ","").replace("porque ","por que ")

        if user_input.split(" ")[0] == "gente":            user_input = user_input.replace("gente ","quien te ")
        if user_input.split(" ")[0] == "siente":           user_input = user_input.replace("siente ","quien te ")
        if user_input.split(" ")[0] == "este":             user_input = user_input.replace("este ","quien te ")
        if user_input.split(" ")[0] == "porque":             user_input = user_input.replace("porque ","por que ")

        if user_input.split(" ")[0] == "en" and user_input.split(" ")[1] == "este": user_input = user_input.replace("en este ","quienes te ")


        self.it = []
        if len(user_input) > 5:
            for answer in self.common_questions:
                for question in self.common_questions[answer]:
                    question = question
                    if user_input in normalize(question.lower()):
                        self.it.append("say-" + answer)
                        self.it.append(question)
                        return True
        else:
            return False
