#!/usr/bin/env python3

import rospy
from os import walk
from os.path import expanduser

ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/data/"
dir, subdirs, archivos = next(walk(ruta))

#############################################################
#############################################################
class Transformer():
#############################################################
#############################################################

    #############################################################
    def __init__(self, nh):
    #############################################################

        if nh == None:
            rospy.init_node("Avatar")
            nodename = rospy.get_name()
            rospy.loginfo("%s started" % nodename)
        else:
            nodename = rospy.get_name()
            rospy.loginfo(" %s started on Transformers" % nodename)

        from transformers import AutoTokenizer, AutoModelForQuestionAnswering, pipeline

        the_model = 'mrm8488/distill-bert-base-spanish-wwm-cased-finetuned-spa-squad2-es'
        tokenizer = AutoTokenizer.from_pretrained(the_model, do_lower_case=False)
        model = AutoModelForQuestionAnswering.from_pretrained(the_model)
        self.nlp = pipeline('question-answering', model=model, tokenizer=tokenizer)

        self.answer = ""

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called')

    #############################################################
    def get_answer(self):
    #############################################################

        return self.answer

    #############################################################
    def check_move_command(self, user_input, score_min):
    #############################################################

        self.answer = ""
        contexto = ""
        for y in archivos:
            with open(ruta + y, "r") as f:
                contexto += f.read()
        try:
            self.answer = self.nlp({'question':user_input.lower(), 'context':contexto.lower()})
            if self.answer["score"] < score_min:
                return False
            return True
        except:
            return False


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    transformer = Transformer()
