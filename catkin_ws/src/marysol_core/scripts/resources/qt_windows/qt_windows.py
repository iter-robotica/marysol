#!/usr/bin/env python3
import os
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
from PyQt5 import QtMultimedia, uic, QtCore

from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QDesktopWidget

from os.path import expanduser
import numpy as np

from resources.qt_windows.MainWindow import Ui_MainWindow
from resources.qt_windows.TopWindow import Ui_TopWindow
from resources.qt_windows.KillButton import Ui_ExitButton

import subprocess

ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/renders/"

from os import walk
from os.path import expanduser
ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/renders/"
dir, subdirs, archivos = next(walk(ruta))

# pyuic5 killbutton.ui > KillButton.py
# pyuic5 mainwindow.ui > MainWindow.py
# pyuic5 topwindow.ui > TopWindow.py
# pyrcc5 qt.qrc -o qt_rc.py

def change_video_resolution(file, width_=800, height_=800):
    import cv2
    vid = cv2.VideoCapture(file)
    height = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
    width = vid.get(cv2.CAP_PROP_FRAME_WIDTH)
    if height != height_ or width != width_:
        print("Resolución: ", str(width) + " - " + str(height))
        print("Cambiamos resolución a: " + str(width_) + " - " + str(height_))
        import moviepy.editor as mp
        clip = mp.VideoFileClip(file)
        clip_resized = clip.resize((width_,height_)) # make the height 360px ( According to moviePy documenation The width is then computed so that the width/height ratio is conserved.)
        clip_resized.write_videofile(file.replace(".mp4", "_") + "temp.mp4")
        os.remove(file)
        os.rename(file.replace(".mp4", "_") + "temp.mp4", file)
    return file

class Intro(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        self.setWindowTitle("IntroMarysol")

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        videoWidget = QVideoWidget()
        videoWidget.setAspectRatioMode(0)

        widget = QWidget(self)
        self.setCentralWidget(widget)

        layout = QVBoxLayout()
        layout.addWidget(videoWidget)

        widget.setLayout(layout)
        self.mediaPlayer.setVideoOutput(videoWidget)

        self.file = ruta + "intro.mp4"
        self.mediaPlayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(self.file)))

        self.resize(450, 450)

        qtRectangle = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())

        self.mediaPlayer.play()

        self.show()

class KillButton(QMainWindow, Ui_ExitButton):
    def __init__(self):
        super().__init__()
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        self.setupUi(self)
        self.setWindowTitle("KillButton")

class TopWindow(QMainWindow, Ui_TopWindow):
    def __init__(self):
        super().__init__()
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        self.setupUi(self)
        self.setWindowTitle("MarysolTopWindow")
        self.hide_ticks = 0
        self.buttons = [self.avatar_button, self.map_button, self.media_button, self.debug_button, self.charging_button]
        self.active_buttons = [self.avatar_button, self.map_button, self.media_button, self.debug_button, self.charging_button]
        self.inactive_buttons = [self.debug_button, self.charging_button]

        #### Connect control buttons ####
        self.power_button.pressed.connect(self.start_stop)
        self.avatar_button.pressed.connect(lambda: self.isCheckeable(self.avatar_button))
        self.map_button.pressed.connect(lambda: self.isCheckeable(self.map_button))
        self.media_button.pressed.connect(lambda: self.isCheckeable(self.media_button))
        self.debug_button.pressed.connect(lambda: self.isCheckeable(self.debug_button))
        self.charging_button.pressed.connect(lambda: self.isCheckeable(self.charging_button))
        self.right_button.pressed.connect(lambda: self.closeEvent(0))

        self.battery_status.hide()

    def disable_buttons(self, buttons):
        for button in buttons:
            button.setEnabled(False)

    def enable_buttons(self, buttons):
        for button in buttons:
            button.setEnabled(True)

    def uncheck_but(self, button):
        for but in self.buttons:
            if not but == button and but.isChecked():
                but.setChecked(False)

    def isCheckeable(self, button):
        #self.wm.check_and_bring("visual_window")
        self.vw.setWindowFlag(Qt.FramelessWindowHint)
        self.vw.setWindowState(Qt.WindowFullScreen)
        if button in self.inactive_buttons and button in self.active_buttons:
            self.uncheck_but(button)
            if not button.isChecked():
                self.vw.show()
                self.vw.stackedWidget.setCurrentIndex(self.buttons.index(button))
            else:
                self.vw.hide()
        elif button in self.active_buttons:
            if self.power_button.isChecked():
                self.uncheck_but(button)
                if not button.isChecked():
                    self.vw.show()
                    self.vw.stackedWidget.setCurrentIndex(self.buttons.index(button))
                    if self.buttons.index(button) == 0:
                        self.hide_ticks = 0
                else:
                    self.vw.hide()
            else:
                button.setChecked(True)
        elif button in self.inactive_buttons:
            if not self.power_button.isChecked():
                self.uncheck_but(button)
                if not button.isChecked():
                    self.vw.show()
                    self.vw.stackedWidget.setCurrentIndex(self.buttons.index(button))
                else:
                    self.vw.hide()
            else:
                button.setChecked(True)

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("MarysolAvatarWindow")
        self.setWindowState(Qt.WindowFullScreen)
        self.setWindowFlag(Qt.FramelessWindowHint)

        self.player = QMediaPlayer()
        self.player.mediaStatusChanged.connect(self.mediaStatusChanged)
        self.player.setVideoOutput(self.VideoWidget)
        self.VideoWidget.setAspectRatioMode(0)
        self.last_Status = None

        self.playerConsole = QMediaPlayer()
        self.playerConsole.mediaStatusChanged.connect(self.mediaStatusChangedConsole)
        self.playerConsole.setVideoOutput(self.VideoConsole)
        self.VideoConsole.setAspectRatioMode(0)

        self.playerBack = QMediaPlayer()
        self.playerBack.mediaStatusChanged.connect(self.mediaStatusChangedBack)
        self.playerBack.setVideoOutput(self.VideoBack)
        self.VideoBack.setAspectRatioMode(0)

        self.maintenance_works.pressed.connect(self.MaintenanceWorks)

        self.BotText.setStyleSheet("color:rgb(118, 181, 196)")
        self.UserText.setStyleSheet("color:rgb(102, 218, 179)")

        self.subprocess_maintenance = None
        self.mic_inactive_label.show()
        self.talking_label.hide()
        self.listening_label.hide()
        self.processing_label.hide()
        self.awaiting_label.hide()
        self.bat4.hide()
        self.bat3.hide()
        self.bat2.hide()
        self.bat1.hide()

        self.USB_path = "_"

        self.video_list = {}
        self.add_videos(ruta)

        self.set_video("awaiting.mp4", self.playerBack)
        self.VideoWidget.show()

    def MaintenanceWorks(self):
        if not self.maintenance_works.isChecked():
            self.start_subprocess()
        else:
            self.stop_subprocess(self.subprocess_maintenance)

    def start_subprocess(self):
        self.set_video("LoadingScreen.mp4", self.playerConsole)
        self.consoleText = ""
        command = "python3 ~/marysol/catkin_ws/src/marysol_core/scripts/resources/maintenace_works.py --USB_path " + self.USB_path
        self.subprocess_maintenance = subprocess.Popen(command, shell=True)

    def stop_subprocess(self, subprocess):
        self.playerConsole.stop()
        import psutil
        process = psutil.Process(subprocess.pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()
        subprocess = None
        self.add_videos(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/speechs')

    def add_videos(self, path):
        from os import walk
        dir, subdirs, archivos = next(walk(path))
        for archivo in archivos:
            if ".mp4" in archivo:
                #print("Añadimos " + archivo + " al contenedor de videos")
                self.video_list[archivo] = QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(path + archivo))


    def mediaStatusChanged(self, Status):
        # if self.debug >= 4:
            # if Status == QMediaPlayer.UnknownMediaStatus:
            #      print("         Player_Status Marysol: UnknownMediaStatus")
            # elif Status == QMediaPlayer.NoMedia:
            #      print("         Player_Status Marysol: NoMedia")
            # elif Status == QMediaPlayer.LoadingMedia:
            #      print("         Player_Status Marysol: LoadingMedia")
            # elif Status == QMediaPlayer.LoadedMedia:
            #      print("         Player_Status Marysol: LoadedMedia")
            # elif Status == QMediaPlayer.StalledMedia:
            #      print("         Player_Status Marysol: StalledMedia")
            # elif Status == QMediaPlayer.BufferingMedia:
            #      print("         Player_Status Marysol: BufferingMedia")
            # elif Status == QMediaPlayer.BufferedMedia:
            #      print("         Player_Status Marysol: BufferedMedia")
            # elif Status == QMediaPlayer.EndOfMedia:
            #      print("         Player_Status Marysol: EndOfMedia")
            # elif Status == QMediaPlayer.InvalidMedia:
            #      print("         Player_Status Marysol: InvalidMedia")

        if Status == QMediaPlayer.EndOfMedia:
            # if self.debug >= 4:     print("        * Player: Hide plater ")
            self.VideoWidget.hide()
            if not self.player.isMuted():
                self.playing.data = False
                self.playing_pub.publish(self.playing)
            else:
                # To keep playing muted_video meanwhile external speech synthetiser is playing
                self.player.play()
        elif Status == QMediaPlayer.BufferedMedia or (Status == QMediaPlayer.LoadedMedia and self.last_Status == QMediaPlayer.LoadingMedia):# or Status == QMediaPlayer.LoadingMedia:
            # if self.debug >= 4:     print("        * Player: Show player ")
            self.VideoWidget.show()
            if not self.player.isMuted():
                self.playing.data = True
                self.playing_pub.publish(self.playing)
        self.last_Status = Status


    def mediaStatusChangedConsole(self, Status):
        if Status == QMediaPlayer.EndOfMedia and self.maintenance_works.isChecked():
            self.set_video("LoadingScreen.mp4", self.playerConsole)

    def mediaStatusChangedBack(self, Status):
        if Status == QMediaPlayer.EndOfMedia:
            self.set_video("awaiting.mp4", self.playerBack)

    def set_video(self, video, player):
        ## if self.debug >= 4:     print("         Player: set_video media " + str(video))
        if player.currentMedia() != self.video_list[video]:
            player.setMedia(self.video_list[video])
            # while player.currentMedia() != self.video_list[video]:
            #     rospy.sleep(0.25)
        player.play()

    def CreateButton(self, name, size, text, image_check=None, image_uncheck=None, checkeable=False, checked=False, maxXsize=2048, maxYsize=100, minXsize=10, minYsize=10):
        from PyQt5 import QtCore, QtGui, QtWidgets
        button = QtWidgets.QPushButton(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(button.sizePolicy().hasHeightForWidth())
        button.setSizePolicy(sizePolicy)
        button.setObjectName(name)
        font = QtGui.QFont()
        font.setPointSize(size)
        button.setFont(font)
        button.setCheckable(checkeable)
        button.setChecked(checked)
        button.setMaximumSize(QtCore.QSize(maxXsize, maxYsize))
        button.setMinimumSize(QtCore.QSize(minXsize, minYsize))
        if image_uncheck:
            if not image_check:
                image_check = image_uncheck
            button.setStyleSheet("#" + name + " \n"
        "{\n"
        "    selection-background-color: transparent;\n"
        "    border-image: url(:/marysol/images/" + image_uncheck + ".png);\n"
        "    background: none;\n"
        "    border: none;\n"
        "    background-repeat: none;\n"
        "}\n"
        "#" + name + ":checked\n"
        "{\n"
        "    border-image: url(:/marysol/images/" + image_check + ".png);\n"
        "}")
        else:
            button.setStyleSheet('QPushButton {background-color: #A3C1DA; color: gray;}')
            #button.setMaximumSize(QtCore.QSize(2048, 100))
            button.setText(text)
        return button

if __name__ == '__main__':
    app = QApplication([])

    topwindow = AddLoc()

    app.exec_()
