#!/usr/bin/env python3

import en_core_web_sm

nlp = en_core_web_sm.load()

import rospy
import spacy
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer
from chatterbot.response_selection import get_random_response, get_first_response, get_most_frequent_response
from os.path import expanduser

directory = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/scripts/resources/"

#############################################################
#############################################################
class ChatBotChatter():
#############################################################
#############################################################

    #############################################################
    def __init__(self, nh=None):
    #############################################################

        if nh == None:
            rospy.init_node("ChatBot")
            nodename = rospy.get_name()
            rospy.loginfo("%s started" % nodename)
        else:
            nodename = rospy.get_name()
            rospy.loginfo(" %s started on ChatBot" % nodename)

        # Create a new instance of a ChatBot
        self.chatbot = ChatBot(
            "Marysol",
            preprocessors=['chatterbot.preprocessors.clean_whitespace'],
            logic_adapters=[
                #'chatterbot.logic.TimeLogicAdapter',
                #'chatterbot.logic.MathematicalEvaluation',
                {
                    'import_path': 'chatterbot.logic.BestMatch',
                    'default_response': 'Lo siento, no he podido entenderte',
                    'maximum_similarity_threshold': 1.0
                },
                #{
                #    'import_path': 'resources.marysol_adapters.WikipediaAdapter',
                #    'language': 'es',
                #    'key_word': 'informacion sobre',
                #    'min_len': 20
                #},
                #{
                #    'import_path': 'resources.marysol_adapters.CommandAdapter',
                #    'language': 'es'
                #},
                #{
                #    'import_path': 'resources.marysol_adapters.TransformerAdapter',
                #    'score_min': 0.0
                #},
                #{
                #    'import_path': 'resources.marysol_adapters.BatteryAdapter',
                #},
                #{
                #    'import_path': 'resources.marysol_adapters.OutputVisionAdapter',
                #    'score_min': 0.2
                #}
            ]
        )
        # Create a new trainer for the chatbot
        trainer = ChatterBotCorpusTrainer(self.chatbot)
        # Train the chatbot based on the spanish corpus
        trainer.train("chatterbot.corpus.spanish")
        trainer = ListTrainer(self.chatbot)
        training_data = open(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/training_data/training_data.yml').read().splitlines()
        trainer.train(training_data)
        # Now we can export the data to a file
        #trainer.export_for_training('./marysol.yaml')

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called')

    #############################################################
    def get_response(self, user_input):
    #############################################################

        return self.chatbot.get_response(user_input)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    chatbotchatter = ChatBotChatter()
