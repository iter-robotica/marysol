#!/usr/bin/env python

import rospy
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseWithCovarianceStamped


#############################################################
#############################################################
class GetPoseFromFrame():
#############################################################
#############################################################

    #############################################################
    def __init__(self, nh):
    #############################################################

        if nh == None:
            rospy.init_node("Avatar")
            nodename = rospy.get_name()
            rospy.loginfo("%s started" % nodename)
        else:
            nodename = rospy.get_name()
            rospy.loginfo(" %s started on GetPoseFromFrame" % nodename)

        # internal data
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1.0))
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)


    #############################################################
    def GetPoseNewFrame(self, pose, original_frame, new_frame):
    #############################################################
        # 2 - Obtenemos la pose de este punto con respecto a footprint
        transform = self.tf_buffer.lookup_transform(new_frame, original_frame, rospy.Time(0), rospy.Duration(1.0))
        pose_transformed = tf2_geometry_msgs.do_transform_pose(pose, transform)
        return pose_transformed


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        get_pose_from_frame = GetPoseFromFrame()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
