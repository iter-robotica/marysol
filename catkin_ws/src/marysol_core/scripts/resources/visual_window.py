#!/usr/bin/env python3

from std_msgs.msg import UInt32MultiArray, String, Bool, Int16MultiArray, String
import numpy as np
import rospy
from resources.qt_windows.qt_windows import MainWindow

from os import walk
from os.path import expanduser

#ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/renders/"
#dir, subdirs, archivos = next(walk(ruta))

#############################################################
#############################################################
class VisualWindow(MainWindow):
#############################################################
#############################################################

    #############################################################
    def __init__(self, nh, debug):
    #############################################################

        super().__init__()

        if nh == None:
            rospy.init_node("VisualWindow")
            nodename = rospy.get_name()
            rospy.loginfo("%s started" % nodename)
        else:
            nodename = rospy.get_name()
            rospy.loginfo(" %s started on MainWindow" % nodename)

        #### subscriptions ####
        rospy.Subscriber("/speech/ShutUp", Bool, self.shutupCallback)
        rospy.Subscriber("/battery", Int16MultiArray, self.batteryCallback)
        self.battery_level = 0
        self.playing = Bool()
        self.playing_pub = rospy.Publisher('/speech/playing', Bool, queue_size=10)
        self.string_to_speech_pub = rospy.Publisher('/speech/string_to_speech', String, queue_size=10)
        rospy.Subscriber("/maintenance/console", String, self.consoleCallback)

        ### internal data ###
        self.botSentence = ""
        self.userSentence = ""
        self.consoleText = ""

        self.go_buttons = []
        self.loc_buttons = []
        self.del_buttons = []
        self.move_around_buttons = []

        self.video_buttons = []
        self.photo_buttons = []
        self.text_buttons = []
        self.web_buttons = []
        self.routine_buttons = []
        self.speech_buttons = []
        self.PDF_buttons = []

        self.USB_labels = []
        self.USB_buttons = []

        self.node_buttons = []
        self.node_labels = []

        self.mic_labels = []
        self.mic_buttons = []

        self.speaker_labels = []
        self.speaker_buttons = []

        self.debug = debug


    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called')

    #############################################################################
    def consoleCallback(self, msg):
    #############################################################################

        #print(msg.data)
        from datetime import datetime
        self.consoleText += str(datetime.now()).split(".")[0] + ": " + msg.data + "\n"
        if self.consoleText.count("\n") > 26:
            self.consoleText = self.consoleText.split("\n",2)[2]
        self.console.setText(self.consoleText)
        if msg.data == "* Renderizado finalizado":
            self.playerConsole.stop()
            self.maintenance_works.setChecked(False)
            self.stop_subprocess(self.subprocess_maintenance)
            self.subprocess_maintenance = None

    #############################################################
    def batteryCallback(self, msg):
    #############################################################

        mean = np.mean(msg.data) / 100
        p = np.polyfit([13.20, 14.337], [51, 97], 1)
        percent = mean * p[0] + p[1]
        self.battery_level = np.clip(percent, 0, 99)

    #############################################################
    def shutupCallback(self, msg):
    #############################################################

        if msg.data:
            self.player.stop()
            # if self.debug >= 4:     print("             VW: Recibimos señal de shutUp ")

    #############################################################
    def update(self, message):
    #############################################################

        if message != "":
            text = (message + ".mp4").lower().replace(" ","_")
            # print(text)
            if text.replace("speech_","") in self.video_list:
                # if self.debug >= 2:     print("             VW: Video existe " + text.replace("speech_",""))
                # print("Video existe")
                if self.player.isMuted():
                    self.player.setMuted(False)
                #print(text.replace("speech_",""))
                self.set_video(text.replace("speech_",""), self.player)
            else:
                # if self.debug >= 2:     print("             VW: Video no existe " + text.replace("speech_",""))
                # print("Video no existe")
                if not self.player.isMuted():
                    self.player.setMuted(True)
                string_to_speech = String()
                string_to_speech.data = message
                self.string_to_speech_pub.publish(string_to_speech)
                try:
                    rospy.wait_for_message("/speech/playing", Bool, timeout=20.0)
                    if "lo siento, no conozco el lugar" in message.lower():
                        self.set_video("lo_siento,_no_conozco_el_lugar_indeterminado.mp4", self.player)
                    elif "lo siento, no conozco la referencia" in message.lower():
                        self.set_video("lo_siento,_no_conozco_la_referencia_indeterminado.mp4", self.player)
                    elif "no tengo respuesta para" in message.lower():
                        self.set_video("no_tengo_respuesta_para_indeterminado.mp4", self.player)
                    elif "objetivo" in message.lower() and "alcanzado" in message.lower():
                        self.set_video("objetivo_punto_de_indeterminado_alcanzado.mp4", self.player)
                    elif "nueva localización" in message.lower():
                        self.set_video("nueva_localización,_indeterminado.mp4", self.player)
                    else:
                        self.set_video("muted_video.mp4", self.player)
                except:
                    pass

    #############################################################################
    def destroy_buttons_map(self):
    #############################################################################
        # if self.debug >= 2:     print("             VW: destroy_buttons_map ")
        for x in self.go_buttons:
            x.deleteLater()
        self.go_buttons = []
        for x in self.loc_buttons:
            x.deleteLater()
        self.loc_buttons = []
        for x in self.del_buttons:
            x.deleteLater()
        self.del_buttons = []
        for x in self.move_around_buttons:
            x.deleteLater()
        self.move_around_buttons = []

    #############################################################################
    def destroy_buttons_resources(self):
    #############################################################################

        # if self.debug >= 2:     print("             VW: destroy_buttons_resources ")
        for x in self.video_buttons:
            x.deleteLater()
        self.video_buttons = []
        for x in self.photo_buttons:
            x.deleteLater()
        self.photo_buttons = []
        for x in self.text_buttons:
            x.deleteLater()
        self.text_buttons = []
        for x in self.web_buttons:
            x.deleteLater()
        self.web_buttons = []
        for x in self.routine_buttons:
            x.deleteLater()
        self.routine_buttons = []
        for x in self.speech_buttons:
            x.deleteLater()
        self.speech_buttons = []
        for x in self.PDF_buttons:
            x.deleteLater()
        self.PDF_buttons = []

    #############################################################################
    def destroy_USB_elements(self):
    #############################################################################

        # if self.debug >= 2:     print("             VW: destroy_USB_elements ")
        for x in self.USB_labels:
            x.deleteLater()
        self.USB_labels = []
        for x in self.USB_buttons:
            x.deleteLater()
        self.USB_buttons = []

    #############################################################################
    def destroy_buttons_debug(self):
    #############################################################################

        # if self.debug >= 2:     print("             VW: destroy_buttons_debug ")
        for x in self.node_buttons:
            x.deleteLater()
        self.node_buttons = []
        for x in self.node_labels:
            x.deleteLater()
        self.node_labels = []

    #############################################################################
    def destroy_mic_elements(self):
    #############################################################################

        # if self.debug >= 2:     print("             VW: destroy_mic_elements ")
        for x in self.mic_labels:
            x.deleteLater()
        self.mic_labels = []
        for x in self.mic_buttons:
            x.deleteLater()
        self.mic_buttons = []

    #############################################################################
    def destroy_speaker_elements(self):
    #############################################################################

        # if self.debug >= 2:     print("             VW: destroy_speaker_elements ")
        for x in self.speaker_labels:
            x.deleteLater()
        self.speaker_labels = []
        for x in self.speaker_buttons:
            x.deleteLater()
        self.speaker_buttons = []


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    visual_window = VisualWindow()
    visual_window.spin()
