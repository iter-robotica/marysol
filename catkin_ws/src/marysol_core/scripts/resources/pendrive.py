#!/usr/bin/env python3

#############################################################
def get_usb_devices():
    import os
    from glob import glob
    sdb_devices = map(os.path.realpath, glob('/sys/block/sd*'))
    usb_devices = (dev for dev in sdb_devices
                   if any(['usb' in dev.split('/')[5],
                           'usb' in dev.split('/')[6]]))
    return dict((os.path.basename(dev), dev) for dev in usb_devices)

def get_mount_points(devices=None):
    import subprocess
    devices = devices or get_usb_devices()  # if devices are None: get_usb_devices
    output = subprocess.check_output(['mount']).splitlines()
    output = [tmp.decode('UTF-8') for tmp in output]

    def is_usb(path):
        return any(dev in path for dev in devices)
    usb_info = (line for line in output if is_usb(line.split()[0]))
    return [(info.split()[2]) for info in usb_info]
#############################################################
if __name__ == '__main__':
    print(get_mount_points())
