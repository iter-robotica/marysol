#!/usr/bin/env python3

import os
import subprocess
from os.path import expanduser

#############################################################
#############################################################
class WindowsManagment():
#############################################################
#############################################################

    #############################################################
    def __init__(self, windows_names, debug):
    #############################################################

        self.windows_names = windows_names

        self.subprocess = None

        self.debug = debug

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called wm')

    #############################################################
    def check_window(self, dict, window):
    #############################################################
        
        if os.system("wmctrl -l| grep '" + dict[window][0] + "' >/dev/null 2>&1") != 0:
            return False
        else:
            return True

    #############################################################
    def check_window_simple(self, window):
    #############################################################
        if os.system("wmctrl -l| grep '" + window + "' >/dev/null 2>&1") != 0:
            #print("Check False")
            return False
        else:
            #print("Check True")
            return True

    #############################################################
    def bring_window(self, window):
    #############################################################
        os.system("wmctrl -a '" + window + "' >/dev/null 2>&1")

    #############################################################
    def check_pidof(self, window):
    #############################################################
        if os.system("pidof " + window + " > /dev/null") != 0:
            #print("Pidof False")
            return False
        else:
            #print("Pidof True")
            return True

    #############################################################
    def check_and_bring(self, window):
    #############################################################
        index = None
        for element in range(len(self.windows_names)):
            if window in self.windows_names[element]:
                index = element
        dict = self.windows_names[index]

        if self.check_window(dict, window):
            os.system("wmctrl -a '" + dict[window][0] + "' >/dev/null 2>&1")
        else:
            self.open(dict, window)

    #############################################################
    def open(self, command):
    #############################################################
        if "google-chrome" in command:
            command += " &"
        self.subprocess = subprocess.Popen(command, shell=True)

    #############################################################
    def close(self):
    #############################################################
        import psutil
        process = psutil.Process(self.subprocess.pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()
        self.subprocess = None
        os.system("pkill chrome")


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        window_management = WindowsManagment()
        window_management.spin()
    except rospy.ROSInterruptException:
        pass
