#!/usr/bin/env python3

import rospy
from std_msgs.msg import UInt8MultiArray, Bool, Int16MultiArray
import roslib
import threading

#############################################################
#############################################################
class StripLeds():
#############################################################
#############################################################

    """
    This class outputs a leds state according to:
        1º  The robot has crashed - Blinking red
        2º  Charging - wipe green
        3º  Low battery - orange
        4º  Awaiting user voice command - Fireworks
        5º  Command voice accepted - Blinking green
        6º  Command voice without answer - orange
        7º  On the way - RainbowCycle
        8º  Recovery behaviour - TheaterChase
        9º  No task - Elastic Rainbow
    """

    #############################################################
    def __init__(self, nh, debug):
    #############################################################
        if nh == None:
            rospy.init_node("Avatar")
            nodename = rospy.get_name()
            rospy.loginfo("%s started" % nodename)
        else:
            nodename = rospy.get_name()
            rospy.loginfo(" %s started on StripLeds" % nodename)

        #### parameters #######
        self.battery_low = rospy.get_param("~battery_low", 12.8)

        # internal_data
        self.battery_level = 100.0
        self.on_charge = False
        self.state = ""
        self.task = UInt8MultiArray()
        # Commands to control LEDs
        self.static_task = None
        self.ON = True
        self.Task = "1"
        self.lastTask = "2"
        self.stop = Bool()

        self.debug = debug

        # subscriptions
        self.task_pub = rospy.Publisher('/commands/Leds', UInt8MultiArray, queue_size=10)
        rospy.Subscriber('/state', Int16MultiArray, self.stopCallback)

        self.start_spin()

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called')
        Task = "off"
        self.load_task(Task, nada="S")

    #############################################################
    def start_spin(self):
    #############################################################

        y = threading.Thread(target=self.handle_leds_tasks, daemon=True)
        y.start()

    #############################################################
    def handle_leds_tasks(self):
    #############################################################
        r = rospy.Rate(5)
        ###### main loop  ######
        Task = None
        while not rospy.is_shutdown():
            if self.ON:
                if True:
                    if self.stop.data:
                        Task = "blinking_red"

                    elif self.state == None:
                        Task = "3"

                    elif self.state == "bring_up":
                        Task = "4"

                    elif self.state == "shut_down":
                        Task = "4"

                    elif self.battery_level < 40:
                        Task = "blinking_red"

                    else:
                        if self.static_task != None:
                            if "none" == self.static_task.split("-")[0]:
                                Task = "white_line"
                            elif "say" == self.static_task.split("-")[0]:
                                try:
                                    Task = self.static_task.split("-")[2]
                                except:
                                    Task = "simple_white"
                            elif "go" == self.static_task.split("-")[0] or "going" == self.static_task.split("-")[0]:
                                Task = "blue"
                            elif "spin" == self.static_task.split("-")[0]:
                                Task = "blue"
                            elif "speech" == self.static_task.split("-")[0]:
                                Task = "simple_white"
                            elif "wait" == self.static_task.split("-")[0]:
                                Task = "white_line"
                            elif "show" == self.static_task.split("-")[0]:
                                Task = "simple_white"
                            elif "follow" == self.static_task.split("-")[0]:
                                Task = "moving"
                            elif "await" == self.static_task.split("-")[0]:
                                if self.Task == "simple_green":
                                    Task = "wipe_orange"
                                elif self.Task != "wipe_orange" and self.Task != "simple_green":
                                    Task = "simple_green"
                            else:
                                Task = "white_line"

                    if Task != None:
                        self.load_task(Task)
            r.sleep()

    #############################################################
    def Activate(self):
    #############################################################
        #print("1 - Activate ")
        self.ON = True

    #############################################################
    def Deactivate(self):
    #############################################################
        #print("1 - Deactivate ")
        self.ON = False
        Task = "off"
        self.load_task(Task)

    #############################################################
    def set_static_task(self, task):
    #############################################################
        # print("2 - RoutineTask " + task)
        self.static_task = task

    #############################################################
    def set_state(self, state):
    #############################################################
        #print("4 - State " + str(state))
        self.state = state

    #############################################################
    def set_task(self, core_command):
    #############################################################
        self.task.data = []
        self.task.data.append(int(core_command.split("-")[0]))
        self.task.data.append(int(core_command.split("-")[1]))
        self.task.data.append(int(core_command.split("-")[2]))
        self.task.data.append(int(core_command.split("-")[3]))
        self.task.data.append(int(core_command.split("-")[4]))
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)

    #############################################################
    def load_task(self, Task):
    #############################################################

        self.Task = Task
        if Task != self.lastTask:

            # print("     " + " Tarea " + str(Task))
            # if self.debug >= 2:     print("             LEDS: load_task " + str(self.Task))

            if Task == "off":
                self.pub_led_task(0,0,0,0,50)   # LESs OFF
            elif Task == "fireworks":
                self.pub_led_task(0,0,0,8,25)   # Fireworks
            elif Task == "wipe_green":
                self.pub_led_task(255,0,0,2,50)   # Wipe green
            elif Task == "wipe_orange":
                self.pub_led_task(80,255,10,2,60)   # Wipe orange
            elif Task == "failed":
                self.pub_led_task(40,255,5,1,50)   # Simple strong orange
            elif Task == "ok":
                self.pub_led_task(255,0,0,1,25)   # green
            elif Task == "white_line":
                self.pub_led_task(255,255,255,13,35) # Line
            elif Task == "succed":
                self.pub_led_task(255,0,0,1,25)   # green
            elif Task == "simple_white":
                self.pub_led_task(50,50,50,1,50)   # Simple white
            elif Task == "simple_green":
                self.pub_led_task(255,20,20,1,50)   # Simple green
            elif Task == "blinking_red":
                self.pub_led_task(0,255,0,12,50)   # green
            elif Task == "blinking_orange":
                self.pub_led_task(80,180,10,12,50)   # green
            elif Task == "blue":
                self.pub_led_task(31,31,127,1,50)   # Simple Blue
            elif Task == "moving":
                self.pub_led_task(0,0,0,7,50)   # TheaterChase
            elif Task == "awaiting":
                self.pub_led_task(255,0,0,12,20)   # green
            else:
                self.pub_led_task(0,0,0,int(Task),50)   # rainbowFade2WhiteTask

            self.lastTask = self.Task

    #############################################################
    def pub_led_task(self, green, red, blue, task, period):
    #############################################################
        self.task.data = []
        self.task.data.append(green)
        self.task.data.append(red)
        self.task.data.append(blue)
        self.task.data.append(task)
        self.task.data.append(period)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)
        self.task_pub.publish(self.task)
        rospy.sleep(0.025)

    #############################################################
    def Battery(self, level):
    #############################################################
        #print("Battery " + str(level))
        self.battery_level = level
        #self.select_task()
        """
        LiFePo Battery 4x nominal cell (3.2) = 12.8 v
        Set Limit at 12.8 v.
        """

    #############################################################
    def stopCallback(self, msg):
    #############################################################
        if msg.data[5] == 0:
            self.stop.data = False
        elif msg.data[5] == 1:
            self.stop.data = True

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        strip_leds = StripLeds()
        strip_leds.spin()
    except rospy.ROSInterruptException:
        pass
