#!/usr/bin/env python3

def normalize(sentence):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        sentence = sentence.replace(a, b)
    return sentence

def get_word_gender(word):
    import yaml
    words = []
    with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/scripts/resources/words_gender.yaml") as file:
        words = yaml.load(file, Loader=yaml.FullLoader)
    if word in words["male"]:
        return 0
    if word in words["female"]:
        return 1
    if word in words["plural_male"]:
        return 2
    if word in words["plural_female"]:
        return 3
    return 4

def get_indefinite_article(word):
    word_gender = get_word_gender(word)
    switcher={
        0:'un',
        1:'una',
        2:'unos',
        3:'unas',
        4:''
    }
    return switcher.get(word_gender,"Invalid gender")

def battery_level():
    from std_msgs.msg import Int16MultiArray
    import rospy
    import numpy as np
    try:
        battery = rospy.wait_for_message("/battery", Int16MultiArray, timeout=3.0)
        mean = np.mean(battery.data) / 100
        p = np.polyfit([13.20, 14.337], [51, 97], 1)
        percent = mean * p[0] + p[1]
        return percent
    except:
        return 0

def quaternion_from_euler(x,y,z,dg):
    from scipy.spatial.transform import Rotation
    Rot = Rotation.from_euler('xyz', [x, y, z], degrees=dg)
    return Rot.as_quat()

def euler_from_quaternion(q,dg):
    from scipy.spatial.transform import Rotation
    Rot = Rotation.from_quat([q.x,q.y,q.z,q.w])
    return Rot.as_euler('zyx',degrees=dg)

#############################################################
def string2speech(msg):
#############################################################
    import vlc
    from gtts import gTTS
    import os
    import os.path
    import time
    import rospy
    import rospkg
    rospack = rospkg.RosPack()
    speech = rospack.get_path("marysol_speech") + "/media/voice/" + msg + ".mp3"
    speech = speech.replace(" ","_").lower()
    if not os.path.isfile(speech):
        #print("creamos " + speech)
        language = 'es'
        myobj = gTTS(text=msg, lang=language, slow=False)
        myobj.save(speech)
    else:
        pass
        #print("cargamos " + speech)
    while not os.path.exists(speech):
        pass
    p = vlc.MediaPlayer(speech)
    p.play()

#############################################################
def sec2time(t):
#############################################################
    day = t//86400
    hour = (t-(day*86400))//3600
    minit = (t - ((day*86400) + (hour*3600)))//60
    seconds = t - ((day*86400) + (hour*3600) + (minit*60))
    time_total = []
    time_total.append(day)
    time_total.append(hour)
    time_total.append(minit)
    time_total.append(seconds)
    return time_total

#############################################################
def get_usb_devices():
    import os
    from glob import glob
    sdb_devices = map(os.path.realpath, glob('/sys/block/sd*'))
    usb_devices = (dev for dev in sdb_devices
                   if any(['usb' in dev.split('/')[5],
                           'usb' in dev.split('/')[6]]))
    return dict((os.path.basename(dev), dev) for dev in usb_devices)
def get_mount_points(devices=None):
    import subprocess
    devices = devices or get_usb_devices()  # if devices are None: get_usb_devices
    output = subprocess.check_output(['mount']).splitlines()
    output = [tmp.decode('UTF-8') for tmp in output]
    def is_usb(path):
        return any(dev in path for dev in devices)
    usb_info = (line for line in output if is_usb(line.split()[0]))
    return [(info.split()[2]) for info in usb_info]
#############################################################

#############################################################
def copytree(src, dest):
#############################################################
    import shutil
    shutil.copytree(src, dest)

#############################################################
def get_active_speakers():
#############################################################
    import subprocess
    # Toggle default device to the next device (wrap around the list)
    cards_info = subprocess.run(['pacmd','list-sinks'], stdout=subprocess.PIPE)
    card_indexes = subprocess.run(['grep', 'index'], stdout=subprocess.PIPE, input=cards_info.stdout)
    indexes_list = card_indexes.stdout.decode().splitlines()
    indices = []
    for index in indexes_list:
        indices.append(index.split("index: ")[1])
    card_descriptions = subprocess.run(['grep', 'device.description'], stdout=subprocess.PIPE, input=cards_info.stdout)
    nombres = []
    for x in range(str(card_descriptions).count('device.description = "')):
        nombres.append(str(card_descriptions).split('device.description = "')[x+1].split('"')[0].split("(")[0])
    active_speakers = {}
    x = 0
    for nombre in nombres:
        active_speakers[nombre] = indices[x]
        x += 1
    return active_speakers

#############################################################
def set_speaker(next_default_index):
#############################################################
    import subprocess
    subprocess.run(['pacmd','set-default-sink %s' %(next_default_index)], stdout=subprocess.PIPE)
    # Move all existing applications to the new default device
    inputs_info = subprocess.run(['pacmd','list-sink-inputs'], stdout=subprocess.PIPE)
    inputs_indexes = subprocess.run(['grep', 'index'], stdout=subprocess.PIPE, input=inputs_info.stdout)
    inputs_indexes_list = inputs_indexes.stdout.decode().splitlines()
    for line in inputs_indexes_list:
        input_index =  (line.split("index: ",1)[1])
        print(subprocess.run(['pacmd','move-sink-input %s %s' %(input_index, next_default_index)], stdout=subprocess.PIPE))

#############################################################
def get_active_mics():
#############################################################
    import subprocess
    # Toggle default device to the next device (wrap around the list)
    cards_info = subprocess.run(['pacmd','list-sources'], stdout=subprocess.PIPE)
    card_indexes = subprocess.run(['grep', 'index'], stdout=subprocess.PIPE, input=cards_info.stdout)
    indexes_list = card_indexes.stdout.decode().splitlines()
    indices = []
    for index in indexes_list:
        indices.append(index.split("index: ")[1])
    card_descriptions = subprocess.run(['grep', 'device.description'], stdout=subprocess.PIPE, input=cards_info.stdout)
    nombres = []
    for x in range(str(card_descriptions).count('device.description = "')):
        nombres.append(str(card_descriptions).split('device.description = "')[x+1].split('"')[0].split("(")[0])
    active_speakers = {}
    x = 0
    for nombre in nombres:
        active_speakers[nombre] = indices[x]
        x += 1
    return active_speakers

#############################################################
def set_mic(next_default_index):
#############################################################
    import subprocess
    subprocess.run(['pacmd','set-default-source %s' %(next_default_index)], stdout=subprocess.PIPE)
    # Move all existing applications to the new default device
    inputs_info = subprocess.run(['pacmd','list-source-outputs'], stdout=subprocess.PIPE)
    inputs_indexes = subprocess.run(['grep', 'index'], stdout=subprocess.PIPE, input=inputs_info.stdout)
    inputs_indexes_list = inputs_indexes.stdout.decode().splitlines()
    for line in inputs_indexes_list:
        input_index =  (line.split("index: ",1)[1])
        print(subprocess.run(['pacmd','move-source-output %s %s' %(input_index, next_default_index)], stdout=subprocess.PIPE))
