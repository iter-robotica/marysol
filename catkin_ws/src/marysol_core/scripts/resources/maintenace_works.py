#!/bin/env python3
import os
from os.path import expanduser
from os import walk
import shutil
import rospy
from std_msgs.msg import String
import time
import argparse
import yaml

#############################################################
#############################################################
class MaintenanceWorks():
#############################################################
#############################################################

    #############################################################
    def __init__(self, args):
    #############################################################

        super().__init__()

        rospy.init_node("maintenance_works")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        self.console = rospy.Publisher('/maintenance/console', String, queue_size=10)
        self.string = String()
        self.USB_path = args.USB_path

    #############################################################
    def start(self):
    #############################################################

        self.publish_console_string(" ")
        self.publish_console_string("* Comenzando renderizado")
        self.publish_console_string("   NO DESCONECTE LA UNIDAD USB")
        # for x in range(35):
        #     self.publish_console_string("   NO DESCONECTE LA UNIDAD USB    NO DESCONECTE LA UNIDAD USB " + str(x) + "* Comenzando renderizado")
        if self.USB_path != "_":

            self.common_questions = {}
            with open(self.USB_path + "/media/common_questions.yaml") as file:
                self.common_questions = yaml.load(file, Loader=yaml.FullLoader)
            from gtts import gTTS
            for file in self.common_questions:
                speech = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/voice/" + str(file) + ".mp3"
                speech = speech.replace(" ","_").lower()
                if not os.path.exists(speech.replace("voice/","renders/").replace(".mp3", ".mp4")):
                    if not os.path.exists(speech):
                        self.publish_console_string("+ Generando archivo de audio '" + str(file + ".mp3").replace("_", " ") + "'")
                        print("Add - " + file)
                        language = 'es'
                        myobj = gTTS(text=str(file), lang=language, tld=language, slow=False)
                        myobj.save(speech)
                # else:
                #     if os.path.exists(speech):
                #         print("Del - " + file)
                #         self.string.data = "- Eliminamos archivo de audio " + str(file) + ".mp3"
                #         self.console.publish(self.string)
                #         os.remove(speech)


            ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/"
            dir, subdirs, archivos = next(walk(ruta + "voice/"))
            for archivo in archivos:
                if ", no encontrado" in archivo or "nueva_localización," in archivo or "lo_siento,_no_conozco" in archivo or "no_tengo_respuesta_para" in archivo or "reinicio_en_" in archivo or "no_se_ha_podido_localizar" in archivo or "no_se_ha_podido_conectar" in archivo or "speech_" in archivo or "no_está_definida" in archivo or ("objetivo" in archivo and "alcanzado" in archivo):
                    self.string.data = "- Eliminamos archivo de audio " + archivo
                    self.console.publish(self.string)
                    if os.path.exists(ruta + "voice/" + archivo):
                        os.remove(ruta + "voice/" + archivo)
                    if os.path.exists(ruta + "wav/" + archivo):
                        os.remove(ruta + "wav/" + archivo)
                else:
                    print(archivo)
                    if not os.path.exists(ruta + "renders/" + archivo.replace(".mp3", ".mp4")):
                        if not os.path.exists(ruta + "wav/" + archivo.replace(".mp3", ".wav")):
                            self.publish_console_string("+ Generando archivo de audio '" + archivo.replace(".mp3", ".wav").replace("_", " ") + "'")
                            os.system("ffmpeg -i " + ruta + "voice/" + archivo + " " + ruta + "wav/" + archivo.replace(".mp3", ".wav") + " >/dev/null 2>&1")
                        self.publish_console_string(" + Generando archivo de video '" + archivo.replace(".mp3", ".mp4").replace("_", " ") + "'")
                        mhd_ruta = expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/"
                        os.system("python3 " + mhd_ruta + "/FLAME/voca/run_voca.py --tf_model_fname '" + mhd_ruta + "FLAME/voca/model/gstep_52280.model' --ds_fname '" + mhd_ruta + "FLAME/voca/ds_graph/output_graph.pb' --audio_fname '" + ruta + "wav/" + archivo.replace(".mp3", ".wav") + "' --template_fname '" + mhd_ruta + "FLAME/voca/FLAME_samples/VOCA_template_04.ply' --condition_idx 3 --uv_template_fname '" + mhd_ruta + "FLAME/voca/template/marysol.obj' --texture_img_fname '" + mhd_ruta + "FLAME/voca/template/marysol.png' --out_path '" + ruta + "renders/" + archivo.replace(".mp3", "/") + "'" + " >/dev/null 2>&1")
                        #os.system(mhd_ruta + "virtualenv/voca/bin/python3 " + mhd_ruta + "/FLAME/voca/run_voca.py --tf_model_fname '" + mhd_ruta + "FLAME/voca/model/gstep_52280.model' --ds_fname '" + mhd_ruta + "FLAME/voca/ds_graph/output_graph.pb' --audio_fname '" + ruta + "wav/" + archivo.replace(".mp3", ".wav") + "' --template_fname '" + mhd_ruta + "FLAME/voca/FLAME_samples/VOCA_template_04.ply' --condition_idx 3 --uv_template_fname '" + mhd_ruta + "FLAME/voca/template/marysol.obj' --texture_img_fname '" + mhd_ruta + "FLAME/voca/template/marysol.png' --out_path '" + ruta + "renders/" + archivo.replace(".mp3", "/") + "'" + " >/dev/null 2>&1")
                        if os.path.exists(ruta + "renders/" + archivo.replace(".mp3", "") + "/video.mp4"):
                            os.rename(ruta + "renders/" + archivo.replace(".mp3", "") + "/video.mp4", ruta + "renders/" + archivo.replace(".mp3", ".mp4"))
                            shutil.rmtree(ruta + "renders/" + archivo.replace(".mp3", ""))
                            if os.path.isfile(ruta + "voice/" + archivo.replace(".txt", ".mp3").replace(" ", "_")):
                                os.remove(ruta + "voice/" + archivo.replace(".txt", ".mp3").replace(" ", "_"))
                            if os.path.isfile(ruta + "wav/" + archivo.replace(".txt", ".wav").replace(" ", "_")):
                                os.remove(ruta + "wav/" + archivo.replace(".txt", ".wav").replace(" ", "_"))

            ruta_speechs = self.USB_path + "/media/speechs/"
            dir, subdirs, archivos = next(walk(ruta_speechs))
            for archivo in archivos:
                if not os.path.exists(ruta_speechs + archivo.replace(".txt", ".mp4")):
                    if os.path.isfile(ruta + "voice/" + archivo):
                        os.remove(ruta + "voice/" + archivo)
                    from gtts import gTTS
                    self.publish_console_string("+ Generando archivo de audio '" + archivo.replace(".txt", ".mp3").replace("_", " ") + "'")
                    speech = ""
                    with open(ruta_speechs + archivo) as f:
                        lines = f.readlines()
                        for line in lines:
                            speech += line.replace("\n"," ")
                    language = 'es'
                    myobj = gTTS(text=str(speech), lang=language, slow=False)
                    myobj.save(ruta + "voice/" + archivo.replace(".txt", ".mp3").replace(" ", "_"))
                    self.publish_console_string(" + Generando archivo de audio '" + archivo.replace(".txt", ".wav").replace("_", " ") + "'")
                    os.system("ffmpeg -i " + ruta + "voice/" + archivo.replace(".txt", ".mp3") + " " + ruta + "wav/" + archivo.replace(".txt", ".wav") + " -y >/dev/null 2>&1")

                    print(archivo)
                    self.publish_console_string("  + Generando archivo de video '" + archivo.replace(".txt", ".mp4").replace("_", " ") + "'")
                    mhd_ruta = expanduser("~") + "/marysol/catkin_ws/marysol_heavy_data/"
                    os.system("python3 " + mhd_ruta + "/FLAME/voca/run_voca.py --tf_model_fname '" + mhd_ruta + "FLAME/voca/model/gstep_52280.model' --ds_fname '" + mhd_ruta + "FLAME/voca/ds_graph/output_graph.pb' --audio_fname '" + ruta + "wav/" + archivo.replace(".txt", ".wav") + "' --template_fname '" + mhd_ruta + "FLAME/voca/FLAME_samples/VOCA_template_04.ply' --condition_idx 3 --uv_template_fname '" + mhd_ruta + "FLAME/voca/template/marysol.obj' --texture_img_fname '" + mhd_ruta + "FLAME/voca/template/marysol.png' --out_path '" + ruta + "renders/" + archivo.replace(".txt", "/") + "'" + " >/dev/null 2>&1")
                    if os.path.exists(ruta + "renders/" + archivo.replace(".txt", "") + "/video.mp4"):
                        shutil.copy(ruta + "renders/" + archivo.replace(".txt", "") + "/video.mp4", ruta_speechs + archivo.replace(".txt", ".mp4"))
                        #os.rename(ruta + "renders/" + archivo.replace(".txt", "") + "/video.mp4", ruta_speechs + archivo.replace(".txt", ".mp4"))
                        shutil.rmtree(ruta + "renders/" + archivo.replace(".txt", ""))
                        if os.path.isfile(ruta + "voice/" + archivo.replace(".txt", ".mp3").replace(" ", "_")):
                            os.remove(ruta + "voice/" + archivo.replace(".txt", ".mp3").replace(" ", "_"))
                        if os.path.isfile(ruta + "wav/" + archivo.replace(".txt", ".wav").replace(" ", "_")):
                            os.remove(ruta + "wav/" + archivo.replace(".txt", ".wav").replace(" ", "_"))

            self.publish_console_string("+ Copiamos contenido de la unidad USB " + self.USB_path.split("/")[3].capitalize() + " al robot")
            if os.path.exists(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media'):
                shutil.rmtree(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media')
            shutil.copytree(self.USB_path + "/media", expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media')

            ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_speech/media/"
            dir, subdirs, archivos = next(walk(ruta + "renders/"))
            for subdir in subdirs:
                if os.path.isdir(ruta + "renders/" + subdir):
                    self.publish_console_string("Eliminamos " + subdir)
                    shutil.rmtree(ruta + "renders/" + subdir)

            self.publish_console_string("- Vaciando papelera de reciclaje")
            os.system("trash-empty >/dev/null 2>&1")

        else:
            self.publish_console_string("Error: No se ha seleccionado una unidad USB")

        self.publish_console_string("* Renderizado finalizado")

    #############################################################
    def publish_console_string(self, string):
    #############################################################

        self.string.data = string
        self.console.publish(self.string)
        rospy.sleep(0.1)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    parser = argparse.ArgumentParser()
    parser.add_argument("--USB_path", help="USB_path ")
    args = parser.parse_args()

    maintenance_works = MaintenanceWorks(args)
    maintenance_works.start()
