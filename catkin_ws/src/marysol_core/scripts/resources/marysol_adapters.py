#!/usr/bin/env python3
from chatterbot.logic import LogicAdapter
from chatterbot.conversation import Statement
import wikipedia
import re
from os import walk
from os.path import expanduser

ruta = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/data/"
dir, subdirs, archivos = next(walk(ruta))

def normalize(sentence):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        sentence = sentence.replace(a, b)
    return sentence

def get_word_gender(word):
    import yaml
    words = []
    with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/scripts/resources/words_gender.yaml") as file:
        words = yaml.load(file, Loader=yaml.FullLoader)
    if word in words["male"]:
        return 0
    if word in words["female"]:
        return 1
    if word in words["plural_male"]:
        return 2
    if word in words["plural_female"]:
        return 3
    return 4

def get_indefinite_article(word):
    word_gender = get_word_gender(word)
    switcher={
        0:'un',
        1:'una',
        2:'unos',
        3:'unas',
        4:''
    }
    return switcher.get(word_gender,"Invalid gender")

#############################################################################
class WikipediaAdapter(LogicAdapter):
#############################################################################
    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)
        self.language = kwargs.get('language')
        self.min_len = kwargs.get('min_len')
        self.key_word = kwargs.get('key_word')

    def can_process(self, statement):
        sentence = normalize(statement.text)
        if self.key_word in sentence:
            return True
        else:
            return False

    def process(self, input_statement, additional_response_selection_parameters):
        input_statement.text = normalize(input_statement.text)
        response_statement = Statement(text="")
        try:
            wikipedia.set_lang(self.language)
            search = wikipedia.search(sentence.split(ask_question)[1].split(" ")[2])
            article = wikipedia.page(search[0])
            sentences = 1
            short = ""
            while len(short) < self.min_len:
                short = wikipedia.summary(article.title, sentences=sentences)
                short = re.sub("[\(\[].*?[\)\]]", "", short)
                if "." in short:
                    short = short.split(".")[0]
                sentences += 1
            response_statement.text += short
        except:
            response_statement.text += "Lo siento, hay un problema con la conexión"

        response_statement.confidence = 0.99

        return response_statement

#############################################################################
class CommandAdapter(LogicAdapter):
#############################################################################
    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)
        self.language = kwargs.get('language')

    def can_process(self, statement):
        import yaml
        words = []
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/commands.yaml") as file:
            words = yaml.load(file, Loader=yaml.FullLoader)
        command_ = statement.text.split(" ")[0]
        for command in words["commands"]:
            if command == command_ and (" la " in statement.text or " el " in statement.text):
                return True
        return False

    def process(self, input_statement, additional_response_selection_parameters):
        response_statement = Statement(text="place")
        if " la " in input_statement.text:
            response_statement.text += normalize(input_statement.text.split(" la")[1].replace(" ", "_"))
        elif " el " in input_statement.text:
            response_statement.text += normalize(input_statement.text.split(" el")[1].replace(" ", "_"))
        response_statement.confidence = 0.98

        return response_statement

#############################################################################
class TransformerAdapter(LogicAdapter):
#############################################################################
    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)
        self.score_min = kwargs.get('score_min')
        self.answer = ""

    def can_process(self, statement):
        from transformers import AutoTokenizer, AutoModelForQuestionAnswering, pipeline
        the_model = 'mrm8488/distill-bert-base-spanish-wwm-cased-finetuned-spa-squad2-es'
        tokenizer = AutoTokenizer.from_pretrained(the_model, do_lower_case=False)
        model = AutoModelForQuestionAnswering.from_pretrained(the_model)
        nlp = pipeline('question-answering', model=model, tokenizer=tokenizer)
        contexto = ""
        for y in archivos:
            with open(ruta + y, "r") as f:
                contexto += f.read()
        try:
            self.answer = nlp({'question':statement.text.lower(), 'context':contexto.lower()})
            if self.answer["score"] < self.score_min:
                return False
            return True
        except:
            return False

    def process(self, input_statement, additional_response_selection_parameters):
        response_statement = Statement(text=self.answer["answer"])
        response_statement.confidence = self.answer["score"]

        return response_statement

#############################################################################
class BatteryAdapter(LogicAdapter):
#############################################################################
    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)
        self.battery = None

    def can_process(self, statement):
        sentence = normalize(statement.text)
        if ("nivel" in sentence or "voltaje" in sentence) and "bateria" in sentence:
            return True

    def process(self, input_statement, additional_response_selection_parameters):
        from std_msgs.msg import Int16MultiArray
        import rospy
        import numpy as np
        response_statement = Statement(text="")
        try:
            self.battery = rospy.wait_for_message("/battery", Int16MultiArray, timeout=3.0)
            mean = np.mean(self.battery.data) / 100
            sentence = normalize(input_statement.text)
            if "voltios" in sentence or "voltaje" in sentence:
                response_statement.text += "El voltaje de la batería es de " + ("%.2f" % mean) + " voltios"
            else:
                p = np.polyfit([13.20, 14.337], [51, 97], 1)
                percent = mean * p[0] + p[1]
                response_statement.text += "El nivel de la batería es del " + ("%.2f" % percent) + " por ciento"
        except:
            response_statement.text += "Hay un fallo con el sistema de medición de la batería"

        response_statement.confidence = 0.99

        return response_statement


#############################################################################
class OutputVisionAdapter(LogicAdapter):
#############################################################################
    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)
        self.score_min = kwargs.get('score_min')
        self.objects = None

    def can_process(self, statement):
        sentence = normalize(statement.text)
        import yaml
        words = []
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/commands.yaml") as file:
            words = yaml.load(file, Loader=yaml.FullLoader)
        for command in words["ask_vision"]:
            if command in sentence:
                return True
        return False

    def process(self, input_statement, additional_response_selection_parameters):
        response_statement = Statement(text="")
        try:
            import rospy
            from marysol_msg.msg import Object
            self.objects = rospy.wait_for_message("/objects", Object, timeout=5.0)
            if len(self.objects.label) > 0:
                from collections import Counter
                response_statement.text += "Veo "
                list = Counter(self.objects.label)
                x = 0
                for item in list:
                    if list[item] == 1:
                        response_statement.text += get_indefinite_article(item) + " " + item
                    else:
                        from pattern.es import pluralize
                        response_statement.text += str(list[item]) + " " + pluralize(item)
                    x += 1
                    if x != len(list) and x != len(list) - 1:
                        response_statement.text += ", "
                    elif x == len(list) - 1:
                        response_statement.text += " y "
            else:
                response_statement.text += "No consigo ver nada relevante"
        except:
            response_statement.text += "Hay un problema con el detector de objetos"

        response_statement.confidence = 0.99

        return response_statement
