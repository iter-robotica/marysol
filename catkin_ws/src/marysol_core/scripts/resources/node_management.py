#!/usr/bin/env python3

import os
import yaml
import rospy
import subprocess
from os.path import expanduser

#############################################################
#############################################################
class NodeManagment():
#############################################################
#############################################################

    #############################################################
    def __init__(self, env, map, loc, sim, approx, debug):
    #############################################################

        self.nodelist = {}
        self.env = env
        self.map = map
        self.loc = loc
        self.sim = sim
        self.approx = approx
        self.active_nodes = None
        self.debug = debug

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called node manager')

    #############################################################
    def get_nodelist(self):
    #############################################################
        #Lista de nodos usados en marysol(sacar de fichero)
        # if self.debug >= 2:     print("             NM: get_nodelist")
        self.nodelist = {}
        #Cargamos el fichero yaml donde se encuentran todos los nodos usados y rellenamos los diferentes arrays
        if self.approx:
            with open(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/config/nodelist_approx.yaml') as file:
                self.nodelist = yaml.load(file, Loader=yaml.FullLoader)
        else:
            with open(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/config/nodelist_' + self.env + '.yaml') as file:
                self.nodelist = yaml.load(file, Loader=yaml.FullLoader)

    #############################################################
    def get_subprocesses_command(self, node):
    #############################################################

        # if self.debug >= 2:     print("             NM: get_subprocesses_command")
        node_name = node
        yaml_command = self.nodelist[node][1]
        if node_name == "control_de_movimiento":
            command = "roslaunch " + expanduser("~") + yaml_command + " env:=" + str(self.env) + " & >/dev/null 2>&1"
        elif node_name == "localizador":
            command = "roslaunch " + expanduser("~") + yaml_command + str(self.loc) + "_simple.launch & >/dev/null 2>&1"
        elif node_name == "mapa":
            command = "roslaunch " + expanduser("~") + yaml_command + " map:=" + str(self.map) + "_" + str(self.env) + " & >/dev/null 2>&1"
        elif node_name == "simulador_robotico":
            command = "roslaunch " + expanduser("~") + yaml_command + " map:=" + str(self.map).replace("_simulation","").replace("_real","") + " & >/dev/null 2>&1"
        elif node_name == "definicion_del_robot":
            command = "roslaunch " + expanduser("~") + yaml_command + " simulation:=" + str(self.sim) + " & >/dev/null 2>&1"
        else:
            command = "roslaunch " + expanduser("~") + yaml_command + " & >/dev/null 2>&1"
        return command

    #############################################################
    def check_status(self, node, button, rate=0.25):
    #############################################################

        # if self.debug >= 1:     print("             NM: check_status")
        nodename = self.nodelist[node][0]
        r = rospy.Rate(rate)
        while not rospy.is_shutdown():
            active_nodes = subprocess.check_output(["rosnode", "list"])
            self.active_nodes = active_nodes.decode("utf-8").split("\n")
            if nodename in self.active_nodes:
                if not button.isChecked():
                    button.setEnabled(True)
                    button.setStyleSheet("#" + button.objectName() + " \n"
                    "{\n"
                    "    selection-background-color: transparent;\n"
                    "    border-image: url(:/marysol/images/off.png);\n"
                    "    background: none;\n"
                    "    border: none;\n"
                    "    background-repeat: none;\n"
                    "}\n"
                    "#" + button.objectName() + ":checked\n"
                    "{\n"
                    "    border-image: url(:/marysol/images/on.png);\n"
                    "}")
                    button.setChecked(True)
            elif button.isChecked():
                button.setEnabled(True)
                button.setStyleSheet("#" + button.objectName() + " \n"
                "{\n"
                "    selection-background-color: transparent;\n"
                "    border-image: url(:/marysol/images/off.png);\n"
                "    background: none;\n"
                "    border: none;\n"
                "    background-repeat: none;\n"
                "}\n"
                "#" + button.objectName() + ":checked\n"
                "{\n"
                "    border-image: url(:/marysol/images/on.png);\n"
                "}")
                button.setChecked(False)
            r.sleep()

    #############################################################
    def check_nodes_status(self, rate=0.25):
    #############################################################

        # if self.debug >= 1:     print("             NM: check_status")
        r = rospy.Rate(rate)
        while not rospy.is_shutdown():
            active_nodes = subprocess.check_output(["rosnode", "list"])
            self.active_nodes = active_nodes.decode("utf-8").split("\n")
            #print(self.active_nodes)
            if len(self.nodelist) > 0:
                for listed_node in self.nodelist:
                    node = self.nodelist[listed_node][0]
                    button = self.nodelist[listed_node][2]
                    if node in self.active_nodes:
                        #print(node)
                        if not button.isChecked():
                            #print(" Activamos")
                            button.setStyleSheet("#" + button.objectName() + " \n"
                            "{\n"
                            "    selection-background-color: transparent;\n"
                            "    border-image: url(:/marysol/images/off.png);\n"
                            "    background: none;\n"
                            "    border: none;\n"
                            "    background-repeat: none;\n"
                            "}\n"
                            "#" + button.objectName() + ":checked\n"
                            "{\n"
                            "    border-image: url(:/marysol/images/on.png);\n"
                            "}")
                            button.setChecked(True)
                            button.setEnabled(True)
                    elif button.isChecked():
                        #print(" Desactivamos")
                        button.setStyleSheet("#" + button.objectName() + " \n"
                        "{\n"
                        "    selection-background-color: transparent;\n"
                        "    border-image: url(:/marysol/images/off.png);\n"
                        "    background: none;\n"
                        "    border: none;\n"
                        "    background-repeat: none;\n"
                        "}\n"
                        "#" + button.objectName() + ":checked\n"
                        "{\n"
                        "    border-image: url(:/marysol/images/on.png);\n"
                        "}")
                        button.setChecked(False)
                        button.setEnabled(True)
            r.sleep()

    #############################################################
    def recall_subprocess(self, node, button):
    #############################################################

        # if self.debug >= 1:     print("             NM: recall_subprocess")
        button.setEnabled(False)
        #button.setChecked(False)
        button.setStyleSheet("#" + button.objectName() + " \n"
    "{\n"
    "    selection-background-color: transparent;\n"
    "    border-image: url(:/marysol/images/wait.png);\n"
    "    background: none;\n"
    "    border: none;\n"
    "    background-repeat: none;\n"
    "}\n"
    "#" + button.objectName() + ":checked\n"
    "{\n"
    "    border-image: url(:/marysol/images/wait.png);\n"
    "}")
        nodename = self.nodelist[node][0]
        active_nodes = subprocess.check_output(["rosnode", "list"])
        active_nodes = active_nodes.decode("utf-8").split("\n")
        if nodename in active_nodes:
            os.system("rosnode kill " + nodename + " & >/dev/null 2>&1")
        else:
            os.system(self.get_subprocesses_command(node))

    #############################################################
    def kill_nodes(self):
    #############################################################

        # if self.debug >= 2:     print("             NM: kill_nodes")
        active_nodes = subprocess.check_output(["rosnode", "list"])
        active_nodes = active_nodes.decode("utf-8").split("\n")
        for node in active_nodes:
            for node2 in self.nodelist:
                if node == self.nodelist[node2][0] and not "teensy" in node:
                    os.system("rosnode kill " + node + " >/dev/null 2>&1")
                    break

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    try:
        node_management = NodeManagment()
        node_management.spin()
    except rospy.ROSInterruptException:
        pass
