#!/usr/bin/env python3

import rospy
import roslib
import actionlib
from actionlib_msgs.msg import GoalID, GoalStatusArray
from geometry_msgs.msg import PoseStamped, Twist, PoseWithCovarianceStamped, Pose
from nav_msgs.msg import Odometry
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionGoal, MoveBaseActionFeedback
from visualization_msgs.msg import MarkerArray, Marker
import yaml
from os.path import expanduser
import time
import numpy as np
import math
from std_srvs.srv import *
from nav_msgs.srv import GetPlan


#############################################################
#############################################################
class Navigation():
#############################################################
#############################################################

    #############################################################
    def __init__(self, nh, env, loc, coordinates, debug):
    #############################################################

        if nh == None:
            rospy.init_node("Avatar")
            nodename = rospy.get_name()
            rospy.loginfo("%s started" % nodename)
        else:
            nodename = rospy.get_name()
            rospy.loginfo(" %s started on Navigation" % nodename)

        self.env = env
        self.loc = loc
        self.debug = debug

        # internal_data
        self.ActionGoal = MoveBaseActionGoal()
        self.ActionGoal.goal_id = GoalID()
        self.ActionGoal.goal.target_pose = PoseStamped()
        self.client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
        self.marker_array_symbols = MarkerArray()
        self.marker_array_labels = MarkerArray()
        self.status = "None"
        self.command = None
        self.current_pose = None

        self.xyw = coordinates

        rospy.Subscriber("/move_base/status", GoalStatusArray, self.GoalStatusCallback)
        rospy.Subscriber("/move_base/feedback", MoveBaseActionFeedback, self.GoalFeedbackCallback)
        self.cancel_pub = rospy.Publisher("/move_base/cancel", GoalID, queue_size=10)
        self.marker_array_symbols_pub = rospy.Publisher('/places/arrows', MarkerArray, queue_size=10)
        self.marker_array_labels_pub = rospy.Publisher('/places/labels', MarkerArray, queue_size=10)

    #############################################################
    def __del__(self):
    #############################################################
        print('Destructor called nav')

    #############################################################
    def set_current_pose(self):
    #############################################################
        pose = None
        while pose == None and not rospy.is_shutdown():
            try:
                if self.loc == "fixed":
                    self.current_pose = rospy.wait_for_message("odom", Odometry, timeout=5.0).pose.pose
                    pose = self.current_pose
                    #rospy.loginfo("Current /odom READY=>")
                elif self.loc == "amcl":
                    self.current_pose = rospy.wait_for_message("amcl_pose", PoseWithCovarianceStamped, timeout=5.0).pose.pose
                    pose = self.current_pose
                    #rospy.loginfo("Current /amcl_pose READY=>")

            except:
                if self.loc == "fixed":
                    rospy.loginfo("Current /odom not ready yet, retrying for getting it")
                elif self.loc == "amcl":
                    rospy.loginfo("Current /amcl_pose not ready yet, retrying for getting it")

    #############################################################
    def send_goal(self, pose, wait_for_result):
    #############################################################
        #print(pose)
        # if self.debug >= 3:     print("             NAV: send_goal")
        try:
            self.ActionGoal.goal.target_pose.header.stamp = rospy.Time.now()
            self.ActionGoal.goal.target_pose.header.frame_id = "map"
            self.ActionGoal.goal.target_pose.pose = pose
            self.client.send_goal(self.ActionGoal.goal)
            self.client.wait_for_result(rospy.Duration.from_sec(wait_for_result))
            #print(self.client.get_state())
            return True
        except:
            return False

    #############################################################
    def cancel_goal(self):
    #############################################################
        # if self.debug >= 3:     print("             NAV: cancel_goal")
        self.cancel_pub.publish(self.ActionGoal.goal_id)
        self.status = "cancel"

    #############################################################
    def resend_goal(self, wait_for_result):
    #############################################################
        # if self.debug >= 3:     print("             NAV: resend_goal")
        self.client.send_goal(self.ActionGoal.goal)
        self.client.wait_for_result(rospy.Duration.from_sec(wait_for_result))
        print(self.client.get_state())

    #############################################################
    def place_to_pose(self, place):
    #############################################################
        #print(place)
        target = str(place.replace(" ","_"))
        if target in self.xyw:
            return self.xyz_to_pose(self.xyw[target][0], self.xyw[target][1], self.xyw[target][2])

    #############################################################
    def xyz_to_pose(self, x, y, z):
    #############################################################
        pose = Pose()
        from resources.utils import quaternion_from_euler
        q = quaternion_from_euler(0,0,z,False)
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]
        pose.position.x = x
        pose.position.y = y
        pose.position.z = 0.0
        return pose

    #############################################################
    def pose_to_xyz(self, pose):
    #############################################################
        xyz = []
        xyz.append(pose.position.x)
        xyz.append(pose.position.y)
        from resources.utils import euler_from_quaternion
        z, y, x = euler_from_quaternion(pose.orientation, False)
        xyz.append(z)
        return xyz

    #############################################################
    def place_exist(self, place):
    #############################################################
        if str(place.replace(" ","_")) in self.xyw:
            return True
        else:
            return False

    #############################################################
    def spin(self, direction):
    #############################################################

        pose = self.get_robot_pose()
        from resources.utils import euler_from_quaternion
        z, y, x = euler_from_quaternion(pose.orientation,False)

        if direction == "left":
            z += 0.8 * math.pi
        elif direction == "right":
            z -= 0.8 * math.pi

        if not self.send_goal(self.xyz_to_pose(self.current_pose.position.x, self.current_pose.position.y, z), 1):
            print("Fallo")

    #############################################################
    def GoalFeedbackCallback(self, msg):
    #############################################################
        #rospy.loginfo("MSG: %s " % msg)
        self.ActionGoal.goal_id = msg.status.goal_id
        if msg.status.goal_id.id.split("-")[0] != rospy.get_name():
            print("detectado goal intruso")
            self.cancel_goal()

    #############################################################
    def GoalStatusCallback(self, msg):
    #############################################################
        #rospy.loginfo("MSG: %s " % msg)

        if len(msg.status_list) > 0:
            for status in msg.status_list:
                if self.ActionGoal.goal_id == status.goal_id:
                    if status.status in [0,1]:
                        self.status = "sent"
                    elif status.status in [2]:
                        self.status = "cancel"
                    elif status.status in [3]:
                        self.status = "reached"
                    elif status.status in [4,5,6,7,8,9]:
                        self.status = "problem"

        """
        PENDING         = 0   # The goal has yet to be processed by the action server
        ACTIVE          = 1   # The goal is currently being processed by the action server
        PREEMPTED       = 2   # The goal received a cancel request after it started executing
                              #   and has since completed its execution (Terminal State)
        SUCCEEDED       = 3   # The goal was achieved successfully by the action server (Terminal State)
        ABORTED         = 4   # The goal was aborted during execution by the action server due
                              #    to some failure (Terminal State)
        REJECTED        = 5   # The goal was rejected by the action server without being processed,
                              #    because the goal was unattainable or invalid (Terminal State)
        PREEMPTING      = 6   # The goal received a cancel request after it started executing
                              #    and has not yet completed execution
        RECALLING       = 7   # The goal received a cancel request before it started executing,
                              #    but the action server has not yet confirmed that the goal is canceled
        RECALLED        = 8   # The goal received a cancel request before it started executing
                              #    and was successfully cancelled (Terminal State)
        LOST            = 9   # An action client can determine that a goal is LOST. This should not be
                              #    sent over the wire by an action server
        """

    #############################################################
    def locate(self, loc):
    #############################################################
        # if self.debug >= 3:     print("             NAV: locate")
        from geometry_msgs.msg import PoseWithCovarianceStamped
        # subscriptions
        initialpose_pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=10)
        initialpose = PoseWithCovarianceStamped()
        initialpose.header.frame_id = "map"
        initialpose.pose.pose.position.x = self.xyw[loc][0]
        initialpose.pose.pose.position.y = self.xyw[loc][1]
        initialpose.pose.pose.position.z = 0.0
        from resources.utils import quaternion_from_euler
        q = quaternion_from_euler(0.0,0.0,self.xyw[loc][2],False)
        initialpose.pose.pose.orientation.x = q[0]
        initialpose.pose.pose.orientation.y = q[1]
        initialpose.pose.pose.orientation.z = q[2]
        initialpose.pose.pose.orientation.w = q[3]
        initialpose_pub.publish(initialpose)
        self.clear_cost_maps()

    #############################################################
    def clear_cost_maps(self):
    #############################################################
        # if self.debug >= 3:     print("             NAV: clear_cost_maps")
        rospy.wait_for_service('/move_base/clear_costmaps')
        try:
            clear_costmap = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
            #rospy.loginfo("Created costmap service proxy")
            try:
                #rospy.loginfo("clearing costmap")
                clear_costmap()
            except rospy.ServiceException:
                rospy.logerr("could not call service: ")
        except rospy.ServiceException:
            rospy.logerr("could not create service proxy: ")

    #############################################################
    def get_path(self, start, goal):
    #############################################################

        Start = PoseStamped()
        Start.header.seq = 0
        Start.header.frame_id = "map"
        Start.header.stamp = rospy.Time(0)
        Start.pose = start

        Goal = PoseStamped()
        Goal.header.seq = 0
        Goal.header.frame_id = "map"
        Goal.header.stamp = rospy.Time(0)
        Goal.pose = goal

        rospy.wait_for_service('/move_base/NavfnROS/make_plan')
        get_plan = rospy.ServiceProxy('/move_base/NavfnROS/make_plan', GetPlan)
        req = GetPlan()
        req.start = Start
        req.goal = Goal
        req.tolerance = .5
        #try:
        path = get_plan(req.start, req.goal, req.tolerance)
        return path
        #except:
        #    return None

    #############################################################
    def get_path_pose(self, path, goal, meters):
    #############################################################
        #print(path.plan.poses)
        for pose in path.plan.poses:
            x = pose.pose.position.x - goal.position.x
            y = pose.pose.position.y - goal.position.y
            h = (x**(2) + y**(2))**(0.5)
            if h < meters:
                return pose.pose
        else:
            return None


    #############################################################
    def get_robot_pose(self):
    #############################################################
        # if self.debug >= 3:     print("             NAV: get_robot_pose")
        if self.loc == "fixed":
            return rospy.wait_for_message("odom", Odometry, timeout=5.0).pose.pose
        elif self.loc == "amcl":
            return rospy.wait_for_message("amcl_pose", PoseWithCovarianceStamped, timeout=5.0).pose.pose

    #############################################################
    def robot_poseCallback(self, msg):
    #############################################################
        self.robot_pose = msg.pose.pose

    #############################################################
    def get_marker_array(self):
    #############################################################
        # if self.debug >= 3:     print("             NAV: get_marker_array")
        x = 0
        self.marker_array_symbols = MarkerArray()
        for coordinate in self.xyw:
            marker = Marker()
            marker.id = x
            marker.ns = "places_symbols"
            marker.pose = self.place_to_pose(coordinate)
            marker.lifetime = rospy.Duration.from_sec(5)
            marker.color.r = 0
            marker.color.g = 0
            marker.color.b = 255
            marker.color.a = 0.5
            marker.scale.x = 2
            marker.scale.y = 0.2
            marker.scale.z = 0.2
            marker.type = 0
            marker.action = 0
            marker.header.frame_id = "/map"
            self.marker_array_symbols.markers.append(marker)
            x += 1

        x = 0
        self.marker_array_labels = MarkerArray()
        for coordinate in self.xyw:
            marker = Marker()
            marker.id = x
            marker.ns = "places_symbols"
            marker.text = coordinate
            marker.pose = self.place_to_pose(coordinate)
            marker.lifetime = rospy.Duration.from_sec(5)
            marker.color.r = 255
            marker.color.g = 0
            marker.color.b = 0
            marker.color.a = 1
            marker.scale.x = 1
            marker.scale.y = 1
            marker.scale.z = 1
            marker.type = 9
            marker.action = 0
            marker.header.frame_id = "/map"
            self.marker_array_labels.markers.append(marker)
            x += 1

    #############################################################
    def publish_marker_array(self):
    #############################################################

        # if self.debug >= 3:     print("             NAV: publish_marker_array")
        for marker in self.marker_array_symbols.markers:
            marker.header.stamp = rospy.Time.now()
        self.marker_array_symbols_pub.publish(self.marker_array_symbols)
        for marker in self.marker_array_labels.markers:
            marker.header.stamp = rospy.Time.now()
        self.marker_array_labels_pub.publish(self.marker_array_labels)


#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    navigation = Navigation()
