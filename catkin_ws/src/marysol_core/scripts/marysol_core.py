#!/usr/bin/env python3

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import os
import sys
import yaml
import rospy
import signal
import roslib
import argparse
import threading
import actionlib
import subprocess
import numpy as np
from os.path import expanduser
from geometry_msgs.msg import Pose, PoseStamped
from std_msgs.msg import Bool, String, UInt8MultiArray, Int16MultiArray, Int32
from actionlib_msgs.msg import GoalStatusArray

from resources.checks import Check
from resources.navigation import Navigation
#from resources.chatbot import ChatBotChatter
from resources.node_management import NodeManagment
from resources.visual_window import VisualWindow
from resources.strip_leds import StripLeds
from resources.transformer import Transformer
from resources.window_management import WindowsManagment

from resources.qt_windows.qt_windows import TopWindow, Intro, KillButton#, resourses_media


"""
This Node control de robot behaviour. The behaviour depends on the user mic input and the environment of the robot.
The robot requieres a serie of task to do that are constantly being modifying according to the environment.
StripLeds and Avatar are classes that show this robot status so we control them from here.
"""

#############################################################
#############################################################
class MarysolCore(TopWindow):
#############################################################
#############################################################

    #############################################################
    def __init__(self, args):
    #############################################################

        super().__init__()

        intro = Intro()
        os.system("pkill ros")
        os.system("pkill gz")
        self.start(args)
        del intro

    #############################################################
    def start(self, args):
    #############################################################

        os.system("roscore &")

        rospy.init_node("marysol_core")
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        #### parameters ####
        self.debug = 5
        self.sim = False
        self.approx = False
        self.use_leds = False
        self.env = args.env
        self.map = args.map
        self.loc = args.loc
        if self.env == "simulation":
            self.sim = True
            if self.loc == "fixed" or self.loc == "rtabmap":
                self.approx = False
            else:
                self.approx = True
        else:
            self.use_leds = True

        #### internal data ####
        self.subprocess = None
        self.subprocess_follow = None
        self.subprocess_speech_recognition = None
        # simple switch to parse the next mic_speech incoming
        self.use_array_mic = False

        #### internal data ####
        # Default task at the beggining is rolling around
        # Rutina de entrenamiento
        #self.routine = ["go-sotano", "spin-right-60", "go-sotano", "spin-left-60", "go-rampa_alta", "spin-left-60", "go-rampa_alta", "spin-right-60", "go-sotano", "spin-left-60", "go-sotano", "spin-right-60", "go-rampa_baja", "spin-left-60", "go-rampa_baja", "spin-right-60"]
        #self.routine = ["spin-right-100", "spin-left-100"]#, "go-cafeteria", "spin-left", "spin-right", "go-entrada"]
        #self.routine = ["say-tiempo_excedido-failed", "say-te_escucho-simple_white", "say-tiempo_excedido-failed", "say-te_escucho-simple_white", "show-mancha", "say-tiempo_excedido-failed", "show-mancha"]
        #self.routine = ["follow"]
        self.routine = ["none"]
        self.task_selector = 0
        self.task = self.routine[self.task_selector]
        self.move_around_routine = []
        self.seconds = 0
        self.routines = {}

        self.vw = VisualWindow(rospy, self.debug)
        self.vw.NewLocButton.pressed.connect(self.addNewButtonLoc)
        self.vw.MoveAround.pressed.connect(self.moveAround)
        self.vw.FollowButton.pressed.connect(self.FollowButtonPressed)
        self.vw.show_top_button.pressed.connect(self.show)
        self.vw.TurnOff.pressed.connect(self.StartTurningOff)
        self.vw.refresh_USB_button.pressed.connect(self.refresh_USB_layout)
        self.refresh_USB_layout()
        self.default_sound_settings = {}
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/sound_default.yaml") as file:
            self.default_sound_settings = yaml.load(file, Loader=yaml.FullLoader)
        self.default_power_settings = {}
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/power_default.yaml") as file:
            self.default_power_settings = yaml.load(file, Loader=yaml.FullLoader)
            if self.default_power_settings == None:
                self.default_power_settings = {}
        self.power_settings()
        self.vw.refresh_audio_input_button.pressed.connect(self.refresh_audio_input_layout)
        self.refresh_audio_input_layout()
        self.vw.refresh_audio_output_button.pressed.connect(self.refresh_audio_output_layout)
        self.refresh_audio_output_layout()
        self.vw.refresh_node_button.pressed.connect(self.add_buttons_MainWindows_debug)

        if self.sim == False:
            os.system("roslaunch ~/marysol/catkin_ws/src/marysol_communications/launch/serial_teensy.launch &")
        if self.use_leds:
            self.leds = StripLeds(rospy, self.debug)
            self.leds.Activate()
        #self.chatbot = ChatBotChatter(rospy)
        self.nm = NodeManagment(self.env, self.map, self.loc, self.sim, self.approx, self.debug)
        self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.debug_button))
        self.uncheck_but(self.debug_button)
        self.debug_button.setChecked(True)
        self.vw.next_task_button.pressed.connect(self.next_task)
        self.vw.previous_task_button.pressed.connect(self.previous_task)
        self.vw.reset_button.pressed.connect(self.reset)

        self.AutoStop_func()

        self.add_buttons_MainWindows_debug()

        #### subscriptions ####
        # user input from the mic
        rospy.Subscriber("/speech/mic_speech", String, self.micCallback)
        # in case we use array_mic we will use de mic direction to face the user
        rospy.Subscriber("/sound_direction", Int32, self.sound_directionCallback)
        self.sound_direction = None
        self.sound_angle = 0

        # bot output to the speakers
        self.ShutUp_pub = rospy.Publisher('/speech/ShutUp', Bool, queue_size=10)
        # speech topics to manage the speech routines
        rospy.Subscriber("/speech/playing", Bool, self.playingCallback)
        self.playing = False
        rospy.Subscriber("/speech/listening", Bool, self.listeningCallback)
        self.listening = False
        rospy.Subscriber("/qr_pose", PoseStamped, self.qr_poseCallback)
        self.qr_pose = None
        rospy.Subscriber("/battery", String, self.battery_status_callback)
        self.stop = Bool()
        rospy.Subscriber('/state', Int16MultiArray, self.stopCallback)

        self.start_spin = threading.Thread(target=self.spin, daemon=True)
        self.node_status = threading.Thread(target=self.nm.check_nodes_status, daemon=True)
        self.node_status.start()

        if self.use_leds:
            rospy.sleep(6)
            self.leds.set_task("0-0-0-3-50")
        self.show()
        self.vw.show()

        self.power_button.animateClick()

    #############################################################
    def restart(self):
    #############################################################

        #### internal data ####
        # if self.debug >= 0:     print(" restart")
        self.key_word = []
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/key_words.yaml") as file:
            self.key_word = yaml.load(file, Loader=yaml.FullLoader)
        self.coordinates = {}
        if os.path.exists(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/coordinates_" + self.map + "_" + self.env + ".yaml"):
            with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/coordinates_" + self.map + "_" + self.env + ".yaml") as file:
                self.coordinates = yaml.load(file, Loader=yaml.FullLoader)
                if self.coordinates == None:
                    self.coordinates = {}
        else:
            file = open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/coordinates_" + self.map + "_" + self.env + ".yaml", "w")
            file.write("# yaml file holding places in simulation map " + self.map)
            file.close()

        # processing active when the mic_speech is being processed by the chatbot
        self.processing = False
        # simple switch to parse the next mic_speech incoming
        #self.set_state(None)
        # simple switch to parse the next mic_speech incoming
        self.playing_phrase = ""
        self.showing_element = ""
        self.on_routine = False
        self.routine = ["none"]
        self.task_selector = 0
        self.task = self.routine[self.task_selector]
        self.move_around_routine = []
        self.seconds = 0
        self.routines = {}

        #### classes ####
        #self.transformer = Transformer(rospy)
        self.nav = Navigation(rospy, self.env, self.loc, self.coordinates, self.debug)
        self.vw.clearButton.pressed.connect(self.nav.clear_cost_maps)
        self.windows_names = self.add_buttons_MainWindows_resourse()
        self.wm = WindowsManagment(self.windows_names, self.debug)
        self.check = Check(self.env, self.coordinates, self.windows_names, self.map)
        self.kb = KillButton()
        self.kb.KillButton.pressed.connect(self.wm.close)

        self.add_buttons_MainWindows_debug()
        self.add_buttons_MainWindows_Map()

        self.show()
        self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.debug_button))
        self.uncheck_but(self.debug_button)
        self.vw.show()

    #############################################################
    def start_stop(self):
    #############################################################

        # if self.debug >= 0:     print(" start_stop")
        if self.subprocess == None:
            self.bring_up()
        else:
            self.shut_down()

    #############################################################
    def bring_up(self):
    #############################################################

        # if self.debug >= 0:     print(" bring_up")
        self.disable_buttons(self.buttons)
        self.set_state("bring_up")
        self.nm.kill_nodes()
        command = "roslaunch ~/marysol/catkin_ws/src/marysol_bringup/launch/marysol.launch loc:=" + self.loc + " map:=" + self.map + " mapping:=false gzclient:=true simulation:=" + str(self.sim) + " approx:=" + str(self.approx) + " speech:=true remote:=false robot:=marysol4 train:=false >/dev/null 2>&1"
        # command = "roslaunch ~/marysol/catkin_ws/src/marysol_bringup/launch/marysol.launch loc:=" + self.loc + " map:=" + self.map + " mapping:=false gzclient:=true simulation:=" + str(self.sim) + " approx:=" + str(self.approx) + " speech:=true remote:=false robot:=marysol4 train:=true >/dev/null 2>&1"
        self.subprocess = subprocess.Popen(command, shell=True)
        if self.use_array_mic:
            command = "roslaunch ~/marysol/catkin_ws/src/marysol_speech/launch/speech_recognition_from_array.launch >/dev/null 2>&1"
        else:
            command = "roslaunch ~/marysol/catkin_ws/src/marysol_speech/launch/speech_recognition_from_mic.launch >/dev/null 2>&1"
        self.subprocess_speech_recognition = subprocess.Popen(command, shell=True)
        self.restart()
        try:
            self.start_spin.start()
        except:
            pass
        self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.avatar_button))
        self.uncheck_but(self.avatar_button)
        if not self.avatar_button.isChecked():
            self.avatar_button.setChecked(True)
        self.enable_buttons(self.buttons)

    #############################################################
    def shut_down(self):
    #############################################################

        # if self.debug >= 0:     print(" shut_down")
        self.disable_buttons(self.buttons)
        self.set_state("shut_down")

    #############################################################
    def closeEvent(self, e):
    #############################################################

        #self.shut_down()
        if self.use_leds:
            self.leds.Deactivate()
            rospy.sleep(0.5)
            self.leds.Deactivate()
            rospy.sleep(0.5)
            self.leds.Deactivate()
            rospy.sleep(0.5)
        os.system("pkill ros")
        os.system("pkill gz")
        os._exit(101)

    #############################################################
    def stop_subprocess(self, subprocess):
    #############################################################

        import psutil
        process = psutil.Process(subprocess.pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()
        subprocess = None

    #############################################################
    def beforeClosing(self):
    #############################################################

        # if self.debug >= 0:     print(" beforeClosing")
        if self.subprocess != None:
            try:
                pose = self.nav.current_pose
                from resources.utils import euler_from_quaternion
                z, y, x = euler_from_quaternion(pose.orientation,False)
                pose_array = []
                pose_array.append(pose.position.x)
                pose_array.append(pose.position.y)
                pose_array.append(z)
                self.coordinates["robot"] = pose_array
                self.rewrite_coordeinates()
            except:
                pass

    #############################################################################
    def spin(self, rate=1):
    #############################################################################

        #Here we check periodically that the robot is doing correctly all
        #the task it have to every second.
        #    1º Handle the speech timeouts.
        #    2º Check the navigation is going well
        #    3º ...

        # if self.debug >= 0:     print(" spin")
        self.robot_started = False

        #######################
        def shut_down_steps():
        #######################
            # if self.debug >= 0:     print(" shut_down_steps")
            rospy.sleep(1)
            self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.avatar_button))
            self.uncheck_but(self.avatar_button)
            if not self.avatar_button.isChecked():
                self.avatar_button.setChecked(True)
            self.vw.BotText.setText("APAGANDO")
            self.vw.UserText.setText("Espere")
            self.vw.set_video("cargando.mp4", self.vw.player)
            self.nm.kill_nodes()
            if self.subprocess != None:
                #self.stop_subprocess(self.subprocess)
                import psutil
                process = psutil.Process(self.subprocess.pid)
                for proc in process.children(recursive=True):
                    try:
                        proc.kill()
                    except:
                        pass
                try:
                    process.kill()
                except:
                    pass
                self.subprocess = None
                del self.nav
                del self.check
                del self.wm
            if self.subprocess_speech_recognition != None:
                #self.stop_subprocess(self.subprocess)
                import psutil
                process = psutil.Process(self.subprocess_speech_recognition.pid)
                for proc in process.children(recursive=True):
                    try:
                        proc.kill()
                    except:
                        pass
                try:
                    process.kill()
                except:
                    pass
                self.subprocess_speech_recognition = None
            if self.subprocess_follow != None:
                #self.stop_subprocess(self.subprocess_follow)
                import psutil
                process = psutil.Process(self.subprocess_follow.pid)
                for proc in process.children(recursive=True):
                    try:
                        proc.kill()
                    except:
                        pass
                try:
                    process.kill()
                except:
                    pass
                self.subprocess_follow = None
            if self.vw.subprocess_maintenance != None:
                #self.stop_subprocess(self.vw.subprocess_maintenance)
                import psutil
                process = psutil.Process(self.subprocess_maintenance.pid)
                for proc in process.children(recursive=True):
                    try:
                        proc.kill()
                    except:
                        pass
                try:
                    process.kill()
                except:
                    pass
                self.subprocess_maintenance = None
            #del self.leds
            #del self.chatbot
            os.system("pkill gz")
            self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.debug_button))
            self.uncheck_but(self.debug_button)
            if not self.debug_button.isChecked():
                self.debug_button.setChecked(True)
            self.nm.kill_nodes()
            self.show()
            self.enable_buttons(self.buttons)
            self.set_state(None)
            self.robot_started = False

        #######################
        def bring_up_steps():
        #######################
            # if self.debug >= 0:     print(" bring_up_steps")
            self.vw.BotText.setText("CARGANDO")
            self.vw.UserText.setText("Espere")
            self.vw.set_video("cargando.mp4", self.vw.player)
            rospy.sleep(0.5)
            self.nav.set_current_pose()
            self.loc_at_start()
            rospy.sleep(3)
            self.loc_at_start()
            rospy.sleep(3)
            self.loc_at_start()
            self.vw.UserText.setText("")
            self.reset(["say-Robot Marysol operativo-ok", "none"])
            #self.reset(["say-Robot Marysol operativo-succed", "wait-2", "say-de acuerdo-ok", "wait-2", "say-frase sin renderizar-ok", "wait-2", "say-robot marysol operativo-ok", "wait-2", "say-frase-ok", "wait-2", "say-frase sin renderizar que es mucho más larga que las anteriormente mencionadas-ok"])
            self.set_state("first_command")
            self.robot_started = True

        #######################
        # Spin Starting
        #######################
        r = rospy.Rate(rate)

        self.ticks = 0
        self.hide_ticks = 0
        self.hide_text_ticks = 0
        last_task = ""
        hide_once = False

        while not rospy.is_shutdown():

            if self.subprocess == None:
                r.sleep()
                continue

            bring_up_steps()

            ongoing = False

            ###### main loop  ######
            while not rospy.is_shutdown():

                # try:

                ## if self.debug >= 2:     print("     " + self.Task)

                if self.state == "shut_down":
                    shut_down_steps()
                    break

                # Checking routine tasks

                # Do Nothing
                if self.task == "none":
                    # if self.debug >= 2:     print("     none")
                    pass

                # Await
                elif self.task.split("-")[0] == "await":
                    # if self.debug >= 2:     print("     await")
                    if not self.state == "awaiting":
                        self.set_state("awaiting")
                    if not self.playing and not self.listening and not self.processing:
                        self.seconds += 1
                        if self.seconds >= int(self.task.split("-")[1]):
                            self.vw.awaiting_label.hide()
                            self.set_state("first_command")
                            self.reset(["say-tiempo excedido-failed", "none"])

                # Going Some pose
                if self.task.split("-")[0] == "going":
                    # if self.debug >= 2:     print("     going")
                    if self.task.split("-")[1] == "sound_pose" and self.nav.status != "sent" and not ongoing:
                        self.nav.send_goal(self.nav.xyz_to_pose(self.nav.current_pose.position.x, self.nav.current_pose.position.y, self.sound_direction), 1)
                        ongoing = True
                    if self.nav.status == "reached" and ongoing:
                        self.next_task()
                        ongoing = False

                # Go Somewhere
                elif self.task.split("-")[0] == "go":
                    # if self.debug >= 2:     print("     go")
                    if self.nav.ActionGoal.goal.target_pose.pose != self.nav.place_to_pose(self.task.split("-")[1]):
                        self.nav.cancel_goal()
                    if self.nav.status == "reached":
                        self.next_task()
                    elif self.nav.status != "sent":
                        if not self.nav.send_goal(self.nav.place_to_pose(self.task.split("-")[1]), 1):
                            # Aquí va la rutina que gestiona el por que no se puede realizar ese goal
                            print("**fallo en el goal**")

                # Spin
                elif self.task.split("-")[0] == "spin":
                    # if self.debug >= 2:     print("     spin")
                    if self.seconds >= int(self.task.split("-")[2]):
                        self.next_task()
                    else:
                        self.seconds += 1
                        self.nav.spin(self.task.split("-")[1])

                # Execute command
                elif self.task.split("-")[0] == "command":
                    # if self.debug >= 2:     print("     command")
                    sudoPassword = "marysol"
                    command = '-'.join(self.task.split("-")[1:])
                    if command == "shutdown -h now":
                        self.Turn_Off()
                    else:
                        os.system('echo %s|sudo -S %s >/dev/null 2>&1' % (sudoPassword, command))
                    self.next_task()

                # Do maintenance works
                elif self.task.split("-")[0] == "maintenance":
                    # if self.debug >= 2:     print("     maintenance")
                    self.vw.MaintenanceWorks()

                # Locate on
                elif self.task.split("-")[0] == "locate":
                    # if self.debug >= 2:     print("     maintenance")
                    self.nav.locate(self.task.split("-")[1])
                    rospy.sleep(0.25)
                    self.nav.locate(self.task.split("-")[1])
                    self.next_task()

                # Say something
                elif self.task.split("-")[0] == "say" or self.task.split("-")[0] == "speech":
                    # if self.debug >= 2:     print("     speech - say")
                    if not self.playing and self.playing_phrase != self.task.split("-")[1]:
                        if self.vw.stackedWidget.currentIndex() != self.buttons.index(self.avatar_button):
                            self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.avatar_button))
                        self.uncheck_but(self.avatar_button)
                        if not self.avatar_button.isChecked():
                            self.avatar_button.setChecked(True)
                        if self.task.split("-")[0] == "speech":
                            self.update(message = "speech_" + self.task.split("-")[1], leds = "simple_white")
                        else:
                            if len(self.task.split("-")) == 3:
                                self.update(message = self.task.split("-")[1], leds = self.task.split("-")[2])
                            else:
                                self.update(message = self.task.split("-")[1], leds = "simple_white")
                    elif not self.playing and self.playing_phrase == self.task.split("-")[1]:
                        self.next_task()

                # Just wait some seconds
                elif self.task.split("-")[0] == "wait":
                    # if self.debug >= 2:     print("     wait")
                    self.seconds += 1
                    if self.seconds >= int(self.task.split("-")[1]):
                        self.next_task()

                # Show media
                elif self.task.split("-")[0] == "show":
                    # if self.debug >= 2:     print("     show")
                    if self.showing_element != self.task.split("-")[1] and not self.wm.check_window_simple(self.task.split("-")[1]):
                        self.kb.show()
                        self.wm.open(self.windows_names[self.task.split("-")[1]][1])# + " > /dev/null")
                        self.showing_element = self.task.split("-")[1]
                        self.hide()
                        rospy.sleep(3)
                        self.vw.hide()
                    elif self.showing_element == self.task.split("-")[1] and not self.wm.check_pidof(self.windows_names[self.task.split("-")[1]][1].split(" ")[0]) and not self.wm.check_window_simple(self.windows_names[self.task.split("-")[1]][0]):
                        self.kb.hide()
                        self.vw.show()
                        if self.vw.stackedWidget.currentIndex() != self.buttons.index(self.avatar_button):
                            self.show()
                        if self.wm.check_window_simple(self.windows_names["visual_window"][0]):
                            self.wm.bring_window(self.windows_names["visual_window"][0])
                            self.vw.setWindowState(Qt.WindowFullScreen)
                            self.next_task()

                # Follow qr code
                elif self.task == "follow":
                    # if self.debug >= 2:     print("     follow")
                    if self.qr_pose != None:
                        if not self.nav.send_goal(self.qr_pose, 1):
                            # Aquí va la rutina que gestiona el por que no se puede realizar ese goal
                            print("**fallo en el follow**")

                # stop video playing
                elif self.task == "stop_video":
                    # if self.debug >= 2:     print("     stop_video")
                    if self.playing:
                        self.vw.player.stop()
                        self.playing = False
                    self.next_task()

                # Checking internal tasks

                self.hide_ticks += 1
                if self.hide_ticks == 15 and self.vw.stackedWidget.currentIndex() == 0:
                    # if self.debug >= 3:     print("         hide_ticks")
                    self.hide()
                    self.vw.show()
                    self.hide_ticks = 0

                self.hide_text_ticks += 1
                if self.hide_text_ticks == 30 and not self.on_routine:
                    # if self.debug >= 3:     print("         hide_text_ticks")
                    self.vw.UserText.setText("")
                    self.vw.BotText.setText("")
                    self.hide_text_ticks = 0

                """if not self.playing and not self.processing and self.subprocess != None and self.state != None and "go" in self.task:
                    self.beforeClosing()"""

                if self.vw.AutoStop.isChecked():
                    self.AutoStop_func()

                """if self.ticks % 3600 == 0 and self.subprocess != None and self.state != None and self.vw.stackedWidget.currentIndex() == 0 and self.task == "none":
                    # if self.debug >= 3:     print("         Me voy de paseo")
                    self.reset(["say-Me voy de paseo-ok", "none"])
                    self.reset(self.coordenates_to_routine(self.coordinates))"""

                if "/rviz" in self.nm.active_nodes:
                    # if self.debug >= 3:     print("         /rviz labels")
                    self.nav.get_marker_array()
                    self.nav.publish_marker_array()
                    if hide_once == False:
                        #self.hide()
                        self.vw.hide()
                        hide_once = True
                else:
                    hide_once = False

                if self.task.split("-")[0] != "spin":
                    self.nav.set_current_pose()

                if self.on_routine and (last_task != self.task or self.task.split("-")[0] == "spin" or self.task.split("-")[0] == "wait"):
                    # if self.debug >= 4:     print("         on_routine")
                    if self.task.split("-")[0] == "spin":
                        self.vw.UserText.setText("Actual:\n" + self.routine[self.task_selector].replace("_"," ").replace(self.task.split("-")[2],str(int(self.task.split("-")[2])-self.seconds)))
                    elif self.task.split("-")[0] == "wait":
                        self.vw.UserText.setText("Actual:\n" + self.routine[self.task_selector].replace("_"," ").replace(self.task.split("-")[1],str(int(self.task.split("-")[1])-self.seconds)))
                    else:
                        self.vw.UserText.setText("Actual:\n" + self.routine[self.task_selector].replace("_"," "))
                    if not self.task_selector + 1 >= len(self.routine):
                        self.vw.BotText.setText("Siguiente:\n" + self.routine[self.task_selector + 1].replace("_"," "))
                    else:
                        self.vw.BotText.setText("Primera\n" + self.routine[0].replace("_"," "))
                    last_task = self.task

                r.sleep()

                # except:
                #     print("Fallo--")
                #     pass

    #############################################################################
    def loc_at_start(self):
    #############################################################################

        # if self.debug >= 2:     print("         loc_at_start")
        try:
            if "robot" in self.coordinates:
                self.nav.locate("robot")
            else:
                self.nav.locate("entrada")
        except:
            pass

    #############################################################################
    def next_task(self):
    #############################################################################

        #print(self.task)
        self.task_selector += 1
        if self.task_selector >= len(self.routine):
            self.task_selector = 0
        self.clear_task()

    #############################################################################
    def previous_task(self):
    #############################################################################

        self.task_selector -= 1
        if self.task_selector <= 0:
            self.task_selector = len(self.routine) - 1
        self.clear_task()

    #############################################################################
    def clear_task(self):
    #############################################################################

        self.task = self.routine[self.task_selector]
        self.seconds = 0
        if self.subprocess != None and self.state != None:
            self.nav.status = "None"
            self.nav.cancel_goal()
            #self.nav.clear_cost_maps()
        self.showing_element = ""
        self.playing_phrase = ""
        #self.vw.player.stop()
        self.set_routine_leds(self.task)
        print(self.task)

    #############################################################################
    def micCallback(self, msg):
    #############################################################################

        # if self.debug >= 2:     print("         micCallback " + msg.data)

        if (self.state == "first_command" or self.state == "awaiting") and self.playing == False and self.robot_started == True and len(msg.data) >= 2:
            self.processing = True
            self.vw.processing_label.show()
            #print(msg.data)

            msg.data = msg.data.replace("localizarte", "localízate").replace("lider", "ITER").replace(" líder", " ITER").replace(" litera", " ITER").replace(" itera", " ITER").replace("marisol", "marysol").replace("marisa", "marysol").replace("el te", "el ITER")
            msg.data = msg.data.replace("localizaste", "localízate").replace("localizaciones", "localízate").replace("localiza te", "localízate").replace("localiza de ", "localízate ").replace("localizada", "localízate").replace("localizaste", "localízate").replace("localizate que", "localízate").replace("localizaste", "localízate").replace("los caliza de", "localízate en")
            msg.data = msg.data.replace("localizasteda", "localízate").replace("lo caliza de", "localízate").replace("los caliza de", "localízate").replace("localizatener", "localízate")
            msg.data = msg.data.replace("te la vuelta", "date la vuelta").replace("de la vuelta", "date la vuelta").replace("dirigente hasta", "dirígete hasta").replace("muestrame", "muéstrame").replace("electrico", "eléctrico").replace("dirigete ", "dirígete ")
            msg.data = msg.data.replace("muestran ", "muéstrame ").replace("tenerife", "Tenerife")
            msg.data = msg.data.replace("dabate la vuelta", "date la vuelta")
            msg.data = msg.data.replace(" madison", " marysol")
            msg.data = msg.data.replace("dice de hasta ", "dirígete hasta ").replace("diste de hasta ", "dirígete hasta ").replace("de este hasta ", "dirígete hasta ").replace("de te hasta ", "dirígete hasta ").replace("teniente hasta ", "dirígete hasta ").replace("dije te hasta ", "dirígete hasta ")
            msg.data = msg.data.replace(" electrica", " eléctrica").replace(" robotica", " robótica").replace(" genomico", " genómica").replace(" genómico", " genómica")

            if self.state == "first_command":
                marysol_core.parse_firts_command(msg)
            elif self.state == "awaiting":
                self.vw.UserText.setText(msg.data.capitalize().replace(" iter", " ITER"))
                if self.state == "awaiting":
                    self.vw.awaiting_label.hide()
                    marysol_core.parser_awaiting(msg)

            self.ticks = 0
            self.hide_ticks = 0
            self.vw.processing_label.hide()
            self.processing = False

    #############################################################################
    def parse_firts_command(self, msg):
    #############################################################################

        # if self.debug >= 4:     print("         parse_firts_command " + msg.data)
        if msg.data in self.key_word["robot_name"] and msg.data.count(" ") == 0:
            # if self.debug >= 2:     print("         parse_firts_command")
            self.reset()
            self.vw.awaiting_label.show()
            self.vw.show()
            #self.show()
            self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.avatar_button))
            self.uncheck_but(self.avatar_button)
            if self.subprocess_follow != None:
                self.stop_follow()
                self.vw.setWindowState(Qt.WindowFullScreen)
            if self.wm.subprocess != None:
                self.next_task()
                self.wm.close()
                self.kb.hide()
                self.vw.setWindowState(Qt.WindowFullScreen)
            self.vw.UserText.setText("Marysol")
            # if the array_mic is active lets turn the robot to the user
            if self.use_array_mic and not self.stop.data and abs(self.sound_angle) > 60:
                #self.vw.UserText.setText("> 30 || " + str(self.sound_angle))
                self.reset(["going-sound_pose", "say-te escucho-awaiting", "await-10", "none"])
            else:
                #self.vw.UserText.setText("< 30 || " + str(self.sound_angle))
                self.reset(["say-te escucho-awaiting", "await-10", "none"])

    #############################################################################
    def update(self, message=None, leds=None):
    #############################################################################

        # if self.debug >= 4:     print("         update " + str(message))
        #if self.playing:
        self.vw.player.stop()
        #while self.playing == True:
            #pass
        self.vw.update(message)
        if not self.on_routine:
            self.vw.BotText.setText(message.replace("_"," ").capitalize().replace(" iter", " ITER"))
        while self.playing == False:
            pass
        #rospy.loginfo("Bot: %s" % message)
        self.playing_phrase = message.replace("speech_","")
        self.hide_text_ticks = 0

    #############################################################################
    def set_routine_leds(self, task):
    #############################################################################

        if self.use_leds:
            self.leds.set_static_task(task)

    #############################################################################
    def set_state(self, state):
    #############################################################################

        self.state = state
        if self.use_leds:
            self.leds.set_state(state)

    #############################################################################
    def parser_awaiting(self, msg):
    #############################################################################

        # if self.debug >= 4:     print("         parser_awaiting " + msg.data)
        user_input = msg.data
        #rospy.loginfo("User: %s" % msg.data)
        if self.stop.data and (
                                self.check.check_move_command(user_input) or
                                self.check.check_move_around(user_input) or
                                self.check.check_turn_around(user_input) or
                                self.check.check_follow(user_input) or
                                self.check.check_turn_off(user_input) or
                                self.check.check_resend_goal_command(user_input) or
                                self.check.check_routine(user_input)
                                ):
            self.reset(["say-Botón de emergencia activado-failed", "none"])

        else:

            if self.check.check_locate(user_input):
                if self.check.it.count(" ") == 0:
                    #self.vw.UserText.setText(str(self.check.it))
                    self.reset(["say-Nueva localización, " + str(self.check.it).replace("_"," ") + "-ok", "locate-" + self.check.it, "none"])
                else:
                    self.reset(["say-" + self.check.it + "-failed", "none"])

            elif self.check.clear_cost_maps(user_input):
                self.reset(["say-De acuerdo-ok", "none"])
                self.nav.clear_cost_maps()

            elif self.check.check_show(user_input):
                if self.check.it.count(" ") == 0:
                    self.reset(["say-De acuerdo-ok", "show-" + self.check.it , "none"])
                else:
                    self.reset(["say-" + self.check.it + "-failed", "none"])

            elif self.check.check_turn_off(user_input):
                self.check.it.insert(0, "say-De acuerdo-ok")
                self.check.it.append("none")
                self.reset(self.check.it)

            elif self.check.check_move_command(user_input):
                if self.nav.place_exist(self.check.it):
                    self.goal_to_routine(self.check.it)
                else:
                    self.reset(["say-Lo siento, no conozco el lugar " + self.check.it + "-failed", "none"])

            elif self.check.check_move_around(user_input):
                self.moveAround()

            elif self.check.check_turn_around(user_input):
                self.reset(["say-De acuerdo-ok", "none"])
                self.turnAround()

            elif self.check.check_follow(user_input):
                self.vw.FollowButton.setChecked(True)
                self.FollowButtonPressed()

            elif self.check.check_resend_goal_command(user_input):
                self.reset(["say-De acuerdo-ok", "none"])
                self.nav.resend_goal(1)

            elif self.check.check_routine(user_input):
                if self.check.it in self.routines:
                    self.set_routine(self.routines[self.check.it])
                else:
                    self.reset(["say-La rutina " + self.check.it.replace("_", " ") + " no está definida-failed", "none"])

            elif self.check.check_common_questions(user_input):
                self.vw.UserText.setText(self.check.it[1].replace(" iter", " ITER"))
                self.check.it.remove(self.check.it[1])
                self.check.it.append("none")
                self.reset(self.check.it)

            else:
                #chatbot_ans = self.chatbot.get_response(user_input).text
                #print(chatbot_ans)
                #if chatbot_ans == "Lo siento, no he podido entenderte":
                self.reset(["say-No tengo respuesta para " + user_input + "-failed", "none"])
                #else:
                #   self.reset(["say-chatbot_ans-simple_white", "none"])

        self.set_state("first_command")

    #############################################################################
    def reset(self, routine="none"):
    #############################################################################

        # if self.playing:
        #     self.vm.player.stop()
        #     rospy.sleep(0.1)
        self.task_selector_ = self.task_selector
        self.routine_ = self.routine
        self.task_ = self.task
        self.on_routine = False
        self.set_state("first_command")
        if isinstance(routine, list):
            self.routine = routine
        else:
            self.routine = []
            self.routine.append(routine)
        self.task_selector = 0
        self.clear_task()

    #############################################################################
    def resume(self):
    #############################################################################

        try:
            self.task_selector = self.task_selector_
            self.routine = self.routine_
            self.task = self.task_
        except:
            self.reset()

    #############################################################################
    def set_routine(self, routine):
    #############################################################################

        self.reset(routine)
        self.on_routine = True

    #############################################################################
    def listeningCallback(self, msg):
    #############################################################################
        self.listening = msg.data
        if self.listening:
            self.vw.listening_label.show()
        else:
            self.vw.listening_label.hide()

    #############################################################################
    def playingCallback(self, msg):
    #############################################################################
        if msg.data:
            self.playing = msg.data
            self.vw.talking_label.show()
        else:
            rospy.sleep(0.5)
            self.playing = msg.data
            self.vw.talking_label.hide()
            self.vw.player.stop()
            self.vw.VideoWidget.hide()
            # Preload videos before calling
            # if self.state == "first_command":
            #     if self.vw.player.currentMedia() != self.vw.video_list["te_escucho.mp4"]:
            #         self.vw.player.setMedia(self.vw.video_list["te_escucho.mp4"])
            # if self.state == "awaiting":
            #     if self.vw.player.currentMedia() != self.vw.video_list["tiempo_excedido.mp4"]:
            #         self.vw.player.setMedia(self.vw.video_list["tiempo_excedido.mp4"])

    #############################################################################
    def goal_to_routine(self, goal):
    #############################################################################

        self.reset(["say-De acuerdo-ok", "go-" + goal, "say-Objetivo " + goal.replace("_", " ") + " alcanzado-succed", "none"])


    #############################################################################
    def del_button(self, button):
    #############################################################################
        if button != "punto_de_carga" or button != "comienzo":
            del self.coordinates[button]
            self.rewrite_coordeinates()
            self.add_buttons_MainWindows_Map()

    #############################################################################
    def moveAround(self):
    #############################################################################

        if len(self.move_around_routine) > 0:
            self.routine = []
            for key in self.move_around_routine:
                if not "robot" in key:
                    self.routine.append(key)
                    self.routine.append("say-Objetivo " + key.split("-")[1].replace("_", " ") + " alcanzado-ok")
            self.set_routine(self.routine)
            rospy.sleep()
        else:
            self.routine.append("say-No se ha seleccionado ningún objetivo-failed")
            self.routine.append("none")
            self.reset(self.routine)

    #############################################################################
    def turnAround(self):
    #############################################################################
        pose = self.nav.current_pose
        from resources.utils import euler_from_quaternion
        z, y, x = euler_from_quaternion(pose.orientation,False)
        pose_array = []
        pose_array.append(pose.position.x)
        pose_array.append(pose.position.y)
        pose_array.append(z + 3.14)
        self.nav.send_goal(self.nav.xyz_to_pose(pose_array[0], pose_array[1], pose_array[2]), 1)

    #############################################################################
    def addNewButtonLoc(self):
    #############################################################################
        pose = self.nav.current_pose
        from resources.utils import euler_from_quaternion
        z, y, x = euler_from_quaternion(pose.orientation,False)
        pose_array = []
        pose_array.append(pose.position.x)
        pose_array.append(pose.position.y)
        pose_array.append(z)
        self.coordinates[self.vw.lineEdit.text().lower().replace(" ","_")] = pose_array
        self.rewrite_coordeinates()
        self.add_buttons_MainWindows_Map()

    #############################################################
    def rewrite_coordeinates(self):
    #############################################################
        for key in self.coordinates:
            for value in range(len(self.coordinates[key])):
                self.coordinates[key][value] = round(self.coordinates[key][value],2)
        try:
            os.remove(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/coordinates_" + self.map + "_" + self.env + ".yaml")
        except:
            pass
        with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/coordinates_" + self.map + "_" + self.env + ".yaml", 'a') as file:
            file.write("# yaml file holding places in " + self.env + " map " + self.map + "\n")
            for key in self.coordinates:
                line = key + ": ["
                for value in range(len(self.coordinates[key])):
                    line += str(self.coordinates[key][value])
                    if value == len((self.coordinates[key])) - 1:
                        line += "]\n"
                    else:
                        line += ","
                file.write(line)

    #############################################################################
    def organize_move_around(self, button, xyz):
    #############################################################################
        if not button.isChecked():
            self.move_around_routine.append("go-" + str(xyz))
        else:
            button.setText("")
            del self.move_around_routine[self.move_around_routine.index("go-" + str(xyz))]

        from resources.utils import normalize
        for x in self.vw.move_around_buttons:
            for y in self.move_around_routine:
                x.setText("")
                if x.objectName() in normalize(y):
                    x.setText(str(self.move_around_routine.index(y) + 1))
                    break

    #############################################################
    def add_buttons_MainWindows_Map(self):
    #############################################################

        #### Adding buttons ####
        self.vw.destroy_buttons_map()
        self.vw.go_buttons = []
        self.vw.loc_buttons = []
        self.vw.del_buttons = []
        self.vw.move_around_buttons = []
        from resources.utils import normalize
        for coordinate in self.coordinates:
            if coordinate != "robot":
                #   GO
                button = self.vw.CreateButton(normalize(str(coordinate)), 25, "Go " + str(coordinate).capitalize().replace("_"," "), None, None, False, minYsize=100)
                button.pressed.connect(lambda x=coordinate: self.goal_to_routine(x))
                self.vw.go_buttons.append(button)
                self.vw.go_layout.addWidget(button)
                #   LOC
                button = self.vw.CreateButton(normalize(str(coordinate)), 25, "Loc " + str(coordinate).capitalize().replace("_"," "), None, None, False, minYsize=100)
                button.pressed.connect(lambda x=coordinate: self.reset(["say-Nueva localización, " + str(x).replace("_"," ") + "-ok", "locate-" + x, "none"]))
                self.vw.loc_buttons.append(button)
                self.vw.loc_layout.addWidget(button)
                #   DEL
                button = self.vw.CreateButton(normalize(str(coordinate)), 25, "", "trash", "trash", False, minYsize=100)
                button.pressed.connect(lambda x=coordinate: self.del_button(x))
                self.vw.del_buttons.append(button)
                self.vw.del_button_layout.addWidget(button)
                #   MoveAround
                button = self.vw.CreateButton(normalize(str(coordinate)), 25, "", "check", "uncheck", True, minYsize=100)
                button.pressed.connect(lambda b=button, c=coordinate: self.organize_move_around(b,c))
                self.vw.move_around_buttons.append(button)
                self.vw.move_around_layout.addWidget(button)

    #############################################################
    def FollowButtonPressed(self):
    #############################################################

        if self.subprocess_follow == None:
            if self.vw.FollowButton.isChecked():
                self.vw.FollowButton.setChecked(True)

            self.reset(["say-De acuerdo-ok", "follow", "none"])
            command = "roslaunch ~/marysol/catkin_ws/src/marysol_core/launch/continuous_detection_and_apriltag2pose.launch sim:=" + str(self.sim) + " >/dev/null 2>&1"
            self.subprocess_follow = subprocess.Popen(command, shell=True)
            self.nav.clear_cost_maps()
        else:

            self.reset()
            self.stop_follow()

    #############################################################################
    def stop_follow(self):
    #############################################################################

        if not self.vw.FollowButton.isChecked():
            self.vw.FollowButton.setChecked(False)
        if self.subprocess_follow != None:
            import psutil
            process = psutil.Process(self.subprocess_follow.pid)
            for proc in process.children(recursive=True):
                proc.kill()
            process.kill()
            self.subprocess_follow = None
            self.qr_pose = None

    #############################################################################
    def qr_poseCallback(self, msg):
    #############################################################################

        self.qr_pose = msg.pose

    #############################################################################
    def sound_directionCallback(self, msg):
    #############################################################################

        self.sound_angle = (msg.data + 90)
        if self.use_array_mic:
            robot_angle = (180 / 3.14) * self.nav.pose_to_xyz(self.nav.current_pose)[2]
            self.sound_direction = (robot_angle - self.sound_angle) * (3.14 / 180)

    #############################################################
    def add_buttons_MainWindows_resourse(self):
    #############################################################

        self.vw.destroy_buttons_resources()
        self.vw.video_buttons = []
        self.vw.photo_buttons = []
        self.vw.text_buttons = []
        self.vw.web_buttons = []
        self.vw.routine_buttons = []
        self.vw.speech_buttons = []
        self.vw.PDF_buttons = []
        windows_names = {}
        windows_names["visual_window"] = ["MarysolAvatarWindow", "None"]
        windows_names["top_window"] = ["MarysolTopWindow", "None"]
        windows_names["visualización_interna"] = ["rviz", "roslaunch ~/marysol/catkin_ws/src/marysol_remote/launch/rviz/rviz_marysol_navigate.launch"]
        for folders in os.listdir(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media'):
            if folders == "webs":
                path = expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/webs/'
                for text in os.listdir(path):
                    os.rename(path + text, path + text.lower().replace(" ","_"))
                    text = text.lower().replace(" ","_")
                    aux_text = text.split(".")
                    button = self.vw.CreateButton(str(aux_text[0]), 20, str(aux_text[0]).capitalize().replace("_", " "), None, None, False)
                    temp_file = open(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/webs/' + text, "r")
                    temp_text = temp_file.readline()
                    if temp_text.startswith("www.") or temp_text.startswith("https://") or temp_text.startswith("http://"):
                        temp_file = open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/webs/" + button.objectName() + ".txt", "r")
                        temp_file = temp_file.read().splitlines()
                        cmd = "google-chrome --kiosk " + temp_file[0]
                        windows_names[button.objectName()] = [temp_file[1], cmd]
                        button.pressed.connect(lambda ObjName=button.objectName(): self.reset(["show-" + ObjName, "none"]))
                        self.vw.web_buttons.append(button)
                        self.vw.text_layout.addWidget(button)
            elif folders == "fotos":
                path = expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/fotos/'
                for photo in os.listdir(path):
                    os.rename(path + photo, path + photo.lower().replace(" ","_"))
                    photo = photo.lower().replace(" ","_")
                    aux_photo = photo.split(".")
                    button = self.vw.CreateButton(str(aux_photo[0]), 20, str(aux_photo[0]).capitalize().replace("_", " "), None, None, False)
                    image_path = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/fotos/" + photo
                    cmd = "eog --fullscreen " + image_path
                    windows_names[button.objectName()] = [button.objectName(), cmd]
                    button.pressed.connect(lambda ObjName=button.objectName(): self.reset(["show-" + ObjName, "none"]))
                    self.vw.photo_buttons.append(button)
                    self.vw.photo_layout.addWidget(button)
            elif folders == "videos":
                path = expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/videos/'
                for video in os.listdir(path):
                    os.rename(path + video, path + video.lower().replace(" ","_"))
                    video = video.lower().replace(" ","_")
                    aux_video = video.split(".")
                    button = self.vw.CreateButton(str(aux_video[0]), 20, str(aux_video[0]).capitalize().replace("_", " "), None, None, False)
                    image_path = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/videos/" + video
                    cmd = "mplayer -fs " + image_path
                    windows_names[button.objectName()] = [button.objectName(), cmd]
                    button.pressed.connect(lambda ObjName=button.objectName(): self.reset(["show-" + ObjName, "none"]))
                    self.vw.video_buttons.append(button)
                    self.vw.video_layout.addWidget(button)
            elif folders == "rutinas":
                path = expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/rutinas/'
                for routine in os.listdir(path):
                    aux_routine = routine.split(".")
                    button = self.vw.CreateButton(str(aux_routine[0]), 20, str(aux_routine[0]).capitalize().replace("_", " "), None, None, False)
                    routine_path = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/rutinas/" + routine
                    with open(routine_path) as f:
                        lines = f.readlines()
                        self.routines[button.objectName().lower().replace(" ","_")] = []
                        for line in lines:
                            self.routines[button.objectName().lower().replace(" ","_")].append(line.replace("\n","").lower().replace(" ","_"))
                    button.pressed.connect(lambda routine=self.routines[button.objectName().lower().replace(" ","_")]: self.set_routine(routine))
                    self.vw.routine_buttons.append(button)
                    self.vw.routines_layout.addWidget(button)
            elif folders == "PDF":
                path = expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/PDF/'
                for pdf in os.listdir(path):
                    os.rename(path + pdf, path + pdf.lower().replace(" ","_"))
                    pdf = pdf.lower().replace(" ","_")
                    aux_pdf = pdf.split(".")
                    button = self.vw.CreateButton(str(aux_pdf[0]), 20, str(aux_pdf[0]).capitalize().replace("_", " "), None, None, False)
                    image_path = expanduser("~") + "/marysol/catkin_ws/src/marysol_core/media/PDF/" + pdf
                    cmd = "evince --fullscreen " + image_path
                    windows_names[button.objectName()] = [button.objectName(), cmd]
                    button.pressed.connect(lambda ObjName=button.objectName(): self.reset(["show-" + ObjName, "none"]))
                    self.vw.PDF_buttons.append(button)
                    self.vw.PDFs_layout.addWidget(button)
            elif folders == "speechs":
                path = expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/speechs/'
                for speech in os.listdir(path):
                    if ".txt" in speech:
                        os.rename(path + speech, path + speech.lower().replace(" ","_"))
                        speech = speech.lower().replace(" ","_")
                        aux_speech = speech.split(".")
                        button = self.vw.CreateButton(str(aux_speech[0]), 20, str(aux_speech[0]).capitalize().replace("_", " "), None, None, False)
                        button.pressed.connect(lambda ObjName=button.objectName(): self.reset(["speech-" + ObjName.lower(), "none"]))
                        self.vw.speech_buttons.append(button)
                        self.vw.speechs_layout.addWidget(button)
        return windows_names

    #############################################################
    def add_buttons_MainWindows_debug(self):
    #############################################################

        from PyQt5 import QtCore
        from resources.utils import normalize
        self.vw.destroy_buttons_debug()
        self.nm.get_nodelist()
        button_size = 75
        for node in self.nm.nodelist:
            #Creamos y añadimos los nodos a la columna nombre
            temp = QLabel(node.replace("_"," ").capitalize())
            temp.setMaximumSize(QtCore.QSize(2048, button_size))
            temp.setMinimumSize(QtCore.QSize(2048, button_size))
            temp.setFont(QFont("Arial", 20))
            self.vw.node_labels.append(temp)
            self.vw.rosnode_name.addWidget(temp)
            #Creamos y añadimos el boton de reiniciar a cada nodo
            button = self.vw.CreateButton(normalize(node), 20, "", "on", "off", checkeable=True, checked=False, maxYsize=button_size, minYsize=button_size, maxXsize=button_size, minXsize=button_size)
            button.pressed.connect(lambda node_=node, button_=button: self.nm.recall_subprocess(node_, button_))
            self.vw.node_buttons.append(button)
            self.vw.rosnode_reboot.addWidget(button)
            #Creamos el subproceso que comprueba si el modulo esta activo
            self.nm.nodelist[node].append(button)

    #############################################################
    def refresh_USB_layout(self):
    #############################################################
        from PyQt5 import QtCore
        from resources.utils import normalize
        from resources.utils import get_mount_points
        usb_sticks = get_mount_points()
        self.vw.destroy_USB_elements()
        def copytree(USB, label):
            import shutil
            if os.path.exists(USB + "/media"):
                self.vw.USB_path = USB
                if os.path.exists(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media'):
                    shutil.rmtree(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media')
                shutil.copytree(USB + "/media", expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media')
                self.add_buttons_MainWindows_resourse()
                self.vw.add_videos(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/speechs/')
                label.setStyleSheet("font-weight: bold; color: green")
                label.setText(USB.split("/")[3].capitalize() + " /media ok")
            else:
                label.setStyleSheet("font-weight: bold; color: red")
                label.setText(USB.split("/")[3].capitalize() + " no /media")

        for USB in usb_sticks:
            USB_name = USB.split("/")[3].capitalize()
            #Creamos y añadimos los nodos a la columna nombre
            temp = QLabel(USB_name)
            temp.setMaximumSize(QtCore.QSize(2048, 60))
            temp.setMinimumSize(QtCore.QSize(2048, 60))
            temp.setFont(QFont("Arial", 20))
            self.vw.USB_labels.append(temp)
            self.vw.USB_sticks_list_layout.addWidget(temp)
            #Creamos y añadimos el boton de reiniciar a cada nodo
            button = self.vw.CreateButton(normalize(USB_name), 20, "", "copy", "copy-flat", checkeable=True, checked=False, maxYsize=60, minYsize=60, maxXsize=60, minXsize=60)
            button.pressed.connect(lambda USB_=USB, temp_=temp: copytree(USB_, temp_))
            self.vw.USB_buttons.append(button)
            self.vw.copyfiles_button_layout.addWidget(button)
        self.add_buttons_MainWindows_resourse()
        self.vw.add_videos(expanduser("~") + '/marysol/catkin_ws/src/marysol_core/media/speechs/')

    #############################################################
    def refresh_audio_input_layout(self):
    #############################################################
        from PyQt5 import QtCore
        from resources.utils import normalize
        from resources.utils import get_active_mics, set_mic
        active_mics = get_active_mics()
        self.vw.destroy_mic_elements()
        self.vw.mic_inactive_label.show()
        def set_mic_(mic, label, button):
            if mic == 'ReSpeaker 4 Mic Array ':
                self.use_array_mic = True
            else:
                self.use_array_mic = False
            self.vw.mic_inactive_label.hide()
            self.default_sound_settings["default_input"] = mic
            with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/sound_default.yaml", "w") as file:
                yaml.dump(self.default_sound_settings, file)
            for lab in self.vw.mic_labels:
                lab.setStyleSheet("")
            for but in self.vw.mic_buttons:
                but.setChecked(False)
            button.setChecked(False)
            label.setStyleSheet("font-weight: bold; color: green")
            set_mic(active_mics[mic])
            if self.subprocess != None:
                if self.subprocess_speech_recognition != None:
                    if "/respeaker_ros" in self.nm.active_nodes:
                        os.system("rosnode kill /respeaker_ros")
                    import psutil
                    process = psutil.Process(self.subprocess_speech_recognition.pid)
                    for proc in process.children(recursive=True):
                        proc.kill()
                    process.kill()
                    self.subprocess_speech_recognition = None
                if self.use_array_mic:
                    command = "roslaunch ~/marysol/catkin_ws/src/marysol_speech/launch/speech_recognition_from_array.launch >/dev/null 2>&1"
                else:
                    command = "roslaunch ~/marysol/catkin_ws/src/marysol_speech/launch/speech_recognition_from_mic.launch >/dev/null 2>&1"
                self.subprocess_speech_recognition = subprocess.Popen(command, shell=True)

        for mic in active_mics:
            #Creamos y añadimos los nodos a la columna nombre
            if not "Monitor" in mic:
                temp = QLabel(mic.capitalize().replace("-", " ").replace("_", " "))
                temp.setMaximumSize(QtCore.QSize(2048, 60))
                temp.setMinimumSize(QtCore.QSize(2048, 60))
                temp.setFont(QFont("Arial", 20))
                self.vw.mic_labels.append(temp)
                self.vw.mic_list_layout.addWidget(temp)
                #Creamos y añadimos el boton de reiniciar a cada nodo
                button = self.vw.CreateButton(normalize(mic).replace(" ", "_"), 20, "", "jack-cable", "jack-cable-flat", checkeable=True, checked=False, maxYsize=60, minYsize=60, maxXsize=60, minXsize=60)
                button.pressed.connect(lambda mic_=mic, temp_=temp, button_=button: set_mic_(mic_, temp_, button_))
                self.vw.mic_buttons.append(button)
                self.vw.select_mic_button_layout.addWidget(button)
                if self.default_sound_settings["default_input"] == mic:
                    button.setChecked(True)
                    set_mic_(mic, temp, button)

    #############################################################
    def refresh_audio_output_layout(self):
    #############################################################
        from PyQt5 import QtCore
        from resources.utils import normalize
        from resources.utils import get_active_speakers, set_speaker
        active_speakers = get_active_speakers()
        self.vw.destroy_speaker_elements()
        def set_speaker_(speaker, label, button):
            self.default_sound_settings["default_output"] = speaker
            with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/sound_default.yaml", "w") as file:
                yaml.dump(self.default_sound_settings, file)
            for lab in self.vw.speaker_labels:
                lab.setStyleSheet("")
            for but in self.vw.speaker_buttons:
                but.setChecked(False)
            button.setChecked(False)
            label.setStyleSheet("font-weight: bold; color: green")
            set_speaker(active_speakers[speaker])
        for speaker in active_speakers:
            #Creamos y añadimos los nodos a la columna nombre
            temp = QLabel(speaker.capitalize().replace("-", " ").replace("_", " "))
            temp.setMaximumSize(QtCore.QSize(2048, 60))
            temp.setMinimumSize(QtCore.QSize(2048, 60))
            temp.setFont(QFont("Arial", 20))
            self.vw.speaker_labels.append(temp)
            self.vw.speakers_sticks_list_layout.addWidget(temp)
            #Creamos y añadimos el boton de reiniciar a cada nodo
            button = self.vw.CreateButton(normalize(speaker).replace(" ", "_"), 20, "", "jack-cable", "jack-cable-flat", checkeable=True, checked=False, maxYsize=60, minYsize=60, maxXsize=60, minXsize=60)
            button.pressed.connect(lambda speaker_=speaker, temp_=temp, button_=button: set_speaker_(speaker_, temp_, button_))
            self.vw.speaker_buttons.append(button)
            self.vw.selec_speaker_button_layout.addWidget(button)
            if self.default_sound_settings["default_output"] == speaker:
                button.setChecked(True)
                set_speaker_(speaker, temp, button)

    #############################################################
    def battery_status_callback(self, msg):
    #############################################################
        mean = np.mean(msg.data) / 100
        #p = np.polyfit([13.20, 14.2], [51, 100], 1)
        p = np.polyfit([13.20, 14.2], [65, 100], 1)
        percent = mean * p[0] + p[1]
        #self.battery_status.setText("{0:.2f}".format(percent) + " %")
        if percent > 70:
            #self.battery_status.setStyleSheet("color:blue")
            self.vw.bat4.show()
        elif percent > 55:
            #self.battery_status.setStyleSheet("color:green")
            self.vw.bat3.show()
            self.vw.bat4.hide()
        elif percent > 40:
            #self.battery_status.setStyleSheet("color:orange")
            self.vw.bat2.show()
            self.vw.bat3.hide()
        elif percent > 25:
            #self.battery_status.setStyleSheet("color:red")
            self.vw.bat1.show()
            self.vw.bat2.hide()
        else:
            #self.battery_status.setStyleSheet("color:red")
            self.vw.bat0.show()
            self.vw.bat1.hide()

        if self.use_leds:
            self.leds.Battery(percent)

    #############################################################
    def stopCallback(self, msg):
    #############################################################

        if msg.data[5] == 0:
            self.stop.data = False
        elif msg.data[5] == 1:
            self.stop.data = True

    #############################################################
    def AutoStop_func(self):
    #############################################################

        for day in self.days_week:
            if self.days_week.index(day) == self.date.dayOfWeek() and day.isChecked():
                if self.currentTime.currentTime() > self.vw.apagado.time() and self.currentTime.currentTime() < self.vw.apagado.time().addSecs(10) and self.task == "none" and self.hide_ticks % 5 == 0:
                    self.StartTurningOff()

    #############################################################
    def StartTurningOff(self):
    #############################################################

        if self.subprocess != None:
            Turn_Off_Routine = []
            Turn_Off_Routine.append("go-punto_de_carga")
            Turn_Off_Routine.append("say-adiós-ok")
            Turn_Off_Routine.append("command-shutdown -h now")
            Turn_Off_Routine.append("none")
            self.reset(Turn_Off_Routine)
        else:
            self.Turn_Off()

    #############################################################
    def Turn_Off(self):
    #############################################################

        if self.use_leds:
            self.leds.Deactivate()
            rospy.sleep(2)
        if self.subprocess != None:
            self.power_button.setChecked(False)
            self.shut_down()
        self.vw.stackedWidget.setCurrentIndex(self.buttons.index(self.charging_button))
        self.uncheck_but(self.charging_button)
        # self.vw.maintenance_works.setChecked(True)
        # self.vw.start_subprocess()
        # rospy.sleep(1)
        # while self.vw.subprocess_maintenance != None:
        #     pass
        sudoPassword = "marysol"
        if self.vw.AutoStart.isChecked():
            self.set_power_defaults()
            min = self.calc_min_to_restart()
            from resources.utils import string2speech, sec2time
            time_elapsed = sec2time(min * 60)
            string2speech("Próximo reinicio en " + str(time_elapsed[0]).replace("1","un") + " dia " + str(time_elapsed[1]) + " horas y " + str(time_elapsed[2]) + " minutos")
            command = 'sh -c "echo 0 > /sys/class/rtc/rtc0/wakealarm"'
            os.system('echo %s|sudo -S %s >/dev/null 2>&1' % (sudoPassword, command))
            command = "sh -c \"echo `date '+%s' -d '+ " + str(min) + " minutes'` > /sys/class/rtc/rtc0/wakealarm\""
            os.system('echo %s|sudo -S %s >/dev/null 2>&1' % (sudoPassword, command))
            command = 'cat /sys/class/rtc/rtc0/wakealarm'
            os.system('echo %s|sudo -S %s >/dev/null 2>&1' % (sudoPassword, command))
            rospy.sleep(8)
        command = 'shutdown -h now'
        os.system('echo %s|sudo -S %s >/dev/null 2>&1' % (sudoPassword, command))

    #############################################################
    def calc_min_to_restart(self):
    #############################################################
        # Calc the amount of minutes till next start up
        min = 60 * (self.vw.encendido.time().hour() - self.currentTime.currentTime().hour()) + self.vw.encendido.time().minute() - self.currentTime.currentTime().minute()
        today = self.date.dayOfWeek() - 1
        days_off = 0
        next_day = today + 1
        if min < 0:
            days_off += 1
        elif not self.days_week[today].isChecked():
            days_off += 1
        for day in range(len(self.days_week)):
            if next_day >= len(self.days_week):
                next_day = 0
            if self.days_week[next_day].isChecked():
                break
            next_day += 1
            days_off += 1
        min += 1440 * days_off
        return min

    #############################################################
    def power_settings(self):
    #############################################################
        from PyQt5.QtCore import QTime, QDate
        self.days = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]
        self.days_week = [self.vw.checkBox_0, self.vw.checkBox_1, self.vw.checkBox_2, self.vw.checkBox_3, self.vw.checkBox_4, self.vw.checkBox_5, self.vw.checkBox_6]
        try:
            self.vw.encendido.setTime(QTime(self.default_power_settings["AutoStart_hour"],self.default_power_settings["AutoStart_min"],0,0))
            self.vw.apagado.setTime(QTime(self.default_power_settings["AutoStop_hour"],self.default_power_settings["AutoStop_min"],0,0))
            if self.default_power_settings["AutoStart"]:
                self.vw.AutoStart.setChecked(True)
            else:
                self.vw.AutoStart.setChecked(False)
            if self.default_power_settings["AutoStop"]:
                self.vw.AutoStop.setChecked(True)
            else:
                self.vw.AutoStop.setChecked(False)
            x = 0
            for day in self.default_power_settings["WeekDays"]:
                if day:
                    self.days_week[x].setChecked(True)
                else:
                    self.days_week[x].setChecked(False)
                x += 1
        except:
            pass
        def get_week_days():
            x = 0
            days = []
            for day in self.days_week:
                if day.isChecked():
                    days.append(True)
                else:
                    days.append(False)
                x += 1
            return days

        def set_power_defaults_AutoStart():
            self.default_power_settings["AutoStart"] = not self.vw.AutoStart.isChecked()
            self.default_power_settings["AutoStart_hour"] = self.vw.encendido.time().hour()
            self.default_power_settings["AutoStart_min"] = self.vw.encendido.time().minute()
            self.default_power_settings["WeekDays"] = get_week_days()
            with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/power_default.yaml", "w") as file:
                yaml.dump(self.default_power_settings, file)
        self.vw.AutoStart.pressed.connect(set_power_defaults_AutoStart)

        def set_power_defaults_AutoStop():
            self.default_power_settings["AutoStop"] = not self.vw.AutoStop.isChecked()
            self.default_power_settings["AutoStop_hour"] = self.vw.apagado.time().hour()
            self.default_power_settings["AutoStop_min"] = self.vw.apagado.time().minute()
            self.default_power_settings["WeekDays"] = get_week_days()
            with open(expanduser("~") + "/marysol/catkin_ws/src/marysol_core/config/power_default.yaml", "w") as file:
                yaml.dump(self.default_power_settings, file)
        self.vw.AutoStop.pressed.connect(set_power_defaults_AutoStop)
        self.currentTime = QTime()
        self.date = QDate().currentDate()


#############################################################
if __name__ == '__main__':
    """ main """
    parser = argparse.ArgumentParser()
    parser.add_argument("--env", help="world selector ", default="simulation")
    parser.add_argument("--map", help="map to use ", default="CV")
    parser.add_argument("--loc", help="location system ", default="fixed")
    args = parser.parse_args()

    app = QApplication([])

    marysol_core = MarysolCore(args)

    app.exec_()
