# Marysol core.
Es la clase principal, asociada a la ventana TopWindow, compuesta por diferentes botones que abren un menú diferente el VisualWindow. En esta clase

# Clases

* [VisialWindow] - Clase que hereda la ventana VisualWindow, compuesta por un stackedWidget con tantas pestañas como menús tenga el TopWindow.
* [Navigation] - Clase donde se gestiona todo lo realcionado con la navegación que afecta al core
* [Check] - Clase que da una respuesta lógica al comando dado por el usuario
* [WindowsManagment] - Clase que controla el gestor de ventanas de Ubuntu, poniendo ventanas al frente o al final

# Ejecución
Ejecutar el archivo Marysol_real o Marysol_simulation dentro de la carpeta scripts del paquete marysol core
