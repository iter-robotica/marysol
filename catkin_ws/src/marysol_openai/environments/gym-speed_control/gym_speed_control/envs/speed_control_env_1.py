import gym
from gym import error, spaces, utils, logger
from gym.utils import seeding
import rospy
import rospkg
from sensor_msgs.msg import LaserScan, Imu
from std_msgs.msg import Float32MultiArray, Int32MultiArray, Int16MultiArray, Float32, Bool
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
import numpy as np
from random import randint


class speed_controlEnv_1(gym.Env):
    metadata = {'render.modes': ['human']}


    def __init__(self):
        """
        Every environment should be derived from gym.Env and at least contain the variables observation_space and action_space
        specifying the type of possible observations and actions using spaces.Box or spaces.Discrete.

        Example:
        >>> EnvTest = speed_controlEnv()
        >>> EnvTest.observation_space=spaces.Box(low=-1, high=1, shape=(3,4))
        >>> EnvTest.action_space=spaces.Discrete(2)
        """

        rospy.init_node('speed_control', anonymous=True, log_level=rospy.FATAL)
        nodename = rospy.get_name()
        rospy.loginfo("%s started" % nodename)

        self.train = rospy.get_param("~train", True)
        self.cmd_int = rospy.get_param("~cmd_int", False)
        self.use_divisor = rospy.get_param("~use_divisor", True)

        self.timesteps = 0

        # We Start all the ROS related Subscribers and publishers
        rospy.Subscriber("/odom", Odometry, self._odom_callback)
        rospy.Subscriber("/imu", Imu, self._imu_callback)
        rospy.Subscriber("/cmd_vel", Twist, self._cmd_vel_callback)
        #rospy.Subscriber("/scan", LaserScan, self._laser_scan_callback)
        rospy.Subscriber('/commands/controlRPM_info', Float32MultiArray, self._RPM_cmd_Callback)
        rospy.Subscriber('/commands/controlDuty', Float32MultiArray, self._duty_read_Callback)
        rospy.Subscriber('/state', Int16MultiArray, self._RPM_read_Callback)

        self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

        self.action_space_pub = rospy.Publisher('/speed_control/action_space', Float32MultiArray, queue_size=10)
        self.reward_pub = rospy.Publisher('/speed_control/reward', Float32MultiArray, queue_size=10)
        self.reward_out = Float32MultiArray()
        self.reward_out.data = np.zeros(2)

        self.ob_pub = rospy.Publisher('/speed_control/observation_space', Float32MultiArray, queue_size=10)
        self.ob_out = Float32MultiArray()

        self.action_space = spaces.Discrete(9)
        self.DRL_out = Float32MultiArray()
        self.DRL_out.data = np.zeros(2)

        #rospy.logdebug(self.action_space)

        self.observation_space = spaces.Box(low=-1, high=1, shape=(18,), dtype=np.float32)

        #rospy.logdebug(self.observation_space)

        self._check_all_systems_ready()

        self._wait_for_update()

        self.seed()

        self.divisor = 0
        self.booster = 0.01
        self.cmd_vel = Twist()
        self.cmd_vel_old = Twist()
        self.stop = Bool()

        self.diferencia_derecha_old = 0.0
        self.diferencia_izquierda_old = 0.0

        return None

    def seed(self, seed=None):

        self.np_random, seed = seeding.np_random(seed)

        return [seed]


    def step(self, action):
        """
        This method is the primary interface between environment and agent.

        Paramters:
        action: int
                the index of the respective action (if action space is discrete)

        Returns:
        output: (array, float, bool)
                information provided by the environment about its current state:
                (observation, reward, done)
        """

        self._wait_for_update()

        if self.cmd_vel != self.cmd_vel_old:
            self.timesteps = 0

        #print(action)

        # Si la velocidad es cero no entrenamos
        #if True: #self.train == False:
        if (self.cmd_vel.linear.x == 0.0 and self.cmd_vel.angular.z == 0.0) or self.stop.data:
            self.DRL_out.data[0] *= 0.75
            self.DRL_out.data[1] *= 0.75

        else:
            if action == 0:
                self.DRL_out.data[0] += -self.booster
                self.DRL_out.data[1] += -self.booster
            elif action == 1:
                self.DRL_out.data[0] += -self.booster
                self.DRL_out.data[1] += 0.00
            elif action == 2:
                self.DRL_out.data[0] += -self.booster
                self.DRL_out.data[1] += +self.booster
            elif action == 3:
                self.DRL_out.data[0] += 0.00
                self.DRL_out.data[1] += -self.booster
            elif action == 4:
                self.DRL_out.data[0] += 0.00
                self.DRL_out.data[1] += 0.00
            elif action == 5:
                self.DRL_out.data[0] += 0.00
                self.DRL_out.data[1] += +self.booster
            elif action == 6:
                self.DRL_out.data[0] += +self.booster
                self.DRL_out.data[1] += -self.booster
            elif action == 7:
                self.DRL_out.data[0] += +self.booster
                self.DRL_out.data[1] += 0.00
            elif action == 8:
                self.DRL_out.data[0] += +self.booster
                self.DRL_out.data[1] += +self.booster

        self.action_space_pub.publish(self.DRL_out)

        if self.cmd_int:
            self.cmd_vel_pub.publish(self.twist)

        obs = self._get_obs()
        done = self._is_done(obs)
        reward = self._compute_reward(obs, done)
        info = self._get_info()

        self.diferencia_derecha_old = self.diferencia_derecha
        self.diferencia_izquierda_old = self.diferencia_izquierda
        self.cmd_vel_old = self.cmd_vel

        return obs, reward, done, info

    def reset(self):
        """
        This method resets the environment to its initial values.

        Returns:
            observation:    array
                            the initial state of the environment
        """

        #print("se resetea")
        self.timesteps = 0

        self.twist = Twist()

        if self.cmd_int:
            opcion = randint(0, 3)
            if opcion == 0:
                print("avance con giro")
                self.twist.linear.x = randint(20, 40) / 100
                self.twist.linear.y = 0.0
                self.twist.linear.z = 0.0
                self.twist.angular.x = 0.0
                self.twist.angular.y = 0.0
                self.twist.angular.z = randint(-40, 40) / 100

            elif opcion == 1:
                print("giro izq")
                self.twist.linear.x = 0.0
                self.twist.linear.y = 0.0
                self.twist.linear.z = 0.0
                self.twist.angular.x = 0.0
                self.twist.angular.y = 0.0
                self.twist.angular.z = 0.5

            elif opcion == 2:
                print("giro der")
                self.twist.linear.x = 0.0
                self.twist.linear.y = 0.0
                self.twist.linear.z = 0.0
                self.twist.angular.x = 0.0
                self.twist.angular.y = 0.0
                self.twist.angular.z = -0.5

            elif opcion == 3:
                print("avance")
                self.twist.linear.x = 0.25
                self.twist.linear.y = 0.0
                self.twist.linear.z = 0.0
                self.twist.angular.x = 0.0
                self.twist.angular.y = 0.0
                self.twist.angular.z = 0.0

            elif opcion == 4:
                print("paro")
                self.twist.linear.x = 0.0
                self.twist.linear.y = 0.0
                self.twist.linear.z = 0.0
                self.twist.angular.x = 0.0
                self.twist.angular.y = 0.0
                self.twist.angular.z = 0.0

            self.cmd_vel_pub.publish(self.twist)

            self.DRL_out.data[0] = randint(-15, 15) / 100
            self.DRL_out.data[1] = randint(-15, 15) / 100
            self.action_space_pub.publish(self.DRL_out)

            #self.booster = randint(10, 40) / 1000

        else:
            print("cmd_vel externa")

        self._wait_for_update()
        return 0

    def render(self, mode='human', close=False):
        """
        This methods provides the option to render the environment's behavior to a window
        which should be readable to the human eye if mode is set to 'human'.
        """
        pass

#------------------------------------------------------------------------

    def _get_obs(self):

        observation_space = []

        RPM_cmd = self._get_rpm_cmd()
        observation_space.append(RPM_cmd.data[0]/100)
        observation_space.append(RPM_cmd.data[1]/100)

        RPM_read = self._get_rpm_read()
        observation_space.append(RPM_read.data[0]/100)
        observation_space.append(RPM_read.data[1]/100)

        Duty_cmd = self._get_duty_cmd()
        observation_space.append(Duty_cmd.data[0])
        observation_space.append(Duty_cmd.data[1])

        imu = self._get_imu()
        observation_space.append(imu.orientation.x)
        observation_space.append(imu.orientation.y)
        observation_space.append(imu.orientation.z)
        observation_space.append(imu.orientation.w)
        observation_space.append(imu.angular_velocity.x)
        observation_space.append(imu.angular_velocity.y)
        observation_space.append(imu.angular_velocity.z)
        observation_space.append(imu.linear_acceleration.x / 10)
        observation_space.append(imu.linear_acceleration.y / 10)
        observation_space.append(imu.linear_acceleration.z / 10)

        cmd_vel = self._get_cmd_vel()
        observation_space.append(cmd_vel.linear.x)
        observation_space.append(cmd_vel.angular.z)

        #observation_space.append(self.booster)

        self.diferencia_izquierda = abs(RPM_cmd.data[0]/100 - RPM_read.data[0]/100)
        self.diferencia_derecha = abs(RPM_cmd.data[1]/100 - RPM_read.data[1]/100)

        self.ob_out.data = []
        self.ob_out.data.append(self.diferencia_izquierda)
        self.ob_out.data.append(self.diferencia_derecha)
        self.ob_out.data.append(self.divisor)
        self.ob_out.data.append(observation_space[0])
        self.ob_out.data.append(observation_space[1])
        #self.ob_out.data.append(10 * self.booster)

        self.ob_pub.publish(self.ob_out)

        return observation_space

    def _compute_reward(self, observation, done):
        """
        Queremos que el robot se mueva a la velocidad deseada, por lo que la recompensa será mayor cuando la diferencia entre
        la velocidad deseada y la medida sea menor. Este valor va desde -100 hasta 100.
        """
        #print(str(self.timesteps) + " / " + str(self.accion_resultado))
        reward = 0

        cambio_dif_izq = self.diferencia_izquierda_old - self.diferencia_izquierda
        cambio_dif_der = self.diferencia_derecha_old - self.diferencia_derecha
        reward = 1 - 3 * (self.diferencia_izquierda + self.diferencia_derecha) + 2 * (cambio_dif_izq + cambio_dif_der)

        self.reward_out.data[0] = reward
        self.reward_pub.publish(self.reward_out)

        return reward

    def _is_done(self, observation):

        self.timesteps += 1

        if self.train:

            if self.use_divisor:

                self.divisor = 1 / (1 + self.timesteps / 5)
                if self.diferencia_izquierda > self.divisor or self.diferencia_derecha > self.divisor or self.timesteps >= 250:
                    return True
                else:
                    return False

            else:

                if self.timesteps >= 250:
                    return True
                else:
                    return False
        else:
            return False

    def _get_info(self):

        return {}

#------------------------------------------------------------------------

    def _check_all_systems_ready(self):
        """
        Checks that all the sensors, publishers and other simulation systems are
        operational.
        """
        self._check_all_sensors_ready()
        return True

    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_odom_ready()
        #self._check_cmd_ready()
        self._check_imu_ready()
        self._check_RPM_cmd_ready()
        self._check_RPM_read_ready()
        self._check_duty_read_ready()
        #self._check_laser_scan_ready()
        rospy.logdebug("ALL SENSORS READY")

        return True

#------------------------------------------

    def _check_odom_ready(self):
        self.odom = None
        rospy.logdebug("Waiting for /odom to be READY...")
        while self.odom is None and not rospy.is_shutdown():
            try:
                self.odom = rospy.wait_for_message("/odom", Odometry, timeout=5.0)
                rospy.logdebug("Current /odom READY=>")

            except:
                rospy.logdebug("Current /odom not ready yet, retrying for getting odom")
        return self.odom

    def _check_cmd_ready(self):
        self.cmd_vel = None
        rospy.logdebug("Waiting for /cmd_vel to be READY...")
        while self.cmd_vel is None and not rospy.is_shutdown():
            try:
                self.cmd_vel = rospy.wait_for_message("/cmd_vel", Twist, timeout=5.0)
                rospy.logdebug("Current /cmd_vel READY=>")

            except:
                rospy.logdebug("Current /cmd_vel not ready yet, retrying for getting cmd_vel")
        return self.cmd_vel

    def _check_imu_ready(self):
        self.imu = None
        rospy.logdebug("Waiting for /imu to be READY...")
        while self.imu is None and not rospy.is_shutdown():
            try:
                self.imu = rospy.wait_for_message("/imu", Imu, timeout=5.0)
                rospy.logdebug("Current /imu READY=>")

            except:
                rospy.logdebug("Current /imu not ready yet, retrying for getting imu")
        return self.imu


    def _check_laser_scan_ready(self):
        self.laser_scan = None
        rospy.logdebug("Waiting for /kinect2/scan to be READY...")
        while self.laser_scan is None and not rospy.is_shutdown():
            try:
                self.laser_scan = rospy.wait_for_message("/scan", LaserScan, timeout=5.0)
                rospy.logdebug("Current /kinect2/scan READY=>")

            except:
                rospy.logdebug("Current /kinect2/scan not ready yet, retrying for getting laser_scan")
        return self.laser_scan

    def _check_RPM_cmd_ready(self):
        self.RPM_cmd = None
        rospy.logdebug("Waiting for /commands/controlRPM_info to be READY...")
        while self.RPM_cmd is None and not rospy.is_shutdown():
            try:
                self.RPM_cmd = rospy.wait_for_message("/commands/controlRPM_info", Float32MultiArray, timeout=5.0)
                rospy.logdebug("Current /commands/controlRPM_info READY=>")

            except:
                rospy.logdebug("Current /commands/controlRPM_info not ready yet, retrying for getting /commands/controlRPM_info")
        return self.RPM_cmd

    def _check_RPM_read_ready(self):
        self.RPM_read = None
        rospy.logdebug("Waiting for /state to be READY...")
        while self.RPM_read is None and not rospy.is_shutdown():
            try:
                self.RPM_read = rospy.wait_for_message("/state", Int16MultiArray, timeout=5.0)
                rospy.logdebug("Current /state READY=>")

            except:
                rospy.logdebug("Current /state not ready yet, retrying for getting RPM_read")
        return self.RPM_read

    def _check_duty_read_ready(self):
        self.duty = None
        rospy.logdebug("Waiting for /commands/controlDuty to be READY...")
        while self.duty is None and not rospy.is_shutdown():
            try:
                self.duty = rospy.wait_for_message("/commands/controlDuty", Float32MultiArray, timeout=5.0)
                rospy.logdebug("Current /commands/controlDuty READY=>")

            except:
                rospy.logdebug("Current /commands/controlDuty not ready yet, retrying for getting Duty")
        return self.duty

#------------------------------------------

    def _wait_for_update(self):
        rospy.logdebug("WAIT ALL SENSORS READY")
        #self._check_odom_ready_()
        #self._check_cmd_ready_()
        #self._check_imu_ready_()
        #self._check_RPM_cmd_ready_()
        self._check_RPM_read_ready_()
        #self._check_duty_read_ready_()
        #self._check_laser_scan_ready_()
        rospy.logdebug("ALL SENSORS READY")

        return True

#------------------------------------------

    def _check_odom_ready_(self):
        self.odom = rospy.wait_for_message("/odom", Odometry, timeout=5.0)

    def _check_cmd_ready_(self):
        self.cmd_vel = rospy.wait_for_message("/cmd_vel", Twist, timeout=5.0)

    def _check_imu_ready_(self):
        self.imu = rospy.wait_for_message("/imu", Imu, timeout=5.0)

    def _check_laser_scan_ready_(self):
        self.laser_scan = rospy.wait_for_message("/scan", LaserScan, timeout=5.0)

    def _check_RPM_cmd_ready_(self):
        self.RPM_cmd = rospy.wait_for_message("/commands/controlRPM_info", Float32MultiArray, timeout=5.0)

    def _check_RPM_read_ready_(self):
        self.RPM_read = rospy.wait_for_message("/state", Int16MultiArray, timeout=5.0)

    def _check_duty_read_ready_(self):
        self.duty = rospy.wait_for_message("/commands/controlDuty", Float32MultiArray, timeout=5.0)


#------------------------------------------

    def _odom_callback(self, data):
        self.odom = data

    def _cmd_vel_callback(self, data):
        self.cmd_vel = data

    def _imu_callback(self, data):
        self.imu = data

    def _laser_scan_callback(self, data):
        self.laser_scan = data

    def _RPM_cmd_Callback(self, data):
        self.RPM_cmd = data

    def _RPM_read_Callback(self, data):
        self.RPM_read = data
        if data[5] == 0:
            self.stop.data = False
        elif data[5] == 1:
            self.stop.data = True

    def _duty_read_Callback(self, data):
        self.duty = data

#------------------------------------------

    def _get_odom(self):
        return self.odom

    def _get_cmd_vel(self):
        return self.cmd_vel

    def _get_imu(self):
        return self.imu

    def _get_laser_scan(self):
        return self.laser_scan

    def _get_rpm_cmd(self):
        return self.RPM_cmd

    def _get_rpm_read(self):
        return self.RPM_read

    def _get_duty_cmd(self):
        return self.duty

#------------------------------------------
