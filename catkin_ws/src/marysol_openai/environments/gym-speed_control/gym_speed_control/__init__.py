from gym.envs.registration import register

# Summer
register(
    id='speed_control-v0',
    entry_point='gym_speed_control.envs:speed_controlEnv_0',
)

register(
    id='speed_control-v1',
    entry_point='gym_speed_control.envs:speed_controlEnv_1',
)

register(
    id='speed_control-v2',
    entry_point='gym_speed_control.envs:speed_controlEnv_2',
)

register(
    id='speed_control-v3',
    entry_point='gym_speed_control.envs:speed_controlEnv_3',
)

register(
    id='speed_control-v4',
    entry_point='gym_speed_control.envs:speed_controlEnv_4',
)
