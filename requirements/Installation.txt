######################################################
# INSTALACIÓN COMÚN PARA SIMULACIÓN Y REAL
######################################################

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-melodic-desktop-full -y
sudo apt install ros-melodic-navigation-* -y
sudo apt install ros-melodic-joy* -y
sudo apt install ros-melodic-teleop-* -y
sudo apt install ros-melodic-vision-visp* -y
sudo apt install ros-melodic-apriltag* -y
sudo apt install ros-melodic-joint-state-publisher-gui -y
sudo apt-get install ros-melodic-rtabmap* -y
sudo add-apt-repository ppa:shutter/ppa
sudo apt-get update
sudo apt-get install shutter

echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential -y
sudo apt install python-rosdep -y
sudo rosdep init
rosdep update
curl -sSL http://get.gazebosim.org | sh

sudo apt update
sudo apt install python3-dev python3-pip python3-venv -y
sudo apt-get install python-pyaudio python3-pyaudio -y
sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev -y
sudo apt-get install gparted -y
sudo apt-get install wmctrl -y
sudo apt-get install python3-tk python3-dev -y
sudo apt-get install qml-module-qtmultimedia -y
sudo apt install onboard -y
sudo apt-get install python3-pyside -y
sudo apt install trash-cli -y
sudo apt-get install vlc -y
sudo apt-get install python3-opencv -y
sudo apt-get install qttools5-dev-tools -y
sudo add-apt-repository ppa:mc3man/gstffmpeg-keep -y
sudo apt-get install libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio -y
sudo apt install python3-pip -y
chmod +x /$HOME/marysol/requirements/qt-unified-linux-x64-4.0.1-1-online.run
/$HOME/marysol/requirements/qt-unified-linux-x64-4.0.1-1-online.run

pip3 install --upgrade setuptools
pip3 install --upgrade pip
pip3 install pyinstaller
#pip3 install tensorflow
#pip3 install torch==1.7.1+cpu torchvision==0.8.2+cpu torchaudio==0.7.2 -f https://download.pytorch.org/whl/torch_stable.html
#pip3 install transformers
pip3 install rospkg
pip3 install numpy
pip3 install deepspeech
pip3 install webrtcvad
pip3 install halo
pip3 install scipy
pip3 install python-vlc
pip3 install wikipedia
pip3 install gTTS
pip3 install chatterbot
pip3 install CMake
pip3 install scikit-build
pip3 install spacy==2.1.8
pip3 install nltk
pip3 install flask
pip3 install PySide2
pip3 install pattern
pip3 install ffpyplayer
pip3 install pygame
pip3 install -U pip setuptools wheel
pip3 install -U spacy
pip3 install paho-mqtt
pip3 install python-etcd
pip3 install pyautogui
pip3 install psutil
pip3 install walk
pip3 install PyQt5
pip3 install pyqt5 pyqt5-tools
pip3 install moviepy
pip3 install apriltag
pip3 install unidecode
pip3 install -U PyYAML
python3 -m spacy download xx_ent_wiki_sm
python3 -m spacy download es_core_news_sm
python3 -m spacy download en_core_web_sm
pip3 install https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-2.2.0/en_core_web_sm-2.2.0.tar.gz

sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev -y
sudo apt-get install -y libglu1-mesa-dev libgl1-mesa-dev libosmesa6-dev xvfb ffmpeg curl patchelf libglfw3 libglfw3-dev cmake zlib1g zlib1g-dev swig -y
pip3 install --upgrade pip
#pip3 install -U 'mujoco-py<2.1,>=2.0'
#pip3 install gym[all]
pip3 install scikit-build
pip3 install stable-baselines[mpi]
pip3 install optuna
pip3 install tensorflow==1.15
pip3 install pyyaml
pip3 install rospkg
pip3 install ... /src/marysol_openai/
deactivate

# en un entorno virtual mqttpython #python2
pip install virtualenv
#virtualenv mqttpython
virtualenv mqttpython --python=python2.7
source mqqttpython/bin/activate
#virtualenv -p /usr/bin/python2.7 mqttpython
sudo apt install python-pip
pip install --upgrade pip
pip install opencv-python==4.2.0.32
pip install paho-mqtt
pip install rospkg
pip install pyyaml
deactivate

# instalamos el FLAME
python3 -m venv marysol_FLAME
source marysol_FLAME/bin/activate
# seguir los pasos indicados en el archivo Instalation_FLAME

######################################################
# PARA EL MODO REAL TAMBIÉN TENDREMOS QUE INSTALAR:
######################################################

# realsense
sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo bionic main" -u
sudo apt-get install librealsense2-dkms -y
sudo apt-get install librealsense2-utils -y
sudo apt-get install ros-melodic-realsense2-camera -y

# kinect2
sudo apt-get install build-essential cmake pkg-config
sudo apt-get install libusb-1.0-0-dev
sudo apt-get install libturbojpeg libjpeg-turbo8-dev
sudo apt-get install libglfw3-dev
cd
git clone https://github.com/OpenKinect/libfreenect2.git
cd libfreenect2
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/freenect2
make
make install
cmake -Dfreenect2_DIR=$HOME/freenect2/lib/cmake/freenect2
sudo cp ../platform/linux/udev/90-kinect2.rules /etc/udev/rules.d/
# Running the test program ./bin/Protonect

# PARA LA PROGRAMACIÓN DE LA TEENSY DESDE EL ATOM HABRÁ QUE INSTALAR:
# descargamos e instalamos ATOM y platformio <settings - install - buscar "platformio">
https://atom.io/
sudo apt-get install clang
# instalamos las rules para la teensy https://www.pjrc.com/teensy/td_download.html
sudo cp /$HOME/marysol/requirements/00-teensy.rules /etc/udev/rules.d/
# Añadimos nuestro usuario al grupo para tener acceso a los puertos
sudo usermod -a -G tty marysol
sudo usermod -a -G dialout marysol

# Instalación del micrófono de 4 canales
# descargar los paquetes respeaker_ros en un nuevo respeaker_ws y luego hacer un overlaying del ws http://wiki.ros.org/catkin/Tutorials/workspace_overlaying
# el paquete de ros necesario para este nuevo ws https://github.com/furushchev/respeaker_ros
# así como la instalación del micro https://wiki.seeedstudio.com/ReSpeaker_Mic_Array_v2.0/
pip install pyusb
sudo apt-get install python-usb python3-usb
sudo apt install ros-melodic-catkin-virtualenv
