# Descargamos el repositorio, creamos entorno virtual e instalamos los requerimientos, todo esto desde la carpeta marysol_heavy_data

sudo apt-get install python3-pip python3-venv
python3 -m venv ./FLAME
source ./virtualenvs/voca/bin/activate

sudo apt install ffmpeg
git clone https://github.com/TimoBolkart/voca.git
cd voca
pip install -U pip
pip install -r requirements.txt

# Descargamos e instalamos el psbody

git clone https://github.com/MPI-IS/mesh.git
sudo apt-get install libboost-dev

# Comprobamos donde está instalado el boost con el comando:
whereis boost
# Añadimos este path al make all
BOOST_INCLUDE_DIRS=<whereis boost> make all

# prueba de voca simple
python run_voca.py --tf_model_fname './model/gstep_52280.model' --ds_fname './ds_graph/output_graph.pb' --audio_fname './audio/test_sentence.wav' --template_fname './template/FLAME_sample.ply' --condition_idx 3 --out_path './animation_output'

# También tenemos que instalar el face-alignment, también se puede instalar desde el repositorio

git clone https://github.com/1adrianb/face-alignment.git

# el siguiente script nos devuelve directamente el array de python de 2 dimensiones necesarios para el TF_FLAME
"""
import face_alignment
from skimage import io
import numpy as np

fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, flip_input=False, device='cpu', face_detector='sfd')

input_img = io.imread('./test/assets/richard.jpeg')
preds = fa.get_landmarks(input_img)

print(preds)

print("--------")

my_array = np.array(preds)

print(my_array[0])

with open('richard.npy', 'wb') as f:
    np.save(f, my_array[0][17:])
"""

# copiamos el array anterior y lo copiamos en la carpeta data del TF_FLAME
# Generamos los modelos
#python sample_FLAME.py --option sample_FLAME --model_fname './models/generic_model.pkl' --num_samples 5 --out_path './FLAME_samples'
python sample_FLAME.py --option sample_VOCA_template --model_fname './models/generic_model.pkl' --num_samples 5 --out_path './FLAME_samples'
# Aquí sustituimos el target_img_path y el target_lmk_path por los nuestros (ver mejor en la página)
python fit_2D_landmarks.py --model_fname './models/female_model.pkl' --flame_lmk_path './data/flame_static_embedding.pkl' --texture_mapping './data/texture_data_512.npy' --target_img_path './data/marysol.jpeg' --target_lmk_path './data/marysol_lmks.npy' --out_path './results'
# generamos el .obj (sustituimos por nuestros valores)
python build_texture_from_image.py --source_img './data/marysol.jpeg' --target_mesh './results/marysol.obj' --target_scale './results/marysol_scale.npy' --texture_mapping './data/texture_data_512.npy' --out_path './results'

# Ahora que ya tenemos todo en la carpeta ./results lo copiamos todo y lo movemos a la carpeta del voca template y ejecutamos el scripts ahora ya con nuestra propia textura
# Para que funciones simplemente añadir al comienzo de la linea de comandos PYOPENGL_PLATFORM=osmesa
PYOPENGL_PLATFORM=osmesa python run_voca.py --tf_model_fname './model/gstep_52280.model' --ds_fname './ds_graph/output_graph.pb' --audio_fname './audio/mama.wav' --template_fname './template/FLAME_sample.ply' --condition_idx 3 --uv_template_fname './template/marysol.obj' --texture_img_fname './template/marysol.png' --out_path './animation_output_textured'

# Parece que hay que instalar CUDA 10 en ubuntu 18
# si todavía da fallos instalar
sudo apt-get install libosmesa6-dev
sudo apt-get install freeglut3-dev

git clone https://github.com/mmatl/pyglet.git
cd pyglet
pip install .

# Instalar osmesa como dice aquí
https://pyrender.readthedocs.io/en/latest/install/index.html
