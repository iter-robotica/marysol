# Prerrequisitos
Es necesario tener instalado los siguientes programas y dependencias en ubuntu 16 o 18.
Se recomienda crear un usuario propio para este proyecto con el nombre "marysol", a pesar de que todas las rutas en los scripts son relativas existen tres casos en los que no es posible:
* Archivos .yaml - Para los mapas
* Archivos .world - Dentro del mundo gazebo
* Shebang, hashbang, hashpling - Ciertos scripts dentro del paquete marysol_edgetpu hacen uso de estas rutas absolutas

* [ros-melodic](http://wiki.ros.org/melodic/Installation/Ubuntu) - Entorno de trabajo
* [gazebo](http://gazebosim.org/tutorials?tut=install_ubuntu) - Entorno de simulación. Si da fallo en la ejecucion ejecutar ```sudo apt upgrade libignition-math2``` Instalar la version 7 en Ubuntu 16.04 para ello mejor utilizar la instalación paso a paso
* [libfreenect2](https://github.com/OpenKinect/libfreenect2) - Drivers para la cámara kinect2
* [RealSense](https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md) - Drivers para la cámara d435
* [rtabmap](https://github.com/introlab/rtabmap/wiki/Installation) - Entorno de mapeado
* [virtualenv](https://virtualenv.pypa.io/en/latest/) - Herramienta para crear entornos de python aislados

# Instalación
A continuación se explica como crear el catkin_ws de este proyecto.
En caso de no venir instalados, instale los paquetes de ros siguientes:
```
sudo apt-get install ros-melodic-navigation*
sudo apt-get install ros-melodic-rtabmap-ros*
sudo apt-get install ros-melodic-rosserial*
sudo apt-get install ros-melodic-joy*
sudo apt-get install ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control
```
Cree un catkin_ws en su carpeta personal:
```
mkdir -p ~/catkin_ws
```
Descargue la carpeta del proyecto en su carpeta personal y copie la carpeta /catkin_marysol/src dentro su catkin_ws:
```
git clone https://gitlab.com/iter-robotica/marysol.git
cp ~/marysol/catkin_marysol/src ~/catkin_ws
```
A continuación descargue también los paquetes externos necesarios para este proyecto dentro de la carpeta src:
```
cd ~/catkin_ws/src
```
* [depth_to_laser](https://github.com/BaptisteLevasseur/depth_to_laser.git) Conversor de imagen de profundidad a scaner laser
```
git clone https://github.com/BaptisteLevasseur/depth_to_laser.git
```
* [Kinect2 Bridge](https://github.com/code-iai/iai_kinect2/tree/master/kinect2_bridge) Puente entre libfreenect2 y ROS
```
git clone https://github.com/code-iai/iai_kinect2.git
```
* [m-explore](https://github.com/hrnr/m-explore) Sólo nos interesa el paquete explore, si estamos en melodic no va a compilar el paquete map_merge por lo que es mejor eliminarlo
```
git clone https://github.com/hrnr/m-explore
```
* [rosserial](https://github.com/ros-drivers/rosserial) Protocolo para la comunicación serie bajo los estándares de ROS
```
git clone https://github.com/ros-drivers/rosserial
```
* [timed_roslaunch](https://github.com/MoriKen254/timed_roslaunch) Paquete que puede retrasar el lanzamiento de un archivo.launch
```
git clone https://github.com/MoriKen254/timed_roslaunch
```
* [RealSense-ros](https://github.com/IntelRealSense/realsense-ros.git) Puente entre RealSense y ROS
```
git clone https://github.com/IntelRealSense/realsense-ros.git
cd realsense-ros/
git checkout `git tag | sort -V | grep -P "^\d+\.\d+\.\d+" | tail -1`
cd ..
```
* [ddynamic_reconfigure](https://github.com/pal-robotics/ddynamic_reconfigure.git) Paquete necesario para la compilación del paquete RealSense-ros
```
git clone https://github.com/pal-robotics/ddynamic_reconfigure.git
```
* [open_ai](https://bitbucket.org/theconstructcore/openai_ros/src/melodic-devel/) Paquete para el aprendizaje por refuerzo
```
git clone https://bitbucket.org/theconstructcore/openai_ros.git
```
* [follow_waypoints](http://wiki.ros.org/follow_waypoints) Paquete que nos permite publicar un array de wayoints
```
git clone https://github.com/danielsnider/follow_waypoints.git
```
* [coral_usb_ros](https://github.com/knorth55/coral_usb_ros) Instrucciones y paquetes necesarios para usar el edgetpu

También será necesario crear las carpetas donde se guardan productos de la ejecución que tienen un peso considerable y que por ello no se pueden guardar en el repositorio. Si se requiere de esta información pueden solicitarla a través del correo electrónico.
```
mkdir -p ~/catkin_ws/marysol_heavy_data/openai_models
mkdir -p ~/catkin_ws/marysol_heavy_data/openai_models/logs
mkdir -p ~/catkin_ws/marysol_heavy_data/openai_models/tmp
mkdir -p ~/catkin_ws/marysol_heavy_data/rtabmap_db
mkdir -p ~/catkin_ws/marysol_heavy_data/ros_bags
```
Por último es necesario hacer un catkin_make
```
cd ~/catkin_ws/
catkin_make
```
En el proceso de instalación de los siguientes paquetes se nos pedirán que instalemos varios paquetes más a través de "pip3 install ..."
* [stable_baselines](https://github.com/hill-a/stable-baselines) Set para la implementación de algoritmos por refuerzo
```
(env)$ sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev
(env)$ pip3 install stable-baselines[mpi]
```
* [gym](http://gym.openai.com/docs/#installation) Set de herramientas para el desarrollo de algoritmos de aprendizaje por refuerzo
```
(env)$ pip3 install gym
```
Comrobamos la correcta instalación de estos dos últimos paquetes, deben de aparecer en la lista
```
(env)$ pip3 list
```
El archivo Installation.txt dentro de la carpeta requirements muestra todos los pasos de instalación
