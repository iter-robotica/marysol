# Introducción para el manejo
Existen cuatro launchs donde se ejecutará todo.

## 1 - marysol.launch
Este archivo puede lanzar cualquier configuración de marysol según se configuren los siguientes argumentos:
* robot: tipo string, indica el nombre del robot que se va a utilizar (marysol0, marysol1, marysol2)
* mapping: tipo bool, si es cierta el robot mapeara, en caso contrario navegara
* play_bag: tipo bool, si es cierta la adquisicion de los sensores se realiza a través de un bolsa guardada, en caso contrario se utilizara el simulation para comprobar si es gazebo o real.
* rosbag: tipo string, el nombre de la bolsa de trabajo a utilizar por el rosbag_play.
* record_bag: tipo bool, si es cierta guarda los topicos en una bolsa para poder ser utlizada en otro momento, en caso contrario no hace nada.
* simulation: tipo bool, si es cierta y play_bag es falsa la adquisición de los sensores se realiza a traves de gazebo, en caso de ser falta y play_bag también los sensores son recogidos a traves de los sensores fisicos del robot.
* loc: tipo string, el tipo de localizador utlizado por en modo navegacion (rtabmap, amcl, fixed)
* map: el nombre del entorno en el que el robot va a interactuar, automaticamente se le añadira la coletilla simulation o real
* gzclient: si se ejecuta el modo de simulación y este estar a true se visualizará el gazebo, si no se ejecutará el gzserver
Los demas argumentos son añadidos directamente a partir de los anteriores

A continuación se exponen algunos ejemplos de como lanzar el robot:

Para ejecutar el robot en modo navegación en un mundo virtual:
```
roslaunch marysol_bringup marysol.launch simulation:=true mapping:=false
```
Para ejecutar el robot en modo navegación en un mundo real:
```
roslaunch marysol_bringup marysol.launch simulation:=false mapping:=false
```
Para ejecutar el robot en modo mapeo en un mundo virtual:
```
roslaunch marysol_bringup marysol.launch simulation:=true mapping:=true
```
Para ejecutar el robot en modo mapeo en un mundo real:
```
roslaunch marysol_bringup marysol.launch simulation:=false mapping:=true
```
Para ejecutar el robot a través de una bolsa de trabajo en modo navegación:
```
roslaunch marysol_bringup marysol.launch mapping:=false play_bag:=true
```
Para ejecutar el robot en un mundo virtual, en modo navegación y guardar la bolsa de trabajo:
```
roslaunch marysol_bringup marysol.launch mapping:=false record_bag:=true
```
Para ejecutar el robot en un mundo virtual, en modo navegación y localizador amcl:
```
roslaunch marysol_bringup marysol.launch mapping:=false simulation:=true loc:=amcl
```
Para ejecutar el robot en un mundo real, en modo navegación y localizador rtabmap:
```
roslaunch marysol_bringup marysol.launch mapping:=false simulation:=false loc:=rtabmap
```

## 2 - joystick.launch
Para menejar el joystick (Gamepad):
![Marysol_controller](media/Marysol_controller.jpg)
Para ejecutar el joystick:
```
roslaunch marysol_teleop joystick.launch
```
* Cruceta de direcciones: Manejar Marysol en las velocidades predefinida (lineal eje x y angular eje z)
* Analógico izquierdo: Velocidad lineal en el eje x en la escala de 0 a velocidad lineal predefinida en x
* Presionar analógico izquierdo: Velocidad lineal en el eje x predefinida a inicial
* Analógico derecha: Velocidad angular en el eje z en la escala de 0 a velocidad angular predefinida en z
* Presionar analógico derecho: Velocidad angular en el eje z predefinida a inicial
* Select: Ambas velocidades predefinidas a iniciales
* Gatillo superior izquierda: Aumentar velocidad lineal predefinida en el eje x
* Gatillo inferior izquierda: Disminuir velocidad lineal predefinida en el eje x
* Gatillo superior derecha: Aumentar velocidad angular predefinida en el eje z
* Gatillo inferior derecha: Disminuir velocidad angular predefinida en el eje z
* Y: Aumentar la velociad máxima lineal para ambas ruedas (limitación dada en la Teensy)
* A: Disminuir la velociad máxima lineal para ambas ruedas (limitación dada en la Teensy)
* X: Aumentar la velociad mínima lineal para ambas ruedas (limitación dada en la Teensy)
* B: Disminuir la velociad mínima lineal para ambas ruedas (limitación dada en la Teensy)

## 3 - start_training.launch
Este nodo nos permite entrenar el modelo en el entorno deseado, el archivo marysol.launch ha de ser previamente ejecutado, dispone de tres parámetros:
* timesteps: número de timesteps que se desea entrenar al modelo
* environment: nombre del environment donde se va a entrenar al modelo
* agent: nombre del agente que va a entrenar el modelo
Antes de ejecutar el archivo debemos de activar en la consola que nos encontremos el entorno virtual de python dentro de la carpeta /marysol_openai/virtualenv que es donde tenemos instalados los paquetes stable-baselines y gym
Ejemplo de ejecución:
```
cd ~/catkin_ws/src/marysol_openai/virtualenv
source bin/activate
(env)$ cd ~/catkin_ws/
(env)$ roslaunch marysol_openai start_training.launch timesteps:=500000 environment:=marysolMaze-v0 agent:=TRPO
```
## 4 - test.py
El script test.py prueba el modelo previamente entrenado tantas veces como se le indique y finalmente hace una media de la recompensa obtenida en todas las pruebas. Debido a que el modelo ya ha sido guardado de manera que en el incluya el agente y el environment, estos dos parámetros no son necesarios de pasarselos.
Una vez ejecutamos el archivo, se desplegará un menú con los modelos disponibles entrenados y guardados previamente y se nos pedirá que elijamos uno de ellos, como también el número de episodios.
Ejemplo para probar un modelo 20 veces (debemos de estar en la carpeta del script /src/marysol_openai/scripts):
Antes de ejecutar el archivo debemos de activar en la consola que nos encontremos el entorno virtual de python dentro de la carpeta /marysol_openai/virtualenv que es donde tenemos instalados los paquetes stable-baselines y gym
```
cd ~/catkin_ws/src/marysol_openai/virtualenv
source bin/activate
(env)$ cd ..
(env)$ python3 test.py
```
		Seleccione un modelo
		---------------------------------------------
		  0) | modelo:  LunarLander-v2_50000_TRPO    
		---------------------------------------------
		  1) | modelo:  LunarLander-v2_1000000_ACER  
		---------------------------------------------
		  2) | modelo:  LunarLander-v2_1000000_ACKTR
		---------------------------------------------

		Indique el numero de modelo >> 0

		 Ha elegido el modelo  ./models/LunarLander-v2_50000_TRPO/LunarLander-v2_50000_TRPO_Best.pkl

		Indique el número de episodios >> 20

		 Numero de episodios  20
